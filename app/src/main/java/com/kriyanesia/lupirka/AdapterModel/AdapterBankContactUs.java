package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.ValueContactUs;
import com.kriyanesia.lupirka.R;

import java.util.List;

public class AdapterBankContactUs extends RecyclerView.Adapter<AdapterBankContactUs.RecyclerViewHolders> {

    private List<ValueContactUs.ContactBank> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;


    public AdapterBankContactUs(Context context, List<ValueContactUs.ContactBank> itemList) {

        try {
            this.itemList = itemList;
            this.mInflater = LayoutInflater.from(context);
        }catch (Error e){
            Log.e("CONTACTUS",e.getMessage());
        }
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_bank_cp, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }


    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        final ValueContactUs.ContactBank data = itemList.get(position);
        holder.deskripsiAn.setText("BANK "+data.getNama_bank()+" Atas Nama "+data.getAtas_nama()+
        " NO Rekening "+data.getNo_rekening());



//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mClickListener != null) {
//                    mClickListener.onAktivitasClickListener(Integer.valueOf(data.get()));
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public TextView deskripsiAn;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            deskripsiAn = (TextView) itemView.findViewById(R.id.deskripsiAn);
        }


    }
    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onAktivitasClickListener(int idAktivitas);
    }
}