package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.TableRegistrasiEvent.Event;
import com.kriyanesia.lupirka.Data.TableRegistrasiEvent.WaktuEvent;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;

public class AdapterTableEventReg  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    ArrayList<Event> event;
    Context ctx;
    RecyclerView rv_child;
    AdapterTableEventDetail childAdapter;

    public AdapterTableEventReg(Context ctx, ArrayList<Event> event) {
        this.ctx = ctx;
        this.event = event;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rv_child;
        TextView tvName,tvKeterangan;

        public ViewHolder(View itemView) {
            super(itemView);
            rv_child = (RecyclerView) itemView.findViewById(R.id.rv_child);
            tvName = (TextView) itemView.findViewById(R.id.txtEventName);
            tvKeterangan = (TextView) itemView.findViewById(R.id.textKeterangan);

        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_table_event_register, parent, false);
        AdapterTableEventReg.ViewHolder pavh = new AdapterTableEventReg.ViewHolder(itemLayoutView);
        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder vh = (ViewHolder) holder;
        Event e = event.get(position);
        vh.tvName.setText(e.getNamaEvent());
        vh.tvKeterangan.setText(e.getKeterangan());
        vh.tvName.setTextColor(Color.WHITE);
        vh.tvKeterangan.setTextColor(Color.WHITE);
        initChildLayoutManager(vh.rv_child, e.getWaktuEvent());
    }

    private void initChildLayoutManager(RecyclerView rv_child, ArrayList<WaktuEvent> childData) {

        rv_child.setLayoutManager(new NestedRecyclerLinearLayoutManager(ctx, NestedRecyclerLinearLayoutManager.HORIZONTAL, true));
        childAdapter = new AdapterTableEventDetail(childData);
        rv_child.setAdapter(childAdapter);
    }

    @Override
    public int getItemCount() {
        return event != null ? event.size() : 0;
    }
    private void addItem(Event event,ArrayList<WaktuEvent> childData) {
        this.event.add(event);
        notifyItemInserted(getItemCount());
        childAdapter.notifyDataSetChanged();

    }
}
