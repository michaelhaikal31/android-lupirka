package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.Data.Sosmed;
import com.kriyanesia.lupirka.R;

import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

public class AdapterListBooth extends RecyclerView.Adapter<AdapterListBooth.RecyclerViewHolders> {

    private List<ValueListBooth.Booth> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    private int lastSelectedPosition = -1;
    Context ctx;


    public AdapterListBooth(Context context, List<ValueListBooth.Booth> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        ctx=context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_tenant, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final RecyclerViewHolders holder, final int position) {
        holder.textKodeTenant.setText(itemList.get(position).getKode_tenant());

        int harga = Integer.valueOf(itemList.get(0).getHarga());
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String h=formatRupiah.format(harga);
        String finalHarga= "Rp. "+h.substring(2);
        holder.textHarga.setText(finalHarga);
        holder.textNote.setText(itemList.get(position).getNote());
        holder.rbCheck.setChecked(lastSelectedPosition == position);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                lastSelectedPosition=holder.getAdapterPosition();
                notifyDataSetChanged();
                holder.rbCheck.setChecked(lastSelectedPosition == position);
                if (mClickListener != null) mClickListener.onItemClick(v, position, itemList.get(position).getId(),
                        Integer.valueOf(itemList.get(position).getHarga()));
//                Toast.makeText(ctx, position+" = "+lastSelectedPosition, Toast.LENGTH_SHORT).show();

            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder{

        public TextView textKodeTenant,textHarga,textNote;
        RadioButton rbCheck;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            textKodeTenant = (TextView)itemView.findViewById(R.id.textKodeTenant);
            textHarga = (TextView)itemView.findViewById(R.id.textHarga);
            textNote = (TextView)itemView.findViewById(R.id.textNote);
            rbCheck = (RadioButton) itemView.findViewById(R.id.rbCheck);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                }
            });
        }


    }
    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position, String idTenant,int amount);
    }
}