package com.kriyanesia.lupirka.AdapterModel;

import android.app.Activity;
import android.os.Handler;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.OnLoadMoreListener;
import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;
import java.util.List;


        public class AdapterEventList extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private final int VIEW_TYPE_ITEM = 0;
        private final int VIEW_TYPE_LOADING = 1;
        private OnLoadMoreListener onLoadMoreListener;
        private OnItemClickListener onItemClickListener;
        private boolean isLoading;
        private Activity activity;
        private List<Event> itemList;
        private int visibleThreshold = 5;
        private int lastVisibleItem, totalItemCount;
        public interface OnItemClickListener{
            void onItemClick(String idEvent);
        }

    public void setOnItemClickListener(OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public AdapterEventList(RecyclerView recyclerView, List<Event> itemList, Activity activity) {
        this.itemList = itemList;
        this.activity = activity;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.row_event_list, parent, false);
            return new EventViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.progressbar_load_event_more, parent, false);
            return new LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof EventViewHolder) {
            EventViewHolder EventViewHolder = (EventViewHolder) holder;
            EventViewHolder.tvTitle.setText(itemList.get(position).getTitle());
            if (!itemList.get(position).getImage().isEmpty()){
                Picasso.with(activity).load(itemList.get(position).getImage()).into(EventViewHolder.iconEvent);
            }
            EventViewHolder.tvTgl.setText(itemList.get(position).getStart());
            EventViewHolder.tvDesc.setText(itemList.get(position).getDesc());
        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        TextView text;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarLoadMore);
            text = (TextView) view.findViewById(R.id.text) ;
            final boolean stop = false;
//            for (int i=0;i<5 && isLoading;i++){
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        progressBar.setVisibility(View.GONE);
                        text.setVisibility(View.GONE);
//                        if (!isLoading) {
//
//
//
//                        }
                    }
                },1000);

        }
    }

    private class EventViewHolder extends RecyclerView.ViewHolder {
        public TextView tvTitle,tvTgl,tvDesc;
        public ImageView iconEvent;

        public EventViewHolder(View view) {
            super(view);
//            itemView.setOnClickListener(this);
            tvTitle = (TextView)itemView.findViewById(R.id.txtEventTitle);
            tvTgl = (TextView)itemView.findViewById(R.id.txtEventTanggal);
            tvDesc = (TextView)itemView.findViewById(R.id.txtAdsDescription);
            iconEvent = (ImageView)itemView.findViewById(R.id.bannerEvent);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        onItemClickListener.onItemClick(itemList.get(getAdapterPosition()).getId());
                    }catch (Exception e){

                    }
                }
            });

        }
    }
}