package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Aktivitas.Aktivitas;
import com.kriyanesia.lupirka.Data.ValueContactUs;
import com.kriyanesia.lupirka.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterListContactUs extends RecyclerView.Adapter<AdapterListContactUs.RecyclerViewHolders> {

    private List<ValueContactUs.ContactUs> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;


    public AdapterListContactUs(Context context, List<ValueContactUs.ContactUs> itemList) {

        try {
            this.itemList = itemList;
            this.mInflater = LayoutInflater.from(context);
        }catch (Error e){
            Log.e("CONTACTUS",e.getMessage());
        }
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_contact_us, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }


    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        final ValueContactUs.ContactUs data = itemList.get(position);
        holder.jabatan.setText(data.getPosisi());
        holder.noHp.setText(data.getPhone());
        holder.contactName.setText(data.getNama());
        holder.email.setText(data.getEmail());



//        holder.itemView.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                if (mClickListener != null) {
//                    mClickListener.onAktivitasClickListener(Integer.valueOf(data.get()));
//                }
//            }
//        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public TextView contactName,email,noHp,jabatan;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            contactName = (TextView) itemView.findViewById(R.id.contactName);
            email = (TextView) itemView.findViewById(R.id.email);
            noHp = (TextView) itemView.findViewById(R.id.noHp);
            jabatan = (TextView) itemView.findViewById(R.id.jabatan);
        }


    }
    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onAktivitasClickListener(int idAktivitas);
    }
}