package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Pembayaran.ValuePembayaran;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;
import java.util.List;

public class AdapterRecyclePembayaran  extends RecyclerView.Adapter<RecyclerView.ViewHolder>  implements AdapterPembayaranChild.ItemBankClickListener {

    Context ctx;
    AdapterPembayaranChild childAdapter;
    List<ValuePembayaran.Payment> payment;

    private ItemBankClickListener itembankClick;

    public void setItembankClick(ItemBankClickListener itembankClick) {
        this.itembankClick = itembankClick;
    }

    public interface ItemBankClickListener {
        void onItemBankClick(String accountBank);
    }

    public AdapterRecyclePembayaran(Context ctx, List<ValuePembayaran.Payment> payment) {
        this.ctx = ctx;
        this.payment = payment;

    }

    @Override
    public void onBankClick(String accountBank) {

        if (itembankClick!=null) {
            itembankClick.onItemBankClick(accountBank);
        }else {
            System.out.println("TIDAK BISA");
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        RecyclerView rv_child;
        TextView tvTitle,tvKeterangan;
        RelativeLayout layoutRecycle;

        public ViewHolder(View itemView) {
            super(itemView);
            rv_child = (RecyclerView) itemView.findViewById(R.id.recycleBank);
            tvTitle = (TextView) itemView.findViewById(R.id.textTitlePembayaranVA);
            tvKeterangan = (TextView) itemView.findViewById(R.id.konfirmasi);
            layoutRecycle = (RelativeLayout) itemView.findViewById(R.id.layoutRecycle);


        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_metode_pembayaran, parent, false);
        AdapterRecyclePembayaran.ViewHolder pavh = new AdapterRecyclePembayaran.ViewHolder(itemLayoutView);

        return pavh;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder vh = (ViewHolder) holder;

        ValuePembayaran.Payment p = payment.get(position);
        vh.tvTitle.setText(p.getTipe());
        vh.tvKeterangan.setText("Konfirmasi Otomatis 24 jam");
        vh.tvTitle.setTextColor(Color.WHITE);
        vh.tvKeterangan.setTextColor(Color.WHITE);
        ArrayList<String> arrayListBank = new ArrayList<>();
        String[]arrBank = new String[3];
        String bank = p.getData();
        if (!bank.isEmpty()){
            arrBank = bank.split(", ");
            for (int i=0;i<arrBank.length;i++){
                arrayListBank.add(i,arrBank[i]);
            }
        }
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vh.layoutRecycle.getVisibility()==View.GONE){
                    vh.layoutRecycle.setVisibility(View.VISIBLE);
                }else {
                    vh.layoutRecycle.setVisibility(View.GONE);

                }
            }
        });


        initChildLayoutManager(vh.rv_child, arrayListBank);
    }
    private void initChildLayoutManager(RecyclerView rv_child, ArrayList<String> childData) {

        int numberOfColumns = 3;
        rv_child.setLayoutManager(new GridLayoutManager(ctx, numberOfColumns));
        AdapterPembayaranChild adapterPembayaranChild = new AdapterPembayaranChild(childData);
        adapterPembayaranChild.setItemBankClickListener(AdapterRecyclePembayaran.this);
        rv_child.setAdapter(adapterPembayaranChild);




    }

    @Override
    public int getItemCount() {
        return payment != null ? payment.size() : 0;
    }
    private void addItem(ValuePembayaran.Payment payment, String [] childData) {
        this.payment.add(payment);
        notifyItemInserted(getItemCount());
        childAdapter.notifyDataSetChanged();

    }
}
