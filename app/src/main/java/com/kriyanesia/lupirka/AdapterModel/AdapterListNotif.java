package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionNotif;

import java.util.List;
import java.util.zip.Inflater;

public class AdapterListNotif extends BaseAdapter {
    Context context;
    List<SessionNotif> list;
    private AdapterListAktivitas.ItemClickListener mClickListener;

    public AdapterListNotif(List<SessionNotif> list,Context c) {
        this.list=list;
        this.context = c;
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return list.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
    SessionNotif notif = list.get(i);
    ViewHolder holder=null;

    if(view==null){
        LayoutInflater inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        view = inflater.inflate(R.layout.row_item_notif,null);
        holder = new ViewHolder();
        holder.txtTitle = (TextView)view.findViewById(R.id.txtJudulNotif);
        holder.txtDesc = (TextView)view.findViewById(R.id.txtDeskripsiNotif);
        view.setTag(holder);
    }else{
        holder = (ViewHolder)view.getTag();
    }

    holder.txtTitle.setText(notif.getTitle());
    holder.txtDesc.setText(notif.getMessage());



        return view;
    }

    private class ViewHolder {
        TextView txtTitle;
        TextView txtDesc;
    }

    public void setmClickListener(AdapterListAktivitas.ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onAktivitasClickListener(int idAktivitas);
    }


}
