
package com.kriyanesia.lupirka.AdapterModel;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kriyanesia.lupirka.Data.Ads.AccountBank;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ferna on 5/7/2018.
 */

public class AdapterPembayaranChild  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<String> childData;
    private ArrayList<String> childDataBk;
    private ArrayList<AccountBank> accountBank=new ArrayList<>();
    ImageView iconBank;
    private ItemBankClickListener ItemBankClickListener;



    public void setItemBankClickListener(AdapterPembayaranChild.ItemBankClickListener itemBankClickListener) {
        ItemBankClickListener = itemBankClickListener;
    }

    public interface ItemBankClickListener {
        void onBankClick(String accountBank);
    }
    List<View> itemViewList = new ArrayList<>();
    public AdapterPembayaranChild(ArrayList<String> childData) {
        this.childData = childData;
        childDataBk = new ArrayList<>();

        for (int i=0; i<childData.size();i++){
            if (childData.get(i).startsWith("Mandiri")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_mandiri);
                accountBank.add(ab);
            }else if (childData.get(i).startsWith("BRI")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_bri);
                accountBank.add(ab);
            }else if (childData.get(i).startsWith("BCA")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_bca);
                accountBank.add(ab);
            }else if (childData.get(i).startsWith("BNI")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_bni);
                accountBank.add(ab);
            }else if (childData.get(i).startsWith("BPRKS")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_bri);
                accountBank.add(ab);
            }else if (childData.get(i).startsWith("BTN")){
                AccountBank ab= new AccountBank();
                ab.setAccountName(childData.get(i));
                ab.setDrawable(R.drawable.icon_bri);
                accountBank.add(ab);
            }
        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private SparseBooleanArray selectedItems = new SparseBooleanArray();

        private final ImageView iconBank;
        ImageView tvExpandCollapseToggle;

        public ViewHolder(View itemView) {
            super(itemView);
            this.iconBank = (ImageView) itemView.findViewById(R.id.iconBank);

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_pembayaran_bank, parent, false);

        AdapterPembayaranChild.ViewHolder cavh = new AdapterPembayaranChild.ViewHolder(itemLayoutView);

        return cavh;
    }


    final Handler handler = new Handler();
    Runnable collapseList = new Runnable() {
        @Override
        public void run() {
            if (getItemCount() > 1) {
                childData.remove(childData.size() - 1);
                notifyDataSetChanged();
                handler.postDelayed(collapseList, 50);
            }
        }
    };

    Runnable expandList = new Runnable() {
        @Override
        public void run() {
            int currSize = childData.size();
            if (currSize == childDataBk.size()) return;

            if (currSize == 0) {
                childData.add(childDataBk.get(currSize));
            } else {
                childData.add(childDataBk.get(currSize));
            }
            notifyDataSetChanged();

            handler.postDelayed(expandList, 50);
        }
    };


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        itemViewList.add(vh.itemView);
        vh.iconBank.setImageResource(accountBank.get(position).getDrawable());

        final SparseBooleanArray selectedItems = new SparseBooleanArray();

        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                handler.removeCallbacksAndMessages(null);
//                if (getItemCount() > 1) {
//                    handler.post(collapseList);
//                } else {
//                    handler.post(expandList);
//                }
                for(View tempItemView : itemViewList) {
                    /** navigate through all the itemViews and change color
                     of selected view to colorSelected and rest of the views to colorDefault **/
                    if(itemViewList.get(vh.getAdapterPosition()) == tempItemView) {
                        tempItemView.setBackgroundResource(R.drawable.drawable_active_bank_select);
                        if (ItemBankClickListener!=null) {
                            ItemBankClickListener.onBankClick(accountBank.get(position).getAccountName());
                        }else {
                            System.out.println("TIDAK BISA");
                        }
                    }
                    else{
                        tempItemView.setBackgroundResource(R.color.white);
                    }
                }



            }
        });
        vh.itemView.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    v.setBackgroundColor(Color.parseColor("#f0f0f0"));
                }
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    v.setBackgroundColor(Color.TRANSPARENT);
                }
                return false;
            }
        });

    }

    @Override
    public int getItemCount() {
        return accountBank.size();
    }

}
