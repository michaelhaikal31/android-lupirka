package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.Paket;
import com.kriyanesia.lupirka.Data.HomePost;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Function;
import com.bumptech.glide.Glide;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;



public class AdapterPaketEvent extends RecyclerView.Adapter<AdapterPaketEvent.MyHolder> {

    static SparseBooleanArray itemStateArray= new SparseBooleanArray();

    private static List<Paket> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    Context ctx;

    String userType;
    public interface ItemClickListener {
        //        void OnItemClickListener(int index,String title,String id, String harga,String kategoriHarga,String paket,boolean checked);
        void OnItemClickListener(int index, String title,String id,
                                 String harga,String kategoriHarga,
                                 String paket, boolean checked);


    }
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }
    public AdapterPaketEvent(Context context, List<Paket> item,String userType) {
        itemStateArray.clear();
        this.userType=userType;
        itemList=item;
        this.mInflater = LayoutInflater.from(context);
        ctx =context;
    }

    //VIEWHOLDER IS INITIALIZED
    @Override
    public MyHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        /*View v= LayoutInflater.from(parent.getContext()).inflate(R.layout.row_list_paket_event,null);
        MyHolder holder=new MyHolder(v);
        return holder;*/
        Context context = parent.getContext();
        int layoutForItem = R.layout.row_list_paket_event;
        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(layoutForItem, parent, false);
        return new MyHolder(view);

    }

    //DATA IS BOUND TO VIEWS
    @Override
    public void onBindViewHolder(final MyHolder holder, int position) {
        holder.textAktivitas.setText(""+itemList.get(position).getAktifitas());
        holder.textHarga.setText(Function.ConvertRupiah(Integer.valueOf(itemList.get(position).getHarga())));
        holder.textKegiatan.setText(""+itemList.get(position).getKegiatan());
        holder.textTargetPeserta.setText(""+itemList.get(position).getKategori_peserta());
        holder.valueDateBatas.setText(""+itemList.get(position).getStart()+" s/d "+itemList.get(position).getEnd());
        holder.textKategoriHarga.setText(""+itemList.get(position).getKategori_harga());
//        int kadaluarsa=0;
//        try {
//        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
//        Date strDate = sdf.parse(itemList.get(position).getEnd());
//        if (new Date().after(strDate)) {
//            kadaluarsa = 1;
//        }
//        } catch (ParseException e) {
//            kadaluarsa=99;
//            e.printStackTrace();
//        }
//
//        if (kadaluarsa==0){
//            holder.mCheckedTextView.setVisibility(View.GONE);
////            holder.mCheckedTextView.setChecked(true);
//        }else if (kadaluarsa==1){
//            holder.mCheckedTextView.setVisibility(View.GONE);
////            holder.mCheckedTextView.setFocusableInTouchMode(false);
//        }else if (kadaluarsa==99){
//            holder.mCheckedTextView.setVisibility(View.VISIBLE);
////            holder.mCheckedTextView.setChecked(true);
//            System.out.println("event "+position+"gagal di filter");
//        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                boolean check=false;
                if (mClickListener != null) {
                    if (holder.mCheckedTextView.isChecked()) {
                        holder.mCheckedTextView.setChecked(false);
                        check=false;
                    }
                    else {
                        holder.mCheckedTextView.setChecked(true);
                        check=true;
                    }
                    mClickListener.OnItemClickListener(holder.getLayoutPosition(),
                            itemList.get(holder.getAdapterPosition()).getAktifitas(),
                            itemList.get(holder.getAdapterPosition()).getId(),
                            itemList.get(holder.getAdapterPosition()).getHarga(),
                            itemList.get(holder.getAdapterPosition()).getKategori_harga(),
                            itemList.get(holder.getAdapterPosition()).getKategori_peserta(),check);


                }
            }
        });

        boolean checkKuota=true;
        try {
            Integer.parseInt(itemList.get(position).getQuota());
        }catch(NumberFormatException e) {
            checkKuota=false;
        }

        if (checkKuota) {
            holder.textKuota.setText("" + itemList.get(position).getQuota());
        }else {
            holder.textKuota.setText("Tak terbatas");
        }




        //System.out.println("TES: POS " + position + " , COUNT: " + getItemCount());
        holder.bind(position);

    }
    static class MyHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textAktivitas,textKegiatan ,textHarga,textKuota,
                textTargetPeserta,valueDateBatas,textKategoriHarga;
        public CheckedTextView mCheckedTextView;

        ItemClickListener itemClickListener;

        public MyHolder(View itemView) {
            super(itemView);

            textKegiatan = (TextView)itemView.findViewById(R.id.valueKegiatan);
            textHarga = (TextView)itemView.findViewById(R.id.valueHarga);
            textAktivitas = (TextView)itemView.findViewById(R.id.valueAktivitas);
            textTargetPeserta = (TextView)itemView.findViewById(R.id.valueTargetPeserta);
            valueDateBatas = (TextView)itemView.findViewById(R.id.valueDateBatas);
            textKategoriHarga = (TextView)itemView.findViewById(R.id.valueKategoriHarga);
            textKuota = (TextView)itemView.findViewById(R.id.valueKuota);
            mCheckedTextView = (CheckedTextView) itemView.findViewById(R.id.checkBox);

            itemView.setOnClickListener(this);

        }
         public void setItemClickListener(ItemClickListener ic)
         {
             this.itemClickListener=ic;
         }
//         @Override
//         public void onClick(View v) {
//
//         }
         interface ItemClickListener {

             void onItemClick(View v,int pos);
         }

        @Override
        public void onClick(View v) {
            int adapterPosition = getAdapterPosition();
            if (!itemStateArray.get(adapterPosition, false)) {
                mCheckedTextView.setChecked(true);
                itemStateArray.put(adapterPosition, true);

            }
            else  {
                mCheckedTextView.setChecked(false);
                itemStateArray.put(adapterPosition, false);
            }

        }

        void bind(int position) {
            // use the sparse boolean array to check
            if (!itemStateArray.get(position, false)) {
                mCheckedTextView.setChecked(false);}
            else {
                mCheckedTextView.setChecked(true);
            }
//            mCheckedTextView.setText(String.valueOf(itemList.get(position).getPosition()));
        }
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

}