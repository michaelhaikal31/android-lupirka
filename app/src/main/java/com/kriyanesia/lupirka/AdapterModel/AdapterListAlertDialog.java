package com.kriyanesia.lupirka.AdapterModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Aktivitas.Aktivitas;
import com.kriyanesia.lupirka.Data.Event.ValueEventCategory;
import com.kriyanesia.lupirka.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterListAlertDialog extends RecyclerView.Adapter<AdapterListAlertDialog.RecyclerViewHolders> {

    private List<ValueEventCategory.Categori> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;


    public AdapterListAlertDialog(Context context, List<ValueEventCategory.Categori> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_alertdialog, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        final ValueEventCategory.Categori data = itemList.get(position);
        holder.title.setText(data.getName());
        if (position%2 != 0){
            holder.itemView.setBackgroundColor(Color.parseColor("#ffffff"));
        }



        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onListAlertClick(Integer.valueOf(data.getId()),data.getName());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public TextView title;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.text);
        }


    }
    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onListAlertClick(int id,String nama);
    }
}