package com.kriyanesia.lupirka.AdapterModel;

import android.annotation.SuppressLint;
import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.kriyanesia.lupirka.R;

public class AdapterChildpembayaranBank extends RecyclerView.Adapter<AdapterChildpembayaranBank.ViewHolder> {
//    List<ValuePembayaranAds.Payment> payment;
    private Uri[] mData ;
    private String[] arrBank;
    private LayoutInflater mInflater;
    Context ctx;
    private SparseBooleanArray selectedItems = new SparseBooleanArray();

    // data is passed into the constructor
    public AdapterChildpembayaranBank(Context context, String[] arrBank) {
        this.mInflater = LayoutInflater.from(context);
        this.arrBank = arrBank;
        ctx = context;
    }



    // inflates the cell layout from xml when needed
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.row_pembayaran_bank, parent, false);
        return new ViewHolder(view);
    }

    // binds the data to the textview in each cell
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        System.out.println("ADAPTER BIND VIEW ");


        for (int i =0;i<arrBank.length;i++){
            if (arrBank[i].equalsIgnoreCase("BCA")){
                holder.icon.setBackgroundResource(R.drawable.icon_bca);
            }else if (arrBank[i].equalsIgnoreCase("Mandiri")){
                holder.icon.setBackgroundResource(R.drawable.icon_mandiri);
            }else if (arrBank[i].equalsIgnoreCase("BRI")){
                holder.icon.setBackgroundResource(R.drawable.icon_bri);
            }else if (arrBank[i].equalsIgnoreCase("ocbc")){
                holder.icon.setBackgroundResource(R.drawable.icon_ocbc);
            }else if (arrBank[i].equalsIgnoreCase("Permata")){
                holder.icon.setBackgroundResource(R.drawable.icon_permata);
            }


        }
    }

    // total number of cells
    @Override
    public int getItemCount() {
        return arrBank.length;
    }


    // stores and recycles views as they are scrolled off screen
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        ImageView icon;
        LinearLayout layoutContent;
        @SuppressLint("WrongViewCast")
        ViewHolder(View itemView) {
            super(itemView);
            icon = (ImageView) itemView.findViewById(R.id.iconBank);
            layoutContent = (LinearLayout) itemView.findViewById(R.id.layoutContent);
            icon.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
//            layoutContent.setBackgroundResource(R.drawable.rectangle_br_red);
            if (selectedItems.get(getAdapterPosition(), false)) {
                selectedItems.delete(getAdapterPosition());
                view.setSelected(false);
            }
            else {
                selectedItems.put(getAdapterPosition(), true);
                view.setSelected(true);
            }
            Log.d("Didieu cobaan: ", "onClick: "+getPosition());
        }
    }



}
