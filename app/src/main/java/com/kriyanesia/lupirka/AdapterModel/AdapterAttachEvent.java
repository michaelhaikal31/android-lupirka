package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.R;

public class AdapterAttachEvent extends RecyclerView.Adapter<AdapterAttachEvent.ViewHolder> {

        private String[] mData ;
        private LayoutInflater mInflater;
        Context ctx;

    private ItemAttachClickListener ItemAttachClickListener;

        // data is passed into the constructor
        public AdapterAttachEvent(Context context, String[] data) {
            this.mInflater = LayoutInflater.from(context);
            ctx =context;
            this.mData = data;

        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.row_attach_file, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final String value = mData[position];

            if (value.startsWith("http://")){
                String[] attachArr = value.split("event/");
                holder.textAttach.setText(attachArr[1].toString());
            }
//            String[] attachArr = value.split("event/");
//            System.out.println(""+attachArr.length);
//            for (int i =0; i<attachArr.length;i++){
//                    if (!attachArr[i].startsWith("http://")){
//                        holder.textAttach.setText(attachArr[i].toString());
//                        holder.koma.setText(" ");
//                    }else {
//                        holder.koma.setVisibility(View.GONE);
//                        holder.textAttach.setVisibility(View.GONE);
//                    }
//
//            }
//                if (!attachArr[1].isEmpty())
            holder.textAttach.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemAttachClickListener.onItemClick(value);
                }
            });


        }

        // total number of cells
        @Override
        public int getItemCount() {
            return mData.length;
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder{
            TextView textAttach,koma;

            ViewHolder(View itemView) {
                super(itemView);
                textAttach = (TextView) itemView.findViewById(R.id.textAttach);
                koma = (TextView) itemView.findViewById(R.id.koma);


            }





        }
    // convenience method for getting data at click position
    String getItem(int id) {
        return mData[id];
    }

    // allows clicks events to be caught
    public void setItemClickListener(ItemAttachClickListener ItemAttachClickListener) {
        this.ItemAttachClickListener = ItemAttachClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemAttachClickListener {
        void onItemClick(String url);
    }



}
