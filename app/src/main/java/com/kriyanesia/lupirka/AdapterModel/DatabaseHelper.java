package com.kriyanesia.lupirka.AdapterModel;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranEvent;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranTopup;
import com.kriyanesia.lupirka.Session.SessionNotif;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by ravi on 15/03/18.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    // Database Version
    private static final int DATABASE_VERSION = 1;

    // Database Name
    private static final String DATABASE_NAME = "lupirka_db";


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        // create SessionPembayaranIklan table
        db.execSQL(SessionPembayaranIklan.CREATE_TABLE);
        // create SessionPembayaranEvent table
        db.execSQL(SessionPembayaranEvent.CREATE_TABLE);
        // create SessionPembayaranTopup table
        db.execSQL(SessionPembayaranTopup.CREATE_TABLE);
        //create notif table
        db.execSQL(SessionNotif.CREATE_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
//        db.execSQL("DROP TABLE IF EXISTS " + SessionPembayaranIklan.TABLE_NAME);

        // Create tables again
//        onCreate(db);
    }
    public void insertPembayaran(String id,String title,String table, String norek, String bataswaktu, String nominal) {
        // get writable database as we want to write data
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(SessionPembayaranIklan.COLUMN_ID, id);
        values.put(SessionPembayaranIklan.COLUMN_TITLE, title);
        values.put(SessionPembayaranIklan.COLUMN_NOREK, norek);
        values.put(SessionPembayaranIklan.COLUMN_BATASWAKTU, bataswaktu);
        values.put(SessionPembayaranIklan.COLUMN_NOMINAL, nominal);
        // insert row
        db.insert(table, null, values);
        // close db connection
        db.close();
        // return newly inserted row id
    }


    public void insertNotif(String title,String message) {
        // get writable database as we want to write data
       SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        // `id` and `timestamp` will be inserted automatically.
        // no need to add them
        values.put(SessionNotif.COLUMN_TITLE, title);
        values.put(SessionNotif.COLUMN_MESSAGE, message);
        // insert row
        db.insert("notifikasi", null, values);
        // close db connection
        db.close();
        // return newly inserted row id
    }


    public SessionPembayaranEvent getPembayaranEvent(String id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        cursor = db.query(SessionPembayaranIklan.TABLE_NAME,
                new String[]{SessionPembayaranIklan.COLUMN_ID, SessionPembayaranIklan.COLUMN_BATASWAKTU
                        ,SessionPembayaranIklan.COLUMN_TITLE,SessionPembayaranIklan.COLUMN_NOREK
                        ,SessionPembayaranIklan.COLUMN_NOMINAL},
                SessionPembayaranIklan.COLUMN_ID + "=?",
                new String[]{id}, null, null, null, null);
        SessionPembayaranEvent sessionPembayaranEvent=null;
        System.out.println("data sebanyak "+cursor.getCount());
        if (cursor.getCount()>0) {
            cursor.moveToFirst();

            // prepare SessionPembayaranIklan object
            sessionPembayaranEvent = new SessionPembayaranEvent(
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_NOMINAL)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_BATASWAKTU)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_TITLE)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_NOREK)));
        }
        // close the db connection
        cursor.close();
        if (sessionPembayaranEvent==null){
            return null;
        }else {
            return sessionPembayaranEvent;
        }
    }

    public List<SessionNotif> getSessionNotif() {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        String selectQuery = "SELECT  * FROM " + SessionNotif.TABLE_NAME;
        cursor      = db.rawQuery(selectQuery, null);
        System.out.println("data sebanyak "+cursor.getCount());
        SessionNotif sessionNotif=null;
        List<SessionNotif> dataReturn=new ArrayList<>();
        if (cursor.getCount()>0) {
            cursor.moveToFirst();
            while (!cursor.isAfterLast()) {
                // prepare SessionPembayaranIklan object
                sessionNotif = new SessionNotif(
                        cursor.getString(cursor.getColumnIndex(SessionNotif.COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(SessionNotif.COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(SessionNotif.COLUMN_MESSAGE)));

                dataReturn.add(sessionNotif);
                cursor.moveToNext();
            }
        }
        // close the db connection
        cursor.close();
        if (dataReturn.isEmpty()){
            return null;
        }else {
            return dataReturn;
        }
    }



    public SessionPembayaranIklan getPembayaranIkaln(String id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        cursor = db.query(SessionPembayaranIklan.TABLE_NAME,
                new String[]{SessionPembayaranIklan.COLUMN_ID, SessionPembayaranIklan.COLUMN_BATASWAKTU
                        ,SessionPembayaranIklan.COLUMN_TITLE,SessionPembayaranIklan.COLUMN_NOREK
                        ,SessionPembayaranIklan.COLUMN_NOMINAL},
                SessionPembayaranIklan.COLUMN_ID + "=?",
                new String[]{id}, null, null, null, null);
        SessionPembayaranIklan sessionPembayaranIklan=null;
        System.out.println("data sebanyak "+cursor.getCount());
        if (cursor.getCount()>0) {
            cursor.moveToFirst();

            // prepare SessionPembayaranIklan object
            sessionPembayaranIklan = new SessionPembayaranIklan(
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_ID)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_NOMINAL)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_BATASWAKTU)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_TITLE)),
                    cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_NOREK)));
        }
        // close the db connection
        cursor.close();
        if (sessionPembayaranIklan==null){
            return null;
        }else {
            return sessionPembayaranIklan;
        }
    }

    public List<SessionPembayaranTopup> getPembayaranTopup(String id) {
        // get readable database as we are not inserting anything
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor=null;
        cursor = db.query(SessionPembayaranIklan.TABLE_NAME,
                new String[]{SessionPembayaranIklan.COLUMN_ID, SessionPembayaranIklan.COLUMN_BATASWAKTU
                        ,SessionPembayaranIklan.COLUMN_TITLE,SessionPembayaranIklan.COLUMN_NOREK
                        ,SessionPembayaranIklan.COLUMN_NOMINAL},
                SessionPembayaranIklan.COLUMN_ID + "=?",
                new String[]{id}, null, null, null, null);
        SessionPembayaranTopup sessionPembayaranTopup=null;
        System.out.println("data sebanyak "+cursor.getCount());
        List<SessionPembayaranTopup> dataReturn=new ArrayList<>();
        if (cursor.getCount()>0) {
            cursor.moveToFirst();

            while (!cursor.isAfterLast()) {
                // prepare SessionPembayaranIklan object
                sessionPembayaranTopup = new SessionPembayaranTopup(
                        cursor.getString(cursor.getColumnIndex(SessionPembayaranTopup.COLUMN_ID)),
                        cursor.getString(cursor.getColumnIndex(SessionPembayaranTopup.COLUMN_NOMINAL)),
                        cursor.getString(cursor.getColumnIndex(SessionPembayaranTopup.COLUMN_BATASWAKTU)),
                        cursor.getString(cursor.getColumnIndex(SessionPembayaranTopup.COLUMN_TITLE)),
                        cursor.getString(cursor.getColumnIndex(SessionPembayaranTopup.COLUMN_NOREK)));

                dataReturn.add(sessionPembayaranTopup);
                cursor.moveToNext();
            }

        }
        // close the db connection
        cursor.close();
        if (dataReturn.isEmpty()){
            return null;
        }else {
            return dataReturn;
        }
    }

    public ArrayList<String[]> getDbTableDetails() {

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor c = db.rawQuery(
                "SELECT name FROM SessionPembayaran_db WHERE type='table'", null);
        ArrayList<String[]> result = new ArrayList<String[]>();
        int i = 0;
        result.add(c.getColumnNames());
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            String[] temp = new String[c.getColumnCount()];
            for (i = 0; i < temp.length; i++) {
                temp[i] = c.getString(i);
            }
            result.add(temp);
        }

        return result;
    }

//    public List<SessionPembayaranIklan> getAllSessionPembayaranIklans() {
//        List<SessionPembayaranIklan> SessionPembayaranIklans = new ArrayList<>();
//
//        // Select All Query
//        String selectQuery = "SELECT  * FROM " + SessionPembayaranIklan.TABLE_NAME + " ORDER BY " +
//                SessionPembayaranIklan.COLUMN_TIMESTAMP + " DESC";
//
//        SQLiteDatabase db = this.getWritableDatabase();
//        Cursor cursor = db.rawQuery(selectQuery, null);
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                SessionPembayaranIklan SessionPembayaranIklan = new SessionPembayaranIklan();
//                SessionPembayaranIklan.setId(cursor.getInt(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_ID)));
//                SessionPembayaranIklan.setSessionPembayaranIklan(cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_SessionPembayaranIklan)));
//                SessionPembayaranIklan.setTimestamp(cursor.getString(cursor.getColumnIndex(SessionPembayaranIklan.COLUMN_TIMESTAMP)));
//
//                SessionPembayaranIklans.add(SessionPembayaranIklan);
//            } while (cursor.moveToNext());
//        }
//
//        // close db connection
//        db.close();
//
//        // return SessionPembayaranIklans list
//        return SessionPembayaranIklans;
//    }

    public int getCountPembayaran(String table) {
        String countQuery = "SELECT  * FROM " + table;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);

        int count = cursor.getCount();
        cursor.close();


        // return count
        return count;
    }

//    public int updateSessionPembayaranIklan(SessionPembayaranIklan SessionPembayaranIklan) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(SessionPembayaranIklan.COLUMN_SessionPembayaranIklan, SessionPembayaranIklan.getSessionPembayaranIklan());
//
//        // updating row
//        return db.update(SessionPembayaranIklan.TABLE_NAME, values, SessionPembayaranIklan.COLUMN_ID + " = ?",
//                new String[]{String.valueOf(SessionPembayaranIklan.getId())});
//    }

    public void deleteSessionPembayaran(String[] id,String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table, SessionPembayaranIklan.COLUMN_ID + " = ?",
                id);
        db.close();
    }
    public void deleteAllNotif() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.execSQL("delete from "+ SessionNotif.TABLE_NAME);
        db.close();
    }
}