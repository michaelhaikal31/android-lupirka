package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;

public class AdapterBannerEvent extends RecyclerView.Adapter<AdapterBannerEvent.ViewHolder> {

        private String[] mData ;
        private LayoutInflater mInflater;
        Context ctx;

        // data is passed into the constructor
        public AdapterBannerEvent(Context context, String[] data) {
            this.mInflater = LayoutInflater.from(context);
            ctx =context;
            this.mData = data;
        }

        // inflates the cell layout from xml when needed
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.row_banner_file, parent, false);
            return new ViewHolder(view);
        }

        // binds the data to the textview in each cell
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            String icon = mData[position];
            if (icon.contains(".png")|| icon.contains(".jpg")) {
                Picasso.with(ctx).load(icon).into(holder.icon);
            }
        }

        // total number of cells
        @Override
        public int getItemCount() {
            return mData.length;
        }


        // stores and recycles views as they are scrolled off screen
        public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
            ImageView icon;

            ViewHolder(View itemView) {
                super(itemView);
                icon = (ImageView) itemView.findViewById(R.id.bannerFile);
                itemView.setOnClickListener(this);

            }

            @Override
            public void onClick(View view) {
            }
        }



}
