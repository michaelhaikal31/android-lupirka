package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Function;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdapterListPaketSelected extends RecyclerView.Adapter<AdapterListPaketSelected.RecyclerViewHolders> {

    private List<PaketTemp> itemList;
    private LayoutInflater mInflater;
    Context ctx;


    public AdapterListPaketSelected(Context context, List<PaketTemp> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        ctx = context;

    }

    @Override
    public AdapterListPaketSelected.RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selected_activity_event, null);
        AdapterListPaketSelected.RecyclerViewHolders rcv = new AdapterListPaketSelected.RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterListPaketSelected.RecyclerViewHolders holder, final int position) {

        String namaAktifitas = itemList.get(position).getAktifitas();
        String harga = itemList.get(position).getHarga();
        String tipeWaktuBayar = itemList.get(position).getKategori_harga();

        if (itemList.get(position).isStatusChecked()){
            holder.tvName.setText(namaAktifitas);
            holder.tvHarga.setText(Function.ConvertRupiah(Integer.valueOf(harga)));
            holder.tvTipeBayar.setText(tipeWaktuBayar);
        }else {
            holder.layoutAktivitas.setVisibility(View.GONE);
        }


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvName, tvHarga, tvTipeBayar;
        LinearLayout layoutAktivitas;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            layoutAktivitas = (LinearLayout) itemView.findViewById(R.id.layoutAktivitas);
            tvName = (TextView) itemView.findViewById(R.id.textNamaKegiatan);
            tvHarga = (TextView) itemView.findViewById(R.id.valueEventHargaKegiatan);
            tvTipeBayar = (TextView) itemView.findViewById(R.id.textTipeWaktuPembayaran);

        }


        @Override
        public void onClick(View view) {


        }
    }
}