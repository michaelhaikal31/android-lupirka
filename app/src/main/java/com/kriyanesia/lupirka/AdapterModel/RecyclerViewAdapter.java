package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ItemHolder>{

    private List<Event> itemsList;
    private OnItemClickListener onItemClickListener;
    private LayoutInflater layoutInflater;
    Context ctx;

    public RecyclerViewAdapter(Context context,List<Event> itemsList){
        layoutInflater = LayoutInflater.from(context);
        this.itemsList = itemsList;
        ctx=context;
    }

    @Override
    public ItemHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = layoutInflater.inflate(R.layout.row_event_list, parent, false);
        return new ItemHolder(itemView, this);
    }

    @Override
    public void onBindViewHolder(ItemHolder holder, int position) {
        holder.tvTitle.setText(itemsList.get(position).getTitle());
        holder.tvTgl.setText(itemsList.get(position).getStart());
        holder.tvDesc.setText(itemsList.get(position).getDesc());
        if (!itemsList.get(position).getImage().isEmpty()){
        Picasso.with(ctx).load(itemsList.get(position).getImage()).into(holder.iconEvent);
        }
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setOnItemClickListener(OnItemClickListener listener){
        onItemClickListener = listener;
    }

    public OnItemClickListener getOnItemClickListener(){
        return onItemClickListener;
    }

    public interface OnItemClickListener{
        public void onItemClick(ItemHolder item, int position);
    }

    public void add(int location, List<Event> list){
        itemsList.add(list.get(location));
        notifyItemInserted(location);
    }

    public void set(int location, Event event){
        itemsList.set(location,event);
        notifyItemChanged(location);
    }

    public void clear(){
        itemsList.clear();
        notifyDataSetChanged();
    }

    public static class ItemHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private RecyclerViewAdapter parent;
        public TextView tvTitle,tvTgl,tvDesc;
        public ImageView iconEvent;
        private Context ctx;

        public ItemHolder(View itemView, RecyclerViewAdapter parent) {
            super(itemView);
            itemView.setOnClickListener(this);
            this.parent = parent;
            tvTitle = (TextView)itemView.findViewById(R.id.txtEventTitle);
            tvTgl = (TextView)itemView.findViewById(R.id.txtEventTanggal);
            tvDesc = (TextView)itemView.findViewById(R.id.txtAdsDescription);
            iconEvent = (ImageView)itemView.findViewById(R.id.bannerEvent);
        }





        @Override
        public void onClick(View v) {
            final OnItemClickListener listener = parent.getOnItemClickListener();
            if(listener != null){
                listener.onItemClick(this, getAdapterPosition());
            }
        }
    }
}