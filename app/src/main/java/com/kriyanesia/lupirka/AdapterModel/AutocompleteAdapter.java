package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Daerah;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class AutocompleteAdapter extends ArrayAdapter implements Filterable{
    private List<String> daerah;
    Context ctx;
    public AutocompleteAdapter(Context context, int resource) {
        super(context, resource);
        daerah = new ArrayList<String>();
        ctx= context;

    }

    @Override
    public int getCount() {
        return daerah.size();
    }

    @Override
    public String getItem(int position) {
        return daerah.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint.length()>=3){

                    daerah = loadDaerah(constraint);

                    filterResults.values = daerah;
                    filterResults.count = daerah.size();
                }
                return filterResults;
            }


            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.row_auto_complate_daerah,parent,false);

        TextView countryName = (TextView) view.findViewById(R.id.provinsi);

        countryName.setText(daerah.get(position));

        return view;
    }

    private List<String> loadDaerah(CharSequence keyword){
        final List<String> data = new ArrayList<>();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("query",keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<Daerah> profesiCall = apiInterface.getDaerah(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<Daerah>() {
            @Override
            public void onResponse(Call<Daerah> call, Response<Daerah> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<String> area ;
                    area = response.body().getData().getArea();



                    for (int i =0; i<area.size();i++){
                        data.add(area.get(i));
                    }

                }
                notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<Daerah> call, Throwable t) {
                Toast.makeText(ctx, "koneksi anda bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
        return data;
    }



}