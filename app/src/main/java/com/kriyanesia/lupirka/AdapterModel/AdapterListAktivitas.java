package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.content.res.Resources;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.load.engine.Resource;
import com.kriyanesia.lupirka.Data.Aktivitas.Aktivitas;
import com.kriyanesia.lupirka.Data.Sosmed;
import com.kriyanesia.lupirka.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class AdapterListAktivitas extends RecyclerView.Adapter<AdapterListAktivitas.RecyclerViewHolders> {

    private List<Aktivitas> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    private Context context;


    public AdapterListAktivitas(Context context, List<Aktivitas> itemList) {

        try {
            this.itemList = itemList;
            this.mInflater = LayoutInflater.from(context);
            this.context = context;
        }catch (Error e){
            Log.e("AKTIVITAS",e.getMessage());
        }
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_aktivitas_list, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }


    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        final Aktivitas data = itemList.get(position);
        holder.title.setText(data.getTitle());
        String TanggalDate= (data.getTanggal()).substring(0,10);
        String timeDate= (data.getTanggal()).substring(11);
        Date date= null;
        Date time= null;
        //Date now = new Date();

        try {
            date = new SimpleDateFormat("yyyy-MM-dd").parse(TanggalDate);
            time = new SimpleDateFormat("H:m:s").parse(timeDate);
            SimpleDateFormat awal = new SimpleDateFormat("E, dd MMMM yyyy");
            SimpleDateFormat akhir = new SimpleDateFormat("HH:mm");
            holder.date.setText(awal.format(date)+" ("+akhir.format(time)+" WIB)");
            if(data.getOngoing().equalsIgnoreCase("1")){
                holder.flag.setImageDrawable(context.getResources().getDrawable(R.drawable.ongoing));
            }

            /*
            Date now = Calendar.getInstance().getTime();
            Date today = new Date();
            long diff =  date.getTime()-today.getTime();
            int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
            int hours = (int) (diff / (1000 * 60 * 60));
            int minutes = (int) (diff / (1000 * 60));
            int seconds = (int) (diff / (1000));
            System.out.println("asd "+numOfDays +" "+hours+" "+minutes+ " "+seconds);
            */


//            waktu.setText(awal.format(dateAwal)+" s/d "+akhir.format(dateAkhir));
        } catch (ParseException e) {
            e.printStackTrace();
        }
//        holder.date.setText(data.getTanggal());
//        holder.time.setText(data.getTanggal());


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onAktivitasClickListener(Integer.valueOf(data.getId_activity()));
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public TextView title,date,time;
        private ImageView flag;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            flag = itemView.findViewById(R.id.flag);
            title = (TextView) itemView.findViewById(R.id.valueTitleAktivitas);
            date = (TextView) itemView.findViewById(R.id.valueDateAktivitas);
//            time = (TextView) itemView.findViewById(R.id.valueTimeAktivitas);
        }


    }
    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onAktivitasClickListener(int idAktivitas);
    }
}