package com.kriyanesia.lupirka.AdapterModel.AdapterRundown;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.kriyanesia.lupirka.Data.Rundown.Rundown;
import com.kriyanesia.lupirka.R;

import java.util.List;


public class AdapterListEvent extends RecyclerView.Adapter<AnotherViewHolder> {
    List<Rundown> listRundown;


    public AdapterListEvent(List<Rundown> listEvent) {
        this.listRundown = listEvent;
    }

    @NonNull
    @Override
    public AnotherViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_event_activity,viewGroup,false);
        AnotherViewHolder holder = new AnotherViewHolder(v);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull AnotherViewHolder anotherViewHolder, int i) {
        anotherViewHolder.time.setText(listRundown.get(i).getStartTime());
        anotherViewHolder.header.setText(listRundown.get(i).getStartTime() +" - "+listRundown.get(i).getEndTime()+" | "+listRundown.get(i).getDescription());
        anotherViewHolder.body.setText(listRundown.get(i).getEvent());
    }

    @Override
    public int getItemCount() {
        return listRundown.size();
    }
}

class AnotherViewHolder extends RecyclerView.ViewHolder{
TextView time,header,body;

    public AnotherViewHolder(@NonNull View itemView) {
        super(itemView);
        time = (TextView)itemView.findViewById(R.id.txtTime);
        header = (TextView)itemView.findViewById(R.id.txtHeader);
        body = (TextView)itemView.findViewById(R.id.txtBody);

    }
}
