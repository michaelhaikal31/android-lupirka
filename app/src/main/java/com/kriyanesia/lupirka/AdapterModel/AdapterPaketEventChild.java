
package com.kriyanesia.lupirka.AdapterModel;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Ads.AccountBank;
import com.kriyanesia.lupirka.Data.Event.Paket;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ferna on 5/7/2018.
 */

public class AdapterPaketEventChild  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<Paket> childData=new ArrayList<>();
    ImageView iconBank;
    private ItemBankClickListener ItemBankClickListener;



//    public void setItemBankClickListener(AdapterPembayaranChild.ItemBankClickListener itemBankClickListener) {
//        ItemBankClickListener = itemBankClickListener;
//    }

    public interface ItemBankClickListener {
        void onBankClick(String accountBank);
    }
    List<View> itemViewList = new ArrayList<>();
    public AdapterPaketEventChild(ArrayList<Paket> childData,String userType) {
        this.childData = childData;
        System.out.println("ADA DATA DISET KE CHILD "+childData.size());
//        for (int i=0; i<childData.size();i++){
//
//        }
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        private SparseBooleanArray selectedItems = new SparseBooleanArray();
        public TextView textKegiatan,textHarga1,textKuota;
        public CheckBox checkBox1,checkBox2;
        ImageView tvExpandCollapseToggle;

        public ViewHolder(View itemView) {
            super(itemView);
            textKegiatan = (TextView)itemView.findViewById(R.id.textUser);
            textHarga1 = (TextView)itemView.findViewById(R.id.textHarga);
//            textHarga2 = (TextView)itemView.findViewById(R.id.textHarga2);
            textKuota = (TextView)itemView.findViewById(R.id.textKuota);
            checkBox1 = (CheckBox)itemView.findViewById(R.id.rbCheck1);
            checkBox2 = (CheckBox)itemView.findViewById(R.id.rbCheck2);

        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_list_paket_event_child, parent, false);

        AdapterPaketEventChild.ViewHolder cavh = new AdapterPaketEventChild.ViewHolder(itemLayoutView);

        return cavh;
    }





    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;
        itemViewList.add(vh.itemView);

        System.out.println("CHILD DATA "+childData.get(position).getKategori_peserta());
        if (position==0 || !childData.get(position).getKategori_peserta().equalsIgnoreCase(childData.get(position -1).getKategori_peserta())){
            vh.textKegiatan.setVisibility(View.VISIBLE);
            vh.textHarga1.setVisibility(View.VISIBLE);
            vh.textKuota.setVisibility(View.VISIBLE);
            vh.checkBox1.setVisibility(View.VISIBLE);
            vh.checkBox2.setVisibility(View.VISIBLE);


            vh.textKegiatan.setText(""+childData.get(position).getKategori_peserta());
//        if (childData.get(position).getKategori_harga().equalsIgnoreCase("Diskon")){
//
//            vh.textHarga1.setText(""+childData.get(position).getHarga() +" (diskon)");
////            vh.textHarga2.setVisibility(View.GONE);
////            vh.checkBox2.setVisibility(View.GONE);
//        }else {
//
////            vh.textHarga1.setVisibility(View.GONE);
////            vh.checkBox1.setVisibility(View.GONE);
//            vh.textHarga1.setText(""+childData.get(position).getHarga());
//        }
            vh.textHarga1.setText(""+childData.get(position).getHarga());
            boolean checkKuota=true;
            try {
                Integer.parseInt(childData.get(position).getQuota());
            }catch(NumberFormatException nfe) {
                checkKuota=false;
            }

            if (checkKuota) {
                vh.textKuota.setText("" + childData.get(position).getQuota());
                vh.checkBox2.setVisibility(View.GONE);


            }else {
                int x=getItemCount();
                if (position+1<x) {
                    if (childData.get(position).getKategori_peserta().equalsIgnoreCase(childData.get(position + 1).getKategori_peserta())){
                        vh.textKuota.setVisibility(View.VISIBLE);
                        vh.textKuota.setText(childData.get(position+1).getHarga());
                    }
                }
                else {
                    vh.textKuota.setVisibility(View.GONE);
                }
            }
        }else {
            vh.textKegiatan.setVisibility(View.GONE);
            vh.textHarga1.setVisibility(View.GONE);
            vh.textKuota.setVisibility(View.GONE);
            vh.checkBox1.setVisibility(View.GONE);
            vh.checkBox2.setVisibility(View.GONE);
        }





        final SparseBooleanArray selectedItems = new SparseBooleanArray();
        vh.checkBox1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vh.checkBox2.isChecked()){
                    vh.checkBox2.setChecked(false);
                }
                System.out.println("ada data XXXXXXXXXXX1 "+position);
            }
        });

        vh.checkBox2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (vh.checkBox1.isChecked()){
                    vh.checkBox1.setChecked(false);
                }
                System.out.println("ada data XXXXXXXXXXX2 "+position);
            }
        });

//        vh.checkBox1.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                System.out.println("ada data XXXXXXXXXXX "+position);
////                for(View tempItemView : itemViewList) {
////                    /** navigate through all the itemViews and change color
////                     of selected view to colorSelected and rest of the views to colorDefault **/
////                    if(itemViewList.get(vh.getAdapterPosition()) == tempItemView) {
////                        tempItemView.setBackgroundResource(R.drawable.drawable_active_bank_select);
////                        if (ItemBankClickListener!=null) {
////                            ItemBankClickListener.onBankClick(accountBank.get(position).getAccountName());
////                        }else {
////                            System.out.println("TIDAK BISA");
////                        }
////                    }
////                    else{
////                        tempItemView.setBackgroundResource(R.color.white);
////                    }
////                }
//
//
//
//            }
//        });


    }

    @Override
    public int getItemCount() {
        System.out.println("ADA GET COUNT CHILD "+childData.size());
        return childData.size();
    }

}
