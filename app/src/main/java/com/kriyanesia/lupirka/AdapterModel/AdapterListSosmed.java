package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Sosmed;
import com.kriyanesia.lupirka.R;

import java.util.List;

public class AdapterListSosmed extends RecyclerView.Adapter<AdapterListSosmed.RecyclerViewHolders> {

    private List<Sosmed> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;


    public AdapterListSosmed(Context context, List<Sosmed> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_share_sosmed, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        holder.tvSosmed.setText(itemList.get(position).getName());
        holder.iconSosmed.setImageResource(itemList.get(position).getImage());
        holder.iconSosmed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mClickListener != null) mClickListener.onItemClick(v, position, itemList.get(position).getName());
            }
        });
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvSosmed;
        public ImageView iconSosmed;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvSosmed = (TextView)itemView.findViewById(R.id.textSosmed);
            iconSosmed = (ImageView)itemView.findViewById(R.id.imageSosmed);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {

//            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }
    // allows clicks events to be caught
    public void setClickListener(ItemClickListener itemClickListener) {
        this.mClickListener = itemClickListener;
    }

    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void onItemClick(View view, int position,String nama);
    }
}