package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class AdapterListMyAds extends RecyclerView.Adapter<AdapterListMyAds.RecyclerViewHolders> {

    private List<Ads> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;
    Context ctx;

    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public AdapterListMyAds(Context context, List<Ads> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        ctx =context;

    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_ads, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {

        String start = itemList.get(position).getStartDate();
        String end = itemList.get(position).getEndDAte();
        String dateStart = start.substring(0,start.length()-2);
        String dateEnd = end.substring(0,start.length()-2);
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
        Date startDate = new Date();
        Date endDate = new Date();
        try {
            startDate = dateFormat.parse(dateStart);
            endDate = dateFormat.parse(dateEnd);
        } catch (ParseException e) {
            e.printStackTrace();
        }


        holder.tvTitle.setText(itemList.get(position).getAdsTitle());
        Picasso.with(ctx).load(itemList.get(position).getUrlImage()).into(holder.iconAds);
//        holder.tvTgl.setText(startDate.getDay()+"-"+endDate.getDay()+" "+getMonthName(startDate.getMonth(),Locale.US, true));
        holder.tvTgl.setText(dateStart+" - "+dateEnd);
        holder.tvDesc.setText(itemList.get(position).getNote());
        holder.tvStatus.setText(itemList.get(position).getStatus());
//        Toast.makeText(ctx, startDate.toString(), Toast.LENGTH_SHORT).show();

//        holder.iconSosmed.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (mClickListener != null) mClickListener.onItemClick(v, position, itemList.get(position).getName());
//            }
//        });
    }
    private String getMonthName(final int index, final Locale locale, final boolean shortName)
    {
        String format = "%tB";

        if (shortName)
            format = "%tb";

        Calendar calendar = Calendar.getInstance(locale);
        calendar.set(Calendar.MONTH, index);
        calendar.set(Calendar.DAY_OF_MONTH, 1);

        return String.format(locale, format, calendar);
    }
    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView tvTitle,tvTgl,tvDesc,tvStatus;
        public ImageView iconAds;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            tvTitle = (TextView)itemView.findViewById(R.id.txtTitleAds);
            tvTgl = (TextView)itemView.findViewById(R.id.txtAdsTanggal);
            tvDesc = (TextView)itemView.findViewById(R.id.txtAdsDescription);
            tvStatus = (TextView)itemView.findViewById(R.id.txtAdsStatus);
            iconAds = (ImageView)itemView.findViewById(R.id.bannerAds);

        }


        @Override
        public void onClick(View view) {

            if (mClickListener != null) {
                String idAds = itemList.get(getAdapterPosition()).getIdAds();
                String idTitle = itemList.get(getAdapterPosition()).getAdsTitle();
                String adsSubTitle = itemList.get(getAdapterPosition()).getAdsSubTitle();
                String categoryName = itemList.get(getAdapterPosition()).getCategoryName();
                String note = itemList.get(getAdapterPosition()).getNote();
                String endDAte = itemList.get(getAdapterPosition()).getEndDAte();
                String startDate = itemList.get(getAdapterPosition()).getStartDate();
                String urlImage = itemList.get(getAdapterPosition()).getUrlImage();
                mClickListener.OnMyAdsClickListener(idAds,idTitle,adsSubTitle
                        ,categoryName,note,startDate,endDAte,urlImage);
            }
        }
    }


    // parent activity will implement this method to respond to click events
    public interface ItemClickListener {
        void OnMyAdsClickListener(String idAds, String idTitle,
                                   String adsSubTitle, String categoryName,
                                   String note, String startDate, String endDAte,
                                   String urlImage);
    }
}