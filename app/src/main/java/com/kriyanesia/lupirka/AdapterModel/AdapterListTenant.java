package com.kriyanesia.lupirka.AdapterModel;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.Data.Tenant;
import com.kriyanesia.lupirka.OnLoadMoreListener;
import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.List;

public class AdapterListTenant extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private OnLoadMoreListener onLoadMoreListener;
    private AdapterListTenant.OnItemClickListener onItemClickListener;
    private boolean isLoading;
    private Activity activity;
    private List<ValueListBooth.Booth> itemList;
    private int visibleThreshold = 5;
    private int lastVisibleItem, totalItemCount;
    private  Context context;
    public interface OnItemClickListener{
        void onItemClick(String idTenant);
    }

    public void setOnItemClickListener(AdapterListTenant.OnItemClickListener onItemClickListener) {
        this.onItemClickListener = onItemClickListener;
    }

    public AdapterListTenant(RecyclerView recyclerView, List<ValueListBooth.Booth> itemList, Activity activity) {
        this.itemList = itemList;
        this.activity = activity;

        final LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                totalItemCount = linearLayoutManager.getItemCount();
                lastVisibleItem = linearLayoutManager.findLastVisibleItemPosition();
                if (!isLoading && totalItemCount <= (lastVisibleItem + visibleThreshold)) {
                    if (onLoadMoreListener != null) {
                        onLoadMoreListener.onLoadMore();
                    }
                    isLoading = true;
                }
            }
        });
    }

    public void setOnLoadMoreListener(OnLoadMoreListener mOnLoadMoreListener) {
        this.onLoadMoreListener = mOnLoadMoreListener;
    }

    @Override
    public int getItemViewType(int position) {
        return itemList.get(position) == null ? VIEW_TYPE_LOADING : VIEW_TYPE_ITEM;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (viewType == VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(activity).inflate(R.layout.row_tenan_list, parent, false);
            return new AdapterListTenant.TenantViewHolder(view);
        } else if (viewType == VIEW_TYPE_LOADING) {
            View view = LayoutInflater.from(activity).inflate(R.layout.progressbar_load_event_more, parent, false);
            return new AdapterListTenant.LoadingViewHolder(view);
        }
        return null;
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof AdapterListTenant.TenantViewHolder) {
            final  AdapterListTenant.TenantViewHolder TenantViewHolder =  (AdapterListTenant.TenantViewHolder) holder;
            TenantViewHolder.namaTenant.setText(itemList.get(position).getKode_tenant());
            TenantViewHolder.keteranganTenant.setText(itemList.get(position).getNote());
            System.out.println("asd [Adapter Tenan get Status]"+itemList.get(position).getStatus());
            if(itemList.get(position).getStatus().equalsIgnoreCase("false")){
                TenantViewHolder.chkTenant.setText("Failed");
                TenantViewHolder.chkTenant.setTextColor(Color.parseColor("#FF0000"));
                TenantViewHolder.iconTenant.setImageResource(R.drawable.ic_cancel_black_24dp);
            }else {
                TenantViewHolder.chkTenant.setText("success");
            }
        } else if (holder instanceof AdapterListTenant.LoadingViewHolder) {
            AdapterListTenant.LoadingViewHolder loadingViewHolder = (AdapterListTenant.LoadingViewHolder) holder;
            loadingViewHolder.progressBar.setIndeterminate(true);
        }
    }

    @Override
    public int getItemCount() {
        return itemList == null ? 0 : itemList.size();
    }

    public void setLoaded() {
        isLoading = false;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {
        public ProgressBar progressBar;
        TextView text;

        public LoadingViewHolder(View view) {
            super(view);
            progressBar = (ProgressBar) view.findViewById(R.id.progressBarLoadMore);
            text = (TextView) view.findViewById(R.id.text) ;
            final boolean stop = false;
//            for (int i=0;i<5 && isLoading;i++){
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    progressBar.setVisibility(View.GONE);
                    text.setVisibility(View.GONE);
//                        if (!isLoading) {
//
//
//
//                        }
                }
            },1000);

        }
    }

    private class TenantViewHolder extends RecyclerView.ViewHolder {
        public TextView namaTenant,keteranganTenant, chkTenant;
        public ImageView iconTenant;

        public TenantViewHolder(View view) {
            super(view);
//            itemView.setOnClickListener(this);
            namaTenant = (TextView)itemView.findViewById(R.id.txtNamaTenan);
            keteranganTenant = (TextView)itemView.findViewById(R.id.txtKeteranganTenan);
            chkTenant = (TextView)itemView.findViewById(R.id.chkTenant);
            iconTenant = (ImageView)itemView.findViewById(R.id.imgTenant);
//            itemView.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    onItemClickListener.onItemClick(itemList.get(getAdapterPosition()).getId());
//                }
//            });

        }
    }
}
