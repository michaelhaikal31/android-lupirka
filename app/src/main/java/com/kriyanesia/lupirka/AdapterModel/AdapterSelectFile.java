package com.kriyanesia.lupirka.AdapterModel;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.R;

public class AdapterSelectFile  extends RecyclerView.Adapter<AdapterSelectFile.MyViewHolder> {
    public java.util.ArrayList<String> myValues;
    public AdapterSelectFile (java.util.ArrayList<String> myValues){
        this.myValues= myValues;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View listItem = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_pdf_file, parent, false);
        return new MyViewHolder(listItem);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
         holder.myTextView.setText(myValues.get(position));
    }


    @Override
    public int getItemCount() {
        return myValues.size();
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {
        private TextView myTextView;
        public MyViewHolder(View itemView) {
            super(itemView);
        myTextView = (TextView)itemView.findViewById(R.id.txtNamaFile);
        }
    }
}