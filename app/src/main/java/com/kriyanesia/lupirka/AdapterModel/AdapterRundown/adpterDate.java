package com.kriyanesia.lupirka.AdapterModel.AdapterRundown;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import com.kriyanesia.lupirka.Data.Rundown.Rundown;
import com.kriyanesia.lupirka.R;


public class adpterDate extends RecyclerView.Adapter<MyViewHolder> {
   private List<Rundown> listRundown=new ArrayList<>();
    SimpleDateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy");
    Date date;
    private ItemClickListener ItemClickListener;
    private Context c;
    MyViewHolder tempHolder;


    public adpterDate(List<Rundown> listEvent) {
        this.listRundown.add(listEvent.get(0));
       for(int i=1;i<listEvent.size();i++){
           if(!this.listRundown.toString().contains(listEvent.get(i).toString())){
               this.listRundown.add(listEvent.get(i));
           }
       }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        c=parent.getContext();
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event_date,parent,false);
        MyViewHolder holder = new MyViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {

        if(position==0){
            tempHolder = holder;
             holder.date.setBackground(c.getResources().getDrawable(R.drawable.oncirclebutton));
             holder.date.setTextColor(Color.WHITE);
             holder.dayOfMonth.setTextColor(c.getResources().getColor(R.color.blackLight3));
        }
        try {
                date = inFormat.parse(listRundown.get(position).getDate());
            } catch (ParseException e) {

            }
            holder.dayOfMonth.setText(android.text.format.DateFormat.format("EEEE", date).subSequence(0, 4));
            holder.date.setText(DateFormat.format("dd", date));
            holder.date.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ItemClickListener.onItemClick(holder.date.getText().toString());
                    holder.date.setBackground(c.getResources().getDrawable(R.drawable.oncirclebutton));
                    holder.date.setTextColor(Color.WHITE);
                    holder.dayOfMonth.setTextColor(c.getResources().getColor(R.color.blackLight3));
                    if (tempHolder != null) {
                        tempHolder.dayOfMonth.setTextColor(c.getResources().getColor(R.color.whiteDark4));
                        tempHolder.date.setTextColor(c.getResources().getColor(R.color.whiteDark6));
                        tempHolder.date.setBackground(c.getResources().getDrawable(R.drawable.offcirclebutton));
                    }
                    tempHolder = holder;
                }
            });
        }

        @Override
        public int getItemCount () {
            return listRundown.size();
        }

        public void setItemClickListener (ItemClickListener ItemClickListener){
            this.ItemClickListener = ItemClickListener;
        }

        // parent activity will implement this method to respond to click events
        public interface ItemClickListener {
            void onItemClick(String date);
        }
    }

    class MyViewHolder extends RecyclerView.ViewHolder{
    public TextView dayOfMonth;
        Button date;
         public MyViewHolder(View v) {
        super(v);
        dayOfMonth = v.findViewById(R.id.textView);
        date = v.findViewById(R.id.button);
        }
}

