package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Sosmed;
import com.kriyanesia.lupirka.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

public class AdapterListDetailEventDate extends RecyclerView.Adapter<AdapterListDetailEventDate.RecyclerViewHolders> {

    private List<Event.EventDetail> itemList;
    private LayoutInflater mInflater;
    Context context;


    public AdapterListDetailEventDate(Context context, List<Event.EventDetail> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        this.context=context;
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_detail_event_time, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        holder.textNamaWaktu.setText(itemList.get(position).getName());
        String startDate= (itemList.get(position).getStart());
        String endDate= (itemList.get(position).getEnd());
        Date dateAkhir= null;
        Date dateAwal= null;
        try {
            dateAwal = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
            dateAkhir = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
            SimpleDateFormat awal = new SimpleDateFormat("dd MMMM yyyy");
            SimpleDateFormat akhir = new SimpleDateFormat("dd MMMM yyyy");
            holder.textTanggal.setText(awal.format(dateAwal)+" s/d "+akhir.format(dateAkhir));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        System.out.println("AWAL "+startDate+" DATE AKHIR "+dateAwal);
//        System.out.println("AWAL "+awal.format(dateAwal)+" AKHIR "+akhir.format(dateAkhir));

//        holder.textTanggal.setText(itemList.get(position).getStart()+" s/d "+itemList.get(position).getEnd());

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener{

        public TextView textNamaWaktu,textTanggal;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            textNamaWaktu = (TextView)itemView.findViewById(R.id.textNamaWaktu);
            textTanggal = (TextView)itemView.findViewById(R.id.textTanggal);

        }

        @Override
        public void onClick(View view) {

//            if (mClickListener != null) mClickListener.onItemClick(view, getAdapterPosition());
        }
    }



}