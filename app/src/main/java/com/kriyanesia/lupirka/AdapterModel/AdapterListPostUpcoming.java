package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.R;
import com.squareup.picasso.Picasso;

import java.util.List;

public class AdapterListPostUpcoming extends RecyclerView.Adapter<AdapterListPostUpcoming.MyViewHolder> {

    private Context mContext;
    private List<Event> homePosts;

    private ItemClickListener mClickListener;
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onEventClickListener(String idEvent);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView desc,textKota;
        public ImageView thumbnail;


        public MyViewHolder(View view) {
            super(view);
            desc = (TextView) view.findViewById(R.id.descriptionPost);
            thumbnail = (ImageView) view.findViewById(R.id.imagePost);
            textKota = (TextView) view.findViewById(R.id.textKota);
        }
    }


    public AdapterListPostUpcoming(Context mContext, List<Event> homePosts) {
        this.mContext = mContext;
        this.homePosts = homePosts;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_home_post, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Event homePost = homePosts.get(position);
        holder.desc.setText(homePost.getTitle());

        // loading album cover using Glide library

        if (homePost.getImage().startsWith("http")) {
            Picasso.with(mContext)
                    .load(homePost.getImage())
                    .fit()// resizing images can really distort the aspect ratio, prevent this
                    .into(holder.thumbnail);
        }
        holder.textKota.setText(homePost.getKota());

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onEventClickListener(homePost.getId());
                }
            }
        });

//        holder.overflow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showPopupMenu(holder.overflow);
//            }
//        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
//    private void showPopupMenu(View view) {
//        // inflate menu
//        PopupMenu popup = new PopupMenu(mContext, view);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.menu_album, popup.getMenu());
//        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
//        popup.show();
//    }

//    /**
//     * Click listener for popup menu items
//     */
//    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
//
//        public MyMenuItemClickListener() {
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//            switch (menuItem.getItemId()) {
//                case R.id.action_add_favourite:
//                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
//                    return true;
//                case R.id.action_play_next:
//                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
//                    return true;
//                default:
//            }
//            return false;
//        }
//    }

    @Override
    public int getItemCount() {
        return homePosts.size();
    }
}
