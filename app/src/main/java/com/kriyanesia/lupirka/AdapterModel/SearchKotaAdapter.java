package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.Data.ValueKota;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SearchKotaAdapter extends RecyclerView.Adapter<SearchKotaAdapter.RecyclerViewHolders> {

    private List<ValueKota.Kota> itemList;
    private ItemClickListener mClickListener;
    private LayoutInflater mInflater;


    public SearchKotaAdapter(Context context, List<ValueKota.Kota> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
    }

    @Override
    public RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_item_alertdialog, null);
        RecyclerViewHolders rcv = new RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(RecyclerViewHolders holder, final int position) {
        final ValueKota.Kota data = itemList.get(position);
        holder.title.setText(data.getKota());
        if (position % 2 != 0) {
            holder.itemView.setBackgroundColor(Color.parseColor("#FAFAFA"));
        }


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onListCityAlertClick(Integer.valueOf(data.getId()), data.getKota());
                }
            }
        });

    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder {

        public TextView title;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            title = (TextView) itemView.findViewById(R.id.text);
        }


    }

    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onListCityAlertClick(int id, String nama);
    }
}