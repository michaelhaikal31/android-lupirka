package com.kriyanesia.lupirka.AdapterModel;

import android.graphics.Color;
import android.os.Handler;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.TableRegistrasiEvent.WaktuEvent;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;

/**
 * Created by ferna on 5/7/2018.
 */

public class AdapterTableEventDetail  extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<WaktuEvent> childData;
    private ArrayList<WaktuEvent> childDataBk;

    public AdapterTableEventDetail(ArrayList<WaktuEvent> childData) {
        this.childData = childData;
        childDataBk = new ArrayList<>(childData);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        TextView tvHarga;
        ImageView tvExpandCollapseToggle;

        public ViewHolder(View itemView) {
            super(itemView);
            tvHarga = (TextView) itemView.findViewById(R.id.tvHarga);
        }

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemLayoutView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_table_event_time, parent, false);

        AdapterTableEventDetail.ViewHolder cavh = new AdapterTableEventDetail.ViewHolder(itemLayoutView);
        return cavh;
    }


    final Handler handler = new Handler();
    Runnable collapseList = new Runnable() {
        @Override
        public void run() {
            if (getItemCount() > 1) {
                childData.remove(childData.size() - 1);
                notifyDataSetChanged();
                handler.postDelayed(collapseList, 50);
            }
        }
    };

    Runnable expandList = new Runnable() {
        @Override
        public void run() {
            int currSize = childData.size();
            if (currSize == childDataBk.size()) return;

            if (currSize == 0) {
                childData.add(childDataBk.get(currSize));
            } else {
                childData.add(childDataBk.get(currSize));
            }
            notifyDataSetChanged();

            handler.postDelayed(expandList, 50);
        }
    };


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, final int position) {
        final ViewHolder vh = (ViewHolder) holder;

//        if (position == 0 && getItemCount() == 1) {
//            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_expand_more_white_24dp);
//            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
//        } else if (position == childDataBk.size() - 1) {
//            vh.tvExpandCollapseToggle.setImageResource(R.drawable.ic_expand_less_white_24dp);
//            vh.tvExpandCollapseToggle.setVisibility(View.VISIBLE);
//        } else {
//            vh.tvExpandCollapseToggle.setVisibility(View.GONE);
//        }

        WaktuEvent c = childData.get(position);
        vh.tvHarga.setText(""+c.getHarga());
        vh.tvHarga.setTextColor(Color.WHITE);
        vh.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                handler.removeCallbacksAndMessages(null);
//                if (getItemCount() > 1) {
//                    handler.post(collapseList);
//                } else {
//                    handler.post(expandList);
//                }
            }
        });
    }


    @Override
    public int getItemCount() {
        return childData != null ? childData.size() : 0;
    }

}
