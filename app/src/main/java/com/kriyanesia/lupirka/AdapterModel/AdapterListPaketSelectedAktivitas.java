package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.R;

import java.util.List;

public class AdapterListPaketSelectedAktivitas extends RecyclerView.Adapter<AdapterListPaketSelectedAktivitas.RecyclerViewHolders> {

    private List<ValueDetailAktivitas.DetailCostTemp> itemList;
    private LayoutInflater mInflater;
    Context ctx;


    public AdapterListPaketSelectedAktivitas(Context context, List<ValueDetailAktivitas.DetailCostTemp> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        ctx = context;

    }

    @Override
    public AdapterListPaketSelectedAktivitas.RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_selected_activity_event, null);
        AdapterListPaketSelectedAktivitas.RecyclerViewHolders rcv = new AdapterListPaketSelectedAktivitas.RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterListPaketSelectedAktivitas.RecyclerViewHolders holder, final int position) {

        String namaAktifitas = itemList.get(position).getPaket();
        String harga = itemList.get(position).getBiaya();
        holder.tvName.setText(namaAktifitas);
        holder.tvHarga.setText("Rp."+harga);
        holder.tvTipeBayar.setText("");



    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvName, tvHarga, tvTipeBayar;
        LinearLayout layoutAktivitas;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            layoutAktivitas = (LinearLayout) itemView.findViewById(R.id.layoutAktivitas);
            tvName = (TextView) itemView.findViewById(R.id.textNamaKegiatan);
            tvHarga = (TextView) itemView.findViewById(R.id.valueEventHargaKegiatan);
            tvTipeBayar = (TextView) itemView.findViewById(R.id.textTipeWaktuPembayaran);

        }


        @Override
        public void onClick(View view) {


        }
    }
}