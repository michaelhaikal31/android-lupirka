package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Dokter.AllDokter;
import com.kriyanesia.lupirka.Data.Dokter.Dokter;
import com.kriyanesia.lupirka.Data.Dokter.ValueDokter;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class SearchDokterAdapter extends ArrayAdapter implements Filterable{
    private List<Dokter> data;
    Context ctx;
    public SearchDokterAdapter(Context context, int resource) {
        super(context, resource);
        data = new ArrayList<>();
        ctx= context;

    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Dokter getItem(int position) {
        return data.get(position);
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint.length()>=3){
                    data.clear();
                    data = loadDokter(constraint);

                    filterResults.values = data;
                    filterResults.count = data.size();
                }
                return filterResults;
            }


            @Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0){
                    notifyDataSetChanged();
                }else{
                    notifyDataSetInvalidated();
                }
            }
        };

        return myFilter;
    }



    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(getContext());
        View view = inflater.inflate(R.layout.row_search_dokter,parent,false);
        TextView text,spesialisText;

        text = (TextView)view.findViewById(R.id.textname);
        spesialisText = (TextView)view.findViewById(R.id.textSpesialis);

        spesialisText.setText(data.get(position).getSpesialis());
        text.setText(data.get(position).getNama());



        return view;
    }

    private List<Dokter> loadDokter(CharSequence keyword){
        final List<Dokter> data = new ArrayList<>();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("query",keyword);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Call<ValueDokter> dataCall = apiInterface.getDokter(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueDokter>() {
            @Override
            public void onResponse(Call<ValueDokter> call, Response<ValueDokter> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Dokter> dataDokter ;
                    dataDokter = response.body().getData().getData();


                    if (dataDokter!=null) {
                        for (int i = 0; i < dataDokter.size(); i++) {

                            data.add(dataDokter.get(i));
                        }
                    }

                }
                notifyDataSetChanged();

            }

            @Override
            public void onFailure(Call<ValueDokter> call, Throwable t) {
                Toast.makeText(ctx, "koneksi anda bermasalah", Toast.LENGTH_SHORT).show();
            }
        });
        return data;
    }



}