package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.R;
import com.bumptech.glide.Glide;

import java.util.List;

public class AdapterListAllAdsLupirka extends RecyclerView.Adapter<AdapterListAllAdsLupirka.MyViewHolder> {

    private Context mContext;
    private List<Ads> data;

    private ItemClickListener mClickListener;
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onAdsClickListener(String idAds);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView desc;
        public ImageView thumbnail;

        public MyViewHolder(View view) {
            super(view);
            desc = (TextView) view.findViewById(R.id.descriptionPost);
            thumbnail = (ImageView) view.findViewById(R.id.imagePost);
        }
    }


    public AdapterListAllAdsLupirka(Context mContext, List<Ads> data) {
        this.mContext = mContext;
        this.data = data;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_home_post, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final Ads ads = data.get(position);
        holder.desc.setText(ads.getNote());

        // loading album cover using Glide library
        Glide.with(mContext).load(ads.getUrlImage()).into(holder.thumbnail);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onAdsClickListener(ads.getIdAds());
                }
            }
        });

//        holder.overflow.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                showPopupMenu(holder.overflow);
//            }
//        });
    }

    /**
     * Showing popup menu when tapping on 3 dots
     */
//    private void showPopupMenu(View view) {
//        // inflate menu
//        PopupMenu popup = new PopupMenu(mContext, view);
//        MenuInflater inflater = popup.getMenuInflater();
//        inflater.inflate(R.menu.menu_album, popup.getMenu());
//        popup.setOnMenuItemClickListener(new MyMenuItemClickListener());
//        popup.show();
//    }

//    /**
//     * Click listener for popup menu items
//     */
//    class MyMenuItemClickListener implements PopupMenu.OnMenuItemClickListener {
//
//        public MyMenuItemClickListener() {
//        }
//
//        @Override
//        public boolean onMenuItemClick(MenuItem menuItem) {
//            switch (menuItem.getItemId()) {
//                case R.id.action_add_favourite:
//                    Toast.makeText(mContext, "Add to favourite", Toast.LENGTH_SHORT).show();
//                    return true;
//                case R.id.action_play_next:
//                    Toast.makeText(mContext, "Play next", Toast.LENGTH_SHORT).show();
//                    return true;
//                default:
//            }
//            return false;
//        }
//    }

    @Override
    public int getItemCount() {
        return data.size();
    }
}
