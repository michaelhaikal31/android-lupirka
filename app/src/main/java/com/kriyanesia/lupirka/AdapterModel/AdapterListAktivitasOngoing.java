package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.R;

import java.util.List;

public class AdapterListAktivitasOngoing extends RecyclerView.Adapter<AdapterListAktivitasOngoing.RecyclerViewHolders> {

    private List<ValueDetailAktivitas.DetailKegiatanTemp> itemList;
    private LayoutInflater mInflater;
    Context ctx;

    private ItemClickListener mClickListener;
    // allows clicks events to be caught
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    public interface ItemClickListener {
        void onQrCodeScanClickListener();
    }

    public AdapterListAktivitasOngoing(Context context, List<ValueDetailAktivitas.DetailKegiatanTemp> itemList) {
        this.itemList = itemList;
        this.mInflater = LayoutInflater.from(context);
        ctx = context;

    }

    @Override
    public AdapterListAktivitasOngoing.RecyclerViewHolders onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_rundown_event, null);
        AdapterListAktivitasOngoing.RecyclerViewHolders rcv = new AdapterListAktivitasOngoing.RecyclerViewHolders(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(AdapterListAktivitasOngoing.RecyclerViewHolders holder, final int position) {

        String namaAktifitas = itemList.get(position).getKegiatan();
        holder.tvName.setText(namaAktifitas);
        holder.tvRoom.setText(itemList.get(position).getDetail());
        holder.tvStart.setText(itemList.get(position).getStarddate().substring(11,16));
        holder.tvEnd.setText(itemList.get(position).getEnddate().substring(11,16));
        holder.textScanQR.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mClickListener != null) {
                    mClickListener.onQrCodeScanClickListener();
                }
            }
        });


    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }


    public class RecyclerViewHolders extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView tvName, tvRoom, tvStart,tvEnd,textScanQR;
        LinearLayout layoutAktivitas;

        public RecyclerViewHolders(View itemView) {
            super(itemView);
            itemView.setOnClickListener(this);
            layoutAktivitas = (LinearLayout) itemView.findViewById(R.id.layoutAktivitas);
            tvName = (TextView) itemView.findViewById(R.id.textNamaKegiatan);
            tvRoom = (TextView) itemView.findViewById(R.id.textRuangan);
            tvStart = (TextView) itemView.findViewById(R.id.textStart);
            tvEnd = (TextView) itemView.findViewById(R.id.textEnd);
            textScanQR = (TextView) itemView.findViewById(R.id.textScanQR);

        }


        @Override
        public void onClick(View view) {


        }
    }
}