package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Pembayaran.ValueNominalTopup;
import com.kriyanesia.lupirka.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

public class AdapterListNominalTopup extends RecyclerView.Adapter<AdapterListNominalTopup.MyViewHolder> {

    private Context mContext;
    private List<ValueNominalTopup.NominalTopup> data;

    private ItemClickListener mClickListener;
    public void setmClickListener(ItemClickListener mClickListener) {
        this.mClickListener = mClickListener;
    }

    List<TextView>textViewList = new ArrayList<>();

    public interface ItemClickListener {
        void onEventClickListener(String nominal);
    }
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView nominal;

        public MyViewHolder(View view) {
            super(view);
            nominal = (TextView) view.findViewById(R.id.nominal);
        }
    }


    public AdapterListNominalTopup(Context mContext, List<ValueNominalTopup.NominalTopup> val) {
        this.mContext = mContext;
        this.data = val;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.row_nominal_topup, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        final ValueNominalTopup.NominalTopup val = data.get(position);
        holder.nominal.setText(val.getNominal());
        textViewList.add(holder.nominal);


        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                for(TextView textView : textViewList){
//                    textView.setCardBackgroundColor(mContext.getResources().getColor(R.color.colorPrimaryDark));
                    textView.setBackgroundResource(R.color.colorPrimaryDark);
                }
                //The selected card is set to colorSelected
//                holder.cardView.setCardBackgroundColor(mContext.getResources().getColor(R.color.black));
                holder.nominal.setBackgroundResource(R.color.grey);

                if (mClickListener!=null) {
                    mClickListener.onEventClickListener(val.getNominal());
                }


            }
        });



    }



    @Override
    public int getItemCount() {
        return data.size();
    }
}
