package com.kriyanesia.lupirka.AdapterModel;

import android.content.Context;
import android.database.DataSetObserver;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.kriyanesia.lupirka.Data.Universitas.Universitas;

import java.util.List;

public class AdapterSpinerUniversitas implements SpinnerAdapter {
    Context context;
    List<Universitas> universitas;

    public AdapterSpinerUniversitas(Context context , List<Universitas> universitas){
        this.context =context;
        this.universitas = universitas;
    }
    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return universitas.size();
    }

    @Override
    public Object getItem(int position) {
        // TODO Auto-generated method stub
        return universitas.get(position);
    }

    @Override
    public long getItemId(int position) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public int getItemViewType(int position) {
        // TODO Auto-generated method stub
        return 0;
    }
    //Note:-Create this two method getIDFromIndex and getIndexByID
    public String getIDFromIndex(int Index) {
        return universitas.get(Index).getId();
    }

    public int getIndexByID(int ID) {
        for(int i=0;i<ID;i++){
            if(Integer.valueOf(universitas.get(i).getId()) == ID){
            return i;
        }
    }
        return -1;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
        textview.setText(universitas.get(position).getName());

        return textview;
    }

    @Override
    public int getViewTypeCount() {
//        return android.R.layout.simple_spinner_item;
        return 1;
    }

    @Override
    public boolean hasStableIds() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isEmpty() {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void registerDataSetObserver(DataSetObserver observer) {
        // TODO Auto-generated method stub

    }

    @Override
    public void unregisterDataSetObserver(DataSetObserver observer) {
        // TODO Auto-generated method stub

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        TextView textview = (TextView) inflater.inflate(android.R.layout.simple_spinner_item, null);
        textview.setText(universitas.get(position).getName());
        textview.setPadding(10,10,10,10);
        return textview;
    }
}
