package com.kriyanesia.lupirka.Services;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;

public class FCMServiceInstance extends FirebaseInstanceIdService {
    private static final String REG_TOKEN = "REG_TOKEN";




    @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();


        String recentToken = FirebaseInstanceId.getInstance().getToken();
        SessionDevice sd = new SessionDevice();
        sd.setTokenDevice(this,"token",recentToken);
        System.out.println("FCM KEY "+recentToken);

        TelephonyManager telephonyManager = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {

            return;
        }
        String imei = telephonyManager.getDeviceId();
        sd.setImei(this,"imei",imei);
        Log.d(REG_TOKEN,recentToken);
        Log.d("REG_TOKEN :",imei);

    }
}
