package com.kriyanesia.lupirka.Services;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.app.TaskStackBuilder;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;

import com.google.gson.JsonObject;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.Menu.ActivityDetailContent;
import com.kriyanesia.lupirka.Menu.MenuDetailEvent;
import com.kriyanesia.lupirka.Menu.MenuMain;
import com.kriyanesia.lupirka.R;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Session.SessionUser;

import org.json.JSONObject;

import java.util.Map;

public class FCMMessagingService  extends FirebaseMessagingService {
    private static final String TAG = "FCM Service";
    private DatabaseHelper databaseHelper;


    @Override
    public void onCreate() {
        super.onCreate();
        databaseHelper = new DatabaseHelper(this);
    }



    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        super.onMessageReceived(remoteMessage);


        Map<String, String> data = remoteMessage.getData();
        String title= data.get("title");
        NotificationHelper nh = new NotificationHelper(this);

        if(title.contains("dev.update")){
            SessionDevice sessionDevice = new SessionDevice();
            sessionDevice.setUpdate(this,data.get("version_name"),data.get("level"));
            nh.createNotification("Update Lupirka Tersedia","Silahkan update aplikasi anda di Google playstore");
            databaseHelper.insertNotif("Update Lupirka Tersedia", "Silahkan update aplikasi anda di Google playstore");
        }else {
            String message=data.get("desc");
            nh.createNotification(title, message);
            databaseHelper.insertNotif(title, message);
        }
//
//
//        String title = remoteMessage.getNotification().getTitle();
//        String eventId=remoteMessage.getData().get("id_event");
//        String message = remoteMessage.getNotification().getBody();
//
//        String clickAction = remoteMessage.getData().get("click_action");

        //-------------------------------------


       /*
        if (clickAction.equalsIgnoreCase("detail_activity")) {
            intent = new Intent(this, ActivityDetailContent.class);
            intent.putExtra("menu","searchDetailEvent");
            intent.putExtra("eventId",eventId);
            System.out.println("DATA CKLICK ACTION  masuk di event");
        }else {
            System.out.println("DATA CKLICK ACTION  masuk di main");
            intent = new Intent(this, MenuMain.class);
        }


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O){
            NotificationManager mNotifyManager;
            Notification.Builder mBuilder;
            String channelId = "1212";
            String channelName = "Channel Name";
            int importance = NotificationManager.IMPORTANCE_HIGH;
        }else {
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setContentTitle(title)
                    .setContentText(message)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setContentIntent(pendingIntent);
            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.notify(0, notificationBuilder.build());
        }
        */

       //--------------------

//        intent = new Intent(this, MenuMain.class);
//        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
//        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);
//        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                .setContentTitle(a)
//                .setContentText(b)
//                .setSmallIcon(R.mipmap.ic_launcher)
//                .setAutoCancel(true)
//                .setContentIntent(pendingIntent);
//        NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//        notificationManager.notify(0, notificationBuilder.build());
    }



}
