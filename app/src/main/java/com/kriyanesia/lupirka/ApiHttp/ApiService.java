package com.kriyanesia.lupirka.ApiHttp;

import com.kriyanesia.lupirka.Data.Ads.ResponCancelAds;
import com.kriyanesia.lupirka.Data.Ads.ValueAdsLupirka;
import com.kriyanesia.lupirka.Data.Ads.ValueAllAds;
import com.kriyanesia.lupirka.Data.Ads.ValueCategoriAds;
import com.kriyanesia.lupirka.Data.Ads.ValueDetailAds;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueAktivitas;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.Event.ValueEventBank;
import com.kriyanesia.lupirka.Data.Event.ValueEventCategory;
import com.kriyanesia.lupirka.Data.Event.ValueJointEventDibayarin;
import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.Data.Event.ValueScanBarcode;
import com.kriyanesia.lupirka.Data.Pembayaran.ValueGenerateVA;
import com.kriyanesia.lupirka.Data.Pembayaran.ValuePembayaran;
import com.kriyanesia.lupirka.Data.Ads.ValueResponRegisAds;
import com.kriyanesia.lupirka.Data.Daerah;
import com.kriyanesia.lupirka.Data.Dokter.ValueDokter;
import com.kriyanesia.lupirka.Data.EO.ValueEO;
import com.kriyanesia.lupirka.Data.Event.ValueDetailEvent;
import com.kriyanesia.lupirka.Data.Event.ValueDetailKegiatan;
import com.kriyanesia.lupirka.Data.Event.ValueEvent;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.Data.Farmasi.ValueFarmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueJabatan;
import com.kriyanesia.lupirka.Data.Hospital.ValueHospital;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiCabang;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiWilayah;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;
import com.kriyanesia.lupirka.Data.Profesi.ValueProfesi;
import com.kriyanesia.lupirka.Data.Profile.ValueProfileUser;
import com.kriyanesia.lupirka.Data.Profile.ValueUpdatePhoto;
import com.kriyanesia.lupirka.Data.Rundown.ValueRundown;
import com.kriyanesia.lupirka.Data.Saldo.ValueCheckBalance;
import com.kriyanesia.lupirka.Data.SoalTest.ValueSoal;
import com.kriyanesia.lupirka.Data.Spesialis.ValueSpesialis;
import com.kriyanesia.lupirka.Data.Universitas.ValueUniversitas;
import com.kriyanesia.lupirka.Data.ValueContactUs;
import com.kriyanesia.lupirka.Data.ValueKota;
import com.kriyanesia.lupirka.Data.ValueTest;
import com.kriyanesia.lupirka.Data.ValueUser;


import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface ApiService {

    @Headers({
            "Content-Type: application/json"
    })
    @POST("resend_token")
    Call<OtpVerifikasi> resendToken(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("login")
    Call<OtpVerifikasi> getVerifikasiLogin(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("vtoken")
    Call<ValueUser> getUser(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("registrasi")
    Call<OtpVerifikasi> getVerifikasiRegistrasi(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("update_profile")
    Call<OtpVerifikasi> updateProfile(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })

    @POST("ads_lupirkaview")
    Call<ValueAdsLupirka> getAdsLupirka(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("get_listprofesi")
    Call<ValueProfesi> getAllProfesi(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listspesialis")
    Call<ValueSpesialis> getAllSpesialis(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listspesialis")
    Call<ValueTest> testHTTPS(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listfarmasi")
    Call<ValueFarmasi> getAllFarmasi(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listeo")
    Call<ValueEO> getAllEO(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listhospital")
    Call<ValueHospital> getAllHospital(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_area2")
    Call<ValueIdiWilayah> getAllIdiWilayah(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_area2")
    Call<ValueIdiCabang> getAllIdiCabang(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_detailprofile")
    Call<ValueProfileUser> getProfileUser(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_data_farmasi")
    Call<ValueJabatan> getAllJabatanFarmasi(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listuniversitas")
    Call<ValueUniversitas> getAllUniversitas(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_eventcalendar")
    Call<ValueEvent> getEventCalendar(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_listevent")
    Call<ValueEventList> getEventList(@Body String body, @Header("Authorization") String authorization);

    @Headers({
        "Content-Type: application/json"
    })

    @POST("get_listcategoryevent")
    Call<ValueEventCategory> getCategoriEvent(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("paket_event")
    Call<ValueDetailKegiatan> getPaketEvent(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("get_tenant")
    Call<ValueListBooth> getListTenant(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("tenant_registration")
    Call<ValueListBooth> daftarTableTenant(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("join_event")
    Call<ValueJointEventDibayarin> joinEventBayarOrang(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("search_event")
    Call<ValueEventList> getEventSearch(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_city")
    Call<ValueKota> getKotaSearch(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_eventdetail")
    Call<ValueDetailEvent> getEventDetail(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_bank_event")
    Call<ValueEventBank> getEventBank(@Body String body, @Header("Authorization") String authorization);

    @Multipart
    @POST("image")
    Call<ValueDetailAktivitas> konfirmasiBuktiTransfer(@Part MultipartBody.Part image,
                                                      @Part MultipartBody.Part body,
                                                      @Header("Authorization") String authorization);


    @Multipart
    @POST("image/profile")
    Call<ValueUpdatePhoto> updateProfileImage(@Part MultipartBody.Part images,
                                              @Part MultipartBody.Part body,
                                              @Header("Authorization") String authorization);



    @Headers({
            "Content-Type: application/json"
    })

    @POST("ads_view")
    Call<ValueAllAds> getAllAds(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })

    @POST("ads_category")
    Call<ValueCategoriAds> getCategoriAds(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("ads_view_detail")
    Call<ValueDetailAds> getDetailAds(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("ads_cancel")
    Call<ResponCancelAds> cancleAds(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("ads_detailpayment")
    Call<ValuePembayaran> getDetailPayment(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("metode_bayar")
    Call<ValuePembayaran> getMetodeBayar(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("generate_payment")
    Call<ValueGenerateVA> createVA(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("search_user")
    Call<ValueDokter> getDokter(@Body String body, @Header("Authorization") String authorization);



    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_area")
    Call<Daerah> getDaerah(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @GET("get_dtm")
    Call<String> getTimeServer(@Header("Authorization") String authorization);



    @Multipart
    @POST("ads_registration")
    Call<ValueResponRegisAds> registerAds(@Part MultipartBody.Part image,
                                          @Part MultipartBody.Part body,
                                          @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("check_balance")
    Call<ValueCheckBalance> checkBalance(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("topup")
    Call<ValueGenerateVA> topUpSaldo(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_contact_bank")
    Call<ValueContactUs> getContactUs(@Body String body, @Header("Authorization") String authorization);


    @Headers({
            "Content-Type: application/json"
    })
    @POST("pending_topup")
    Call<ValueGenerateVA> topupHistory(@Body String body, @Header("Authorization") String authorization);


    //AKTIVITY
    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_activity")
    Call<ValueAktivitas> getAllAktivity(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("detail_activity")
    Call<ValueDetailAktivitas> getDetailActivity(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("authorize")
    Call<ValueDetailAktivitas> approvalEvent(@Body String body, @Header("Authorization") String authorization);



    @Headers({
            "Content-Type: application/json"
    })
    @POST("scan_barcode")
    Call<ValueScanBarcode> scanBarcode(@Body String body, @Header("Authorization") String authorization);

    //by Morten


    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_rundown")
    Call<ValueRundown> getRundown(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_soal_test")
    Call<ValueSoal> getSoalTes(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("result_test")
    Call<OtpVerifikasi> getResultTest(@Body String body, @Header("Authorization") String authorization);

    @Headers({
            "Content-Type: application/json"
    })
    @POST("get_all_result_test")
    Call<String> getAllResultTest(@Body String body, @Header("Authorization") String authorization);




}
