package com.kriyanesia.lupirka.Util;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;

import java.text.NumberFormat;
import java.util.Locale;

public class Function {
    private static Context context;
    private static ProgressDialog progressDialog;
    public Function(Context context) {
        this.context = context;
        progressDialog = new ProgressDialog(context);
    }

    public static void showProgress(Context mContext, String message) {

        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
    }
    public static void dismisProgress() {

        progressDialog.cancel();
    }
    public static String ConvertRupiah(int saldo){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String h=formatRupiah.format(Long.valueOf(saldo));
        String finalHarga= "Rp. "+h.substring(2)+",-";
        return finalHarga;
    }


}
