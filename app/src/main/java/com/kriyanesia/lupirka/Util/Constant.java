package com.kriyanesia.lupirka.Util;

public class Constant {
    public static String HTTP = "http://";
    //local
//    public static String IP = "172.168.100.58:8211";
    //public
    public static String IP = "117.54.3.201:8211";

    public static String SERVICE_URI = HTTP + IP + "/";
    public static String APPS = "lupirka/api/";
    public static String APPS2 = "lupirka/api2/";
    public static String APPS3 = "lupirka/api3/";
    public static String FOLDER_IMAGE= "android/";
    public static String VERSI = "v1/";
    public static String URL_SERVICE = SERVICE_URI +APPS+VERSI;
    public static String URL_SERVICE_TIME = SERVICE_URI +APPS;
    public static String URL_HOME = SERVICE_URI +APPS;
    public static String URL_SERVICE_VER2 = SERVICE_URI +APPS2+VERSI;
    public static String URL_SERVICE_VER3 = SERVICE_URI +APPS3+FOLDER_IMAGE;
    public static String URL_SERVICE_UPDATE_PROFILE = SERVICE_URI +APPS3;

    public static String URL_TESTING = "http://eagleschool.dx.am/ANDROID/";


    public static String TEXT_SHARE = "Sudah merasakan berbagai kemudahan dengan aplikasi LUPIRKA ?" +
            "Ajak orang-orang terdekat anda untuk ikut menikmati mudahnya mendapatkan";
    public static String TEXT_INTRO_CONTAC_US ="Jika anda memiliki kendala atau ada pertanyaan seputar event" +
            " silahkan hubungi kami";

}
