package com.kriyanesia.lupirka.Menu;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.ValueResponRegisAds;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.Menu.KegiatanEvent.ActivityEvent;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.CameraUtils;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import rebus.bottomdialog.BottomDialog;
import rebus.bottomdialog.Item;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.Context.CLIPBOARD_SERVICE;
import static android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPembayaranAkhir extends AppCompatActivity {

    DatabaseHelper db;
    private String from;
    private String idPembayaran;
    TextView textTilte, tv_hour, tv_minute, tv_second,valueTitle,jamNow,valueVA,valueTotalPembayaran,titikDua1,titikDua2;
    SwipeRefreshLayout swipeRefresh;
    RelativeLayout layoutContent;
    private static String imageStoragePath;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1211;
    public static final String GALLERY_DIRECTORY_NAME = "LUPIRKA";
    public static final String IMAGE_EXTENSION = "jpg";
    Button btnKirimBukti;
    String batasWaktu="",totalPembayaran="",noRekBank="",title="";
    ImageButton btnBack;
    Button btnKonfirmasi;
    private BottomDialog dialogCamera;
    Intent intent;
    Button btnSalinNoRek;
    ImageView imagePreview;
    String namaBank,namaAkunBank;
    ScrollView scrollview;
    Context context = MenuPembayaranAkhir.this;
    ProgressDialog progressDialog;
    TextView textVirtualAccount,textKeterangan,txtDetailPaket,txtNamaAkunBank;
    private CharSequence[] detail_paket;
    int isCamera=99;
    private CountDownTimer mCountDownTimer;
    private ValueDetailAktivitas.DetailAktivitasTemp detailAktivitas;
    private String role;
    private String idAktivitas;
    public static boolean recentlyPay;
    public MenuPembayaranAkhir() {
        // Required empty public constructor
    }


    private int RESULT_CODE_BANNER_BUKTI=1212;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_menu_pembayaran_ads_akhir);
        scrollview = ((ScrollView) findViewById(R.id.scrollViewPembayaranAkhir));
        intent = getIntent();
        Bundle extras = intent.getExtras();
        idAktivitas=extras.getString("idAktivitas");
        from =extras.getString("from");
        title =extras.getString("title");
        namaAkunBank =extras.getString("namaAkunBank");
        totalPembayaran =extras.getString("totalPembayaran");
        noRekBank =extras.getString("noRekBank");
        namaBank = extras.getString("namaBank");
        batasWaktu =intent.getStringExtra("expTime");
        idPembayaran =intent.getStringExtra("idPembayaran");


        swipeRefresh = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
        textTilte = (TextView)findViewById(R.id.textTilte);
        tv_hour = (TextView)findViewById(R.id.textJam);
        tv_minute = (TextView)findViewById(R.id.textMenit);
        tv_second = (TextView)findViewById(R.id.textDetik);
        titikDua1 = (TextView)findViewById(R.id.titikDua1);
        titikDua2 = (TextView)findViewById(R.id.titikDua2);
        valueTitle = (TextView)findViewById(R.id.valueTitleAds);
        btnKirimBukti = (Button) findViewById(R.id.btnKirimBukti);
        textVirtualAccount = (TextView)findViewById(R.id.textVirtualAccount);
        textKeterangan = (TextView)findViewById(R.id.textKeterangan);
        txtNamaAkunBank = findViewById(R.id.valueNamaAkun);
        jamNow = (TextView)findViewById(R.id.jamNow);
        valueVA = (TextView)findViewById(R.id.valueVA);
        layoutContent = (RelativeLayout) findViewById(R.id.layoutContent);
        valueTotalPembayaran = (TextView)findViewById(R.id.valueTotalPembayaran);
        btnSalinNoRek = (Button) findViewById(R.id.btnSalinNoRek);
        btnKonfirmasi=(Button) findViewById(R.id.btnKonfirmasi);
        btnBack=(ImageButton) findViewById(R.id.btnBack);
        imagePreview = (ImageView) findViewById(R.id.imagePreview);
        txtDetailPaket = findViewById(R.id.txtDetailPaket);
        if(from.equalsIgnoreCase("MenuPembayaranEventUnpaid")) {
            detail_paket = extras.getCharSequenceArray("aktivitasCost");
            txtDetailPaket.setVisibility(View.VISIBLE);
        }


        Object clipboardService = MenuPembayaranAkhir.this.getSystemService(CLIPBOARD_SERVICE);
        final ClipboardManager clipboardManager = (ClipboardManager)clipboardService;

        if (imagePreview.getDrawable() == null){
            btnKirimBukti.setVisibility(View.GONE);
        }else {
            btnKirimBukti.setVisibility(View.VISIBLE);
        }
        textKeterangan.setText("Total Pembayaran diatas otomatis ditambahkan dengan kode unik LUPIRKA, " +
                "harap transfer dengan nominal yang sama");

        textVirtualAccount.setText("Pembayaran Ke EO Via Bank "+namaBank);
        btnSalinNoRek.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Create a new ClipData.
                ClipData clipData = ClipData.newPlainText("Source Text", noRekBank);
                // Set it as primary clip data to copy text to system clipboard.
                clipboardManager.setPrimaryClip(clipData);
                // Popup a snackbar.
//                Snackbar snackbar = Snackbar.make(this, "VA has been copied to system clipboard.", Snackbar.LENGTH_LONG);
//                snackbar.show();
            }
        });

        if (from.equalsIgnoreCase("topup")){
            textTilte.setText("Konfirmasi Pembayaran Topup");
        }else if (from.equalsIgnoreCase("ads")){
            textTilte.setText("Konfirmasi Pembayaran Ads");
        }else if (from.equalsIgnoreCase("MenuPembayaranEvent")){
            textTilte.setText("Konfirmasi Pembayaran Event");
        }
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //
                Intent i = new Intent(MenuPembayaranAkhir.this,MenuMain.class);
                i.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                MenuPembayaranAkhir.this.finish();
            }
        });
        txtDetailPaket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog_detail_paket();
            }
        });
        clearView();
        loadData();
        loadTimeServer();
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clearView();
                loadData();
                swipeRefresh.setRefreshing(false);
            }
        });
        btnKonfirmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialogCamera = new BottomDialog(MenuPembayaranAkhir.this);
                dialogCamera.title("Pilih File");
                dialogCamera.canceledOnTouchOutside(true);
                dialogCamera.cancelable(true);
                dialogCamera.inflateMenu(R.menu.menu_select_img);
                dialogCamera.setOnItemSelectedListener(new BottomDialog.OnItemSelectedListener() {
                    @Override
                    public boolean onItemSelected(int id) {
                        switch (id) {
                            case R.id.action_camera_bukti:
                                if (CameraUtils.checkPermissions(getApplicationContext())) {
                                    captureImage();
                                } else {
                                    requestPermission("camera");
                                }
                                if (imagePreview.getDrawable() == null){
                                    btnKirimBukti.setVisibility(View.GONE);
                                }else {
                                    btnKirimBukti.setVisibility(View.VISIBLE);
                                }
                                return true;
                            case R.id.action_galerry_bukti:
                                if (CameraUtils.checkPermissions(getApplicationContext())) {
                                    Intent intent = new Intent();
                                    intent.setType("image/*");
                                    intent.setAction(Intent.ACTION_GET_CONTENT);
                                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER_BUKTI);

                                } else {
                                    requestPermission("gallery");
                                }

                                return true;

                            default:
                                return false;
                        }
                    }
                });
                dialogCamera.show();
            }
        });

        btnKirimBukti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                progressDialog = new ProgressDialog(MenuPembayaranAkhir.this);
                progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progressDialog.setCancelable(false);
                progressDialog.setMessage("Please Wait...");
                progressDialog.show();
                sendRegistrasiEvent();
            }
        });
    }


    private void dialog_detail_paket(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Detail Paket")
                .setItems(detail_paket, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        // The 'which' argument contains the index position
                        // of the selected item
                    }
                });
        builder.show();


    }

    private void sendRegistrasiEvent(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line


                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_VER3)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        ApiService apiInterface = retrofit.create(ApiService.class);
        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(MenuPembayaranAkhir.this,"nohp");
        role=sessionUser.getRole(MenuPembayaranAkhir.this,"role");
        JSONObject paramObject = new JSONObject();
        System.out.println("asd asd asd "+idAktivitas);
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("id",idPembayaran);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        File sourceFile = null;
        if (isCamera==0){
            String path="";
            String value =getPathFromURI(MenuPembayaranAkhir.this,uriImage);
            if (value!=null){
                path=value;
                sourceFile = new File(path);
            }
        }else if (isCamera==1) {

            sourceFile = new File(imageStoragePath);
        }
        System.out.println("asd asd "+paramObject.toString());
        System.out.println("file "+sourceFile.getAbsoluteFile());
        RequestBody reqBody = RequestBody.create(MediaType.parse("multipart/form-file"), sourceFile);
        MultipartBody.Part partImage = MultipartBody.Part.createFormData("images",sourceFile.getName(),reqBody);
        final MultipartBody.Part body = MultipartBody.Part.createFormData("body", paramObject.toString());
        final Call<ValueDetailAktivitas> data = apiInterface.konfirmasiBuktiTransfer(partImage,body,"saxasdas");
        System.out.println("asd body "+body.toString());
        data.enqueue(new Callback<ValueDetailAktivitas>() {
            @Override
            public void onResponse(Call<ValueDetailAktivitas> call, Response<ValueDetailAktivitas> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    detailAktivitas = new ValueDetailAktivitas.DetailAktivitasTemp();
                    ValueDetailAktivitas.DetailAktivitas datas = response.body().getData();
                            String[] data = datas.getPembayaran().split("-");
                            if (role.equalsIgnoreCase("2")) {
                                if (data != null) {
                                    if (data[1].equalsIgnoreCase("Farmasi")) {
                                        detailAktivitas.setFarmasi(datas.getFarmasi());
                                    } else {
                                        detailAktivitas.setFarmasi("-");
                                    }
                                }
                            } else {
                                detailAktivitas.setFarmasi("-");
                            }
                            detailAktivitas.setHarga(datas.getHarga());
                            detailAktivitas.setLokasi(datas.getLokasi());
                            detailAktivitas.setNama(datas.getNama());
                            detailAktivitas.setNomor_hp(datas.getNomor_hp());
                            detailAktivitas.setStatus(datas.getStatus());
                            detailAktivitas.setPembayaran(datas.getPembayaran());
                            detailAktivitas.setStatus_event(datas.getStatus_event());
                            detailAktivitas.setStatus_message(datas.getStatus_message());

                            List<ValueDetailAktivitas.DetailCostTemp> dataCost = new ArrayList<>();
                            List<ValueDetailAktivitas.DetailKegiatanTemp> dataAktivitas = new ArrayList<>();
                            for (int i = 0; i < datas.getDetail_cost().size(); i++) {
                                ValueDetailAktivitas.DetailCostTemp cost = new ValueDetailAktivitas.DetailCostTemp();
                                cost.setBiaya(datas.getDetail_cost().get(i).getBiaya());
                                cost.setPaket(datas.getDetail_cost().get(i).getPaket());
                                dataCost.add(cost);
                            }
                            if (datas.getDetail_kegiatan() != null) {
                                for (int i = 0; i < datas.getDetail_kegiatan().size(); i++) {
                                    ValueDetailAktivitas.DetailKegiatanTemp dataAktivitasTemp = new ValueDetailAktivitas.DetailKegiatanTemp();
                                    dataAktivitasTemp.setDetail(datas.getDetail_kegiatan().get(i).getDetail());
                                    dataAktivitasTemp.setStarddate(datas.getDetail_kegiatan().get(i).getStarddate());
                                    dataAktivitasTemp.setEnddate(datas.getDetail_kegiatan().get(i).getEnddate());
                                    dataAktivitasTemp.setKegiatan(datas.getDetail_kegiatan().get(i).getKegiatan());
                                    dataAktivitas.add(dataAktivitasTemp);

                                }
                            }

                            detailAktivitas.setStatus_event(datas.getStatus_event());
                            detailAktivitas.setId_event(datas.getId_event());
                            detailAktivitas.setTitle(datas.getTitle());


                            Intent intent = new Intent(MenuPembayaranAkhir.this, ActivityDetailContent.class);
                            intent.putExtra("menu", "Detail Aktivitas");
                            intent.putExtra("idAktivitas", idAktivitas);
                            System.out.println("asd asd detail "+idAktivitas);
                            intent.putExtra("aktivitasFarmasi", detailAktivitas.getFarmasi());
                            intent.putExtra("aktivitasHarga", detailAktivitas.getHarga());
                            intent.putExtra("aktivitasLokasi", detailAktivitas.getLokasi());
                            intent.putExtra("aktivitasNama", detailAktivitas.getNama());
                            intent.putExtra("aktivitasNoHp", detailAktivitas.getNomor_hp());
                            intent.putExtra("aktivitasStatus", detailAktivitas.getStatus());
                            intent.putExtra("aktivitasOngoing", detailAktivitas.getStatus_event());
                            intent.putExtra("aktivitasPembayaran", detailAktivitas.getPembayaran());
                            intent.putExtra("aktivitasTitle", detailAktivitas.getTitle());
                            intent.putExtra("aktivitasId", detailAktivitas.getId_event());
                            intent.putExtra("aktivitasCost", (Serializable) dataCost);
                            intent.putExtra("aktivitasKegiatan", (Serializable) dataAktivitas);
                            intent.putExtra("aktivitasMessage",detailAktivitas.getStatus_message());
                            intent.putExtra("recentlyPay",true);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                            progressDialog.dismiss();
                            startActivity(intent);
                            finish();

                }else {
                    showAllert("Informasi", response.body().getMessage());
                }
            }

            @Override
            public void onFailure(Call<ValueDetailAktivitas> call, Throwable t) {
                showAllert("Informasi",t.getMessage());

                progressDialog.dismiss();

                System.out.println("Informasi "+t.getMessage());
            }

        });
    }
    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    public static boolean checkPermissions(Context context) {
        String[] permissions = {Manifest.permission.CAMERA,
                Manifest.permission.INTERNET,

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions((Activity) context,
                permissions,
                1212);
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ;
    }
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            checkPermissions(context);
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPathFromURI(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }


    private void captureImage() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile();
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getApplicationContext(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1211:
                    isCamera=1;

                    previewCapturedImage(data);
                    break;
                case 1212:

                    isCamera=0;
                    getFileImage(data);
                    break;

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    private void previewCapturedImage(Intent data) {
        try {

            CameraUtils.refreshGallery(getApplicationContext(), imageStoragePath);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 4;

            Bitmap bitmap = CameraUtils.optimizeBitmap(options.inSampleSize, imageStoragePath);

            imagePreview.setImageBitmap(bitmap);
            if (imagePreview.getDrawable() == null){
                btnKirimBukti.setVisibility(View.GONE);
            }else {
                btnKirimBukti.setVisibility(View.VISIBLE);
                scrollview.post(new Runnable() {
                    @Override
                    public void run() {
                        scrollview.fullScroll(ScrollView.FOCUS_DOWN);
                    }
                });
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
    public String saveImage(Bitmap myBitmap) {
        File wallpaperDirectory =
        new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM),
                "LUPIRKA");
        // have the object build the directory structure, if needed.
        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }

        try {
            File f = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            f.createNewFile();
            FileOutputStream fo = new FileOutputStream(f);
            MediaScannerConnection.scanFile(this,
                    new String[]{f.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath());

            return f.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        return "";
    }

    String imageAds;
    Uri uriImage;
    private void getFileImage(Intent data) {
        uriImage = data.getData();
        imageAds = uriImage.toString();
        System.out.println("PATH IMAGE " + imageAds);
        int filesize = 0;

        InputStream inputStream = null;
        try {
            inputStream = getContentResolver().openInputStream(uriImage);
            filesize = inputStream.available();
        } catch (FileNotFoundException e) {
            return;
        } catch (IOException e) {
            return;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {/*blank*/}
        }
        if (filesize != 0) {
            int fileInKB = filesize / 1024;
            if (fileInKB > 500) {
                showAllert("Informasi", "File gambar ads tidak boleh lebih dari 500KB");

            } else {
                imagePreview.setImageURI(uriImage);
                if (imagePreview.getDrawable() == null){
                    btnKirimBukti.setVisibility(View.GONE);
                }else {
                    btnKirimBukti.setVisibility(View.VISIBLE);
                    scrollview.post(new Runnable() {
                        @Override
                        public void run() {
                            scrollview.fullScroll(ScrollView.FOCUS_DOWN);
                        }
                    });
                }

            }
        }
    }


    private static final int REQUEST_CAMERA_PERMISSION = 786;
    String tipe;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            if (tipe.equalsIgnoreCase("camera")){
                captureImage();
            }else if (tipe.equalsIgnoreCase("gallery")){
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER_BUKTI);
            }
        }
    }

//    @RequiresApi(api = Build.VERSION_CODES.M)
    private void requestPermission(String from) {
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        ActivityCompat.requestPermissions(MenuPembayaranAkhir.this, PERMISSIONS, REQUEST_CAMERA_PERMISSION);

        tipe=from;
    }
    private void clearView(){
        layoutContent.setVisibility(View.GONE);
    }
    private void loadData(){
            txtNamaAkunBank.setText(namaAkunBank);
            valueTitle.setText(title);
            valueVA.setText(noRekBank);
            valueTotalPembayaran.setText(ConvertRupiah(Integer.valueOf(totalPembayaran)));
            layoutContent.setVisibility(View.VISIBLE);


    }
    public String ConvertRupiah(int saldo){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String h=formatRupiah.format(Long.valueOf(saldo));
        String finalHarga= "Rp. "+h.substring(2)+",-";
        return finalHarga;
    }

    private void loadTimeServer(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_TIME)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        Call<String> data = apiInterface.getTimeServer("saxasdas");

        data.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    String str_date=response.body();

                    SimpleDateFormat formatBatas = new SimpleDateFormat("dd MMM yyyy | HH:mm");
                    SimpleDateFormat formatDateEnd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    SimpleDateFormat formatDateStart = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");


                    Date dateNow = (Date)formatDateStart.parse(str_date);
                    Calendar calNow=Calendar.getInstance();
                    calNow.setTime(dateNow);
                    Date endDate = formatDateEnd.parse(batasWaktu);
                    jamNow.setText(formatBatas.format(endDate));
                    System.out.println("VALUE JAM now "+str_date +" batas "+batasWaktu+" xx "+formatBatas.format(dateNow));
                    startCountDown(endDate,dateNow);
                } catch (ParseException e)
                {
                    System.out.println("Exception :"+e);
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

    long startTime;
    private void startCountDown(Date endDate , Date now)
    {
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//        formatter.setLenient(false);

//        Toast.makeText(MenuPembayaranAkhir.this, ""+now.toString(), Toast.LENGTH_SHORT).show();

//        String endTime = "06.06.2018, 12:05:36";
        long milliseconds=0;



        milliseconds = endDate.getTime();
        startTime = now.getTime();


//        startTime = System.currentTimeMillis();


        mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {
                startTime=startTime-1;
                Long serverUptimeSeconds =
                        (millisUntilFinished - startTime) / 1000;

//                System.out.println("NOWWWW "+startTime +" END "+millisUntilFinished+ " HASIL "+serverUptimeSeconds);

                String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
                //txtViewDays.setText(daysLeft);
                String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);

                if (Integer.valueOf(hoursLeft)<1){
                    tv_hour.setText("Waktu Habis");
                    tv_hour.setTextSize(30f);
                    titikDua1.setVisibility(View.GONE);
                    titikDua2.setVisibility(View.GONE);
                    tv_minute.setVisibility(View.GONE);
                    tv_second.setVisibility(View.GONE);
                }else {
                    titikDua1.setVisibility(View.VISIBLE);
                    titikDua2.setVisibility(View.VISIBLE);
                    tv_minute.setVisibility(View.VISIBLE);
                    tv_second.setVisibility(View.VISIBLE);
                    if (Integer.valueOf(hoursLeft)<10 && Integer.valueOf(hoursLeft)>0) {
                        if (hoursLeft.startsWith("0")) {
                            tv_hour.setText(hoursLeft);
                        } else {
                            tv_hour.setText("0" + hoursLeft);
                        }
                    }else {
                        tv_hour.setText(hoursLeft);
                    }

                    String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
                    if (Integer.valueOf(minutesLeft)<10) {
                        if (minutesLeft.startsWith("0")) {
                            tv_minute.setText(hoursLeft);
                        } else {
                            tv_minute.setText("0" + minutesLeft);
                        }
                    }else {
                        tv_minute.setText(minutesLeft);
                    }

                    String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
                    if (Integer.valueOf(secondsLeft)<10) {
                        if (secondsLeft.startsWith("0")) {
                            tv_second.setText(secondsLeft);
                        } else {
                            tv_second.setText("0" + secondsLeft);
                        }
                    }else {
                        tv_second.setText(secondsLeft);
                    }
                }




            }

            @Override
            public void onFinish() {

            }
        };
        mCountDownTimer.start();


    }

    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(MenuPembayaranAkhir.this);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mCountDownTimer!=null) {
            mCountDownTimer.cancel();
        }
    }

    private void loadAktivitasDetail(){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        SessionUser sessionUser=new SessionUser();
        String noHp=sessionUser.getNoHp(MenuPembayaranAkhir.this,"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("id_activity",idAktivitas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA json list Aktivitas : "+paramObject.toString());
        final Call<ValueDetailAktivitas> data = apiInterface.getDetailActivity(paramObject.toString(),"saxasdas");
        data.enqueue(new Callback<ValueDetailAktivitas>() {
            @Override
            public void onResponse(Call<ValueDetailAktivitas> call, Response<ValueDetailAktivitas> response) {
                if (response.body() !=null) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        detailAktivitas = new ValueDetailAktivitas.DetailAktivitasTemp();
                        ValueDetailAktivitas.DetailAktivitas datas = response.body().getData();

                        if (datas != null) {
                            if(datas.getStatus_message().equalsIgnoreCase("UNPAID")) {//cek apkah sudah bayar atau blm
                                Intent i = new Intent(MenuPembayaranAkhir.this, MenuPembayaranAkhir.class);
                                i.putExtra("from", "MenuPembayaranEventUnpaid");
                                i.putExtra("title", "Status pendaftaran");
                                i.putExtra("namaAkunBank", datas.getBank_data().get(0).getNama_akun());
                                i.putExtra("totalPembayaran", datas.getHarga());
                                i.putExtra("noRekBank", datas.getBank_data().get(0).getNo_rekening());
                                i.putExtra("namaBank", datas.getBank_data().get(0).getNama_bank());
                                i.putExtra("expTime", datas.getBank_data().get(0).getExp_time());
                                i.putExtra("idPembayaran", datas.getBank_data().get(0).getPayment_id());
                                CharSequence[] c = new String[datas.getDetail_cost().size()];
                                for (int x = 0; x < datas.getDetail_cost().size(); x++) {
                                    c[x] = datas.getDetail_cost().get(x).getPaket() + " " + datas.getDetail_cost().get(x).getBiaya();
                                }
                                i.putExtra("aktivitasCost", c);
                                startActivity(i);
                            }else if(response.body().getData().getStatus_message().equalsIgnoreCase("PAID") &&response.body().getData().getStatus_event().equalsIgnoreCase("ongoing")){
                                Intent i = new Intent(MenuPembayaranAkhir.this,ActivityEvent.class);
                                i.putExtra("id_event",response.body().getData().getId_event());
                                i.putExtra("nama_event",response.body().getData().getTitle());
                                startActivity(i);
                            }else{
                                String[] data = datas.getPembayaran().split("-");
                                if (role.equalsIgnoreCase("2")) {
                                    if (data != null) {
                                        if (data[1].equalsIgnoreCase("Farmasi")) {
                                            detailAktivitas.setFarmasi(datas.getFarmasi());
                                        } else {
                                            detailAktivitas.setFarmasi("-");
                                        }
                                    }
                                } else {
                                    detailAktivitas.setFarmasi("-");
                                }
//                    detailAktivitas.setFarmasi(datas.getFarmasi());

                                detailAktivitas.setHarga(datas.getHarga());
                                detailAktivitas.setLokasi(datas.getLokasi());
                                detailAktivitas.setNama(datas.getNama());
                                detailAktivitas.setNomor_hp(datas.getNomor_hp());
                                detailAktivitas.setStatus(datas.getStatus());
                                detailAktivitas.setPembayaran(datas.getPembayaran());
                                detailAktivitas.setStatus_event(datas.getStatus_event());
                                detailAktivitas.setStatus_message(datas.getStatus_message());

                                List<ValueDetailAktivitas.DetailCostTemp> dataCost = new ArrayList<>();
                                List<ValueDetailAktivitas.DetailKegiatanTemp> dataAktivitas = new ArrayList<>();
                                for (int i = 0; i < datas.getDetail_cost().size(); i++) {
                                    ValueDetailAktivitas.DetailCostTemp cost = new ValueDetailAktivitas.DetailCostTemp();
                                    cost.setBiaya(datas.getDetail_cost().get(i).getBiaya());
                                    cost.setPaket(datas.getDetail_cost().get(i).getPaket());
                                    dataCost.add(cost);
                                }
                                if (datas.getDetail_kegiatan() != null) {
                                    for (int i = 0; i < datas.getDetail_kegiatan().size(); i++) {
                                        ValueDetailAktivitas.DetailKegiatanTemp dataAktivitasTemp = new ValueDetailAktivitas.DetailKegiatanTemp();
                                        dataAktivitasTemp.setDetail(datas.getDetail_kegiatan().get(i).getDetail());
                                        dataAktivitasTemp.setStarddate(datas.getDetail_kegiatan().get(i).getStarddate());
                                        dataAktivitasTemp.setEnddate(datas.getDetail_kegiatan().get(i).getEnddate());
                                        dataAktivitasTemp.setKegiatan(datas.getDetail_kegiatan().get(i).getKegiatan());
                                        dataAktivitas.add(dataAktivitasTemp);

                                    }
                                }

                                detailAktivitas.setStatus_event(datas.getStatus_event());
                                detailAktivitas.setId_event(datas.getId_event());
                                detailAktivitas.setTitle(datas.getTitle());


                                Intent intent = new Intent(MenuPembayaranAkhir.this, ActivityDetailContent.class);
                                intent.putExtra("menu", "Detail Aktivitas");
                                intent.putExtra("idAktivitas", idAktivitas);
                                System.out.println("asd asd detail "+idAktivitas);
//                            Toast.makeText(getActivity(), "1 "+idAktivitas, Toast.LENGTH_SHORT).show();
                                intent.putExtra("aktivitasFarmasi", detailAktivitas.getFarmasi());
                                intent.putExtra("aktivitasHarga", detailAktivitas.getHarga());
                                intent.putExtra("aktivitasLokasi", detailAktivitas.getLokasi());
                                intent.putExtra("aktivitasNama", detailAktivitas.getNama());
                                intent.putExtra("aktivitasNoHp", detailAktivitas.getNomor_hp());
                                intent.putExtra("aktivitasStatus", detailAktivitas.getStatus());
                                intent.putExtra("aktivitasOngoing", detailAktivitas.getStatus_event());
                                intent.putExtra("aktivitasPembayaran", detailAktivitas.getPembayaran());
                                intent.putExtra("aktivitasTitle", detailAktivitas.getTitle());
                                intent.putExtra("aktivitasId", detailAktivitas.getId_event());
                                intent.putExtra("aktivitasCost", (Serializable) dataCost);
                                intent.putExtra("aktivitasKegiatan", (Serializable) dataAktivitas);
                                intent.putExtra("aktivitasMessage",detailAktivitas.getStatus_message());

                            /*
                              intent.putExtra("menu", "Pembayaran Topup Lanjut");
                            intent.putExtra("from", "MenuPembayaranEvent");
                            intent.putExtra("title", "Pembayaran Event " + eventTitle);
                            intent.putExtra("totalPembayaran", totalPembayaran);
                            intent.putExtra("noRekBank", noRekBank);
                            intent.putExtra("namaBank", namaBank);
                            intent.putExtra("namaAkunBank", namaAkunBank);
                            intent.putExtra("expTime", expTime);
                            intent.putExtra("idPembayaran", idPembayaran);
                            startActivity(intent);
                             */
                                startActivity(intent);
                                System.out.println("morten value inten " + intent.getExtras().toString());


                            }
                        } else {
                            Toast.makeText(MenuPembayaranAkhir.this, "ada kesalahan respon server", Toast.LENGTH_SHORT).show();
                        }

                    } else {

                    }
                }

            }

            @Override
            public void onFailure(Call<ValueDetailAktivitas> call, Throwable t) {
                System.out.println("error "+call+" | "+t.getMessage());
                Toast.makeText(MenuPembayaranAkhir.this, "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();
//                showAllert("Respon Server","Tidak dapat terhubung ke server");


            }
        });
    }


}
