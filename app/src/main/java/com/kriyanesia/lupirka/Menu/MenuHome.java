package com.kriyanesia.lupirka.Menu;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPost;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPostUpcoming;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Ads.ValueAdsLupirka;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.Data.Saldo.ValueCheckBalance;
import com.kriyanesia.lupirka.R;

import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import ss.com.bannerslider.banners.Banner;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuHome extends Fragment implements AdapterListPost.ItemClickListener,AdapterListPostUpcoming.ItemClickListener {
    SliderLayout bannerSlider;
    List<Banner> banners;
    AdapterListPost adapterListPost;
    List<Event> eventList ;
    SessionUser sessionUser;
    String noHp,namaPengguna;
    View v;
    TextView viewAllAds,kondisiHari,namaUser,linkSeeAllUpComing,linkSeeAllUpdate;
    FrameLayout layoutFragmentHome;
    SwipeRefreshLayout layoutRefresh;
    RecyclerView recyclerEvent,recycleHomeComingSoon;
    TextView saldoCash;
    Button btnTopupSaldo;
    RelativeLayout layoutHomeAll,slide;
    ImageView btnReload;
    String waktuServer="";
    public MenuHome() {
        // Required empty public constructor
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_menu_home, container, false);
        bannerSlider = (SliderLayout) v.findViewById(R.id.home_slider_banner);
        viewAllAds = (TextView) v.findViewById(R.id.linkViewAll);
        linkSeeAllUpdate = (TextView) v.findViewById(R.id.linkSeeAllUpdate);
        linkSeeAllUpComing = (TextView) v.findViewById(R.id.linkSeeAllUpComing);
        saldoCash = (TextView) v.findViewById(R.id.saldoCash);
        kondisiHari = (TextView) v.findViewById(R.id.kondisiHari);
        namaUser = (TextView) v.findViewById(R.id.namaUser);
        btnTopupSaldo = (Button) v.findViewById(R.id.btnTopupSaldo);
        btnReload= (ImageView) v.findViewById(R.id.reloadData);
        layoutHomeAll= (RelativeLayout) v.findViewById(R.id.layoutHomeAll);
        slide= (RelativeLayout) v.findViewById(R.id.slide);
        layoutFragmentHome = (FrameLayout) v.findViewById(R.id.layoutFragmentHome);
        sessionUser = new SessionUser();
        noHp=sessionUser.getNoHp(getActivity(),"nohp");
        namaPengguna=sessionUser.getName(getActivity(),"name");
        recyclerEvent = (RecyclerView) v.findViewById(R.id.recycleHomePost);
        recycleHomeComingSoon = (RecyclerView) v.findViewById(R.id.recycleHomeComingSoon);
        layoutRefresh = (SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshData();
            }
        });
        loadTimeServer();
//        loadBannerLupirka("BANNER");
//        loadEventList("0","5","updated");
//        loadEventList("0","5","upcoming");
//        checkBalance();
        Date currentTime = Calendar.getInstance().getTime();
        SimpleDateFormat sdf = new SimpleDateFormat("hh:mm aa");
        String waktu;
        if (waktuServer.isEmpty() || waktuServer.equalsIgnoreCase("")){
            waktu = sdf.format(currentTime);
        }else {
            waktu=waktuServer;
        }
        String am= waktu.substring(6,8);
        String jam= waktu.substring(0,2);
        int inJam;
        if (jam.startsWith("0")){
            jam=waktu.substring(1,2);

        }
//        Toast.makeText(getActivity(), jam, Toast.LENGTH_SHORT).show();
        inJam=Integer.valueOf(jam);
        if (am.equalsIgnoreCase("AM")){
            if (inJam<=9){
                kondisiHari.setText("Selamat Pagi,");
            }else {
                kondisiHari.setText("Selamat Siang,");
            }
        }else if (am.equalsIgnoreCase("PM")){
            if (inJam<3){
                kondisiHari.setText("Selamat Siang,");
            }else if (inJam>3 && inJam<6){
                kondisiHari.setText("Selamat Sore,");
            }else if (inJam>6 && inJam<=11){
                kondisiHari.setText("Selamat Malam,");
            }
        }
        System.out.println("DATA JAM "+inJam+" "+am +"  waktu "+waktu);
//        Toast.makeText(getActivity(), "DATA JAM "+inJam+" "+am +"  waktu "+waktu, Toast.LENGTH_SHORT).show();
        namaUser.setText(namaPengguna);

        btnReload.setVisibility(View.GONE);

        linkSeeAllUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.layoutFragmentHome, new MenuEvent().newInstance("updated"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack("allLupirkaAds");
                trans.commit();
                TabLayout tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
                TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(1));
                selectedTab.select();
            }
        });
        linkSeeAllUpComing.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.layoutFragmentHome, new MenuEvent().newInstance("upcoming"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack("allLupirkaAds");
                trans.commit();
                TabLayout tabLayout = (TabLayout) getActivity().findViewById(R.id.tabs);
                TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(1));
                selectedTab.select();
            }
        });
        viewAllAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.layoutFragmentHome, new MenuAllAdsLupirka().newInstance(""));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack("allLupirkaAds");
                trans.commit();
//                Toast.makeText(getActivity(), "ada", Toast.LENGTH_SHORT).show();
            }
        });
        btnTopupSaldo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(getActivity(),ActivityDetailContent.class);
                i.putExtra("menu","topup");
                startActivity(i);
            }
        });
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                btnReload.setVisibility(View.GONE);
                refreshData();
            }
        });

        return v;
    }
    private void loadTimeServer(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_TIME)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        Call<String> data = apiInterface.getTimeServer("saxasdas");

        data.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    String str_date=response.body();
                    Date date ;
                    SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm aa");
                    SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    date = (Date)formatDate.parse(str_date);
                    Calendar cal=Calendar.getInstance();
                    cal.setTime(date);
                    Date dateBatas = (Date)formatDate.parse(str_date);
                    Calendar calBatas=Calendar.getInstance();
                    calBatas.setTime(dateBatas);
                    waktuServer=formatTime.format(dateBatas);
                    System.out.println("Today is " +cal.toString() );
                } catch (ParseException e)
                {
                    System.out.println("Exception :"+e);
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

    @Override
    public void onResume() {
        refreshData();
        super.onResume();
    }

    private void refreshData(){

        layoutRefresh.setRefreshing(true);
        layoutHomeAll.setVisibility(View.VISIBLE);
        bannerSlider.setVisibility(View.VISIBLE);
        recyclerEvent.setVisibility(View.VISIBLE);
        loadBannerLupirka("BANNER");
        loadEventList("0","5","updated");
        loadEventList("0","5","upcoming");
        checkBalance();
        layoutRefresh.setRefreshing(false);
    }

    private void loadBannerLupirka(String tipe){
        slide.setVisibility(View.VISIBLE);
        bannerSlider.removeAllSliders();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("tipe",tipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());
        Call<ValueAdsLupirka> data = apiInterface.getAdsLupirka(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueAdsLupirka>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(Call<ValueAdsLupirka> call, Response<ValueAdsLupirka> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Ads> lupirkaAds;
                    final List<String> idAds = new ArrayList<>();

                    lupirkaAds= response.body().getData().getLupirkaAds().getContent();
                    if (lupirkaAds.size()>0){
                        for (int i=0;i<lupirkaAds.size();i++) {
                            idAds.add(lupirkaAds.get(i).getIdAds());

                            TextSliderView textSliderView = new TextSliderView(getActivity());
                            // initialize a SliderLayout
                            textSliderView
                                    .description(lupirkaAds.get(i).getNote())
                                    .image(lupirkaAds.get(i).getUrlImage())
                                    .setScaleType(BaseSliderView.ScaleType.Fit)
                                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                                        @Override
                                        public void onSliderClick(BaseSliderView slider) {
                                            String ads = (String) slider.getBundle().get("idAds");
                                            FragmentTransaction trans = getFragmentManager()
                                                    .beginTransaction();
                                            trans.replace(R.id.layoutFragmentHome, new MenuDetailOtherAds().newInstance(ads));
                                            trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                                            trans.addToBackStack("DetailAds");
                                            trans.commit();
    //                                        Toast.makeText(getActivity(),slider.getBundle().get("idAds") + "",Toast.LENGTH_SHORT).show();
                                        }
                                    });

                            //add your extra information
                            textSliderView.bundle(new Bundle());
                            textSliderView.getBundle()
                                    .putString("idAds",lupirkaAds.get(i).getIdAds());
                            bannerSlider.addSlider(textSliderView);

                        }
                        bannerSlider.setPresetTransformer(SliderLayout.Transformer.Accordion);
                        bannerSlider.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
                        bannerSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Visible);
                        bannerSlider.setCustomAnimation(new DescriptionAnimation());
                        bannerSlider.setDuration(4000);
                        bannerSlider.setCustomIndicator((PagerIndicator) v.findViewById(R.id.custom_indicator));
    //                    bannerSlider.addOnPageChangeListener(this);

                    }else {
                        slide.setVisibility(View.GONE);
                    }
                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueAdsLupirka> call, Throwable t) {
                layoutHomeAll.setVisibility(View.GONE);
                btnReload.setVisibility(View.VISIBLE);



            }
        });


    }

    private void loadEventList(String offset, final String limit, final String tipe){
        eventList = new ArrayList<>();
        eventList.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("offset",offset);
            paramObject.put("limit",limit);
            paramObject.put("tipe",tipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueEventList> profesiCall = apiInterface.getEventList(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueEventList>() {
            @Override
            public void onResponse(Call<ValueEventList> call, Response<ValueEventList> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Event> list;
                    list = response.body().getData().getList_event();
                    if (tipe.equalsIgnoreCase("updated")){
                        LinearLayoutManager  layoutManager = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                        layoutManager.setAutoMeasureEnabled(true);
                        recyclerEvent.setLayoutManager(layoutManager);
                        recyclerEvent.setNestedScrollingEnabled(true);
                        adapterListPost = new AdapterListPost(getActivity(), list);
                        recyclerEvent.setAdapter(adapterListPost);
                        adapterListPost.setmClickListener(MenuHome.this);
                    }else {
                        LinearLayoutManager  layoutManager2 = new LinearLayoutManager(getActivity(),LinearLayoutManager.HORIZONTAL,false);
                        recycleHomeComingSoon.setLayoutManager(layoutManager2);
                        recycleHomeComingSoon.setNestedScrollingEnabled(false);
                        AdapterListPostUpcoming adapterListPost2 = new AdapterListPostUpcoming(getActivity(), list);
                        recycleHomeComingSoon.setAdapter(adapterListPost2);
                        adapterListPost2.setmClickListener(MenuHome.this);
                    }

                }

            }

            @Override
            public void onFailure(Call<ValueEventList> call, Throwable t) {

                layoutHomeAll.setVisibility(View.GONE);
                btnReload.setVisibility(View.VISIBLE);
            }
        });

    }

    private void checkBalance(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueCheckBalance> data = apiInterface.checkBalance(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueCheckBalance>() {
            @Override
            public void onResponse(Call<ValueCheckBalance> call, Response<ValueCheckBalance> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    Locale localeID = new Locale("in", "ID");
                    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    String h=formatRupiah.format(Long.valueOf(response.body().getSaldo()));
                    String finalHarga= "Rp. "+h.substring(2)+",-";
                    saldoCash.setText(finalHarga);

                }

            }

            @Override
            public void onFailure(Call<ValueCheckBalance> call, Throwable t) {
                layoutHomeAll.setVisibility(View.GONE);
                btnReload.setVisibility(View.VISIBLE);
//                Toast.makeText(getActivity(), "periksa koneksi internet anda", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onEventClickListener(String idEvent) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.layoutFragmentHome, new MenuDetailEvent().newInstance(idEvent,"home"));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(null);
        trans.commit();
    }


//    @Override
//    public void onSliderClick(BaseSliderView slider) {
//
//        Toast.makeText(getActivity(), ""+slider.getDescription(), Toast.LENGTH_SHORT).show();
//    }




}
