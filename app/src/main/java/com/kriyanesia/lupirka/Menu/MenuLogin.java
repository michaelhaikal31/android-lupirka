package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;

import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MenuLogin extends Activity implements View.OnClickListener, Callback<OtpVerifikasi> {
    @BindView(R.id.idLogin)
    EditText etIdLogin;

//
    @BindView(R.id.btnLogin)
    Button btnLogin;
    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialog;
    @BindView(R.id.linkReg)
    TextView linkReg;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_login);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);
        btnLogin.setOnClickListener(this);
        alertDialog = new AlertDialog.Builder(this);
        linkReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MenuLogin.this,MenuRegister.class);
                startActivity(intent);
            }
        });
        InputMethodManager mgr = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(etIdLogin.getWindowToken(), 0);
        mgr.showSoftInput(etIdLogin, InputMethodManager.SHOW_IMPLICIT);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnLogin :
                checkLogin();
                break;

        }
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }
    private void checkLogin(){
        String idLogin = etIdLogin.getText().toString();
            if (idLogin.isEmpty()){
                showAllert("Information","No Hp / Email harus diisi");

            }else {
                char [] dataArr= idLogin.toCharArray();
                boolean validasi=true;

                for (int i=0;i<dataArr.length;i++){
                    try{
                        Integer.parseInt(String.valueOf(dataArr[i]));
                        System.out.println("VALIDASI TRUE DI "+i+" "+idLogin.indexOf(i)+" x "+dataArr[i]);
                    }catch (NumberFormatException e){
                        validasi=false;
                        System.out.println("VALIDASI FALSE DI "+i+" "+idLogin.indexOf(i)+" x "+dataArr[i]);
                    }
                }

                if (validasi){
                    if (idLogin.length()>=11 && idLogin.length()<=13){
                        LOGIN(idLogin,"phone");
                    }else {
                        showAllert("Information","isi No Hp anda dengan benar");
                    }
                }else {
                    if (isEmailValid(idLogin)){
                        LOGIN(idLogin,"email");
                    }else {
                        showAllert("Information","Email anda tidak sesuai format");
                    }
                }



            }

    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }




    private void LOGIN(String idLogin, String tipe){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        SessionDevice sd = new SessionDevice();
        String fcm = sd.getTokenDevice(MenuLogin.this,"token");
        String imei =sd.getImei(MenuLogin.this,"imei");
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("fcm_key", fcm);
            paramObject.put("id_login", idLogin);
            paramObject.put("imei", imei);
            paramObject.put("from", "login");
            paramObject.put("tipe_login", tipe);
            Call<OtpVerifikasi> verCall = apiInterface.getVerifikasiLogin(paramObject.toString(),"saxasdas");
            verCall.enqueue(MenuLogin.this);
            System.out.println("asd "+paramObject);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResponse(Call<OtpVerifikasi> call, Response<OtpVerifikasi> response) {
        progressDialog.dismiss();
        Log.d("RESPON SERVER",response.toString());
        if (response.body().getStatus().equalsIgnoreCase("success")){
            Intent i = new Intent(this, OtpValidation.class);
            i.putExtra("nohp",etIdLogin.getText().toString());
            i.putExtra("from","login");
            startActivity(i);
            finish();
        }else {
            showAllert("Login Gagal",response.body().getMessage());
        }

    }

    @Override
    public void onFailure(Call<OtpVerifikasi> call, Throwable t) {
        progressDialog.dismiss();
        showAllert("Login Gagal","Gagal Koneksi ke server");
        Log.d("RESPON SERVER",t.getMessage().toString());
    }
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
}
