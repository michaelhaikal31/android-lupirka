package com.kriyanesia.lupirka.Menu.KegiatanEvent;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kriyanesia.lupirka.AdapterModel.AdapterRundown.*;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Rundown.Rundown;
import com.kriyanesia.lupirka.Data.Rundown.ValueRundown;
import com.kriyanesia.lupirka.Data.SoalTest.ValueSoal;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class MenuRundown extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id_event";
    private static final String ARG_PARAM2 = "param2";

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private List<Rundown> listRundown;
    private RecyclerView listDate,descEvent;
    private adpterDate adpterDate;
    private AdapterListEvent adapterListEvent;
    private SimpleDateFormat inFormat = new SimpleDateFormat("dd/MM/yyyy");
    private Date date;
    private LinearLayoutManager linearLayoutManager;
    private SwipeRefreshLayout swipeRefreshLayout;
    private JSONObject paramObject;
    private String noHp;
    private SessionUser sessionUser;
    private TextView textNoData;



    public MenuRundown() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuRundown.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuRundown newInstance(String param1, String param2) {
        MenuRundown fragment = new MenuRundown();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sessionUser = new SessionUser();
        noHp=sessionUser.getNoHp(getActivity(),"nohp");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       View v =inflater.inflate(R.layout.fragment_menu_rundown, container, false);
       swipeRefreshLayout = v.findViewById(R.id.swiperefreshlayoutRundown);
        textNoData = v.findViewById(R.id.textNoData);

        listRundown = new ArrayList<>();

        listDate = (RecyclerView)v.findViewById(R.id.cardView);
        descEvent = (RecyclerView)v.findViewById(R.id.cardView2);
        listDate.setHasFixedSize(true);
        descEvent.setHasFixedSize(true);

        swipeRefreshLayout.setRefreshing(true);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadData();
            }
        });
        loadData();
        return v;
    }

    private void NoData() {
        textNoData.setVisibility(View.VISIBLE);
        listDate.setVisibility(View.INVISIBLE);
        descEvent.setVisibility(View.INVISIBLE);
    }

    private void ShowData() {
        textNoData.setVisibility(View.INVISIBLE);
        listDate.setVisibility(View.VISIBLE);
        descEvent.setVisibility(View.VISIBLE);
    }


    // TODO: Rename method, update argument and hook method into UI event

    private void getEvent(String selectedDate){
        List<Rundown> currentEvent=new ArrayList<>();
        for(int i=0;i<listRundown.size();i++){
            try {
                date = inFormat.parse(listRundown.get(i).getDate());
            }catch (ParseException e){
            }
            if(DateFormat.format("dd",date).equals(selectedDate)){
                currentEvent.add(listRundown.get(i));
            }
        }
        adapterListEvent = new AdapterListEvent(currentEvent);
        linearLayoutManager = new LinearLayoutManager(getActivity());
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        if(descEvent!=null){
            descEvent.setAdapter(adapterListEvent);
        }
        descEvent.setLayoutManager(linearLayoutManager);
    }


    private void loadData(){
        swipeRefreshLayout.setRefreshing(true);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        final SessionUser sessionUser=new SessionUser();
        paramObject = new JSONObject();
        try{
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",mParam1);

        }catch (JSONException e){

        }
        System.out.println("asd "+paramObject.toString());

        Call<ValueRundown> call = apiInterface.getRundown(paramObject.toString(),"saxasdas");
        call.enqueue(new Callback<ValueRundown>() {
            @Override
            public void onResponse(Call<ValueRundown> call, Response<ValueRundown> response) {
                System.out.println("asd [Menu Rundown Respon]  :"+response.toString());
                System.out.println("asd [Menu Rundown REspon Data List]"+response.body().getData().getList_rundown());
                try {
                    listRundown = response.body().getData().getList_rundown();
                    if (listRundown != null && !listRundown.get(0).id.equalsIgnoreCase("-")) {

                        adpterDate = new adpterDate(listRundown);
                        adpterDate.setItemClickListener(new adpterDate.ItemClickListener() {
                            @Override
                            public void onItemClick(String date) {
                                getEvent(date);
                            }
                        });
                        linearLayoutManager = new LinearLayoutManager(getActivity());
                        linearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
                        listDate.setAdapter(adpterDate);
                        listDate.setLayoutManager(linearLayoutManager);
                        getEvent(listRundown.get(0).getDate().substring(0, listRundown.get(0).getDate().indexOf("/")));
                        ShowData();
                    }else{
                        NoData();
                        Snackbar.make(getView(),"Tidak ada jawal tersedia",Snackbar.LENGTH_LONG)
                                .show();
                    }
                }catch(Exception e){
                    System.out.println("nih "+ e.toString());
                    NoData();
                }
                swipeRefreshLayout.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ValueRundown> call, Throwable t) {
                System.out.println("nih error :"+call+", "+t.getMessage());
                swipeRefreshLayout.setRefreshing(false);
                Snackbar.make(getView(),"Connection problem !",Snackbar.LENGTH_INDEFINITE)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadData();
                                swipeRefreshLayout.setRefreshing(true);
                            }
                        })
                        .show();
                NoData();
            }
        });


    }

    @Override
    public void onResume() {
        super.onResume();
        //loadData();
    }
    /*
    private void loadData(){
        listRundown.add(new Rundown("1","12/12/2018","08:00","09:00","Event-1 : Seminar Penyuluhan","this is description example"));
        listRundown.add(new Rundown("2","12/12/2018","09:00","11:00","Event-2 : Uji kelayakan alat","this is description example"));
        listRundown.add(new Rundown("3","13/12/2018","08:00","10:00","Event-3 : Mencoba alat baru","this is description example"));
        listRundown.add(new Rundown("4","13/12/2018","10:00","12:30","Event-4 : Dunia dan Manusia","this is description example"));
        listRundown.add(new Rundown("5","13/12/2018","13:00","15:30","Event-5 : Tes dasar operasi bedah","this is description example"));
        listRundown.add(new Rundown("6","14/12/2018","08:00","12:00","Event-6 : Penyuluhan mendasar uji krepatiskionitar","this is description example"));
        listRundown.add(new Rundown("7","14/12/2018","13:00","16:00","Event-7 : Pholyostheorpist menggunakan jarum suntik","this is description example"));
        listRundown.add(new Rundown("8","14/12/2018","16:00","17:00","Event-8 : Atasi penularan virus","this is description example"));
    }

*/

}
