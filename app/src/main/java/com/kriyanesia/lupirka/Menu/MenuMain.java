package com.kriyanesia.lupirka.Menu;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.design.widget.NavigationView;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.crashlytics.android.Crashlytics;
import com.google.firebase.messaging.FirebaseMessaging;
import com.kriyanesia.lupirka.AdapterModel.AdapterListNotif;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.AdapterModel.SearchEventAdapter;
import com.kriyanesia.lupirka.Menu.KegiatanEvent.ActivityEvent;
import com.kriyanesia.lupirka.Menu.KegiatanEvent.MenuSoal;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Session.SessionNotif;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.squareup.picasso.Picasso;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;
import io.fabric.sdk.android.Fabric;

public class MenuMain extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener{
    Dialog dialogConfirmation;
    private TabLayout tabLayout;
    android.app.AlertDialog.Builder alertDialog;
    ProgressDialog progressDialog;
    TextView namaUser,notifCount;
    CircleImageView iconUser;
    SessionUser sessionUser;
    String sessionPhone ;
    String sessionName ;
    String sessionPhoto ;
    NavigationView navigationView;
    FrameLayout frameMenuMain;
    private ListView listView;
    boolean statusExit=true;
    private ArrayAdapter<SessionNotif> adapterNotif;
    private DatabaseHelper databaseHelper;
    private List<SessionNotif> listNotif;
    private AdapterListNotif adapterListNotif;
    private Button selesai;
//    Button btnFilter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Fabric.with(this, new Crashlytics());
        setContentView(R.layout.activity_main_menu);


        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle("");
        View view = this.getCurrentFocus();
//        btnFilter = (Button) findViewById(R.id.btnFilter);

        FirebaseMessaging.getInstance().subscribeToTopic("lupirka-dak");

        /*


                */

        sessionUser = new SessionUser();
        SessionDevice sd = new SessionDevice();
        System.out.println("FCM_TOKEN "+sd.getTokenDevice(MenuMain.this,"token"));
        sessionPhone = sessionUser.getNoHp(MenuMain.this,"nohp");
        sessionName = sessionUser.getName(MenuMain.this,"name");
        sessionPhoto = sessionUser.getProfile(MenuMain.this,"image");

        if (sessionPhone==""|| sessionPhone==null || sessionPhone.isEmpty()){
            Intent intent = new Intent(this,MenuLogin.class);
            startActivity(intent);
        }
        if (sessionUser.getPerusahaanFarmasi(MenuMain.this,"perusahaanFarmasi").equalsIgnoreCase("")){
//            setSessionFarmasi();
        }



        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        drawer.addDrawerListener(this);

        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        final AutoCompleteTextView searchView = (AutoCompleteTextView) findViewById(R.id.form_search);
        searchView.setCompoundDrawablesWithIntrinsicBounds(getResources().getDrawable(R.drawable.icon_search_new), null, null, null);

        View header = navigationView.getHeaderView(0);

        notifCount = findViewById(R.id.txtJumlahNotif);
        namaUser = (TextView) header.findViewById(R.id.namaUser);
        iconUser = (CircleImageView) header.findViewById(R.id.imageUser);
        namaUser.setText(sessionName);

        if (sessionPhoto.contains("http")) {
            Picasso.with(this).load(sessionPhoto)
                    .placeholder(R.drawable.icon_account_name).error(R.drawable.ic_launcher)
                    .into(iconUser);
        }
//        Picasso.with(this).load(sessionPhoto).into(iconUser);

        progressDialog = new ProgressDialog(this);
        searchView.clearFocus();
        searchView.setDropDownBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.bgWhite)));

        final SearchEventAdapter adapter = new SearchEventAdapter(this,android.R.layout.simple_spinner_dropdown_item);
        searchView.setAdapter(adapter);


//        frameMenuMain = (FrameLayout) findViewById(R.id.frameMenuMain);
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setSelectedTabIndicatorHeight(0);
        tabLayout.addTab(tabLayout.newTab().setText("Home"));
        tabLayout.addTab(tabLayout.newTab().setText("Event"));
        tabLayout.addTab(tabLayout.newTab().setText("Activity"));
        tabLayout.addTab(tabLayout.newTab().setText("Ads"));
        tabLayout.addTab(tabLayout.newTab().setText("Profile"));

        if(getIntent().getExtras()!=null) {
            //boolean intentFragment = getIntent().getExtras().getBoolean("frgToLoad");
                addfragment(new MenuAktivitas(), "MenuEvent");
                tabLayout.getTabAt(2).select();
        }else {
            addfragment(new MenuHome(), "MenuHome");
        }
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tabLayout.getSelectedTabPosition()) {
                    case 0:
                        addfragment(new MenuHome(),"MenuHome");
                        break;
                    case 1:
                        addfragment(new MenuEvent().newInstance("x"),"MenuEvent");
                        sessionUser.setEventTabIndex(MenuMain.this,"index","0");
                        break;
                    case 2:
                        addfragment(new MenuAktivitas(),"MenuEvent");
                        //Intent i = new Intent(MenuMain.this,ActivityEvent.class);
                        //startActivity(i);
                        break;
                    case 3:
                        addfragment(new MenuAdsView(),"MenuAdsView");
                        break;
                    case 4:
                        addfragment(new MenuProfile(),"MenuProfile");
                        break;
                    default:
                        break;
                }

            }


            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });



        //when autocomplete is clicked
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @SuppressLint("WrongConstant")
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String idEvent = adapter.getItem(position);


                searchView.setText("");
//                FragmentTransaction trans = getSupportFragmentManager()
//                        .beginTransaction();
//                trans.replace(R.id.eventLayout, new MenuDetailEvent().newInstance(idEvent,"main"));
//                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                trans.addToBackStack(null);
//                trans.commit();
                Intent intent = new Intent(MenuMain.this,ActivityDetailContent.class);
                intent.putExtra("menu","searchDetailEvent");
                intent.putExtra("eventId",idEvent);
                startActivity(intent);

            }
        });
//        btnFilter.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(MenuMain.this,ActivityDetailContent.class);
//                intent.putExtra("menu","filterEvent");
//                startActivity(intent);
//            }
//        });
        ImageView btnNotif = (ImageView) findViewById(R.id.notif);
        databaseHelper = new DatabaseHelper(MenuMain.this);
        listNotif = databaseHelper.getSessionNotif();
        if(listNotif!=null) {
            notifCount.setText(String.valueOf(listNotif.size()));
        }
        btnNotif.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(listNotif!=null){
                    Intent intent = new Intent(MenuMain.this,NotificationsActivity.class);
                    intent.putExtra("notif",true);
                    startActivity(intent);
//                //adapterNotif = new ArrayAdapter<SessionNotif>(MenuMain.this,android.R.layout.simple_list_item_1,listNotif);
//                adapterListNotif = new AdapterListNotif(listNotif,MenuMain.this);
//                final AlertDialog.Builder bmneu = new AlertDialog.Builder(MenuMain.this);
//                // Inflate
//                LayoutInflater inflate = getLayoutInflater();
//                // View
//                final View view = inflate.inflate(R.layout.dialog_notif, null);
//                // Assign view to AlertDialogBuilder
//                listView = (ListView)view.findViewById(R.id.listViewNotif);
//                listView.setDivider(null);
//                // Alert Dialog
//                    listView.setAdapter(adapterListNotif);
//                    bmneu.setView(view);
//                    bmneu.setPositiveButton("Clear", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            databaseHelper.deleteAllNotif();
//                            listNotif.clear();
//                            adapterNotif=null;
//                            Snackbar.make(findViewById(R.id.drawer_layout),"Success !",Snackbar.LENGTH_SHORT).show();
//                            return;
//                        }
//                    }).setNegativeButton("Close", new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            return;
//                        }
//                    });
//                    final AlertDialog alertHelp = bmneu.create();
//                    alertHelp.setTitle("NOTIFICATIONS");
//                    // Show AlertDialog
//                    alertHelp.show();
                }else{
                    Toast.makeText(MenuMain.this,"Notifikasi kosong !",Toast.LENGTH_SHORT).show();
                }

            }
        });

        hideSoftKeyboard();


    }
    void addfragment(Fragment fragment,String packageName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        // this will clear the back stack and displays no animation on the screen
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.add(R.id.frameMenuMain, fragment,"MAIN");
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(packageName);
        trans.commit();
        getFragmentManager().executePendingTransactions();
        statusExit=true;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    public void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        final View view = this.findViewById(android.R.id.content);
        if (view != null){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);

        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            FragmentManager fragmentManager = getSupportFragmentManager();
            if (fragmentManager.getBackStackEntryCount() == 1){
                finish();

            }


            super.onBackPressed();
        }

    }

    @Override
    protected void onPostResume() {
        super.onPostResume();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {


        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_setting) {
            Intent intent = new Intent(MenuMain.this, MenuSetting.class);
            startActivity(intent);
        }
        else if (id == R.id.nav_share) {
            Intent i = new Intent(MenuMain.this,MenuShare.class);
            startActivity(i);

        }
        else if (id == R.id.nav_contact) {
            Intent i = new Intent(MenuMain.this,MenuContactUs.class);
            startActivity(i);

        }
        else if (id == R.id.nav_help) {
            Intent i = new Intent(MenuMain.this,MenuHelp.class);
            startActivity(i);

        }
//        else if(id==R.id.nav_saldo){
////            if (navigationView.getMenu().ge)
//            Intent i = new Intent(MenuMain.this,ActivityDetailContent.class);
//            i.putExtra("menu","topup");
//            startActivity(i);
//        }
//        else if (id == R.id.nav_mutasi) {
//            Intent i = new Intent(MenuMain.this,ActivityDetailContent.class);
//            i.putExtra("menu","topup");
//            startActivity(i);
//
//        }
        else if (id == R.id.nav_barcode) {
            Intent i = new Intent(MenuMain.this,MenuBarcode.class);
            startActivity(i);

        }else if (id == R.id.nav_activity) {
            TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(2));
            selectedTab.select();

        }else if (id == R.id.nav_event) {
            TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(1));
            selectedTab.select();

        }
        else if (id == R.id.nav_ads) {
            TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(3));
            selectedTab.select();

        }
        else if (id == R.id.nav_logout) {
            showAllert("Information","Apakah anda yakin untuk keluar dari aplikasi ini ?","logout");
        }
//        else if (id == R.id.nav_manage) {
//
//        } else if (id == R.id.nav_share) {
//
//        } else if (id == R.id.nav_send) {
//
//        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }






    private void showAllert(String title, String message, final String action){
        alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (action.equalsIgnoreCase("logout")){
                            logout();
                        }else if (action.equalsIgnoreCase("exit")){
                            finish();
                        }
                    }
                });
        if (action.equalsIgnoreCase("logout")){
            alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }
        alertDialog.show();

    }
    private void logout(){
        SessionUser sessionUser = new SessionUser();
        sessionUser.setNoHp(MenuMain.this,"nohp","");
        sessionUser.setName(MenuMain.this,"name","");
        sessionUser.setNamaSertifikat(MenuMain.this,"namaSertifikat","");
        Intent i = new Intent(MenuMain.this,MenuLogin.class);
        startActivity(i);
        finish();
    }

    @Override
    public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(@NonNull View drawerView) {
        if (sessionPhoto.contains("http")){
            Picasso.with(this).load(sessionPhoto)
                    .placeholder(R.drawable.icon_account_name).error(R.drawable.ic_launcher)
                    .into(iconUser);
        }
    }

    @Override
    public void onDrawerClosed(@NonNull View drawerView) {

    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }




    //    private void setSessionFarmasi(){
//        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
//        httpClient.addInterceptor(new Interceptor() {
//            @Override
//            public okhttp3.Response intercept(Chain chain) throws IOException {
//                Request original = chain.request();
//
//                // Request customization: add request headers
//                Request.Builder requestBuilder = original.newBuilder()
//                        .header("Authorization", "auth-value"); // <-- this is the important line
//
//                Request request = requestBuilder.build();
//                return chain.proceed(request);
//            }
//        });
//
//        OkHttpClient client = httpClient.build();
//
//        Retrofit retrofit = new Retrofit.Builder()
//                .baseUrl(Constant.URL_SERVICE)
//                .addConverterFactory(ScalarsConverterFactory.create())
//                .addConverterFactory(GsonConverterFactory.create())
//                .client(client)
//                .build();
//
//        ApiService apiInterface = retrofit.create(ApiService.class);
//        sessionUser = new SessionUser();
//        String nohp = sessionUser.getNoHp(this,"nohp");
//        String roleUser= sessionUser.getRole(this,"role");
//        JSONObject paramObject = new JSONObject();
//        try {
//            paramObject.put("id_login",nohp);
//            paramObject.put("role",roleUser);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//        System.out.println("VALUENYA load profile: "+paramObject.toString());
//
//        Call<ValueProfileUser> data = apiInterface.getProfileFarmasi(paramObject.toString(),"saxasdas");
//
//        data.enqueue(new Callback<ValueProfileUser>() {
//            @Override
//            public void onResponse(Call<ValueProfileUser> call, Response<ValueProfileUser> response) {
//                if (response.body().getStatus().equalsIgnoreCase("success")){
//                    sessionUser = new SessionUser();
//                    ValueProfileUser.Data data;
//                    data = response.body().getData();
//                    sessionUser.setPerusahaanFarmasi(MenuMain.this,"perusahaanFarmasi",data.getNama_perusahaan());
//                    sessionUser.setJabatanFarmasi(MenuMain.this,"jabatan",data.getJabatan());
//                    sessionUser.setTipeJabatanFarmasi(MenuMain.this,"tipeJabatan",data.getTipe_jabatan());
//                    sessionUser.setAlamatFarmasi(MenuMain.this,"alamat",data.getAlamat());
//
//
//                }
//                System.out.println("VALUENYA : "+response.body().getStatus());
//
//            }
//
//            @Override
//            public void onFailure(Call<ValueProfileUser> call, Throwable t) {
//
////                showAllert("Respon Server","Tidak dapat terhubung ke server","loadEO");
//
//            }
//        });
//    }
    public interface StatusBack{
//        void setStatus(boolean statusExit);
    }



}





