package com.kriyanesia.lupirka.Menu;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListSosmed;
import com.kriyanesia.lupirka.Data.Sosmed;
import com.kriyanesia.lupirka.R;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ShareSosmed extends AppCompatActivity implements AdapterListSosmed.ItemClickListener {
    List<Sosmed> sosmed;
    @BindView(R.id.recycleSosmed)
    RecyclerView recyclerSosmed;

    AdapterListSosmed adapter;
    private LinearLayoutManager lLayout;

    String type = "image/*";
    String filename = "/myPhoto.jpg";
    String mediaPath = Environment.getExternalStorageDirectory() + filename;
    String textShare = "Download Aplikasinya di link berikut ini http://xsadas.com";



    private RecyclerView.LayoutManager mLayoutManager;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_share_sosmed);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        List<Sosmed> rowListItem = getAllSosmedList();
        lLayout = new LinearLayoutManager(ShareSosmed.this);

        int numberOfColumns = 3;
        recyclerSosmed.setLayoutManager(new GridLayoutManager(ShareSosmed.this, numberOfColumns));
        adapter = new AdapterListSosmed(ShareSosmed.this, rowListItem);
        adapter.setClickListener(ShareSosmed.this);
        recyclerSosmed.setAdapter(adapter);





    }
    private void





    shareApkToSosmed(String packageName) {
        PackageManager pm = getPackageManager();
        try {
            pm.getPackageInfo(packageName, PackageManager.GET_ACTIVITIES);
            // Create the new Intent using the 'Send' action.
            Intent share = new Intent(Intent.ACTION_SEND);

            // Set the MIME type
            share.setType(type);

            // Create the URI from the media
            File media = new File(mediaPath);
            Uri uri = Uri.fromFile(media);
            URL url;
            try {
                url = new URL("http://2.bp.blogspot.com/-qF8JlEkvv6w/UM75UJD3aHI/AAAAAAAABT0/JVmn4_ukUwg/s1600/gambar+kartun+spongebob+%25283%2529.jpg");
                uri = Uri.parse(url.toURI().toString());
            } catch (MalformedURLException e1) {
                e1.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
                Toast.makeText(this, e.getMessage(), Toast.LENGTH_SHORT).show();
                System.out.println("PESAN GAGAL : "+e.getMessage());
            }
//            Uri uri =  Uri.parse("http://www.lagioke.net/wp-content/uploads/2015/04/Gambar-ini-Bikin-Salah-Fokus.jpg" );

            //set packagename apps
            share.setPackage(packageName);

            // Add the URI to the Intent.
//            share.putExtra(Intent.EXTRA_STREAM, uri);

            //put description text
            share.putExtra(packageName+".EXTRA_DESCRIPTION", textShare);
            share.putExtra(Intent.EXTRA_TEXT, textShare);
            // Broadcast the Intent.
            startActivity(Intent.createChooser(share, "Share to"));
        }
        catch (PackageManager.NameNotFoundException e) {
            Toast.makeText(ShareSosmed.this, "Aplikasi Belum terinstall, install terlebih dahulu", Toast.LENGTH_SHORT).show();

        }

    }

    private List<Sosmed> getAllSosmedList(){

        List<Sosmed> allItems = new ArrayList<Sosmed>();
        allItems.add(new Sosmed("Instagram", R.drawable.icon_instagram));
        allItems.add(new Sosmed("Google", R.drawable.icon_google_plus));
        allItems.add(new Sosmed("Facebook", R.drawable.icon_facebook));
        allItems.add(new Sosmed("Pinterest", R.drawable.icon_pinterest));
        allItems.add(new Sosmed("Twitter", R.drawable.icon_twitter));
        allItems.add(new Sosmed("Whatsapp", R.drawable.icon_whatsapp));
        return allItems;
    }




    @Override
    public void onItemClick(View view, int position,String nama) {
        switch (nama){
            case "Instagram":
                shareApkToSosmed("com.instagram.android");
                break;
            case "Google":
                shareApkToSosmed("com.google.android.apps.plus");
                break;
            case "Facebook":
                shareApkToSosmed("com.facebook.katana");
                break;
            case "Pinterest":
                shareApkToSosmed("com.pinterest");
                break;
            case "Twitter":
                shareApkToSosmed("com.twitter.android");
                break;
            case "Whatsapp":
                shareApkToSosmed("com.whatsapp");
                break;

        }

    }
}
