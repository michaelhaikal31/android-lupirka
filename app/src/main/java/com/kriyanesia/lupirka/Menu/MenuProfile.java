package com.kriyanesia.lupirka.Menu;


import android.Manifest;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerHospital;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerProfesi;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerSpesialis;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerUniversitas;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.EO.EO;
import com.kriyanesia.lupirka.Data.EO.ValueEO;
import com.kriyanesia.lupirka.Data.Farmasi.Farmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueFarmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueJabatan;
import com.kriyanesia.lupirka.Data.Hospital.Hospital;
import com.kriyanesia.lupirka.Data.Hospital.ValueHospital;
import com.kriyanesia.lupirka.Data.Idi.IdiCabang;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiCabang;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiWilayah;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;
import com.kriyanesia.lupirka.Data.Profesi.Profesi;
import com.kriyanesia.lupirka.Data.Profesi.ValueProfesi;
import com.kriyanesia.lupirka.Data.Profile.ValueProfileUser;
import com.kriyanesia.lupirka.Data.Profile.ValueUpdatePhoto;
import com.kriyanesia.lupirka.Data.Spesialis.Spesialis;
import com.kriyanesia.lupirka.Data.Spesialis.ValueSpesialis;
import com.kriyanesia.lupirka.Data.Universitas.Universitas;
import com.kriyanesia.lupirka.Data.Universitas.ValueUniversitas;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.CameraUtils;
import com.kriyanesia.lupirka.Util.Constant;
//import com.raj.spinnerdatepick.IDatePicker;
import com.basavaraj.spinnerdatepick.IDatePicker;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;
import com.squareup.picasso.Picasso;
import com.tuyenmonkey.mkloader.MKLoader;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.content.Context.INPUT_METHOD_SERVICE;
import static android.support.v4.provider.FontsContractCompat.FontRequestCallback.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuProfile extends Fragment implements View.OnClickListener,IDatePicker {

    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialog;
    onUserChangeIcon onUserChangeIcon;
    //var show profile
    @BindView(R.id.textName)
    TextView textName;
    @BindView(R.id.textGender)
    TextView textGender;
    @BindView(R.id.textAddress)
    TextView textAddress;
    @BindView(R.id.textBirthday)
    TextView textBirthday;
    @BindView(R.id.textPhone)
    TextView textPhone;
    @BindView(R.id.textEmail)
    TextView textEmail;

    @BindView(R.id.iconViewName)
    ImageView iconViewName;
    //iconUmum
    @BindView(R.id.iconViewGender)
    ImageView iconViewGender;
    //iconUmum
    @BindView(R.id.iconViewBirthday)
    ImageView iconViewBirthday;
    //iconUmum
    @BindView(R.id.iconViewAddress)
    ImageView iconViewAddress;
    //iconUmum
    @BindView(R.id.iconViewPhone)
    ImageView iconViewPhone;
    //iconUmum
    @BindView(R.id.iconViewEmail)
    ImageView iconViewEmail;

    //iconUmum
    @BindView(R.id.iconName)
    ImageView iconName;
    //iconUmum
    @BindView(R.id.iconGender)
    ImageView iconGender;
    //iconUmum
    @BindView(R.id.iconBirthday)
    ImageView iconBirthday;
    //iconUmum
    @BindView(R.id.iconAddress)
    ImageView iconAddress;
    //iconUmum
    @BindView(R.id.iconPhone)
    ImageView iconPhone;
    //iconUmum
    @BindView(R.id.iconEmail)
    ImageView iconEmail;

    @BindView(R.id.btnEditProfile)
    Button btnEditProfile;

    //var edit profile
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etBirthday)
    TextView etBirthday;
    @BindView(R.id.txtNameUser)
    TextView txtNameUser;
    @BindView(R.id.etAddressUser)
    EditText etAddressUser;
    @BindView(R.id.etPrefixPhone)
    CountryCodePicker etPrefixPhone;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etAcountName)
    EditText etAcountName;
    @BindView(R.id.etBank)
    EditText etBank;
    @BindView(R.id.etCabangBank)
    EditText etCabangBank;
    @BindView(R.id.spinerEoName)
    Button spinerEoName;
    @BindView(R.id.btnEoName)
    Button btnEoName;
    @BindView(R.id.etEOAdress)
    EditText etEoAdress;
    @BindView(R.id.iconEoOther)
    ImageView iconEoOther;
    @BindView(R.id.etEoOther)
    EditText etEoOther;

    @BindView(R.id.btnChangePhoto)
    ImageButton btnChangePhoto;

    //EO
    @BindView(R.id.farmasiPosition)
    Button farmasiPosition;
    @BindView(R.id.btnFarmasiPosition)
    Button btnFarmasiPosition;
    @BindView(R.id.etNoRek)
    EditText etNoRek;

    //others
    @BindView(R.id.etOtherName)
    EditText etOtherName;
    @BindView(R.id.etOtherAdress)
    EditText etOtherAdress;
    //    @BindView(R.id.rgTitle)
//    RadioGroup rgTitle;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.rgFemale)
    RadioButton rgFemale;
    @BindView(R.id.rgMale)
    RadioButton rgMale;
    @BindView(R.id.rtDrg)
    CheckBox rtDrg;
    @BindView(R.id.rtDR)
    CheckBox rtDR;
    @BindView(R.id.rtDr)
    CheckBox rtDr;
    @BindView(R.id.rtProf)
    CheckBox rtProf;
    @BindView(R.id.iconType)
    ImageView iconType;
    @BindView(R.id.userType)
    Button spinerUserType;
    @BindView(R.id.btnUserType)
    Button btnUserType;
    @BindView(R.id.spesificTitle)
    Button spinerSpesificTitle;
    //    @BindView(R.id.daerah)
//    AutoCompleteTextView textDaerah;
    @BindView(R.id.wilayah)
    Button spinerIDIwilayah;
    @BindView(R.id.btnWilayah)
    Button btnIDIwilayah;
    @BindView(R.id.kecamatan)
    Button spinerIDICabang;
    @BindView(R.id.btnKecamatan)
    Button btnIDICabang;

    @BindView(R.id.residentHospital)
    EditText residentHospital;
    @BindView(R.id.medicalMajorMahasiswa)
    Button spinerMedicalMajorMahasiswa;
    @BindView(R.id.univMahasiswa)
    Button spinerUnivMahasiswa;
    @BindView(R.id.perusahaanFarmasi)
    Button spinerPerusahaanFarmasi;
    @BindView(R.id.btnPerusahaanFarmasi)
    Button btnPerusahaanFarmasi;
    @BindView(R.id.farmasiAddress)
    EditText etFarmasiAddress;
    @BindView(R.id.iconTitle)
    ImageView iconTitle;
    @BindView(R.id.iconSpesificTitle)
    ImageView iconSpesificTitle;
    @BindView(R.id.iconSpesificTitleMahasiswa)
    ImageView iconSpesificTitleMahasiswa;
    @BindView(R.id.iconDaerah)
    ImageView iconDaerah;
    @BindView(R.id.iconKecamatan)
    ImageView iconKecamatan;
    @BindView(R.id.iconResidentHospital)
    ImageView iconResidentHospital;
    @BindView(R.id.iconUnivMahasiswa)
    ImageView iconUnivMahasiswa;
    @BindView(R.id.iconEoName)
    ImageView iconEoName;
    @BindView(R.id.iconAcountName)
    ImageView iconAcountName;
    @BindView(R.id.iconNoRek)
    ImageView iconNoRek;
    @BindView(R.id.iconEOAddress)
    ImageView iconEOAddress;
    @BindView(R.id.iconBank)
    ImageView iconBank;
    @BindView(R.id.iconCabangBank)
    ImageView iconCabangBank;
    @BindView(R.id.iconPerusahaanFarmasi)
    ImageView iconPerusahaanFarmasi;
    @BindView(R.id.iconFarmasiPosition)
    ImageView iconFarmasiPosition;
    @BindView(R.id.iconFarmasiAddress)
    ImageView iconFarmasiAddress;
    @BindView(R.id.iconFarmasiPositionOther)
    ImageView iconFarmasiPositionOther;
    @BindView(R.id.iconOtherName)
    ImageView iconOtherName;
    @BindView(R.id.iconOtherAddress)
    ImageView iconOtherAddress;

    @BindView(R.id.iconOtherFarmasiName)
    ImageView iconOtherFarmasiName;
    @BindView(R.id.otherFarmasiName)
    EditText otherFarmasiName;
    String roleSpesialis="";

    @BindView(R.id.rumahSakitMhs)
    EditText rumahSakitMhs;

    @BindView(R.id.iconRumahSakitMhs)
    ImageView iconRumahSakitMhs;

    //    @BindView(R.id.btnResidentHospital)
//    Button btnResidentHospital;
    @BindView(R.id.btnMedicalMajorMahasiswa)
    Button btnMedicalMajorMahasiswa;
    @BindView(R.id.btnUnivMahasiswa)
    Button btnUnivMahasiswa;
    @BindView(R.id.btnSpesificTitle)
    Button btnSpesificTitle;

    @BindView(R.id.pilihPosisi)
    Button spinerPilihPosisi;
    @BindView(R.id.btnPilihPosisi)
    Button btnPilihPosisi;
    @BindView(R.id.iconPilihPosisi)
    ImageView iconPilihPosisi;
    @BindView(R.id.farmasiPositionOther)
    EditText farmasiPositionOther;

    @BindView(R.id.btnUnivResident)
    Button btnUnivResident;
    @BindView(R.id.univResident)
    Button spinerUnivResident;
    @BindView(R.id.iconUnivResident)
    ImageView iconUnivResident;
    @BindView(R.id.imageUser)
    CircleImageView imageUser;
    @BindView(R.id.backgroundLoading)
    CircleImageView backgroundLoading;
    @BindView(R.id.loading)
    MKLoader loadingChangePhoto;

    @BindView(R.id.btnSaveUpdateProfile)
    Button btnSaveUpdateProfile;
//    @BindView(R.id.circle_loading_view)
//    AnimatedCircleLoadingView animatedCircleLoadingView;

    private static String imageStoragePath;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 1211;
    private int RESULT_CODE_BANNER_BUKTI=1212;
    String prefixPhone="62";
    String sessionNoHp;
    List<Profesi> profesi = new ArrayList<>();
    List<Spesialis> spesialis = new ArrayList<>();
    List<Hospital> hospital = new ArrayList<>();
    List<Universitas> universitas  = new ArrayList<>();
    private List<Farmasi> farmasi= new ArrayList<>();
    private List<EO> eventOrganizer= new ArrayList<>();
    private List<String> wilayahIdi= new ArrayList<>();
    private List<IdiCabang> cabangIdi= new ArrayList<>();
    private List<String> jabatanFarmasi= new ArrayList<>();
    private List<String> tipeJabatanFarmasi= new ArrayList<>();

    private DatePickerDialog formTglLahir;

    private SimpleDateFormat dateFormatter;

    //parameter utama
    String noHp="", gender="",birthdayDate="",userAddress="",fax="",postalCode="",telpNumber="",name="",email="",role="";

    //parameter tambahan Dokter role 2
    String spesialiss="",provinsi="",kota="",kecamatan="",kelurahan="",kodepos="",gelar="",idiWilayah="",idiCabang="";

    //parameter tambahan resident 4 spesialis,gelar termasuk
    String rumahSakit="",universitasResident="",tipe="";

    //parameter tambahan mahasiswa 5 spesialis termasuk
    String universitasMhs="";

    //parameter tambahan EO 6
    String eoName="",eoAddress="",accountName="",accountNumber="",accountBank="",accountBranch="";

    //parameter tambahan farmasi 7
    String officeName="",officeOther="",jabatan="",officeAddress="";

    //parameter tambahan Others 8
    String namaOrganisasi="",alamatOrganisasi="";
    JSONObject paramObject;
    AdapterSpinerProfesi adapterSpinerProfesi;;
    AdapterSpinerSpesialis adapterSpesialis;
    AdapterSpinerHospital adapterSpinerHospital;
    AdapterSpinerUniversitas adapterSpinerUniversitas;
    View v;
    public MenuProfile() {
        // Required empty public constructor
    }

    public static final int MY_REQUEST_CAMERA   = 10;
    public static final int MY_REQUEST_WRITE_CAMERA   = 11;

    public static final int MY_REQUEST_READ_GALLERY   = 13;
    public static final int MY_REQUEST_WRITE_GALLERY   = 14;
    SessionUser sessionUser;
    String namaUser;


    int sessionBtnEditProfile=0;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_menu_profile, container, false);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this,v);

        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();

        backgroundLoading.setVisibility(View.GONE);
        loadingChangePhoto.setVisibility(View.GONE);
        sessionUser= new SessionUser();
        allFormGone();

        loadProfesi();
        loadHospital();
        loadUniversitas();
        loadFarmasi();
        loadEoName();
        loadWilayahIDI();
        loadTipeJabatan();
        sessionNoHp=sessionUser.getNoHp(getActivity(),"nohp");
        String imageProfile=sessionUser.getProfile(getActivity(),"image");
        if (imageProfile.contains("http")) {
            Picasso.with(getActivity()).load(imageProfile)
                    .placeholder(R.drawable.icon_account_name).error(R.drawable.ic_launcher)
                    .into(imageUser);
        }
        role=sessionUser.getRole(getActivity(),"role");
        namaUser=sessionUser.getName(getActivity(),"name");
        txtNameUser.setText(namaUser);


        aksiBtnEditProfile();
        btnEditProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (sessionBtnEditProfile==0) {
                    sessionBtnEditProfile =1;
                }else {
                    sessionBtnEditProfile =0;
                }

                aksiBtnEditProfile();
            }
        });

        loadDataUser();
        if (namaUser.contains("Prof.")){
            rtProf.post(new Runnable() {
                @Override
                public void run() {
                    rtProf.setChecked(true);
                }
            });
        }
        if (namaUser.contains("DR.")){
            rtDR.post(new Runnable() {
                @Override
                public void run() {
                    rtDR.setChecked(true);
                }
            });
        }
        if (namaUser.contains("Drg.")){
            rtDrg.post(new Runnable() {
                @Override
                public void run() {
                    rtDrg.setChecked(true);
                    if (role.equalsIgnoreCase("4")){
                        tipe="Drg";
                        roleSpesialis="2";
                    }else {
                        roleSpesialis="3";

                    }
                    loadSpesialis(roleSpesialis);
                }
            });
        }
        if (namaUser.contains("Dr.")){
            rtDr.post(new Runnable() {
                @Override
                public void run() {
                    rtDr.setChecked(true);
                    if (role.equalsIgnoreCase("4")){
                        tipe="Dr ";
                        roleSpesialis="3";
                    }else {
                        roleSpesialis="2";
                    }
                    loadSpesialis(roleSpesialis);
                }
            });
        }




        spinerUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                checkDataUserType();
//                changeView(roles);
                Toast.makeText(getActivity(), "Anda tidak diizinkan untuk mengubah tipe user anda", Toast.LENGTH_SHORT).show();
            }
        });
        btnUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                checkDataUserType();
//                changeView(roles);
                Toast.makeText(getActivity(), "Anda tidak diizinkan untuk mengubah tipe user anda", Toast.LENGTH_SHORT).show();
            }
        });

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String number = etPhoneNumber.getText().toString();
                etPhoneNumber.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {

                        if (number.length()==2){
                            if (number.startsWith("08")){
                                etPhoneNumber.setText("8");
                                etPhoneNumber.setSelection(1);
                            }
                        }

                        else if (number.length()==3 && keyCode != KeyEvent.KEYCODE_DEL){
                            etPhoneNumber.setText(number+"-");
                            etPhoneNumber.setSelection(4);
                        }else if (number.length()==8 && keyCode != KeyEvent.KEYCODE_DEL){
                            etPhoneNumber.setText(number+"-");
                            etPhoneNumber.setSelection(9);
                        }
                        return false;
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {
//                Toast.makeText(MenuRegister.this, etPhoneNumber.getText().toString().replace("-",""), Toast.LENGTH_SHORT).show();
            }
        });

        spinerEoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEo();
            }
        });
        btnEoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEo();
            }
        });

        spinerMedicalMajorMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJurusan();
            }
        });
        btnMedicalMajorMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJurusan();
            }
        });


        spinerUnivMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUniversitas("mahasiswa");
            }
        });
        btnUnivMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataUniversitas("mahasiswa");
            }
        });


        spinerUnivResident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUniversitas("resident");
            }
        });
        btnUnivResident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataUniversitas("resident");
            }
        });


        spinerIDIwilayah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataIdiWilayah();
            }
        });
        btnIDIwilayah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataIdiWilayah();
            }
        });

        spinerIDICabang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataIdiCabang();
            }
        });
        btnIDICabang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataIdiCabang();
            }
        });



        spinerSpesificTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataSpesialisasi();
            }
        });
        btnSpesificTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataSpesialisasi();
            }
        });


        spinerPerusahaanFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataFarmasi();
            }
        });
        btnPerusahaanFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataFarmasi();
            }
        });


        farmasiPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJabatanFarmasi();
            }
        });
        btnFarmasiPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataJabatanFarmasi();
            }
        });
        spinerPilihPosisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataTipeJabatanFarmasi();
            }
        });
        btnPilihPosisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataTipeJabatanFarmasi();
            }
        });


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();


        etBirthday.setOnClickListener(this);
        btnSaveUpdateProfile.setOnClickListener(this);
        rtDr.setOnClickListener(this);
        rtProf.setOnClickListener(this);
        rtDR.setOnClickListener(this);
        rtDrg.setOnClickListener(this);


        getActivity().getWindow().setSoftInputMode(WindowManager.
                LayoutParams.SOFT_INPUT_STATE_HIDDEN);



        btnChangePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Toast.makeText(getActivity(), "x", Toast.LENGTH_SHORT).show();
                chooseChangePhoto();
            }
        });
        return v;
    }

    private void aksiBtnEditProfile(){
        allFormGone();
        if (sessionBtnEditProfile==0){
            iconName.setVisibility(View.GONE);
            iconGender.setVisibility(View.GONE);
            iconBirthday.setVisibility(View.GONE);
            iconAddress.setVisibility(View.GONE);
            iconPhone.setVisibility(View.GONE);
            iconEmail.setVisibility(View.GONE);

            etName.setVisibility(View.GONE);
            rgGender.setVisibility(View.GONE);
            etBirthday.setVisibility(View.GONE);
            etAddressUser.setVisibility(View.GONE);
            etPrefixPhone.setVisibility(View.GONE);
            etPhoneNumber.setVisibility(View.GONE);
            etEmail.setVisibility(View.GONE);
            btnUserType.setVisibility(View.GONE);
            spinerUserType.setVisibility(View.GONE);
            iconType.setVisibility(View.GONE);
            btnSaveUpdateProfile.setVisibility(View.GONE);

            iconViewName.setVisibility(View.VISIBLE);
            iconViewGender.setVisibility(View.VISIBLE);
            iconViewBirthday.setVisibility(View.VISIBLE);
            iconViewAddress.setVisibility(View.VISIBLE);
            iconViewPhone.setVisibility(View.VISIBLE);
            iconViewEmail.setVisibility(View.VISIBLE);

            textName.setVisibility(View.VISIBLE);
            textGender.setVisibility(View.VISIBLE);
            textBirthday.setVisibility(View.VISIBLE);
            textAddress.setVisibility(View.VISIBLE);
            textPhone.setVisibility(View.VISIBLE);
            textEmail.setVisibility(View.VISIBLE);
        }else if (sessionBtnEditProfile==1){
            etName.setVisibility(View.VISIBLE);
            rgGender.setVisibility(View.VISIBLE);
            etBirthday.setVisibility(View.VISIBLE);
            etAddressUser.setVisibility(View.VISIBLE);
            etPrefixPhone.setVisibility(View.VISIBLE);
            etPhoneNumber.setVisibility(View.VISIBLE);
            etEmail.setVisibility(View.VISIBLE);
            btnSaveUpdateProfile.setVisibility(View.VISIBLE);

            iconName.setVisibility(View.VISIBLE);
            iconGender.setVisibility(View.VISIBLE);
            iconBirthday.setVisibility(View.VISIBLE);
            iconAddress.setVisibility(View.VISIBLE);
            iconPhone.setVisibility(View.VISIBLE);
            iconEmail.setVisibility(View.VISIBLE);


            iconViewName.setVisibility(View.GONE);
            iconViewGender.setVisibility(View.GONE);
            iconViewBirthday.setVisibility(View.GONE);
            iconViewAddress.setVisibility(View.GONE);
            iconViewPhone.setVisibility(View.GONE);
            iconViewEmail.setVisibility(View.GONE);

            textName.setVisibility(View.GONE);
            textGender.setVisibility(View.GONE);
            textBirthday.setVisibility(View.GONE);
            textAddress.setVisibility(View.GONE);
            textPhone.setVisibility(View.GONE);
            textEmail.setVisibility(View.GONE);

            btnUserType.setVisibility(View.VISIBLE);
            spinerUserType.setVisibility(View.VISIBLE);
            iconType.setVisibility(View.VISIBLE);

            changeView(role);
        }
    }

    private void loadDataUser(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",sessionNoHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueProfileUser> dataCall = apiInterface.getProfileUser(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueProfileUser>() {
            @Override
            public void onResponse(Call<ValueProfileUser> call, Response<ValueProfileUser> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    set id spiner
                    rumahSakit= response.body().getData().getRumahsakit_id();
                    officeName=response.body().getData().getId_perusahaan();

                    spesialiss=response.body().getData().getSpesialis_id();
                    idiCabang = response.body().getData().getCabang_id();
                    idiWilayah = response.body().getData().getCabang_id();
//                    universitasResident = response.body().getData().;
                    universitasMhs = response.body().getData().getUnversitas_id();



                //  umum
                    etName.setText(response.body().getData().getName());
                    if (response.body().getData().getGender().equalsIgnoreCase("Female")){
                        rgFemale.setChecked(true);
                    }else if (response.body().getData().getGender().equalsIgnoreCase("Male")){
                        rgMale.setChecked(true);
                    }
                    etBirthday.setText(response.body().getData().getBirthday().substring(0,10));
                    etAddressUser.setText(response.body().getData().getAddress());
                    etPhoneNumber.setText(response.body().getData().getPhone().substring(2));
                    etEmail.setText(response.body().getData().getEmail());
                    etPrefixPhone.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
                        @Override
                        public void onCountrySelected(Country selectedCountry) {
                            prefixPhone=selectedCountry.getPhoneCode();
                        }
                    });
                    PhoneNumberUtils.formatNumber(etPhoneNumber.getText().toString(),"+"+response.body().getData().getPhone().substring(0,2));


                    textName.setText(response.body().getData().getName());
                    textGender.setText(response.body().getData().getGender());
                    textBirthday.setText(response.body().getData().getBirthday().substring(0,10));
                    textAddress.setText(response.body().getData().getAddress());
                    textPhone.setText(response.body().getData().getPhone().substring(2));
                    textEmail.setText(response.body().getData().getEmail());


                    //spesifik
                    if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")){
                        spinerIDIwilayah.setText(response.body().getData().getWilayah());
                        spinerSpesificTitle.setText(response.body().getData().getSpesialis());
                        gelarBelakang=response.body().getData().getGelar_belakang();
                        spinerIDICabang.setText(response.body().getData().getCabang());
                        loadCabangIDI();
                    }else if (role.equalsIgnoreCase("4")){
                        spinerSpesificTitle.setText(response.body().getData().getSpesialis());
                        spinerUnivResident.setText(response.body().getData().getUnversitas());

                    }else if (role.equalsIgnoreCase("5")){
                        spinerUnivMahasiswa.setText(response.body().getData().getUnversitas());

                    }else if (role.equalsIgnoreCase("6")){
//                        etAcountName

                    }else if (role.equalsIgnoreCase("7")){
                        spinerPerusahaanFarmasi.setText(response.body().getData().getNama_perusahaan());
                        spinerPilihPosisi.setText(response.body().getData().getTipe_jabatan());
                        etFarmasiAddress.setText(response.body().getData().getAlamat_kantor());
                        farmasiPosition.setText(response.body().getData().getJabatan());
                        loadJabatanFarmasi();
                    }else if (role.equalsIgnoreCase("8")){
                        etOtherName.setText(response.body().getData().getNama_perusahaan());
                        etOtherAdress.setText(response.body().getData().getAlamat_kantor());
                    }
                    eoName= spinerEoName.getText().toString();
                    jabatan=farmasiPosition.getText().toString();

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

                progressDialog.dismiss();
            }


            @Override
            public void onFailure(Call<ValueProfileUser> call, Throwable t) {
                progressDialog.dismiss();
                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }

    boolean isNumber(String string) {
        try {
            int amount = Integer.parseInt(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT,this,year,month,day);

            calendar.add(Calendar.YEAR, -20);
            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
//            calendar.add(Calendar.YEAR, -20);
//            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
            return  dpd;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            // Do something with the chosen date
            TextView tv = (TextView) getActivity().findViewById(R.id.etBirthday);

            // Create a Date variable/object with user chosen date
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day, 0, 0, 0);
            Date chosenDate = cal.getTime();


            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = formatDate.format(chosenDate);

            // Display the chosen date to app interface
            tv.setText(formattedDate);
        }
    }
    public void hideSoftKeyboard() {
        if(getActivity().getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getActivity().getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getActivity().getCurrentFocus().getWindowToken(), 0);
        }
    }

    String gelarBelakang="";
    private void checkDataSpesialisasi() {
        if (!spesialis.isEmpty()) {
            final String[] arrData =new String[spesialis.size()];
            for (int i = 0; i < spesialis.size(); i++) {
                arrData[i] = spesialis.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Spesialisasi");

            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerSpesificTitle.setText(spesialis.get(position).getName());
                    spesialiss=spesialis.get(position).getId();
                    gelarBelakang=spesialis.get(position).getGelar();
                    if (gelarBelakang==null){
                        gelarBelakang="x";
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }else {
            loadSpesialis(role);
        }
    }

    private void checkDataFarmasi() {
        if (!farmasi.isEmpty()) {
            final String[] arrData =new String[farmasi.size()];
            for (int i = 0; i < farmasi.size(); i++) {
                arrData[i] = farmasi.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Farmasi");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerPerusahaanFarmasi.setText(farmasi.get(position).getName());
                    officeName=farmasi.get(position).getId();
                    officeOther=farmasi.get(position).getName();
                    if (farmasi.get(position).getName().equalsIgnoreCase("others")){
                        otherFarmasiName.setVisibility(View.VISIBLE);
                        iconOtherFarmasiName.setVisibility(View.VISIBLE);
                    }else {
                        otherFarmasiName.setVisibility(View.GONE);
                        iconOtherFarmasiName.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataTipeJabatanFarmasi() {
        if (!tipeJabatanFarmasi.isEmpty()) {
            final String[] arrData =new String[tipeJabatanFarmasi.size()];
            for (int i = 0; i < tipeJabatanFarmasi.size(); i++) {
                arrData[i] = tipeJabatanFarmasi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Tipe Jabatan");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerPilihPosisi.setText(tipeJabatanFarmasi.get(position));
//                    tipePosisi=tipeJabatanFarmasi.get(position);

                    loadJabatanFarmasi();
                    farmasiPosition.setVisibility(View.VISIBLE);
                    btnFarmasiPosition.setVisibility(View.VISIBLE);
                    iconFarmasiPosition.setVisibility(View.VISIBLE);

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataJabatanFarmasi() {
        if (!jabatanFarmasi.isEmpty()) {
            final String[] arrData =new String[jabatanFarmasi.size()];
            for (int i = 0; i < jabatanFarmasi.size(); i++) {
                arrData[i] = jabatanFarmasi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Jabatan");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    farmasiPosition.setText(jabatanFarmasi.get(position));
                    jabatan=jabatanFarmasi.get(position);
                    if (jabatan.equalsIgnoreCase("others")){
                        farmasiPositionOther.setVisibility(View.VISIBLE);
                        iconFarmasiPositionOther.setVisibility(View.VISIBLE);
                    }else {
                        farmasiPositionOther.setVisibility(View.GONE);
                        iconFarmasiPositionOther.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataUniversitas(final String status) {
        if (!universitas.isEmpty()) {
            final String[] arrData =new String[universitas.size()];
            for (int i = 0; i < universitas.size(); i++) {
                arrData[i] = universitas.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Universitas");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    if (status.equalsIgnoreCase("mahasiswa")){
                        spinerUnivMahasiswa.setText(universitas.get(position).getName());
                        universitasMhs=universitas.get(position).getId();
                    }else if (status.equalsIgnoreCase("resident")){
                        spinerUnivResident.setText(universitas.get(position).getName());
                        universitasResident=universitas.get(position).getId();

                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataIdiWilayah() {
        if (!wilayahIdi.isEmpty()) {
            final String[] arrData =new String[wilayahIdi.size()];
            for (int i = 0; i < wilayahIdi.size(); i++) {
                arrData[i] = wilayahIdi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Wilayah IDI");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerIDIwilayah.setText(wilayahIdi.get(position));
                    idiWilayah=wilayahIdi.get(position);
                    loadCabangIDI();
                    spinerIDICabang.setVisibility(View.VISIBLE);
                    btnIDICabang.setVisibility(View.VISIBLE);
                    iconKecamatan.setVisibility(View.VISIBLE);

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataIdiCabang() {
        if (!cabangIdi.isEmpty()) {
            final String[] arrData =new String[cabangIdi.size()];
            for (int i = 0; i < cabangIdi.size(); i++) {
                arrData[i] = cabangIdi.get(i).getCabang();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Cabang IDI");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerIDICabang.setText(cabangIdi.get(position).getCabang());
                    idiCabang=cabangIdi.get(position).getId();

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }else {
        }
    }

    private void checkDataJurusan() {
        if (!hospital.isEmpty()) {
            final String[] arrData =new String[hospital.size()];
            for (int i = 0; i < hospital.size(); i++) {
                arrData[i] = hospital.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Jurusan");
            final CharSequence[] data = { "Belum Ada data"};
            builder.setItems(data, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
//
//                            spinerResidentHospital.setText(hospital.get(position).getName());
//                            changeView(hospital.get(position).getId());
//                            rumahSakit=hospital.get(position).getId();
//
                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataEo() {
        if (!eventOrganizer.isEmpty()) {
            final String[] arrData =new String[eventOrganizer.size()];
            for (int i = 0; i < eventOrganizer.size(); i++) {
                arrData[i] = eventOrganizer.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih EO Name");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerEoName.setText(eventOrganizer.get(position).getName());
                    eoName=eventOrganizer.get(position).getName();
                    if (eoName.equalsIgnoreCase("Others")) {
                        etEoOther.setVisibility(View.VISIBLE);
                        iconEoOther.setVisibility(View.VISIBLE);
                    }else {
                        etEoOther.setVisibility(View.GONE);
                        iconEoOther.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void changeView(String ID){
        switch (ID){
            case "2"  :
                AktifDokter();
                spinerUserType.setText("Dokter");
                break;
            case "3"  :
                AktifDrg();
                spinerUserType.setText("Dokter");
                break;
            case "4"  :
                spinerUserType.setText("Resident");
                AktifResident();
                break;
            case "5"  :
                spinerUserType.setText("Mahasiswa");
                AktifMahasiswa();
                break;
            case "6"  :
                spinerUserType.setText("Event Organizer");
                AktifEO();
                break;
            case "7"  :
                spinerUserType.setText("Farmasi/Alkes");
                AktifFarmasi();
                break;
            case "8"  :
                spinerUserType.setText("Others");
                AktifOther();
                break;

        }
    }

    String gelarDepan;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.etBirthday:
//                showCalendar();
                // Initialize a new date picker dialog fragment
                DialogFragment dFragment = new MenuRegister.DatePickerFragment();

                // Show the date picker dialog fragment
                dFragment.show(getActivity().getSupportFragmentManager(), "Date Picker");
                hideSoftKeyboard();
                break;
            case R.id.btnSaveUpdateProfile:
                if (checkReg()){
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.show();

                    saveUpdateProfile();

                }else {
                    showAllert("Message","tolong isi semua form yang dibutuhkan dengan benar","none");
                }


                break;

            case R.id.rtDr:
                if (role.equalsIgnoreCase("4")){
                    spinerSpesificTitle.setText("Pilih Pendidikan Spesialis");
                    spesialiss="Pilih Pendidikan Spesialis";
                }else {
                    spinerSpesificTitle.setText("pilih spesifik gelar");
                    spesialiss="pilih spesifik gelar";
                }
                if(!rtDrg.isChecked() && !rtDr.isChecked()|| role.equalsIgnoreCase("5")){
                    spinerSpesificTitle.setVisibility(View.GONE);
                    btnSpesificTitle.setVisibility(View.GONE);
                    iconSpesificTitle.setVisibility(View.GONE);
                }else {

                    spinerSpesificTitle.setVisibility(View.VISIBLE);
                    btnSpesificTitle.setVisibility(View.VISIBLE);
                    iconSpesificTitle.setVisibility(View.VISIBLE);

                    if (role.equalsIgnoreCase("4")){
                        tipe="Dr";
                        roleSpesialis="3";
                    }else {
                        roleSpesialis="2";
                    }
                    loadSpesialis(roleSpesialis);
                }

                if (rtDrg.isChecked()){
                    rtDrg.setChecked(false);

                }
                break;
            case R.id.rtDrg:
                if (role.equalsIgnoreCase("4")){
                    spinerSpesificTitle.setText("Pilih Pendidikan Spesialis");
                    spesialiss="Pilih Pendidikan Spesialis";
                }else {
                    spinerSpesificTitle.setText("pilih spesifik gelar");
                    spesialiss="pilih spesifik gelar";
                }
                if(!rtDrg.isChecked() && !rtDr.isChecked()|| role.equalsIgnoreCase("5")){
                    spinerSpesificTitle.setVisibility(View.GONE);
                    btnSpesificTitle.setVisibility(View.GONE);
                    iconSpesificTitle.setVisibility(View.GONE);
                }else {
                    spinerSpesificTitle.setVisibility(View.VISIBLE);
                    btnSpesificTitle.setVisibility(View.VISIBLE);
                    iconSpesificTitle.setVisibility(View.VISIBLE);
                    if (role.equalsIgnoreCase("4")){
                        tipe="Drg";
                        roleSpesialis="2";
                    }else {
                        roleSpesialis="3";
                    }
                    loadSpesialis(roleSpesialis);
                }

                if (rtDr.isChecked()){
                    rtDr.setChecked(false);

                }
                break;
            case R.id.rtProf:
                loadSpesialis(roleSpesialis);
                break;
            case R.id.rtDR:
                loadSpesialis(roleSpesialis);
                break;

        }
    }

    private void chooseChangePhoto() {
        final String[] arrData = {"Camera","Gallery"};

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Pilih Foto");
        builder.setItems(arrData, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                if (arrData[position].equalsIgnoreCase("Camera")){
                    if (checkPermissions(getActivity())) {
                        captureImage();

                    } else {
                        requestPermissions("camera");
                    }

//
                }else if (arrData[position].equalsIgnoreCase("Gallery")){
                    if (checkPermissions(getActivity())) {
                        Intent intent = new Intent();
                        intent.setType("image/*");
                        intent.setAction(Intent.ACTION_GET_CONTENT);
                        startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER_BUKTI);

                    } else {
                        requestPermissions("gallery");
                    }
                }

            }
        });
        // Create the alert dialog
        AlertDialog dialog = builder.create();
        // Get the alert dialog ListView instance
        ListView listView = dialog.getListView();
        // Set the divider color of alert dialog list view
        listView.setDivider(new ColorDrawable(Color.GRAY));
        // Set the divider height of alert dialog list view
        listView.setDividerHeight(1);
        // Finally, display the alert dialog
        dialog.show();

    }

    private static final int REQUEST_CAMERA_PERMISSION = 786;
    String type;
//    @Override
//    public void onRequestPermissionsResult(@NonNull int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//
//        if (requestCode == REQUEST_CAMERA_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            Toast.makeText(getActivity(), "berhasil", Toast.LENGTH_SHORT).show();
//            if (type.equalsIgnoreCase("camera")){
//                captureImage();
//            }else if (type.equalsIgnoreCase("gallery")){
//                Intent intent = new Intent();
//                intent.setType("image/*");
//                intent.setAction(Intent.ACTION_GET_CONTENT);
//                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER_BUKTI);
//            }
//        }else {
//            Toast.makeText(getActivity(), "gagal ", Toast.LENGTH_SHORT).show();
//        }
//
//    }

    private void requestPermissions(String from) {
        String[] PERMISSIONS = {
                android.Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE,
                android.Manifest.permission.CAMERA
        };

        requestPermissions( PERMISSIONS, REQUEST_CAMERA_PERMISSION);

        type=from;

        if (checkPermissions(getActivity())){
            if (type.equalsIgnoreCase("camera")){
                captureImage();
            }else if (type.equalsIgnoreCase("gallery")){
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER_BUKTI);
            }
        }else {
        }
    }



    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }
    public static boolean checkPermissions(Context context) {
        String[] permissions = {Manifest.permission.CAMERA,
                Manifest.permission.INTERNET,

                Manifest.permission.WRITE_EXTERNAL_STORAGE,
                Manifest.permission.READ_EXTERNAL_STORAGE};
        ActivityCompat.requestPermissions((Activity) context,
                permissions,
                1212);
        return ActivityCompat.checkSelfPermission(context, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                ;
    }
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            checkPermissions(context);
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }

        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPathFromURI(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    private void captureImage() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        File file = CameraUtils.getOutputMediaFile();
        if (file != null) {
            imageStoragePath = file.getAbsolutePath();
        }

        Uri fileUri = CameraUtils.getOutputMediaFileUri(getActivity(), file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);


        // start the image capture Intent
        startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }
    int isCamera=99;
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        System.out.println("DIAKSES HASILNYA "+requestCode);
        System.out.println("DIAKSES res code OK "+resultCode);
        System.out.println("DIAKSES res ok "+RESULT_OK+" RES ACT OK "+Activity.RESULT_OK);

//        getFileImage(data);
        if (resultCode == Activity.RESULT_OK) {
            System.out.println("DIAKSES HASILNYA OK "+resultCode);
            switch (requestCode) {
                case 1211:
                    isCamera=1;
                    previewCapturedImage();
                    break;
                case 1212:
                    isCamera=0;
                    getFileImage(data);
                    break;

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    String imageAds;
    Uri uriImage;
    private void getFileImage(Intent data) {
        uriImage = data.getData();
        imageAds = uriImage.toString();
        System.out.println("PATH IMAGE " + imageAds);
        int filesize = 0;

        InputStream inputStream = null;
        try {
            inputStream = getActivity().getContentResolver().openInputStream(uriImage);
            filesize = inputStream.available();
        } catch (FileNotFoundException e) {
            return;
        } catch (IOException e) {
            return;
        } finally {
            try {
                inputStream.close();
            } catch (IOException e) {/*blank*/}
        }
        if (filesize != 0) {
            int fileInKB = filesize / 1024;
            if (fileInKB > 500) {
                showAllert("Informasi", "File gambar ads tidak boleh lebih dari 500KB","none");

            } else {
                backgroundLoading.setVisibility(View.VISIBLE);
                loadingChangePhoto.setVisibility(View.VISIBLE);
                Handler handler = new Handler();
                handler.postDelayed(new Runnable() {
                    public void run() {
                        Bitmap bitmap=null;
                        aksiUploadProfile(bitmap);

                    }
                }, 2000);
//                lakukan pengiriman image disini

            }
        }
    }




    private Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = image.getWidth();
        int height = image.getHeight();

        float bitmapRatio = (float)width / (float) height;
        if (bitmapRatio > 1) {
            width = maxSize;
            height = (int) (width / bitmapRatio);
        } else {
            height = maxSize;
            width = (int) (height * bitmapRatio);
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }
    public static int sizeOf(Bitmap data) {
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB_MR1) {
            return data.getRowBytes() * data.getHeight();
        } else if (Build.VERSION.SDK_INT<Build.VERSION_CODES.KITKAT){
            return data.getByteCount();
        } else{
            return data.getAllocationByteCount();
        }
    }


    private void previewCapturedImage() {
        try {

            CameraUtils.refreshGallery(getActivity(), imageStoragePath);
            // bimatp factory
            BitmapFactory.Options options = new BitmapFactory.Options();
            // down sizing image as it throws OutOfMemory Exception for larger
            // images
            options.inSampleSize = 4;

            final Bitmap bitmap = CameraUtils.optimizeBitmap(options.inSampleSize, imageStoragePath);
            System.out.println("asd bitmap widht "+bitmap.getWidth());
            System.out.println("asd bitmap height "+bitmap.getHeight());
            System.out.println("asd bitmap size "+sizeOf(bitmap));
            final Bitmap bitmap2 = getResizedBitmap(bitmap,300);
            System.out.println("asd bitmap 2 widht "+bitmap2.getWidth());
            System.out.println("asd bitmap 2 height "+bitmap2.getHeight());
            System.out.println("asd bitmap 2 size "+sizeOf(bitmap2));


            backgroundLoading.setVisibility(View.VISIBLE);
            loadingChangePhoto.setVisibility(View.VISIBLE);
            Handler handler = new Handler();
            handler.postDelayed(new Runnable() {
                public void run() {
                    Uri uri=null;
                    aksiUploadProfile(bitmap2);
                }
            }, 2000);

            //kirim image ke database
        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }





    public static Bitmap RotateBitmap(Bitmap source, float angle)
    {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
    }
    private void aksiUploadProfile(final Bitmap fileBitmap){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line


                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_UPDATE_PROFILE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        ApiService apiInterface = retrofit.create(ApiService.class);
        final SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);



        } catch (JSONException e) {
            e.printStackTrace();
        }


        File sourceFile = null;
        if (isCamera==0){
            String path="";
            String value =getPathFromURI(getActivity(),uriImage);
            if (value!=null){
                path=value;
                sourceFile = new File(path);
            }
        }else if (isCamera==1) {

            sourceFile = new File(imageStoragePath);
        }
        System.out.println("file "+sourceFile.getAbsoluteFile());
        RequestBody reqBody = RequestBody.create(MediaType.parse("multipart/form-file"), sourceFile);
        MultipartBody.Part partImage = MultipartBody.Part.createFormData("images",sourceFile.getName(),reqBody);
        RequestBody req = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),paramObject.toString());
        final MultipartBody.Part body = MultipartBody.Part.createFormData("body", paramObject.toString());
        Call<ValueUpdatePhoto> data = apiInterface.updateProfileImage(partImage,body,"saxasdas");

        data.enqueue(new Callback<ValueUpdatePhoto>() {
            @Override
            public void onResponse(Call<ValueUpdatePhoto> call, Response<ValueUpdatePhoto> response) {
                System.out.println("RESPONSE "+response.errorBody()+" rc "+response.code());
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    if (isCamera==0) {
                        imageUser.setImageURI(uriImage);
                    }else{
                        imageUser.setImageBitmap(fileBitmap);

//                        Picasso.with(getActivity()).load(response.body().getData().getImage())
//                                .placeholder(R.drawable.icon_account_name).error(R.drawable.ic_launcher)
//                                .into(imageUser);
                    }
                    backgroundLoading.setVisibility(View.GONE);
                    loadingChangePhoto.setVisibility(View.GONE);
                    sessionUser.setProfile(getContext(),"image",response.body().getData().getImage());

                }else {
                    showAllert("Informasi", response.body().getMessage(),"none");
                }


                System.out.println("Informasi sukses "+response.body().getMessage());



            }

            @Override
            public void onFailure(Call<ValueUpdatePhoto> call, Throwable t) {
                backgroundLoading.setVisibility(View.GONE);
                loadingChangePhoto.setVisibility(View.GONE);
                showAllert("Informasi",t.getMessage(),"none");



                System.out.println("Informasi "+t.getMessage());
            }

        });
    }
    private boolean checkReg(){
        boolean validate=true;
        if (!etName.getText().toString().isEmpty()){
            name = etName.getText().toString();
        }else {
            validate=false;

            System.out.println("VALIDASI : NAMA");
        }
        if (!etBirthday.getText().toString().isEmpty()){
            birthdayDate = etBirthday.getText().toString();
        }else {
            birthdayDate="";
            validate=false;

            System.out.println("VALIDASI : Birthday");

        }
        if (!etEmail.getText().toString().isEmpty()){
            if (isEmailValid(etEmail.getText().toString())) {
                email = etEmail.getText().toString();
            }else {
                validate=false;
                System.out.println("VALIDASI : Email");
            }
        }else {
            validate=false;
            System.out.println("VALIDASI : Email");

        }
        if (!etAddressUser.getText().toString().isEmpty()){
            userAddress = etAddressUser.getText().toString();
        }else {
            userAddress="";

        }

        String phoneNumber =etPhoneNumber.getText().toString().replace("-","");
        if (!phoneNumber.isEmpty()){
            if (phoneNumber.startsWith("8")) {
                noHp = prefixPhone + phoneNumber;
//                Toast.makeText(this, noHp, Toast.LENGTH_SHORT).show();


            }else if(phoneNumber.startsWith("08")){

//                noHp = "62" + etPhoneNumber.getText().toString().substring(1);
                validate=false;
                System.out.println("VALIDASI : phone number");
            }else {
                validate = false;
                System.out.println("VALIDASI : phone number");
            }


        }else {
            validate = false;
            System.out.println("VALIDASI : phone number");

        }

        int rgGenderSelected = rgGender.getCheckedRadioButtonId();
        if (rgGenderSelected != -1)
        {
            RadioButton selectedRadioButton = (RadioButton) v.findViewById(rgGenderSelected);
            gender = selectedRadioButton.getText().toString();

        }
        else
        {
            validate=false;
            System.out.println("VALIDASI : Gender");
        }

        if (role.equalsIgnoreCase("2")||role.equalsIgnoreCase("3")){
            //jika pilih user type Dokter dan drg
            if (gelarBelakang.equalsIgnoreCase("")|| gelarBelakang.isEmpty()){
                validate=false;
                System.out.println("VALDIDASI : gelar belakang");
            }
            if (idiCabang.isEmpty() || idiCabang.equalsIgnoreCase("")||
                    idiCabang.equalsIgnoreCase("Pilih Cabang IDI")){
                validate=false;
                System.out.println("VALIDASI : Idi Cabang");

            }

            //check rg title dan set value gelar
            gelarDepan="";
            if (rtProf.isChecked()){
                gelarDepan=gelarDepan+"Prof. ";
            }
            if (rtDR.isChecked()){
                gelarDepan=gelarDepan+"DR. ";
            }
            if (rtDr.isChecked()){
                gelarDepan=gelarDepan+"Dr. ";
            }
            if (rtDrg.isChecked()){
                gelarDepan=gelarDepan+"Drg. ";

            }


            if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : gelar depan");
            }
            if (gelarBelakang.equalsIgnoreCase("x")){
                name= gelarDepan+name;
            }else {
                name= gelarDepan+name+", "+gelarBelakang;
            }

            //check spiner title spesialis dan set value spesialis
            if (spesialiss.equalsIgnoreCase("pilih spesifik gelar") ||
                    spesialiss.equalsIgnoreCase("Pilih Pendidikan Spesialis") ||
                    spesialiss.isEmpty() || spesialiss.equalsIgnoreCase("")){
                validate=false;
                System.out.println("VALIDASI : spesialis");
            }

        }else if (role.equalsIgnoreCase("4")){
            //Resident
            //check rg title dan set value gelar
            gelarDepan="";
            if (rtProf.isChecked()){
                gelarDepan="Prof. ";
            }
            if (rtDR.isChecked()){
                gelarDepan=gelarDepan+"DR. ";
            }
            if (rtDr.isChecked()){
                gelarDepan=gelarDepan+"Dr. ";
            }
            if (rtDrg.isChecked()){
                gelarDepan=gelarDepan+"Drg. ";
            }


            if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : gelar depan");
            }
            name= gelarDepan+name;
            //check spiner title spesialis dan set value spesialis
            if (spesialiss.equalsIgnoreCase("pilih spesifik gelar") ||
                    spesialiss.isEmpty() || spesialiss.equalsIgnoreCase("")){
                validate=false;
                System.out.println("VALIDASI : spesialis");
            }
            //check spiner hospital dan set value rumahsakit
            rumahSakit= residentHospital.getText().toString();
            if (rumahSakit.equalsIgnoreCase("") || rumahSakit.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : rumah sakit resident");

            }
//            rumahSakit =adapterSpinerHospital.getIDFromIndex(spinerResidentHospital.getSelectedItemPosition());

//            check spiner univResident dan set value universitas
            if (universitasResident.equalsIgnoreCase("pilih universitas")){
                validate=false;

                System.out.println("VALIDASI : universitas resident");
            }


        }else if (role.equalsIgnoreCase("5")){
            //Mahasiswa

            gelarDepan="";

            if (rtDr.isChecked()){
                gelarDepan=gelarDepan+"Dr. ";
            }
            if (rtDrg.isChecked()){
                gelarDepan=gelarDepan+"Drg. ";
            }


            if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : gelar depan");
            }
            //check spiner title spesialis dan set value spesialis
//            spesialiss ="1";

            //check spiner univMahasiswa dan set value universitas
            if (universitasMhs.equalsIgnoreCase("pilih universitas")){
                validate=false;

                System.out.println("VALIDASI : universitas resident");
            }


            if (rumahSakitMhs.getText().toString().isEmpty()){
                validate=false;
                System.out.println("VALIDASI : rumah sakit mahasiswa");
            }
        }else if (role.equalsIgnoreCase("6")){
            //EO

            //check edittext eo name dan set value eoName
            if (eoName.equalsIgnoreCase("Pilih EO Name")){
                validate=false;

                System.out.println("VALIDASI : EO name");
            }else if (eoName.equalsIgnoreCase("Others")){
                if (etEoOther.getText().toString().isEmpty()){
                    validate=false;
                }else {
                    eoName=etEoOther.getText().toString();
                }
            }

            //check edittext eo address dan set value eoAddress
            if (!etEoAdress.getText().toString().isEmpty()){
                eoAddress = etEoAdress.getText().toString();
            }else {
                validate=false;
            }

            //check edittext norek dan set value accountNumber
            if (!etNoRek.getText().toString().isEmpty()){
                accountNumber = etNoRek.getText().toString();
            }else {
                validate=false;
            }

            //check edittext eo acountName dan set value accountName
            if (!etAcountName.getText().toString().isEmpty()){
                accountName = etAcountName.getText().toString();
            }else {
                validate=false;
            }

            //check edittext noRek dan set value accountBank
            if (!etBank.getText().toString().isEmpty()){
                accountBank = etBank.getText().toString();
            }else {
                validate=false;
            }

            //check edittext noRek dan set value accountBranch
            if (!etCabangBank.getText().toString().isEmpty()){
                accountBranch = etCabangBank.getText().toString();
            }else {
                validate=false;
            }


        }else if (role.equalsIgnoreCase("7")){
            //farmasi

            //check spiner farmasiAlkes dan setValue officeName
            if (officeName.equalsIgnoreCase("Pilih Farmasi")){
                validate=false;
                System.out.println("VALIDASI : FARMASI");

            }else if (officeOther.equalsIgnoreCase("Others")){
                officeOther=otherFarmasiName.getText().toString();
                if (officeOther.equalsIgnoreCase("")){
                    validate=false;
                    System.out.println("VALIDASI : FARMASI");
                }
            }

            if (jabatan.equalsIgnoreCase("Pilih Jabatan")){
                validate=false;
                System.out.println("VALIDASI : JABATAN FARMASI");

            }else if (jabatan.equalsIgnoreCase("Others")){
                if (farmasiPositionOther.getText().toString().isEmpty()){
                    validate=false;
                    System.out.println("VALIDASI : JABATAN FARMASI");
                }else {
                    jabatan=farmasiPositionOther.getText().toString();
                }
            }

            //check spiner officeAddress dan setValue officeAddress
            if (!etFarmasiAddress.getText().toString().isEmpty()){
                officeAddress =etFarmasiAddress.getText().toString();
            }else {
                validate=false;
            }



        }else if (role.equalsIgnoreCase("8")){
            //others

            //check edittext noRek dan set value accountBranch
            if (!etOtherName.getText().toString().isEmpty()){
                namaOrganisasi = etOtherName.getText().toString();
            }else {
                validate=false;
            }
            //check edittext noRek dan set value accountBranch
            if (!etOtherAdress.getText().toString().isEmpty()){
                alamatOrganisasi = etOtherAdress.getText().toString();
            }else {
                validate=false;
            }
        }else {
            validate=false;
        }
        System.out.println("Selesai di eksekusi dengan nilai "+validate);
        return validate;
    }
    private void loadProfesi(){
        profesi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueProfesi> profesiCall = apiInterface.getAllProfesi(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueProfesi>() {
            @Override
            public void onResponse(Call<ValueProfesi> call, Response<ValueProfesi> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Profesi> prof;
                    prof = response.body().getData().getProfesi();

                    if (!prof.isEmpty()){
                        profesi.addAll(prof);

                    }

                    System.out.println("VALUENYA : "+profesi.toString());

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueProfesi> call, Throwable t) {

            }
        });
    }
    private void loadSpesialis(String role){

        spesialis.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
            paramObject.put("tipe",tipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueSpesialis> spesialisCall = apiInterface.getAllSpesialis(paramObject.toString(),"saxasdas");

        spesialisCall.enqueue(new Callback<ValueSpesialis>() {
            @Override
            public void onResponse(Call<ValueSpesialis> call, Response<ValueSpesialis> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<Spesialis> spesialisData;
                    spesialisData = response.body().getData().getSpesialis();

                    if (!spesialisData.isEmpty()){

                        for (int i=0;i<spesialisData.size();i++){
                            if (spesialisData.get(i).getName().equalsIgnoreCase("Dokter Gigi Umum")&&
                                    (rtDR.isChecked() || rtProf.isChecked())){
                                System.out.println("load diakses gigi");

                            }else if (spesialisData.get(i).getName().equalsIgnoreCase("Dokter Umum") &&
                                    (rtDR.isChecked() || rtProf.isChecked())){
                                System.out.println("load diakses umum");
                            }else {
                                spesialis.add(spesialisData.get(i));
                                System.out.println("load diakses setdata");
                            }

                        }

                    }else {

                        System.out.println("DATAKU KOSONG");
                    }

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueSpesialis> call, Throwable t) {
            }
        });
    }
    private void loadWilayahIDI(){
        wilayahIdi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueIdiWilayah> dataCall = apiInterface.getAllIdiWilayah(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueIdiWilayah>() {
            @Override
            public void onResponse(Call<ValueIdiWilayah> call, Response<ValueIdiWilayah> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        wilayahIdi.addAll(data);

                    }
                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueIdiWilayah> call, Throwable t) {
            }
        });
    }
    private void loadCabangIDI(){
//        spinerIDICabang.setText("Pilih Cabang IDI");
        cabangIdi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("search",spinerIDIwilayah.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueIdiCabang> dataCall = apiInterface.getAllIdiCabang(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueIdiCabang>() {
            @Override
            public void onResponse(Call<ValueIdiCabang> call, Response<ValueIdiCabang> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<IdiCabang> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        cabangIdi.addAll(data);

                    }

                }
                System.out.println("VALUENYA IDI CABANG : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueIdiCabang> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }
    private void loadTipeJabatan(){
        tipeJabatanFarmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_user","");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueJabatan> dataCall = apiInterface.getAllJabatanFarmasi(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJabatan>() {
            @Override
            public void onResponse(Call<ValueJabatan> call, Response<ValueJabatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        tipeJabatanFarmasi.addAll(data);

                    }

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueJabatan> call, Throwable t) {
            }
        });
    }
    private void loadJabatanFarmasi(){
        jabatanFarmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_user","");
            paramObject.put("search",spinerPilihPosisi.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueJabatan> dataCall = apiInterface.getAllJabatanFarmasi(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJabatan>() {
            @Override
            public void onResponse(Call<ValueJabatan> call, Response<ValueJabatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        jabatanFarmasi.addAll(data);

                    }

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueJabatan> call, Throwable t) {

            }
        });
    }
    private void loadFarmasi(){
        farmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueFarmasi> farmasiCall = apiInterface.getAllFarmasi(paramObject.toString(),"saxasdas");

        farmasiCall.enqueue(new Callback<ValueFarmasi>() {
            @Override
            public void onResponse(Call<ValueFarmasi> call, Response<ValueFarmasi> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<Farmasi> farmasiData;
                    farmasiData = response.body().getData().getFarmasi();

                    if (!farmasiData.isEmpty()){
                        farmasi.addAll(farmasiData);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueFarmasi> call, Throwable t) {
            }
        });
    }
    private void loadEoName(){
        eventOrganizer.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueEO> farmasiCall = apiInterface.getAllEO(paramObject.toString(),"saxasdas");

        farmasiCall.enqueue(new Callback<ValueEO>() {
            @Override
            public void onResponse(Call<ValueEO> call, Response<ValueEO> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<EO> eoData;
                    eoData = response.body().getData().getEvent_organizer();

                    if (!eoData.isEmpty()){
                        eventOrganizer.addAll(eoData);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueEO> call, Throwable t) {

            }
        });
    }
    private void loadHospital(){
        hospital.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueHospital> data = apiInterface.getAllHospital(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueHospital>() {
            @Override
            public void onResponse(Call<ValueHospital> call, Response<ValueHospital> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<Hospital> hospitalData;
                    hospitalData = response.body().getData().getHospital();

                    if (!hospitalData.isEmpty()){
                        hospital.addAll(hospitalData);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueHospital> call, Throwable t) {
            }
        });
    }
    private void loadUniversitas(){
        universitas.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueUniversitas> call = apiInterface.getAllUniversitas(paramObject.toString(),"saxasdas");

        call.enqueue(new Callback<ValueUniversitas>() {
            @Override
            public void onResponse(Call<ValueUniversitas> call, Response<ValueUniversitas> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<Universitas> universitasData;
                    universitasData = response.body().getData().getUniversitas();

                    if (!universitasData.isEmpty()){
                        universitas.addAll(universitasData);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueUniversitas> call, Throwable t) {
            }
        });
    }
    private void saveUpdateProfile(){

        SessionDevice sd = new SessionDevice();
        String fcm = sd.getTokenDevice(getActivity(),"token");
        String imei =sd.getImei(getActivity(),"imei");
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        try {
            paramObject = new JSONObject();
            paramObject.put("id_login", sessionNoHp);
            if (sessionNoHp.equalsIgnoreCase(noHp)){
                paramObject.put("phone_number", "");
            }else {
                paramObject.put("phone_number", noHp);
            }
            paramObject.put("name", name);
            paramObject.put("gender", gender);
            paramObject.put("birthday", birthdayDate);
            paramObject.put("address", userAddress);
            paramObject.put("kode_pos", postalCode);
            paramObject.put("no_telp", telpNumber);
            paramObject.put("no_fax", fax);
            paramObject.put("email", email);
            paramObject.put("imei", imei);
            paramObject.put("fcm_key", fcm);
            paramObject.put("role", role);
            if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("provinsi",provinsi);
                paramObject.put("kota",kota);
                paramObject.put("kecamatan",kecamatan);
                paramObject.put("kelurahan",kelurahan);
                paramObject.put("kodepos",kodepos);
                paramObject.put("gelar",gelarDepan);
                paramObject.put("id_cabang",idiCabang);

            }else if (role.equalsIgnoreCase("4")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("rumah_sakit",rumahSakit);
                paramObject.put("gelar",gelarDepan);
                paramObject.put("tipe",tipe);
                paramObject.put("universitas",universitasResident);

            }else if (role.equalsIgnoreCase("5")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("universitas",universitasMhs);
                paramObject.put("gelar",gelarDepan);

            }else if (role.equalsIgnoreCase("6")){
                paramObject.put("eo_name",eoName);
                paramObject.put("eo_address",eoAddress);
                paramObject.put("account_name",accountName);
                paramObject.put("account_number",accountNumber);
                paramObject.put("account_bank",accountBank);
                paramObject.put("account_branch",accountBranch);

            }else if (role.equalsIgnoreCase("7")){
                paramObject.put("office_name", officeName);
                paramObject.put("office_others", officeOther);
                paramObject.put("jabatan", jabatan);
                paramObject.put("office_address", officeAddress);
                paramObject.put("tipe_jabatan", spinerPilihPosisi.getText().toString());

            }else if (role.equalsIgnoreCase("8")){
                paramObject.put("nama_organisasi", namaOrganisasi);
                paramObject.put("alamat_organisasi", alamatOrganisasi);
            }

            Call<OtpVerifikasi> verCall = apiInterface.updateProfile(paramObject.toString(),"saxasdas");
            System.out.println("PARAMETER SEND "+paramObject.toString());
            verCall.enqueue(new Callback<OtpVerifikasi>() {
                @Override
                public void onResponse(Call<OtpVerifikasi> call, Response<OtpVerifikasi> response) {

                    progressDialog.dismiss();
                    if (response.body().getStatus().equalsIgnoreCase("success")){
                        Log.d("Informasi", response.body().getMessage());
                        SessionUser updateSessionUser=new SessionUser();
                        updateSessionUser.setNoHp(getActivity(),"nohp",noHp);
                        textPhone.setText(noHp);
                        updateSessionUser.setBirthday(getActivity(),"birthday",birthdayDate);
                        textBirthday.setText(birthdayDate);
                        updateSessionUser.setName(getActivity(),"name",name);
                        textName.setText(name);
                        txtNameUser.setText(name);
                        updateSessionUser.setEmail(getActivity(),"email",email);
                        textEmail.setText(email);
//                        updateSessionUser.setProfile(getActivity(),"image",valueUsers.getImage());
                        updateSessionUser.setGender(getActivity(),"gender",gender);
                        textGender.setText(gender);
                        updateSessionUser.setAlamat(getActivity(),"alamat",userAddress);
                        textAddress.setText(userAddress);
                        updateSessionUser.setKodePos(getActivity(),"kodepos",postalCode);
//                        updateSessionUser.setAlamatRs(getActivity(),"alamatRs",);
                        updateSessionUser.setNamaRs(getActivity(),"namaRs",rumahSakit);
                        showAllert("Informasi",response.body().getMessage(),"none");
                        sessionBtnEditProfile=0;
                        aksiBtnEditProfile();

                    }else {

                        showAllert("Update Profile Gagal",response.body().getMessage(),"none");
                    }
                }

                @Override
                public void onFailure(Call<OtpVerifikasi> call, Throwable t) {
                    progressDialog.dismiss();
                    showAllert("Update Profile Gagal","Gagal Koneksi ke server","none");
                    System.out.println("Informasi "+t.getMessage());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showAllert(String title, String message, final String action){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (action.equalsIgnoreCase("loadProfesi")){
                            loadProfesi();
                        }else if (action.equalsIgnoreCase("loadHospital")){
                            loadHospital();
                        }else if (action.equalsIgnoreCase("loadUniversitas")){
                            loadUniversitas();
                        }
                    }
                })
                .show();
    }
    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    private void uncheckAllTitle(){
        rtDr.setChecked(false);
        rtDR.setChecked(false);
        rtProf.setChecked(false);
        rtDrg.setChecked(false);
    }
    private void AktifDokter() {
        allFormGone();
        rtProf.setVisibility(View.VISIBLE);
        rtDR.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
        spinerIDIwilayah.setVisibility(View.VISIBLE);
        btnIDIwilayah.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
        iconDaerah.setVisibility(View.VISIBLE);
        spinerIDICabang.setVisibility(View.VISIBLE);
        btnIDICabang.setVisibility(View.VISIBLE);
        iconKecamatan.setVisibility(View.VISIBLE);


        spinerSpesificTitle.setVisibility(View.VISIBLE);
        btnSpesificTitle.setVisibility(View.VISIBLE);
        iconSpesificTitle.setVisibility(View.VISIBLE);

    }
    private void AktifDrg() {
        allFormGone();
        rtProf.setVisibility(View.VISIBLE);
        rtDR.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
        spinerSpesificTitle.setVisibility(View.VISIBLE);
        btnSpesificTitle.setVisibility(View.VISIBLE);
        spinerIDICabang.setVisibility(View.VISIBLE);
        btnIDICabang.setVisibility(View.VISIBLE);
        spinerIDIwilayah.setVisibility(View.VISIBLE);
        btnIDIwilayah.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
        iconSpesificTitle.setVisibility(View.VISIBLE);
        iconDaerah.setVisibility(View.VISIBLE);
        iconKecamatan.setVisibility(View.VISIBLE);


    }
    private void AktifResident() {
        allFormGone();
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
        residentHospital.setVisibility(View.VISIBLE);
        iconUnivResident.setVisibility(View.VISIBLE);
        spinerUnivResident.setVisibility(View.VISIBLE);
        btnUnivResident.setVisibility(View.VISIBLE);
        spinerSpesificTitle.setVisibility(View.VISIBLE);
        btnSpesificTitle.setVisibility(View.VISIBLE);
        iconSpesificTitle.setVisibility(View.VISIBLE);

        iconTitle.setVisibility(View.VISIBLE);
        iconResidentHospital.setVisibility(View.VISIBLE);
        uncheckAllTitle();
        gelarBelakang="";
    }
    private void AktifMahasiswa() {
        allFormGone();
        spinerUnivMahasiswa.setVisibility(View.VISIBLE);
        btnUnivMahasiswa.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
        iconUnivMahasiswa.setVisibility(View.VISIBLE);

        rumahSakitMhs.setVisibility(View.VISIBLE);
        iconRumahSakitMhs.setVisibility(View.VISIBLE);
    }
    private void AktifEO() {
        allFormGone();
        spinerEoName.setVisibility(View.VISIBLE);
        btnEoName.setVisibility(View.VISIBLE);
        etNoRek.setVisibility(View.VISIBLE);
        etAcountName.setVisibility(View.VISIBLE);
        etBank.setVisibility(View.VISIBLE);
        etCabangBank.setVisibility(View.VISIBLE);
        etEoAdress.setVisibility(View.VISIBLE);

        iconEoName.setVisibility(View.VISIBLE);
        iconNoRek.setVisibility(View.VISIBLE);
        iconAcountName.setVisibility(View.VISIBLE);
        iconBank.setVisibility(View.VISIBLE);
        iconCabangBank.setVisibility(View.VISIBLE);
        iconEOAddress.setVisibility(View.VISIBLE);
    }
    private void AktifFarmasi() {
        allFormGone();
        spinerPerusahaanFarmasi.setVisibility(View.VISIBLE);
        etFarmasiAddress.setVisibility(View.VISIBLE);

        btnPerusahaanFarmasi.setVisibility(View.VISIBLE);
        btnPilihPosisi.setVisibility(View.VISIBLE);
        spinerPilihPosisi.setVisibility(View.VISIBLE);
        iconPilihPosisi.setVisibility(View.VISIBLE);

        iconPerusahaanFarmasi.setVisibility(View.VISIBLE);
        iconFarmasiAddress.setVisibility(View.VISIBLE);


        farmasiPosition.setVisibility(View.VISIBLE);
        btnFarmasiPosition.setVisibility(View.VISIBLE);
        iconFarmasiPosition.setVisibility(View.VISIBLE);


    }
    private void AktifOther() {
        allFormGone();
        etOtherName.setVisibility(View.VISIBLE);
        etOtherAdress.setVisibility(View.VISIBLE);
        iconOtherName.setVisibility(View.VISIBLE);
        iconOtherAddress.setVisibility(View.VISIBLE);
    }
    private void allFormGone(){
        rtProf.setVisibility(View.GONE);
        rtDR.setVisibility(View.GONE);
        rtDrg.setVisibility(View.GONE);
        rtDr.setVisibility(View.GONE);
        spinerSpesificTitle.setVisibility(View.GONE);
        btnUnivMahasiswa.setVisibility(View.GONE);
        btnSpesificTitle.setVisibility(View.GONE);
//        textDaerah.setVisibility(View.GONE);
        spinerIDICabang.setVisibility(View.GONE);
        btnIDICabang.setVisibility(View.GONE);
        spinerIDIwilayah.setVisibility(View.GONE);
        btnIDIwilayah.setVisibility(View.GONE);
        etFarmasiAddress.setVisibility(View.GONE);
        spinerMedicalMajorMahasiswa.setVisibility(View.GONE);
        btnMedicalMajorMahasiswa.setVisibility(View.GONE);
        spinerPerusahaanFarmasi.setVisibility(View.GONE);
        btnPerusahaanFarmasi.setVisibility(View.GONE);
        spinerUnivMahasiswa.setVisibility(View.GONE);
        residentHospital.setVisibility(View.GONE);
//        btnResidentHospital.setVisibility(View.GONE);
        rumahSakitMhs.setVisibility(View.GONE);
        iconRumahSakitMhs.setVisibility(View.GONE);

        iconUnivResident.setVisibility(View.GONE);
        spinerUnivResident.setVisibility(View.GONE);
        btnUnivResident.setVisibility(View.GONE);


        spinerPilihPosisi.setVisibility(View.GONE);
        btnPilihPosisi.setVisibility(View.GONE);
        iconPilihPosisi.setVisibility(View.GONE);


        etEoOther.setVisibility(View.GONE);
        iconEoOther.setVisibility(View.GONE);

        iconOtherFarmasiName.setVisibility(View.GONE);
        otherFarmasiName.setVisibility(View.GONE);

        etAcountName.setVisibility(View.GONE);
        farmasiPosition.setVisibility(View.GONE);
        btnFarmasiPosition.setVisibility(View.GONE);
        etBank.setVisibility(View.GONE);
        etCabangBank.setVisibility(View.GONE);
        etOtherName.setVisibility(View.GONE);
        etOtherAdress.setVisibility(View.GONE);
        spinerEoName.setVisibility(View.GONE);
        btnEoName.setVisibility(View.GONE);
        etNoRek.setVisibility(View.GONE);
        etEoAdress.setVisibility(View.GONE);


        iconAcountName.setVisibility(View.GONE);
        iconBank.setVisibility(View.GONE);
        iconCabangBank.setVisibility(View.GONE);
        iconEoName.setVisibility(View.GONE);
        iconFarmasiAddress.setVisibility(View.GONE);
        iconFarmasiPosition.setVisibility(View.GONE);
        iconDaerah.setVisibility(View.GONE);
        iconKecamatan.setVisibility(View.GONE);
        iconSpesificTitleMahasiswa.setVisibility(View.GONE);
        iconNoRek.setVisibility(View.GONE);
        iconEOAddress.setVisibility(View.GONE);
        iconOtherName.setVisibility(View.GONE);
        iconOtherAddress.setVisibility(View.GONE);
        iconPerusahaanFarmasi.setVisibility(View.GONE);
        iconResidentHospital.setVisibility(View.GONE);
        iconSpesificTitle.setVisibility(View.GONE);
        iconTitle.setVisibility(View.GONE);
        iconUnivMahasiswa.setVisibility(View.GONE);
        farmasiPositionOther.setVisibility(View.GONE);
        iconFarmasiPositionOther.setVisibility(View.GONE);

    }


    @Override
    public void onOkClick(String s) {

    }

    @Override
    public void onDialogDismiss() {

    }

    public interface onUserChangeIcon{
        void onUserSet(Bitmap bitmap);
    }






}
