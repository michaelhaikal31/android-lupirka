package com.kriyanesia.lupirka.Menu;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kriyanesia.lupirka.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDetailKegiatanActivity extends Fragment {


    public MenuDetailKegiatanActivity() {
        // Required empty public constructor
    }

    View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_menu_detail_kegiatan, container, false);

        return view;
    }

}
