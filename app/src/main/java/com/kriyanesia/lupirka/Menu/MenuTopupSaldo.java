package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListNominalTopup;
import com.kriyanesia.lupirka.AdapterModel.AdapterRecyclePembayaran;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Pembayaran.ValueGenerateVA;
import com.kriyanesia.lupirka.Data.Pembayaran.ValueNominalTopup;
import com.kriyanesia.lupirka.Data.Pembayaran.ValuePembayaran;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranTopup;
import com.kriyanesia.lupirka.Data.Saldo.ValueCheckBalance;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuTopupSaldo extends Fragment implements AdapterListNominalTopup.ItemClickListener,AdapterRecyclePembayaran.ItemBankClickListener {


    public MenuTopupSaldo() {
        // Required empty public constructor
    }
    SessionUser sessionUser;
    String accountBank;
    String noHp;
    AdapterListNominalTopup adapterNominal;
    RecyclerView recyclerView;
    RelativeLayout layoutOtherNominal,layoutNominalTransaksi;
    SwipeRefreshLayout layoutRefresh;
    LinearLayout layoutHistoryTopup;
    List<ValueNominalTopup.NominalTopup> nominalTopup;
    FrameLayout layoutIsiSaldo;
    EditText etOtherNominal;
    TextView topupSaldoValue,saldoValue,valueNominalHistory,historyTopup,textSaldoSekarang;
    Button btnPilihOtherNominal,btnLanjutkan;
    ImageView iconKeterangan,iconKeteranganSaldo,garis;
    ScrollView scrollView;
    int saldo;
    private DatabaseHelper db;
    ProgressDialog progressDialog;
    Timestamp timestamp;
    ImageButton btnBack;
    ImageView btnReload;
    RecyclerView recycleMetodeBayar;
    TextView topupBiayaAdmValue,textTitlePembayaran;


    String title,totalPembayaran,vaNumber,expTime;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmss");
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_topup_saldo, container, false);
        recyclerView= (RecyclerView) v.findViewById(R.id.recyclerNominalTopup);
        layoutOtherNominal= (RelativeLayout) v.findViewById(R.id.layoutOtherNominal);
        layoutNominalTransaksi= (RelativeLayout) v.findViewById(R.id.layoutNominalTransaksi);
        layoutIsiSaldo= (FrameLayout) v.findViewById(R.id.layoutIsiSaldo);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);
        btnReload= (ImageView) v.findViewById(R.id.reloadData);
        layoutHistoryTopup= (LinearLayout) v.findViewById(R.id.layoutHistoryTopup);
        layoutRefresh=(SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        sessionUser=new SessionUser();
        noHp= sessionUser.getNoHp(getActivity(),"nohp");
        etOtherNominal= (EditText) v.findViewById(R.id.etOtherNominal);
        btnPilihOtherNominal= (Button) v.findViewById(R.id.btnPilihOtherNominal);
        btnLanjutkan= (Button) v.findViewById(R.id.btnLanjutkan);
        topupSaldoValue= (TextView) v.findViewById(R.id.topupSaldoValue);
        textSaldoSekarang= (TextView) v.findViewById(R.id.textSaldoSekarang);
        saldoValue= (TextView) v.findViewById(R.id.saldoValue);
        valueNominalHistory= (TextView) v.findViewById(R.id.valueNominalHistory);
        iconKeterangan= (ImageView) v.findViewById(R.id.iconKeterangan);
        iconKeteranganSaldo= (ImageView) v.findViewById(R.id.iconKeteranganSaldo);

        btnBack = (ImageButton) v.findViewById(R.id.btnBack);
        recycleMetodeBayar = (RecyclerView) v.findViewById(R.id.recycleMetodeBayar);
        textTitlePembayaran = (TextView) v.findViewById(R.id.textTitlePembayaran);
        topupBiayaAdmValue = (TextView) v.findViewById(R.id.topupBiayaAdmValue);


        historyTopup= (TextView) v.findViewById(R.id.historyTopup);
        garis= (ImageView) v.findViewById(R.id.garis3);

        checkBalance();
        timestamp = new Timestamp(System.currentTimeMillis());
        layoutRefresh.setRefreshing(true);
        layoutHistoryTopup.setVisibility(View.GONE);
        db = new DatabaseHelper(getActivity());
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
//        ArrayList<String[]>listTable= db.getDbTableDetails();
//        if (listTable.isEmpty()){
//            Toast.makeText(getContext(), "tabel tidak ada", Toast.LENGTH_SHORT).show();
//        }else {
//            for (int x=0;x<listTable.size();x++){
//                System.out.println("DATA TABLE DATABASE"+listTable.get(x));
//            }
//        }


        List<SessionPembayaranTopup> listPembayaran = new ArrayList<>();
        listPembayaran=db.getPembayaranTopup("");
        if (listPembayaran==null){
            System.out.println("DATA PEMBAYARAN TOPUP NULL");
        }else {
            for (int i=0;i<listPembayaran.size();i++){
                System.out.println("DATA PEMBAYARAN TOPUP "+i+" "+listPembayaran.get(i).getId());
                System.out.println("DATA PEMBAYARAN TOPUP "+i+" "+listPembayaran.get(i).getNominal());
                System.out.println("DATA PEMBAYARAN TOPUP "+i+" "+listPembayaran.get(i).getBataswaktu());
                System.out.println("DATA PEMBAYARAN TOPUP "+i+" "+listPembayaran.get(i).getNorek());
                System.out.println("DATA PEMBAYARAN TOPUP "+i+" "+listPembayaran.get(i).getTitle());
            }
        }
        nominalTopup = new ArrayList<>();
        final String [] saldoTemp={"25000","50000","100000","Others"};
        for (int i=0;i<saldoTemp.length;i++){
            ValueNominalTopup.NominalTopup nt= new ValueNominalTopup.NominalTopup();
            nt.setNominal(saldoTemp[i]);
            nominalTopup.add(i,nt);
        }
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));


        adapterNominal = new AdapterListNominalTopup(getActivity(), nominalTopup);
        adapterNominal.setmClickListener(MenuTopupSaldo.this);
        recyclerView.setAdapter(adapterNominal);
        nonAktifOtherNominal();
        nonAktifLayoutTransaksi();
        btnReload.setVisibility(View.GONE);
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                scrollView.setVisibility(View.VISIBLE);
                layoutRefresh.setRefreshing(true);
                checkBalance();
                loadHistory();
            }
        });


        btnPilihOtherNominal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String nominalOther=etOtherNominal.getText().toString();
                int other= Integer.valueOf(nominalOther);
                if (!nominalOther.isEmpty() && other>25000){
                    aktifLayoutTransaksi();
                    topupSaldoValue.setText(ConvertRupiah(other));
                    saldoValue.setText(ConvertRupiah(other));
                    saldo=other;
                }
            }
        });

        btnLanjutkan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!String.valueOf(saldo).isEmpty()){
                    generateVA();

                }else {
                    showAllert("Informasi !","Pilih saldo yang ingin anda topup");
                }
            }
        });
        layoutHistoryTopup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                intent.putExtra("menu","Pembayaran Topup Lanjut");
                intent.putExtra("title","Topup Saldo "+totalPembayaran);
                intent.putExtra("totalPembayaran",totalPembayaran);
                intent.putExtra("vaNumber",vaNumber);
                intent.putExtra("expTime",expTime);
                startActivity(intent);
            }
        });

        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                scrollView.setVisibility(View.VISIBLE);
                loadHistory();
            }
        });

        loadMetodePembayaran();
        return v;
    }
    public String ConvertRupiah(int saldo){
        Locale localeID = new Locale("in", "ID");
        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
        String h=formatRupiah.format(Long.valueOf(saldo));
        String finalHarga= "Rp. "+h.substring(2)+",-";
        return finalHarga;
    }
    private boolean[] generateVA(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        final boolean[] status = {false,false};
//        amount":50000,"va_number":"7007000501906253","exp_time":"26-07-2018 19:48:32"
//        Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
//        intent.putExtra("menu","Pembayaran Topup Lanjut");
//        intent.putExtra("title","Topup Saldo "+50000);
//        intent.putExtra("totalPembayaran","50000");
//        intent.putExtra("vaNumber","7007000501906253");
//        intent.putExtra("expTime","27-07-2018 19:48:32");
//        startActivity(intent);


        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        final SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("amount",saldo);
            paramObject.put("bank_pembayaran",accountBank);
            paramObject.put("tipe_pembayaran","Virtual Account");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueGenerateVA> data = apiInterface.topUpSaldo(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueGenerateVA>() {
            @Override
            public void onResponse(Call<ValueGenerateVA> call, Response<ValueGenerateVA> response) {
                System.out.println("VALUE "+response.body().toString());
                if (response.body().getStatus().equalsIgnoreCase("success")){
//                    status[0] = true;
//
//                    boolean save= saveVA(String.valueOf(timestamp),"Topup Saldo "+sessionUser.getName(getActivity(),"name"),
//                            response.body().getData().getVa_number()
//                            ,response.body().getData().getExp_time(),String.valueOf(saldo));
//                    if (save) {
//                        status[1] = true;
//                    }
                    ValueGenerateVA.VA data= response.body().getData();
                    Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                    intent.putExtra("menu","Pembayaran Topup Lanjut");
                    intent.putExtra("title","Topup Saldo "+data.getAmount());
                    intent.putExtra("totalPembayaran",data.getAmount());
                    intent.putExtra("vaNumber",data.getVa_number());
                    intent.putExtra("expTime",data.getExp_time());
                    startActivity(intent);
//                    String from,String title, String totalPembayaran,String VA,String batasWaktu

                }
                System.out.println("VALUENYA RESPON : "+response.body().getStatus());


            }

            @Override
            public void onFailure(Call<ValueGenerateVA> call, Throwable t) {
                scrollView.setVisibility(View.GONE);
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                showAllert("Respon Server","Tidak dapat terhubung ke server");

            }
        });
        progressDialog.dismiss();
        return status;
    }

    private void loadMetodePembayaran(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValuePembayaran> dataCall = apiInterface.getMetodeBayar(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValuePembayaran>() {
            @Override
            public void onResponse(Call<ValuePembayaran> call, Response<ValuePembayaran> response) {

                List<ValuePembayaran.Payment> payment = new ArrayList<>();
                payment = response.body().getData().getPayment();

                    int numberOfColumns = 1;
                    recycleMetodeBayar.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
                    AdapterRecyclePembayaran adapterRecyclePembayaran = new AdapterRecyclePembayaran(getActivity(), payment);
                    adapterRecyclePembayaran.setItembankClick(MenuTopupSaldo.this);
                    recycleMetodeBayar.setAdapter(adapterRecyclePembayaran);

            }

            @Override
            public void onFailure(Call<ValuePembayaran> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

//                progressDialog.dismiss();
//                showAllert("Respon Server","Periksa Koneksi anda, anda tidak dapat terhubung ke server","none");

            }
        });
    }


    private void loadHistory(){

        btnReload.setVisibility(View.GONE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        final SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        try {
            paramObject.put("id_login",nohp);
        } catch (JSONException e) {
            e.printStackTrace();
        }



        Call<ValueGenerateVA> data = apiInterface.topupHistory(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueGenerateVA>() {
            @Override
            public void onResponse(Call<ValueGenerateVA> call, Response<ValueGenerateVA> response) {

                if (response.body().getStatus().equalsIgnoreCase("success")){

                    layoutHistoryTopup.setVisibility(View.VISIBLE);

                    ValueGenerateVA.VA data= response.body().getData();
                    title="Topup Saldo "+data.getAmount();
                    totalPembayaran=data.getAmount();
                    vaNumber=data.getVa_number();
                    expTime=data.getExp_time();
                    try {
                        Locale localeID = new Locale("in", "ID");
                        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                        String h=formatRupiah.format(Long.valueOf(data.getAmount()));
                        String finalHarga= "Rp. "+h.substring(2)+",-";
                        valueNominalHistory.setText(finalHarga);
                    }catch (Exception e){

                    }

                    historyTopup.setVisibility(View.VISIBLE);
                    garis.setVisibility(View.VISIBLE);


                }else {
                    historyTopup.setVisibility(View.GONE);
                    garis.setVisibility(View.GONE);
                }
                layoutRefresh.setRefreshing(false);
                System.out.println("VALUENYA RESPON : "+response.body().getStatus());


            }

            @Override
            public void onFailure(Call<ValueGenerateVA> call, Throwable t) {
                scrollView.setVisibility(View.GONE);
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                nonAktifLayoutTransaksi();
                nonAktifOtherNominal();
                layoutRefresh.setRefreshing(false);
                btnReload.setVisibility(View.VISIBLE);
                Toast.makeText(getActivity(), "tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();

            }
        });
    }


    private boolean saveVA(String id,String title, String norek, String batasWaktu, String nominal){
        boolean saveVA=false;
        db.insertPembayaran(id,title, SessionPembayaranTopup.TABLE_NAME,norek,batasWaktu,nominal);
        List<SessionPembayaranTopup> s=db.getPembayaranTopup(id);
        if (s!=null){
            saveVA=true;
//            System.out.println("DATA xx"+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
        }

        return saveVA;

    }
    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
    private void nonAktifOtherNominal(){
        layoutOtherNominal.setVisibility(View.GONE);
    }

    private void aktifOtherNominal(){
        layoutOtherNominal.setVisibility(View.VISIBLE);
    }

    private void nonAktifLayoutTransaksi(){
        layoutNominalTransaksi.setVisibility(View.GONE);
        btnLanjutkan.setVisibility(View.GONE);
        btnLanjutkan.setBackgroundResource(R.drawable.shape_background_stroke_red);
        btnLanjutkan.setTextColor(Color.BLACK);
    }

    private void aktifLayoutTransaksi(){
        layoutNominalTransaksi.setVisibility(View.VISIBLE);
        btnLanjutkan.setBackgroundResource(R.drawable.shape_background_red_stroke_white);
        btnLanjutkan.setTextColor(Color.WHITE);
        btnLanjutkan.setVisibility(View.VISIBLE);
    }

    @Override
    public void onEventClickListener(String nominal) {
        if (nominal.equalsIgnoreCase("Others")){
            aktifOtherNominal();
            nonAktifLayoutTransaksi();
        }else {
            aktifLayoutTransaksi();
            nonAktifOtherNominal();
            topupSaldoValue.setText(ConvertRupiah(Integer.valueOf(nominal)));
            saldoValue.setText(ConvertRupiah(Integer.valueOf(nominal)));
            saldo=Integer.valueOf(nominal);
        }
    }
    private void checkBalance(){

        btnReload.setVisibility(View.GONE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueCheckBalance> data = apiInterface.checkBalance(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueCheckBalance>() {
            @Override
            public void onResponse(Call<ValueCheckBalance> call, Response<ValueCheckBalance> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    saldo= Integer.valueOf(response.body().getSaldo());
                    Locale localeID = new Locale("in", "ID");
                    NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                    String h=formatRupiah.format(Long.valueOf(saldo));
                    String finalHarga= "Rp. "+h.substring(2)+",-";
                    textSaldoSekarang.setText(finalHarga);

                }

            }

            @Override
            public void onFailure(Call<ValueCheckBalance> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                btnReload.setVisibility(View.VISIBLE);
                nonAktifLayoutTransaksi();
                nonAktifOtherNominal();
                layoutRefresh.setRefreshing(false);
                Toast.makeText(getActivity(), "tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();
            }
        });

    }

    @Override
    public void onItemBankClick(String accountBank) {

        String[] arr=accountBank.split(";");
        if (arr.length>1){
            String adm= arr[1];
            this.accountBank=arr[0];
            topupBiayaAdmValue.setText("Rp."+adm);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        checkBalance();
        loadHistory();
    }
}
