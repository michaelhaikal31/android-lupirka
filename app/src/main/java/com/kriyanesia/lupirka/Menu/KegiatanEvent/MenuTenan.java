package com.kriyanesia.lupirka.Menu.KegiatanEvent;

import android.content.Context;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kriyanesia.lupirka.AdapterModel.AdapterListTenant;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.Data.Event.ValueScanBarcode;
import com.kriyanesia.lupirka.Data.Tenant;
import com.kriyanesia.lupirka.Menu.MenuBarcode;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;


public class MenuTenan extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id_event";
    private static final String ARG_PARAM2 = "param2";
    private RecyclerView recyclerView;
    private LinearLayoutManager layoutManager;
    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;
    private AdapterListTenant adapterListTenan;
    private List<ValueListBooth.Booth> input;
    private SwipeRefreshLayout swTenant;
    ImageView btnReload;
    TextView textNoData;

    public MenuTenan() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment MenuTenan.
     */
    // TODO: Rename and change types and number of parameters
    public static MenuTenan newInstance(String param1, String param2) {
        MenuTenan fragment = new MenuTenan();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }


    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu_tenan, container, false);
        recyclerView = (RecyclerView) v.findViewById(R.id.recycleKegiatanEvent);
        swTenant = v.findViewById(R.id.swipeRefreshTenant);
        btnReload = v.findViewById(R.id.reloadData);
        textNoData = v.findViewById(R.id.textNoData);
        // use this setting to
        // improve performance if you know that changes
        // in content do not change the layout size
        // of the RecyclerView
        recyclerView.setHasFixedSize(true);
        // use a linear layout manager
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        swTenant.setRefreshing(true);
        swTenant.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadTenant();
            }
        });
        loadTenant();

        return v;
    }

    private void NoDataList() {
        btnReload.setVisibility(View.VISIBLE);
        textNoData.setVisibility(View.VISIBLE);
        recyclerView.setVisibility(View.GONE);
    }

    private void ShowDataList() {
        btnReload.setVisibility(View.GONE);
        textNoData.setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
    }

    private void loadTenant() {
        swTenant.setRefreshing(true);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        final SessionUser sessionUser = new SessionUser();
        String noHp = sessionUser.getNoHp(getActivity(), "nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        System.out.println("asd id event " + mParam1);
        try {
            paramObject.put("id_login", noHp);
            paramObject.put("tipe", "all");
            paramObject.put("id_event", mParam1);
            paramObject.put("request", "byuser");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("asd param " + paramObject);
        Call<ValueListBooth> data = apiInterface.getListTenant(paramObject.toString(), "saxasdas");
        data.enqueue(new Callback<ValueListBooth>() {
            @Override
            public void onResponse(Call<ValueListBooth> call, Response<ValueListBooth> response) {

                //showAllert("Informasi",response.body().getMessage());
                //onBackPressed();
                System.out.println("asd [response get message]" + response.body().getMessage());
                System.out.println("asd [response get Status]" + response.body().getStatus());
                System.out.println("asd [response get Data]" + response.body().getData().getList());
                if (response.body().getStatus().equalsIgnoreCase("success")) {
                    input = response.body().getData().getList();
                    adapterListTenan = new AdapterListTenant(recyclerView, input, getActivity());
                    recyclerView.setAdapter(adapterListTenan);
                    ShowDataList();
                } else {
                    Snackbar.make(getView(), "Oops, something wrong with our server", Snackbar.LENGTH_SHORT).show();
                    System.out.println("asd tenant " + response.message());
                    NoDataList();
                }
                swTenant.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ValueListBooth> call, Throwable t) {
                NoDataList();
                Snackbar.make(getView(), "Connection problem , maks sure you have internet connection ! ", Snackbar.LENGTH_SHORT).show();
                swTenant.setRefreshing(false);

            }
        });
    }

    private void showAllert(String title, String message) {
        android.app.AlertDialog.Builder alertDialog;
        alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

//    private void loadTenant(){
//        input = new ArrayList<>();
//        input.add(new Tenant("1","Tenant-1","Keterangan-1","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("2","Tenant-2","Keterangan-2","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("3","Tenant-3","Keterangan-3","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("4","Tenant-4","Keterangan-4","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("5","Tenant-5","Keterangan-5","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("6","Tenant-6","Keterangan-6","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("7","Tenant-7","Keterangan-7","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("8","Tenant-8","Keterangan-8","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("9","Tenant-9","Keterangan-9","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("10","Tenant-10","Keterangan-10","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//        input.add(new Tenant("11","Tenant-11","Keterangan-11","https://png.pngtree.com/element_origin_min_pic/17/07/30/b5de6383fa407db602d38c7561e3d1a8.jpg"));
//    }
    // TODO: Rename method, update argument and hook method into UI event

}
