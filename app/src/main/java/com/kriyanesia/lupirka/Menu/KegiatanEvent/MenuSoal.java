package com.kriyanesia.lupirka.Menu.KegiatanEvent;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.github.lzyzsd.circleprogress.CircleProgress;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;
import com.kriyanesia.lupirka.Data.SoalTest.AllSoal;
import com.kriyanesia.lupirka.Data.SoalTest.Soal;
import com.kriyanesia.lupirka.Data.SoalTest.ValueSoal;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseSequence;
import uk.co.deanwild.materialshowcaseview.MaterialShowcaseView;
import uk.co.deanwild.materialshowcaseview.ShowcaseConfig;


public class MenuSoal extends Fragment implements View.OnClickListener{
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "id_event";
    private static final String ARG_PARAM2 = "step";
    private static final String ARG_PARAM3 = "AllSoal";
    // TODO: Rename and change types of parameters
    private String mParam1;
    private int mParam2;
    private AllSoal mParam3;
    private CircleProgress pb;
    private TextView txtSoal,txtTimer;
    private ListView listView;
    private List<Soal> list;
    private Button next,prev,btnSelesai;
    private int cursor=0;
    private String[] pilihan;
    private int valueProgress;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectedItemPos;
    private CountDownTimer countDownTimer;
    private JSONObject paramObject;
    private JsonArray paramArray;
    ProgressDialog progressDialog;
    private String noHp;
    private SessionUser sessionUser;
    private long durasi;
    private int durasiTemp;
    private Snackbar snackbar;
    private Button btnStartPre;
    private Button btnStartPost;
    private Button btnReviewPost;
    private Callbacks mCallbacks;
    private TextView scorePre;
    private TextView scorePost;
    private TextView datePre;
    private TextView datePost;


    public MenuSoal() {
        // Required empty public constructor
    }
    public static MenuSoal newInstance(AllSoal param1, int param2,String param3) {
        System.out.println("asd newInstance");
        MenuSoal fragment = new MenuSoal();
        Bundle args = new Bundle();
        args.putSerializable(ARG_PARAM3, param1);
        args.putInt(ARG_PARAM2, param2);
        args.putString(ARG_PARAM1,param3);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);

        sessionUser = new SessionUser();
        noHp=sessionUser.getNoHp(getActivity(),"nohp");
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getInt(ARG_PARAM2);
            mParam3 = (AllSoal)getArguments().getSerializable(ARG_PARAM3);
        }


        //calendar

        /*
        Calendar now = Calendar.getInstance();
        System.out.println("asd StartTime "+now.getTime());
        now.add(Calendar.MINUTE, 60);
        Calendar f = now;
        System.out.println("asd endTime" +f.getTime().toString());
        long diff = Calendar.getInstance().getTimeInMillis() - f.getTimeInMillis();
        long seconds = diff / 1000;
        long minutes = seconds / 60;
        System.out.println("asd timeLeft "+minutes+":"+seconds);
        */



        //calendar




    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
//
        View v;
        if(mParam2==3 || mParam2==2  || (sessionUser.getSoalPengerjaan(getActivity())
                !=null && sessionUser.getSoalPengerjaan(getActivity()).getListSoal().size()>0)){
            v = inflater.inflate(R.layout.fragment_menu_soal, container, false);
            txtSoal = v.findViewById(R.id.txtSoal);
            listView = v.findViewById(R.id.listSoal);
            next = v.findViewById(R.id.btnNextSoal);
            prev =v.findViewById(R.id.btnPrevSoal);
            txtTimer = v.findViewById(R.id.txtTimer);
            btnSelesai = v.findViewById(R.id.btnSelesai);
            swipeRefreshLayout = v.findViewById(R.id.swiperefreshlayoutTest);
            listView.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //Snackbar.make(view,"terpilih "+pilihan[i],Snackbar.LENGTH_SHORT).show();
                //pb.setProgress(pb.getProgress()+1);
                selectedItemPos=i+1;
                list.get(cursor).setUser_answer(String.valueOf(i));

            }
            });
            next.setOnClickListener(this);
            prev.setOnClickListener(this);

            btnSelesai.setOnClickListener(this);

            if(mParam2==2) {
                list = mParam3.getListSoal();
                generateSoal(0);
                Double a = Double.valueOf(mParam3.getDuration());
                int menit = (int) a.doubleValue();
                int milis = (int) ((a.doubleValue() - menit) * 60000);
                durasi = Long.valueOf((menit * 60000) + milis);
                durasiTemp = menit;
                startCountDown();
            }else if(mParam2==3){
                btnSelesai.setText("Back");
                list = mParam3.getListSoal();
                generateSoal(3);
                txtTimer.setVisibility(View.INVISIBLE);
            }else{
                mParam3 = sessionUser.getSoalPengerjaan(getActivity());
                list = mParam3.getListSoal();
                durasi = sessionUser.getTimeLeft(getActivity());
                generateSoal(0);
                startCountDown();
            }
        }else{
            v = inflater.inflate(R.layout.fragment_menu_histoi_soal, container, false);
            swipeRefreshLayout = v.findViewById(R.id.swiperefreshlayoutTest);
            btnStartPre = v.findViewById(R.id.btnStartPre);
            btnStartPost = v.findViewById(R.id.btnStartkPost);
            btnReviewPost = v.findViewById(R.id.btnReviewPost);
            btnStartPre.setOnClickListener(this);
            btnStartPost.setOnClickListener(this);
            btnReviewPost.setOnClickListener(this);
            scorePost = v.findViewById(R.id.txtScorePost);
            scorePre = v.findViewById(R.id.txtScorePre);
            datePost = v.findViewById(R.id.txtDatePost);
            datePre = v.findViewById(R.id.txtDatePre);
        }
        load_pengerjaan();
        return v;
    }
    private void showCase1(){
        ShowcaseConfig config = new ShowcaseConfig();
        //config.setDelay(100); // half second between each showcase view
        MaterialShowcaseSequence sequence = new MaterialShowcaseSequence(getActivity());
        sequence.setConfig(config);
        sequence.addSequenceItem(new MaterialShowcaseView.Builder(getActivity())
                .setTarget(txtSoal)
                .setMaskColour(getResources().getColor(R.color.show_case_mask))
                .setDismissText("Next")
                .setContentText("Halo, ijinkan saya untuk menjelaskan beberapa fungsi-fungsi untuk anda agar dapat mengerjakan soal, kalimat di atas merupakan pertanyaan yang harus anda jawab")
                .withRectangleShape(true)
                .build());

        sequence.addSequenceItem(new MaterialShowcaseView.Builder(getActivity())
                .setTarget(listView)
                .setMaskColour(getResources().getColor(R.color.show_case_mask))
                .setDismissText("Next")
                .setContentText("Dan ini merupakan pilihan yang tersedia untuk menjawab pertanyaan ")
                .withRectangleShape(true)
                .build());


        sequence.addSequenceItem(next,"Jika anda sudah yakin dengan jawaban anda, maka pilih tombol ini untuk menuju soal berikutnya","Next");

        sequence.addSequenceItem(prev,"Jika anda kurang yakin dengan jawaban sebelumnya, pilih tombol ini","Next");

        sequence.addSequenceItem(pb,"Ini akan membantu anda untuk mengetahui persentase pengerjaan anda, Selamat mengerjakan !","Got It");
        sequence.start();


    }

    private void loadSoal(final String jenis){
       // blankLayout();
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait !");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        final SessionUser sessionUser=new SessionUser();
        paramObject = new JSONObject();
        try{
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",mParam1);
            paramObject.put("jenis",jenis);
        }catch (JSONException e){
        }
        System.out.println("asd "+paramObject.toString());
        Call<ValueSoal> call = apiInterface.getSoalTes(paramObject.toString(),"saxasdas");
        call.enqueue(new Callback<ValueSoal>() {
            @Override
            public void onResponse(Call<ValueSoal> call, Response<ValueSoal> response) {
                if(response.body().getStatus().equalsIgnoreCase("failed")) {
                    //pb.setVisibility(View.INVISIBLE);
                    progressDialog.dismiss();
                    snackbar.make(getView(), response.body().getMessage(), Snackbar.LENGTH_LONG)
                            .setAction("OK", new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {
                                    return;
                                }
                            })
                            .show();

                }else if(response.body().getMessage().equalsIgnoreCase("review")){
                    progressDialog.dismiss();
                    mCallbacks.onButtonClicked(response.body().getData(), 3);
                }else{
                    //swipeRefreshLayout.setRefreshing(false);
                    progressDialog.dismiss();
                    showDialog(response.body().getData());

                    //pb.setVisibility(View.VISIBLE);
                    //showCase1();
                }


            }

            @Override
            public void onFailure(Call<ValueSoal> call, Throwable t) {
                progressDialog.dismiss();
                Snackbar.make(getView(),"Connection problem !",Snackbar.LENGTH_LONG)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                loadSoal(jenis);
                                swipeRefreshLayout.setRefreshing(true);
                            }
                        })
                        .show();

            }


        });
    }

    private void load_pengerjaan(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait !");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        final SessionUser sessionUser=new SessionUser();
        paramObject = new JSONObject();
        try{
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",mParam1);
        }catch (JSONException e){
        }
        System.out.println("asd "+paramObject.toString());
        Call<String> call = apiInterface.getAllResultTest(paramObject.toString(),"saxasdas");
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try{
                    JSONObject respon = new JSONObject(response.body());
                    if(respon.getString("message").equalsIgnoreCase("sukses")){
                        respon = respon.getJSONObject("data");
                        JSONArray data = respon.getJSONArray("data");
                        if(data.length()<2){
                            if(data.getJSONObject(0).getString("jenis")=="1"){
                                scorePre.setText("Score : " + data.getJSONObject(0).getString("score"));
                                datePre.setText("Date : " + data.getJSONObject(0).getString("date"));
                            }else{
                                scorePost.setText("Score : " + data.getJSONObject(0).getString("score"));
                                datePost.setText("Date : " + data.getJSONObject(0).getString("date"));
                            }
                        }else {
                            scorePre.setText("Score : " + data.getJSONObject(0).getString("score"));
                            datePre.setText("Date : " + data.getJSONObject(0).getString("date"));
                            scorePost.setText("Score : " + data.getJSONObject(1).getString("score"));
                            datePost.setText("Date : " + data.getJSONObject(1).getString("date"));
                        }
                    }
                }catch(Exception e){
                    System.out.println("asd exception "+e.toString());
                }
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                progressDialog.dismiss();
                System.out.println("asd failure "+call.toString());
                Snackbar.make(getView(),"Connection problem !",Snackbar.LENGTH_LONG)
                        .setAction("Retry", new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {

                                swipeRefreshLayout.setRefreshing(true);
                            }
                        })
                        .show();

            }


        });

    }


    private void get_user_answer(){

    }

    private void startCountDown(){
        countDownTimer = new CountDownTimer(durasi,1000){
            @Override
            public void onTick(long l) {
                txtTimer.setText((l / 60000)+":"+(l % 60000 / 1000));

                //txtTimer.setText(""+l/1000);
            }

            @Override
            public void onFinish() {
                if(selectedItemPos!=0) {
                    list.get(cursor).setUser_answer(String.valueOf(selectedItemPos-1));
                }
                sendPengerjaan();
            }
        }.start();
    }

    private void showDialog(final AllSoal allSoal) {
        AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
        alert.setTitle(allSoal.getJenis());
        alert.setMessage("Anda akan mengerjakan soal " + allSoal.getJenis() + " kurang lebih selama " + allSoal.getDuration() + " menit, mulai sekarang ?");
        alert.setPositiveButton("MULAI", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                int duration = Integer.parseInt(allSoal.getDuration());
                sessionUser.setPengerjaanTes(getActivity(),allSoal,duration);
                mCallbacks.onButtonClicked(allSoal, 2);
                return;
            }
        });
        alert.setNegativeButton("Lain kali", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                return;


//                blankLayout();
//                list.clear();
            }
        });
        alert.create().show();

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    private void generateSoal(int tipe){
                try {
                    pilihan = list.get(cursor).getPilihan().split(";");
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, pilihan);
                    listView.setAdapter(adapter);
                    txtSoal.setText(list.get(cursor).getSoal());
                    if(tipe==3){
                        listView.setItemChecked(Integer.parseInt(list.get(cursor).getQuestion_answer()),true);
                    }else {
                        if (list.get(cursor).getUser_answer() != null) {
                            listView.setItemChecked(Integer.parseInt(list.get(cursor).getUser_answer()), true);
                        }
                    }
                    //initialize circle progress
                    valueProgress = 100 / list.size();
                }catch(Exception e){
                    //System.out.println();
                }
    }

    private  void next(){
        if(selectedItemPos!=0) {
            list.get(cursor).setUser_answer(String.valueOf(selectedItemPos-1));
        }
        if(cursor+1!=list.size()) {
            selectedItemPos=0;
            cursor++;
            pilihan = list.get(cursor).getPilihan().split(";");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, pilihan);
            listView.setAdapter(adapter);
            if(mParam2==3){
                listView.setItemChecked(Integer.parseInt(list.get(cursor).getQuestion_answer()),true);
            }else {
                if (list.get(cursor).getUser_answer() != null) {
                    listView.setItemChecked(Integer.parseInt(list.get(cursor).getUser_answer()), true);
                }
            }
            txtSoal.setText(list.get(cursor).getSoal());
        }else{
            Snackbar.make(getView(),"Soal sudah selesai !",Snackbar.LENGTH_SHORT).show();
        }

        ///update circle progress
        //pb.setProgress(pb.getProgress()+valueProgress);

    }

    private void prev(){
        if(selectedItemPos!=0) {
            list.get(cursor).setUser_answer(String.valueOf(selectedItemPos-1));
        }
        System.out.println("asd selected item position :"+listView.getSelectedItemPosition());
        if(cursor!=0) {
            selectedItemPos=0;
            cursor--;
            pilihan = list.get(cursor).getPilihan().split(";");
            ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), android.R.layout.simple_list_item_single_choice, pilihan);
            listView.setAdapter(adapter);
            if(mParam2==3){
                listView.setItemChecked(Integer.parseInt(list.get(cursor).getQuestion_answer()),true);
            }else {
                if (list.get(cursor).getUser_answer() != null) {
                    listView.setItemChecked(Integer.parseInt(list.get(cursor).getUser_answer()), true);
                }
            }
            txtSoal.setText(list.get(cursor).getSoal());
        }else{
            Snackbar.make(getView(),"Ini soal pertama anda !",Snackbar.LENGTH_SHORT).show();
        }
        //update circle progress
        //pb.setProgress(pb.getProgress()-valueProgress);
    }

    @Override
    public void onDestroyView() {
        if(snackbar!=null){
            snackbar.dismiss();
        }
//        if(selectedItemPos!=0) {
//            list.get(cursor).setUser_answer(String.valueOf(selectedItemPos - 1));
//        }
        super.onDestroyView();

        if(mParam2!=3) {
            if (list != null) {
                mParam3.setList_soal(list);
            }
            sessionUser.setPengerjaanTes(getActivity(), mParam3, 0);
        }
    }
    // TODO: Rename method, update argument and hook method into UI event

    private void sendPengerjaan(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Harap tunggu, sedang mengirim jawaban anda !");
        progressDialog.show();

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        final SessionUser sessionUser=new SessionUser();

        try {
            String[] listPilihan;
            SessionUser su = new SessionUser();
            paramArray = new JsonArray();
            paramObject = new JSONObject();
            paramObject.put("id_login", su.getNoHp(getActivity(), "nohp"));
            paramObject.put("event_id",mCallbacks.getIdEvent());
            if(mParam3.getJenis().equalsIgnoreCase("Pre-Test")){
                paramObject.put("jenis","1");
            }else{
                paramObject.put("jenis","2");
            }
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < list.size(); i++) {
                listPilihan = list.get(i).getPilihan().split(";");
                JSONObject obj = new JSONObject();
                if (list.get(i).getUser_answer() != null) {
                    obj.put("id_soal",list.get(i).getId_soal());
                    obj.put("answer",list.get(i).getUser_answer());
                } else {
                    obj.put("id_soal",list.get(i).getId_soal());
                    obj.put("answer","-");
                }
                jsonArray.put(obj);
            }
            paramObject.put("data",jsonArray);
            System.out.println("value json send soal "+paramObject.toString());
            Call<OtpVerifikasi> testCall = apiInterface.getResultTest(paramObject.toString(),"saxasdas");
            testCall.enqueue(new Callback<OtpVerifikasi>() {
                @Override
                public void onResponse(Call<OtpVerifikasi> call, Response<OtpVerifikasi> response) {

                    if(response.body().getStatus().equalsIgnoreCase("success")){
                        progressDialog.dismiss();
                        Snackbar.make(getView(),"Jawaban terikirm ",Snackbar.LENGTH_SHORT).show();
                        list = new ArrayList<>();
                        sessionUser.setPengerjaanTes(getContext(),new AllSoal(),0);
                        mCallbacks.onButtonClicked(new AllSoal(),1);
                    }else{
                        System.out.println("asd "+response.body().getMessage() +" | "+response.body().getStatus());
                        progressDialog.dismiss();
                        snackbar.make(getView(),"Maaf, server kami sedang dalam proses perbaikan !",Snackbar.LENGTH_LONG)
                                .setAction("Try again", new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        sendPengerjaan();
                                    }
                                }).show();
                    }
                }

                @Override
                public void onFailure(Call<OtpVerifikasi> call, Throwable t) {
                progressDialog.dismiss();
                }
            });

        }catch (Exception e){
            progressDialog.dismiss();
            System.out.println("asd asd "+e.toString());
            Snackbar.make(getView(),"Oops, somethink wrong ,"+e.toString(),Snackbar.LENGTH_SHORT).show();
        }
    }

    private void blankLayout(){
        txtSoal.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.INVISIBLE);
        prev.setVisibility(View.INVISIBLE);
        next.setVisibility(View.INVISIBLE);
        txtTimer.setVisibility(View.INVISIBLE);
        btnSelesai.setVisibility(View.INVISIBLE);
    }

    private void revealLayout(){
        txtSoal.setVisibility(View.VISIBLE);
        listView.setVisibility(View.VISIBLE);
        prev.setVisibility(View.VISIBLE);
        next.setVisibility(View.VISIBLE);
        txtTimer.setVisibility(View.VISIBLE);
        btnSelesai.setVisibility(View.VISIBLE);
    }


    @Override
    public void onViewStateRestored(@Nullable Bundle savedInstanceState) {
        super.onViewStateRestored(savedInstanceState);
        if(savedInstanceState!=null){

        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        System.out.println("asd");
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id){
            case R.id.btnStartPre:
                loadSoal("1");
                break;
            case R.id.btnStartkPost:
                loadSoal("2");
                break;
            case R.id.btnReviewPost:
                loadSoal("2");
                break;
            case R.id.btnNextSoal:
                next();
                break;
            case R.id.btnPrevSoal:
                prev();
                break;
            case R.id.btnSelesai:
                if(mParam2!=3) {
                    if (selectedItemPos != 0) {
                        list.get(cursor).setUser_answer(String.valueOf(selectedItemPos - 1));
                    }
                    sendPengerjaan();
                    sessionUser.setPengerjaanTes(getActivity(), new AllSoal(), 9);
                }else{
                    mCallbacks.onButtonClicked(new AllSoal(),1);
                }
                break;
        }
    }


    public interface Callbacks {
        //Callback for when button clicked.
        void onButtonClicked(AllSoal AllSoal, int step);
        String getIdEvent();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mCallbacks = (Callbacks)context;

    }


//    @Override
//    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
//        inflater.inflate(R.menu.menu_activity_event, menu);
//    }
//
//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.action_settings) {
//
//        list = new ArrayList<>();
//        Toast.makeText(getActivity(),"refreshed...",Toast.LENGTH_SHORT).show();
//            return true;
//        }
//        return super.onOptionsItemSelected(item);
//    }
}
