package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListAktivitas;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPost;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Aktivitas.Aktivitas;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueAktivitas;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.Event.ValueJointEventDibayarin;
import com.kriyanesia.lupirka.Data.Hospital.Hospital;
import com.kriyanesia.lupirka.Data.Hospital.ValueHospital;
import com.kriyanesia.lupirka.Menu.KegiatanEvent.ActivityEvent;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuAktivitas extends Fragment implements AdapterListAktivitas.ItemClickListener{

    List<Aktivitas> aktivitas=new ArrayList<>();
    RecyclerView recycleAktivitas;
    SwipeRefreshLayout layoutRefresh;
    ImageView btnReload;
    TextView textNoData;
    AdapterListAktivitas adapterListAktivitas;
    ValueDetailAktivitas.DetailAktivitasTemp detailAktivitas ;
    SessionUser sessionUser;
    String role;
    public MenuAktivitas() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_aktivitas, container, false);
        recycleAktivitas= (RecyclerView) v.findViewById(R.id.recycleAktivitas);
        layoutRefresh = (SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        btnReload= (ImageView) v.findViewById(R.id.reloadData);
        textNoData= (TextView) v.findViewById(R.id.textNoData);
        layoutRefresh.setRefreshing(true);
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                nonAktifView();
                loadAktivitas();
            }
        });
        nonAktifView();
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nonAktifView();
                layoutRefresh.setRefreshing(true);
                loadAktivitas();
            }
        });
        sessionUser=new SessionUser();
        role=sessionUser.getRole(getActivity(),"role");
//        loadAktivitas();
        return v;
    }

    private void nonAktifView(){
        btnReload.setVisibility(View.GONE);
        textNoData.setVisibility(View.GONE);
        recycleAktivitas.setVisibility(View.GONE);
    }
    private void loadAktivitas(){
        aktivitas.clear();
        recycleAktivitas.setVisibility(View.VISIBLE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        SessionUser sessionUser=new SessionUser();
        String noHp=sessionUser.getNoHp(getActivity(),"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA list Aktivitas : "+paramObject.toString());
        final Call<ValueAktivitas> data = apiInterface.getAllAktivity(paramObject.toString(),"saxasdas");
        data.enqueue(new Callback<ValueAktivitas>() {
            @Override
            public void onResponse(Call<ValueAktivitas> call, Response<ValueAktivitas> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<Aktivitas> datas= response.body().getData().getList_activity();
//                    if (datas.size()>0){
//                        aktivitas.addAll(datas);
//
//                    }
//                    else {
//                        nonAktifView();
//                        textNoData.setVisibility(View.VISIBLE);
//                    }

                    try {
                        if (getContext()!= null) {
                            adapterListAktivitas = new AdapterListAktivitas(getActivity(), datas);
                            recycleAktivitas.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycleAktivitas.setItemAnimator(new DefaultItemAnimator());
                            //SET ADAPTER
                            recycleAktivitas.setAdapter(adapterListAktivitas);
                            adapterListAktivitas.setmClickListener(MenuAktivitas.this);
                        }
                    }catch (Error e){
                    }
                }else {
                    Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                    nonAktifView();
                    textNoData.setVisibility(View.VISIBLE);
                }
                layoutRefresh.setRefreshing(false);
            }

            @Override
            public void onFailure(Call<ValueAktivitas> call, Throwable t) {
                nonAktifView();
                btnReload.setVisibility(View.VISIBLE);
                layoutRefresh.setRefreshing(false);
                Toast.makeText(getActivity(), "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();
//                showAllert("Respon Server","Tidak dapat terhubung ke server");


            }
        });
    }


    private void loadAktivitasDetail(){
        aktivitas.clear();
        recycleAktivitas.setVisibility(View.VISIBLE);
        layoutRefresh.setRefreshing(true);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        SessionUser sessionUser=new SessionUser();
        String noHp=sessionUser.getNoHp(getActivity(),"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("id_activity",idAktivitas);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA json list Aktivitas : "+paramObject.toString());
        final Call<ValueDetailAktivitas> data = apiInterface.getDetailActivity(paramObject.toString(),"saxasdas");
        data.enqueue(new Callback<ValueDetailAktivitas>() {
            @Override
            public void onResponse(Call<ValueDetailAktivitas> call, final Response<ValueDetailAktivitas> response) {
                layoutRefresh.setRefreshing(false);
                if (response.body() !=null) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        final ValueDetailAktivitas.DetailAktivitas datas = response.body().getData();
                       if(datas.getStatus().equalsIgnoreCase("rejected")){
                           alertDialog = new AlertDialog.Builder(getActivity());
                           alertDialog
                                   .setTitle("Informasi")
                                   .setMessage("Bukti pembayaran ditolak oleh EO event, silahkan upload bukti pembayaran yang valid dan benar !")
                                   .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                       @Override
                                       public void onClick(DialogInterface dialog, int which) {
                                        openDetailActivity(datas,response);
                                       }
                                   })
                                   .show();
                        }else{
                           openDetailActivity(datas,response);
                       }




                        //ini

                    } else {
                        nonAktifView();
                        textNoData.setVisibility(View.VISIBLE);
                    }
                }
                layoutRefresh.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<ValueDetailAktivitas> call, Throwable t) {
                System.out.println("error "+call+" | "+t.getMessage());
                nonAktifView();
                btnReload.setVisibility(View.VISIBLE);
                layoutRefresh.setRefreshing(false);
                Toast.makeText(getActivity(), "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();
//                showAllert("Respon Server","Tidak dapat terhubung ke server");


            }
        });
    }

    private void openDetailActivity(ValueDetailAktivitas.DetailAktivitas datas,Response<ValueDetailAktivitas> response){
        detailAktivitas = new ValueDetailAktivitas.DetailAktivitasTemp();
        if (datas != null) {
            if(datas.getStatus_message().equalsIgnoreCase("UNPAID")) {//cek apkah sudah bayar atau blm
                Intent i = new Intent(getActivity(), MenuPembayaranAkhir.class);
                i.putExtra("from", "MenuPembayaranEventUnpaid");
                i.putExtra("title", "Status pendaftaran");
                i.putExtra("namaAkunBank", datas.getBank_data().get(0).getNama_akun());
                i.putExtra("totalPembayaran", datas.getHarga());
                i.putExtra("noRekBank", datas.getBank_data().get(0).getNo_rekening());
                i.putExtra("namaBank", datas.getBank_data().get(0).getNama_bank());
                i.putExtra("expTime", datas.getBank_data().get(0).getExp_time());
                i.putExtra("idPembayaran", datas.getBank_data().get(0).getPayment_id());
                CharSequence[] c = new String[datas.getDetail_cost().size()];
                for (int x = 0; x < datas.getDetail_cost().size(); x++) {
                    c[x] = datas.getDetail_cost().get(x).getPaket() + " " + datas.getDetail_cost().get(x).getBiaya();
                }
                i.putExtra("aktivitasCost", c);
                startActivity(i);
            }else if(response.body().getData().getStatus_message().equalsIgnoreCase("PAID") &&response.body().getData().getStatus_event().equalsIgnoreCase("ongoing")){
                Intent i = new Intent(getActivity(),ActivityEvent.class);
                i.putExtra("id_event",response.body().getData().getId_event());
                i.putExtra("nama_event",response.body().getData().getTitle());
                startActivity(i);
            }else{
                String[] data = datas.getPembayaran().split("-");
                if (role.equalsIgnoreCase("2")) {
                    if (data != null) {
                        if (data[1].equalsIgnoreCase("Farmasi")) {
                            detailAktivitas.setFarmasi(datas.getFarmasi());
                        } else {
                            detailAktivitas.setFarmasi("-");
                        }
                    }
                } else {
                    detailAktivitas.setFarmasi("-");
                }
//                    detailAktivitas.setFarmasi(datas.getFarmasi());

                detailAktivitas.setHarga(datas.getHarga());
                detailAktivitas.setLokasi(datas.getLokasi());
                detailAktivitas.setNama(datas.getNama());
                detailAktivitas.setNomor_hp(datas.getNomor_hp());
                detailAktivitas.setStatus(datas.getStatus());
                detailAktivitas.setPembayaran(datas.getPembayaran());
                detailAktivitas.setStatus_event(datas.getStatus_event());
                detailAktivitas.setStatus_message(datas.getStatus_message());

                List<ValueDetailAktivitas.DetailCostTemp> dataCost = new ArrayList<>();
                List<ValueDetailAktivitas.DetailKegiatanTemp> dataAktivitas = new ArrayList<>();
                for (int i = 0; i < datas.getDetail_cost().size(); i++) {
                    ValueDetailAktivitas.DetailCostTemp cost = new ValueDetailAktivitas.DetailCostTemp();
                    cost.setBiaya(datas.getDetail_cost().get(i).getBiaya());
                    cost.setPaket(datas.getDetail_cost().get(i).getPaket());
                    dataCost.add(cost);
                }
                if (datas.getDetail_kegiatan() != null) {
                    for (int i = 0; i < datas.getDetail_kegiatan().size(); i++) {
                        ValueDetailAktivitas.DetailKegiatanTemp dataAktivitasTemp = new ValueDetailAktivitas.DetailKegiatanTemp();
                        dataAktivitasTemp.setDetail(datas.getDetail_kegiatan().get(i).getDetail());
                        dataAktivitasTemp.setStarddate(datas.getDetail_kegiatan().get(i).getStarddate());
                        dataAktivitasTemp.setEnddate(datas.getDetail_kegiatan().get(i).getEnddate());
                        dataAktivitasTemp.setKegiatan(datas.getDetail_kegiatan().get(i).getKegiatan());
                        dataAktivitas.add(dataAktivitasTemp);

                    }
                }

                detailAktivitas.setStatus_event(datas.getStatus_event());
                detailAktivitas.setId_event(datas.getId_event());
                detailAktivitas.setTitle(datas.getTitle());


                Intent intent = new Intent(getActivity(), ActivityDetailContent.class);
                intent.putExtra("menu", "Detail Aktivitas");
                intent.putExtra("idAktivitas", idAktivitas);
                System.out.println("asd asd detail "+idAktivitas);
//                            Toast.makeText(getActivity(), "1 "+idAktivitas, Toast.LENGTH_SHORT).show();
                intent.putExtra("aktivitasFarmasi", detailAktivitas.getFarmasi());
                intent.putExtra("aktivitasHarga", detailAktivitas.getHarga());
                intent.putExtra("aktivitasLokasi", detailAktivitas.getLokasi());
                intent.putExtra("aktivitasNama", detailAktivitas.getNama());
                intent.putExtra("aktivitasNoHp", detailAktivitas.getNomor_hp());
                intent.putExtra("aktivitasStatus", detailAktivitas.getStatus());
                intent.putExtra("aktivitasOngoing", detailAktivitas.getStatus_event());
                intent.putExtra("aktivitasPembayaran", detailAktivitas.getPembayaran());
                intent.putExtra("aktivitasTitle", detailAktivitas.getTitle());
                intent.putExtra("aktivitasId", detailAktivitas.getId_event());
                intent.putExtra("aktivitasCost", (Serializable) dataCost);
                intent.putExtra("aktivitasKegiatan", (Serializable) dataAktivitas);
                intent.putExtra("aktivitasMessage",detailAktivitas.getStatus_message());


                            /*
                              intent.putExtra("menu", "Pembayaran Topup Lanjut");
                            intent.putExtra("from", "MenuPembayaranEvent");
                            intent.putExtra("title", "Pembayaran Event " + eventTitle);
                            intent.putExtra("totalPembayaran", totalPembayaran);
                            intent.putExtra("noRekBank", noRekBank);
                            intent.putExtra("namaBank", namaBank);
                            intent.putExtra("namaAkunBank", namaAkunBank);
                            intent.putExtra("expTime", expTime);
                            intent.putExtra("idPembayaran", idPembayaran);
                            startActivity(intent);
                             */
                startActivity(intent);
                System.out.println("morten value inten " + intent.getExtras().toString());


            }
        } else {
            Toast.makeText(getActivity(), "ada kesalahan respon server", Toast.LENGTH_SHORT).show();
        }

    }

/*
    private void pembayaranEvent(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        String noHp=sessionUser.getNoHp(getActivity(),"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",eventId);
            paramObject.put("paket",idPaket);
//            Toast.makeText(getActivity(), idPaket, Toast.LENGTH_SHORT).show();
            paramObject.put("role",role);
            System.out.println("DAFTARNYA "+farmasiDaftar);
            if (farmasiDaftar.split("-")[2].equalsIgnoreCase("x")){
                paramObject.put("farmasi","");
                if (role.equalsIgnoreCase("5")){
                    if (farmasiDaftar.split("-")[1].equalsIgnoreCase("Sendiri")){
                        paramObject.put("pembayaran", "Mahasiswa-Sendiri-Sekarang");
                    }else {
                        paramObject.put("farmasi",idFarmasi);
                        paramObject.put("pembayaran", "Mahasiswa-Farmasi-Sekarang");
                    }

                }else if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")) {
                    if (farmasiDaftar.split("-")[1].equalsIgnoreCase("Sendiri")){
                        paramObject.put("pembayaran", "Dokter-Sendiri-Sekarang");
                    }else {
                        paramObject.put("farmasi",idFarmasi);
                        paramObject.put("pembayaran", "Dokter-Farmasi-Sekarang");
                    }
                }
            }else {
                paramObject.put("pembayaran", farmasiDaftar);
                paramObject.put("id_dokter",idDokter);
                paramObject.put("farmasi",idFarmasi);

            }
            paramObject.put("nama_dokter",namaDokter);
            paramObject.put("tipe_bayar",0);
            paramObject.put("alamat",eventAlamat);
            paramObject.put("rumah_sakit",eventRs);
            paramObject.put("amount",TotalPembayaran);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA daftar event : "+paramObject.toString());

        Call<ValueJointEventDibayarin> dataCall = apiInterface.joinEventBayarOrang(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJointEventDibayarin>() {
            @Override
            public void onResponse(Call<ValueJointEventDibayarin> call, Response<ValueJointEventDibayarin> response) {
                System.out.println("respon bank: "+response.body());
                valueJointEventDibayarin=response.body();
                SessionUser sessionUser=new SessionUser();
                sessionUser.setAlamatRs(getActivity(),"alamatRs",eventRs);
                sessionUser.setNamaRs(getActivity(),"namaRs",eventAlamat);
                sessionUser.setNamaSertifikat(getActivity(),"namaSertifikat",namaDokter);
                if (valueJointEventDibayarin.getMessage().equalsIgnoreCase("Anda sudah pernah mendaftar event ini, mohon lakukan pembayaran sebelum waktu pembayaran habis")
                        || valueJointEventDibayarin.getStatus().equalsIgnoreCase("success")) {


                    int total = Integer.valueOf(valueJointEventDibayarin.getData().getAmount()) +
                            Integer.valueOf(valueJointEventDibayarin.getData().getUnique_code());
                    totalPembayaran = String.valueOf(total);
                    expTime= valueJointEventDibayarin.getData().getExp_time().substring(0,19);
                    idPembayaran=valueJointEventDibayarin.getData().getId();

                }else {

                }
                progressDialog.dismiss();
                showAllert("Informasi",valueJointEventDibayarin.getStatus(),valueJointEventDibayarin.getMessage());

            }

            @Override
            public void onFailure(Call<ValueJointEventDibayarin> call, Throwable t) {
                progressDialog.dismiss();
                showAllert("Informasi","failed","Koneksi ke server terputus");
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

*/
    @Override
    public void onResume() {

        layoutRefresh.setRefreshing(true);
        nonAktifView();
//
        loadAktivitas();
        super.onResume();
    }

    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
    private int idAktivitas;

    @Override
    public void onAktivitasClickListener(int idAktivitas) {
        this.idAktivitas=idAktivitas;
        //System.out.println("id aktivitas "+idAktivitas);
        loadAktivitasDetail();
        //Intent i = new Intent(getActivity(),ActivityEvent.class);
        //startActivity(i);
    }
}
