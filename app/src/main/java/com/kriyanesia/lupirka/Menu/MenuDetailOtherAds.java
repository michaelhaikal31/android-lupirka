package com.kriyanesia.lupirka.Menu;


import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Ads.ValueDetailAds;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDetailOtherAds extends Fragment {
    private String idAds;
    TextView valueTitle,valuePublishDate,valueDeskripsi;
    Button btnBack;
    ImageView bannerAds;


    public MenuDetailOtherAds newInstance(String id) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);

        MenuDetailOtherAds fragment = new MenuDetailOtherAds();
        fragment.setArguments(bundle);
        return fragment;
    }

    ImageView garisSatu,garisDua;
    TextView textDeskripsi;
    ProgressDialog progressDialog;

    SwipeRefreshLayout layoutRefresh;
    RelativeLayout layoutContent;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_detail_other_ads, container, false);
        idAds =getArguments().getString("id");
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        textDeskripsi = (TextView) v.findViewById(R.id.textDeskripsi);
        garisSatu = (ImageView) v.findViewById(R.id.garisSatu);
        garisDua = (ImageView) v.findViewById(R.id.garisDua);


        valueTitle = (TextView) v.findViewById(R.id.valueTitle);
        valuePublishDate = (TextView) v.findViewById(R.id.valueTanggal);
        valueDeskripsi = (TextView) v.findViewById(R.id.valueDescription);
        btnBack = (Button) v.findViewById(R.id.btnBackFragment);
        bannerAds = (ImageView) v.findViewById(R.id.bannerAds);
        layoutContent = (RelativeLayout) v.findViewById(R.id.layoutContent);

        loadDetailAds(idAds);
        layoutRefresh = (SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                layoutContent.setVisibility(View.GONE);
                loadDetailAds(idAds);

                layoutContent.setVisibility(View.VISIBLE);
                layoutRefresh.setRefreshing(false);
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fm = getActivity()
                        .getSupportFragmentManager();
                fm.popBackStack ("DetailAds", FragmentManager.POP_BACK_STACK_INCLUSIVE);
            }
        });








        return v;


    }
    private void loadDetailAds(String idEvent){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("adsId",idAds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueDetailAds> profesiCall = apiInterface.getDetailAds(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueDetailAds>() {
            @Override
            public void onResponse(Call<ValueDetailAds> call, Response<ValueDetailAds> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    Ads ads =new Ads();
                    ads = response.body().getData().getDetailAds();
                    textDeskripsi.setVisibility(View.VISIBLE);
                    garisDua.setVisibility(View.VISIBLE);
                    garisSatu.setVisibility(View.VISIBLE);
                    btnBack.setVisibility(View.VISIBLE);

                    valueDeskripsi.setText(ads.getNote());
                    valueTitle.setText(ads.getAdsTitle());
                    valuePublishDate.setText(ads.getStartDate()+" s/d "+ads.getEndDAte());


                    if (ads.getUrlImage()!=null) {
                        Picasso.with(getActivity()).load(ads.getUrlImage()).into(bannerAds);
                    }




                }
                System.out.println("VALUENYA : "+response.body().getStatus());
                progressDialog.dismiss();
            }

            @Override
            public void onFailure(Call<ValueDetailAds> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

}
