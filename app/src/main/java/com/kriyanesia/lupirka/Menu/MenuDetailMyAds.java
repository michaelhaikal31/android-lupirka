package com.kriyanesia.lupirka.Menu;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Ads.ResponCancelAds;
import com.kriyanesia.lupirka.Data.Ads.ValueDetailAds;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDetailMyAds extends Fragment {
    private String idAds,from;
    TextView valueTitle,valuePublishDate,valueNote,valueAdsStatus;
    String cp;
    Button btnActionDetailAds;
    ImageView bannerAds;
    private Animator mCurrentAnimator;

    // The system "short" animation time duration, in milliseconds. This
    // duration is ideal for subtle animations or animations that occur
    // very frequently.
    private int mShortAnimationDuration;

    android.app.AlertDialog.Builder alertDialog;

    public static MenuDetailMyAds newInstance(String id,String from) {
        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("from", from);

        MenuDetailMyAds fragment = new MenuDetailMyAds();
        fragment.setArguments(bundle);
        return fragment;
    }

    TextView textAdsTitle,textPublishDate,textNote,titikDua1,titikDua2,titikDua3,valueAdsKetStatus;
    ProgressDialog progressDialog;
    SwipeRefreshLayout layoutRefresh;

    private DatabaseHelper db;
    private String urlImage,startDateperpanjangan,subTitle,noHp,note,adsCategori,adsIdCategori;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_detail_ads, container, false);
        idAds =getArguments().getString("id");
        from =getArguments().getString("from");
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        db= new DatabaseHelper(getActivity());

        textAdsTitle = (TextView) v.findViewById(R.id.textAdsTitle);
        textPublishDate = (TextView) v.findViewById(R.id.textPublishDate);
        valueAdsKetStatus = (TextView) v.findViewById(R.id.valueAdsKetStatus);
        textNote = (TextView) v.findViewById(R.id.textNote);
        titikDua1 = (TextView) v.findViewById(R.id.titikDua1);
        titikDua2 = (TextView) v.findViewById(R.id.titikDua2);
        titikDua3 = (TextView) v.findViewById(R.id.titikDua3);

        valueTitle = (TextView) v.findViewById(R.id.valueTitle);
        valuePublishDate = (TextView) v.findViewById(R.id.valuePublishDate);
        valueNote = (TextView) v.findViewById(R.id.valueNote);
        valueAdsStatus = (TextView) v.findViewById(R.id.valueAdsStatus);
        btnActionDetailAds = (Button) v.findViewById(R.id.btnAdsDetailAction);
        bannerAds = (ImageView) v.findViewById(R.id.bannerAds);

        valueAdsKetStatus.setVisibility(View.GONE);
        layoutRefresh = (SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDetailAds(idAds);
                valueAdsKetStatus.setVisibility(View.GONE);
                layoutRefresh.setRefreshing(false);
            }
        });

        loadDetailAds(idAds);

        btnActionDetailAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (btnActionDetailAds.getText().toString().equalsIgnoreCase("Batalkan Iklan")){
                    showAllert("Confirmation","Apakah anda yakin ingin membatalkan pendaftaran iklan ?","batalkan iklan");

                }else if (btnActionDetailAds.getText().toString().equalsIgnoreCase("Back to My Ads")){
                    if (from.equalsIgnoreCase("menuAds")){
                        FragmentManager fm = getActivity()
                                .getSupportFragmentManager();
                        fm.popBackStack ("DetailAds", FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    }else if (from.equalsIgnoreCase("pendaftaran")){
                        getActivity().finish();
                    }

                } else if (btnActionDetailAds.getText().toString().equalsIgnoreCase("Next")){
                    Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                    intent.putExtra("menu","Pembayaran Ads");
                    intent.putExtra("adsId",idAds);
                    intent.putExtra("adsTitle",valueTitle.getText().toString());
                    startActivity(intent);
                }else if (btnActionDetailAds.getText().toString().equalsIgnoreCase("Lihat Info Pembayaran")){
                    Intent intent = new Intent(getActivity(), ActivityDetailContent.class);
                    intent.putExtra("menu", "Pembayaran Ads Lanjut");
                    intent.putExtra("adsId", idAds);
                    startActivity(intent);
                }else if (btnActionDetailAds.getText().toString().equalsIgnoreCase("Perpanjang iklan")){
                    Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                    intent.putExtra("menu","Perpanjang Iklan");
                    intent.putExtra("adsTitle",valueTitle.getText().toString());
                    intent.putExtra("adsSubTitle",valueTitle.getText().toString());
                    intent.putExtra("adsStartDate",startDateperpanjangan);
                    intent.putExtra("cp",cp);
                    intent.putExtra("adsDesc",note);
                    intent.putExtra("urlImage",urlImage);
                    intent.putExtra("adsCategori",adsCategori);
                    intent.putExtra("adsIdCategori",adsIdCategori);
                    startActivity(intent);
                }
            }
        });
        final ImageView expandedImageView = (ImageView) v.findViewById(
                R.id.expanded_image);
        bannerAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!foto.isEmpty()) {
                    // Load the high-resolution "zoomed-in" image.

                    expandedImageView.setVisibility(View.VISIBLE);
                    Picasso.with(getActivity()).load(foto).into(expandedImageView);
                    Toast.makeText(getActivity(), ""+foto, Toast.LENGTH_SHORT).show();
                }
            }
        });
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);
        expandedImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                expandedImageView.setVisibility(View.GONE);
            }
        });

        return v;
    }
    String foto;
    private void loadDetailAds(String idEvent){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        SessionUser sessionUser = new SessionUser();
        final String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("adsId",idAds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueDetailAds> dataCall = apiInterface.getDetailAds(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueDetailAds>() {
            @Override
            public void onResponse(Call<ValueDetailAds> call, Response<ValueDetailAds> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    Ads ads =new Ads();
                    ads = response.body().getData().getDetailAds();
                    textAdsTitle.setVisibility(View.VISIBLE);
                    textPublishDate.setVisibility(View.VISIBLE);
                    textNote.setVisibility(View.VISIBLE);
                    titikDua1.setVisibility(View.VISIBLE);
                    titikDua2.setVisibility(View.VISIBLE);
                    titikDua3.setVisibility(View.VISIBLE);
                    btnActionDetailAds.setVisibility(View.VISIBLE);
                    valueNote.setText(ads.getNote());
                    valueTitle.setText(ads.getAdsTitle());
                    String startDate= ads.getStartDate().substring(0,10);
                    String endDate= ads.getEndDAte().substring(0,10);
                    Date dateAkhir= null;
                    Date dateAwal= null;
                    try {
                        dateAwal = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
                        dateAkhir = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                    SimpleDateFormat awal = new SimpleDateFormat("dd MMMM yyyy");
                    SimpleDateFormat akhir = new SimpleDateFormat("dd MMMM yyyy");


                    valuePublishDate.setText(awal.format(dateAwal)+" s/d "+akhir.format(dateAkhir));
                    valueAdsStatus.setText(ads.getStatus());
                    startDateperpanjangan=endDate;
                    subTitle=ads.getAdsSubTitle();
                    urlImage=ads.getUrlImage();
                    adsCategori=ads.getCategoryName();
                    adsIdCategori=ads.getCategoryName();
                    note=ads.getNote();
                    cp=ads.getCp();
//                    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
//                    Date date = null;
//                    try {
//                        date = sdf.parse(ads.getStartDate());
//                    } catch (ParseException e) {
//                        e.printStackTrace();
//                    }
//                    Calendar cal = Calendar.getInstance();
//                    cal.setTime(date);
//
//                    Toast.makeText(getActivity(), cal.get(Calendar.MONTH), Toast.LENGTH_SHORT).show();

                    if (ads.getUrlImage()!=null) {
                        Picasso.with(getActivity()).load(ads.getUrlImage()).into(bannerAds);
                        foto=ads.getUrlImage();

                    }else {
                        System.out.println("URL GAGAL"+ads.getUrlImage());
                    }
                    if (ads.getStatus().equalsIgnoreCase("In Confirmation")){
                        btnActionDetailAds.setText("Batalkan Iklan");
                        valueAdsKetStatus.setVisibility(View.VISIBLE);
                    }else if (ads.getStatus().equalsIgnoreCase("Cancel")){
                        btnActionDetailAds.setText("Back to My Ads");
                    }else if (ads.getStatus().equalsIgnoreCase("Approved")){
                        btnActionDetailAds.setText("Next");
                    }else if (ads.getStatus().equalsIgnoreCase("published")){
                        btnActionDetailAds.setText("Perpanjang Iklan");
                    }else if (ads.getStatus().equalsIgnoreCase("Waiting for Payment")){
                        btnActionDetailAds.setText("Lihat Info Pembayaran");
                    }
//                    btnActionDetailAds.setText("Back to My Ads");

//                    String vaNumber="7007000547153522";
//                    String batas="07-06-2018 11:17:38";
//                    String adsTitle="Contoh Title Ads89";
//                    String totalBayar="3000000";
//                    boolean save= saveVA(idAds,adsTitle,vaNumber
//                            ,batas,totalBayar);
//                    if (save){
//                        Toast.makeText(getActivity(), "berhasil", Toast.LENGTH_SHORT).show();
//                    }

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

                progressDialog.dismiss();

            }
//            private boolean saveVA(String adsId,String adsTitle, String norek, String batasWaktu, String nominal){
//                boolean saveVA=false;
//                db.insertPembayaranIklan(adsId,adsTitle,norek,batasWaktu,nominal);
//                SessionPembayaranIklan s=db.getPembayaranIkaln(adsId);
//                if (s!=null){
//                    saveVA=true;
//                    System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
//                }
//
//                return saveVA;
//
//            }
            @Override
            public void onFailure(Call<ValueDetailAds> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

                progressDialog.dismiss();
                showAllert("Respon Server","Periksa Koneksi anda, anda tidak dapat terhubung ke server","none");

            }
        });
    }


    private void actionBatalIklan(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("adsId",idAds);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ResponCancelAds> dataCall = apiInterface.cancleAds(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ResponCancelAds>() {
            @Override
            public void onResponse(Call<ResponCancelAds> call, Response<ResponCancelAds> response) {
                progressDialog.dismiss();
                showAllert("Respon server",response.body().getStatus(),"respon batal iklan");


            }

            @Override
            public void onFailure(Call<ResponCancelAds> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

                progressDialog.dismiss();
                showAllert("Respon Server","Periksa Koneksi anda, anda tidak dapat terhubung ke server","none");

            }
        });
    }


    private void showAllert(String title, final String message, final String action){
        alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
        .setMessage(message);


        if (!action.equalsIgnoreCase("none")){
            String btnPositive="Yes";
            boolean negative=true;
            if (action.equalsIgnoreCase("respon batal iklan")){
                btnPositive="Ok";
                negative=false;
            }else {
                btnPositive="Yes";
            }

            alertDialog.setPositiveButton(btnPositive, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    if (action.equalsIgnoreCase("batalkan iklan")){
                        actionBatalIklan();
                    }else if (action.equalsIgnoreCase("respon batal iklan")){
                        if (message.equalsIgnoreCase("success")){
                            valueAdsStatus.setText("Cancel");
                            btnActionDetailAds.setText("Back to My Ads");
                            valueAdsKetStatus.setVisibility(View.GONE);
                        }
                    }
                }
            });
            if (negative){
                alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                    }
                });
            }
        }else {
            alertDialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
        }


        alertDialog.show();

    }


}
