package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListAktivitasOngoing;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPaketSelected;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPaketSelectedAktivitas;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.kriyanesia.lupirka.Util.Function;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDetailActivity extends Fragment implements AdapterListAktivitasOngoing.ItemClickListener{
    private String aktivitasFarmasi, aktivitasHarga ,
     aktivitasLokasi , aktivitasNama , aktivitasNoHp ,aktivitasOngoing,
     aktivitasStatus,aktivitasPembayaran,aktivitasMessage;
    int idAktivitas;
    String aktivitasTitle,aktivitasId;
    Button backBawah;
    ImageButton backAtas;
    RecyclerView recyclerRundown,recyclerHistoryAbsensi;
    Button btn_pretest,btn_posttest,btn_tenant;
    List<ValueDetailAktivitas.DetailCostTemp> aktivitasListCost;
    SessionUser sessionUser;
    List<ValueDetailAktivitas.DetailKegiatanTemp> aktivitasListKegiatan;
    String role;
    Boolean fromPembayaran;

    ArrayList<PaketTemp> listKegiatan;

    public MenuDetailActivity() {
        // Required empty public constructor
    }
    public static MenuDetailActivity newInstance(int idAktivitas, String aktivitasFarmasi,String aktivitasHarga ,
                                                 String aktivitasLokasi ,String aktivitasNama ,String aktivitasNoHp ,
                                                 String aktivitasStatus,String aktivitasMessage,String aktivitasOngoing,String aktivitasPembayaran,
                                                 String aktivitasTitle,String aktivitasId,
                                                 List<ValueDetailAktivitas.DetailCostTemp> aktivitasListCost,
                                                 List<ValueDetailAktivitas.DetailKegiatanTemp> aktivitasListKegiatan) {

        Bundle bundle = new Bundle();
        //bundle.putBoolean("fromPembayaran", fromPembayaran);
        bundle.putInt("idAktivitas", idAktivitas);
        bundle.putString("aktivitasFarmasi", aktivitasFarmasi);
        bundle.putString("aktivitasHarga", aktivitasHarga);
        bundle.putString("aktivitasLokasi", aktivitasLokasi);
        bundle.putString("aktivitasNama", aktivitasNama);
        bundle.putString("aktivitasNoHp", aktivitasNoHp);
        bundle.putString("aktivitasStatus", aktivitasStatus);
        bundle.putString("aktivitasOngoing", aktivitasOngoing);
        bundle.putString("aktivitasPembayaran", aktivitasPembayaran);
        bundle.putString("aktivitasTitle", aktivitasTitle);
        bundle.putString("aktivitasId", aktivitasId);
        bundle.putSerializable("aktivitasListCost", (Serializable) aktivitasListCost);
        bundle.putSerializable("aktivitasListKegiatan", (Serializable) aktivitasListKegiatan);
        bundle.putString("aktivitasMessage",aktivitasMessage);

        MenuDetailActivity fragment = new MenuDetailActivity();
        fragment.setArguments(bundle);
        return fragment;
    }

    View view;
    TextView title, detail_event, name, phone_number, provinsi, kota, resident, workshop1, total, guarantee_letter;
    Button btn_approve, btn_reject;


    ImageButton btnBack;
    TextView provider, status;
    Button btn_cancel;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        sessionUser=new SessionUser();
        role=sessionUser.getRole(getActivity(),"role");
        //fromPembayaran = getArguments().getBoolean("fromPembayaran");
        idAktivitas =getArguments().getInt("idAktivitas");
        aktivitasFarmasi =getArguments().getString("aktivitasFarmasi");
        aktivitasHarga =getArguments().getString("aktivitasHarga");
        aktivitasLokasi =getArguments().getString("aktivitasLokasi");
        aktivitasNama =getArguments().getString("aktivitasNama");
        aktivitasNoHp =getArguments().getString("aktivitasNoHp");
        aktivitasStatus =getArguments().getString("aktivitasStatus");
        aktivitasOngoing =getArguments().getString("aktivitasOngoing");
        aktivitasPembayaran =getArguments().getString("aktivitasPembayaran");
        aktivitasTitle =getArguments().getString("aktivitasTitle");
        aktivitasId =getArguments().getString("aktivitasId");
        aktivitasMessage=getArguments().getString("aktivitasMessage");
//        Toast.makeText(getActivity(), "3 "+idAktivitas, Toast.LENGTH_SHORT).show();
        aktivitasListCost = (List<ValueDetailAktivitas.DetailCostTemp>) getArguments().getSerializable("aktivitasListCost");
        aktivitasListKegiatan = (List<ValueDetailAktivitas.DetailKegiatanTemp>) getArguments().getSerializable("aktivitasListKegiatan");
// if (aktivitasListKegiatan!=null){
//            if (aktivitasListKegiatan.size()>0){
//                Toast.makeText(getActivity(), "Pindah menu ke ongoing "+aktivitasListKegiatan.size(), Toast.LENGTH_SHORT).show();
//
//            }else {
//                Toast.makeText(getActivity(), "Pindah menu ke not ", Toast.LENGTH_SHORT).show();
//            }
//
//        }else {
//            Toast.makeText(getActivity(), "Pindah menu ke not", Toast.LENGTH_SHORT).show();
//        }


        String [] aktivitasPembayaranArr = aktivitasPembayaran.split("-");
        if (aktivitasPembayaranArr!=null){
            if (role.equalsIgnoreCase("7")){
                if (aktivitasPembayaranArr[0].equalsIgnoreCase("Farmasi") &&
                        aktivitasPembayaranArr[1].equalsIgnoreCase("Dokter")){
                    view = inflater.inflate(R.layout.fragment_menu_detail2, container, false);
                    initUIDetail();
                }else if (aktivitasPembayaranArr[0].equalsIgnoreCase("Dokter") &&
                        aktivitasPembayaranArr[1].equalsIgnoreCase("Farmasi")){
                    view = inflater.inflate(R.layout.fragment_menu_detail, container, false);
                    initUiApprove();
                }
            }else if (role.equalsIgnoreCase("2") ||
                    role.equalsIgnoreCase("3")||
                    role.equalsIgnoreCase("5")){
                if (aktivitasOngoing.equalsIgnoreCase("ongoing") && aktivitasStatus.equalsIgnoreCase("Ok")) {
                    if (aktivitasListKegiatan.size()>0){
                        view = inflater.inflate(R.layout.fragment_menu_detail_aktivitas, container, false);
                        initUiOngoing();
                    }else {
                        Toast.makeText(getActivity(), "Datax tidak tersedia", Toast.LENGTH_SHORT).show();
                        getActivity().finish();
                    }

                }else {
                    if (aktivitasPembayaranArr[0].equalsIgnoreCase("Farmasi") &&
                            (aktivitasPembayaranArr[1].equalsIgnoreCase("Dokter") ||
                                    aktivitasPembayaranArr[1].equalsIgnoreCase("Mahasiswa"))){
                        view = inflater.inflate(R.layout.fragment_menu_detail, container, false);
                        initUiApprove();

                    }else if ((aktivitasPembayaranArr[0].equalsIgnoreCase("Dokter") ||
                            aktivitasPembayaranArr[0].equalsIgnoreCase("Mahasiswa")) &&
                            (aktivitasPembayaranArr[1].equalsIgnoreCase("Farmasi") ||
                            aktivitasPembayaranArr[1].equalsIgnoreCase("Sendiri"))){
                        view = inflater.inflate(R.layout.fragment_menu_detail2, container, false);
                        initUIDetail();
                    }
                }
            }else {
                Toast.makeText(getActivity(), "untuk Role ini belum ada detail ", Toast.LENGTH_SHORT).show();
            }

        }



        return view;
    }

    private void initUIDetail() {
        RecyclerView recyclerListJointKegiatan = (RecyclerView) view.findViewById(R.id.recyclerListJointKegiatan);
        title = (TextView)view.findViewById(R.id.title);
        detail_event = (TextView)view.findViewById(R.id.detail_event);
        provider = (TextView)view.findViewById(R.id.provider);
        status = (TextView)view.findViewById(R.id.status);
        btn_cancel = (Button)view.findViewById(R.id.btn_cancel);
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);

        TextView valueEventTotal = (TextView)view.findViewById(R.id.valueEventTotal);
        TextView provider = (TextView) view.findViewById(R.id.provider);
        TextView textTitleMenunggu = (TextView) view.findViewById(R.id.textTitleMenunggu);
        TextView textKeteranganMenunggu = (TextView) view.findViewById(R.id.textKeteranganMenunggu);
        String [] aktivitasPembayaranArr = aktivitasPembayaran.split("-");
        listKegiatan=new ArrayList<>();
        int TotalPembayaran=0;
        if (aktivitasListCost!=null){
            for (int i=0; i<aktivitasListCost.size();i++){
                PaketTemp paketTemp=new PaketTemp();
                paketTemp.setAktifitas(aktivitasListCost.get(i).getPaket());
                paketTemp.setHarga(aktivitasListCost.get(i).getBiaya());
                paketTemp.setKategori_harga(aktivitasListCost.get(i).getTipe_bayar());
                paketTemp.setStatusChecked(true);
                System.out.println("DATAKU "+aktivitasListCost.get(i).getTipe_bayar());
                TotalPembayaran= TotalPembayaran+Integer.valueOf(aktivitasListCost.get(i).getBiaya());
                listKegiatan.add(paketTemp);
            }


            valueEventTotal.setText(Function.ConvertRupiah(TotalPembayaran));
        }
        int numberOfColumns = 1;
        recyclerListJointKegiatan.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        AdapterListPaketSelected adapterListPaketSelected = new AdapterListPaketSelected(getActivity(), listKegiatan);
        recyclerListJointKegiatan.setAdapter(adapterListPaketSelected);

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(((ActivityDetailContent) getActivity()).recentlyPay){
                 Intent i = new Intent(getActivity(),MenuMain.class);
                 i.putExtra("frgToLoad",true);
                 i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                 startActivity(i);
                 getActivity().finish();
                }else {
                    getActivity().finish();
                }
            }
        });
//        title.setText(aktivitas);
        detail_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.layoutDetailActivity, new MenuDetailEvent().newInstance(aktivitasId,"aktivitas"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();
            }
        });
        status.setText(aktivitasMessage);
        if (aktivitasMessage.equalsIgnoreCase("Verifikasi")){
            if (aktivitasPembayaranArr[1].equalsIgnoreCase("sendiri")){
                provider.setText("Event Organizer");
            }else {
                provider.setText(aktivitasPembayaranArr[1]);
            }
            btn_cancel.setText("Cancel Event");
        }else {
            provider.setVisibility(View.GONE);
            textTitleMenunggu.setVisibility(View.GONE);
            textKeteranganMenunggu.setVisibility(View.GONE);
            btn_cancel.setText("BACK");
        }
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        title.setText(aktivitasTitle);


    }
    private void initUiOngoing(){
        recyclerRundown=(RecyclerView) view.findViewById(R.id.recyclerRundown);
        recyclerHistoryAbsensi=(RecyclerView) view.findViewById(R.id.recyclerHistoryAbsensi);
        title = (TextView)view.findViewById(R.id.title);
        detail_event = (TextView)view.findViewById(R.id.detail_event);
        name = (TextView)view.findViewById(R.id.name);
        TextView textWaktu= (TextView) view.findViewById(R.id.textWaktu);
        backBawah = (Button)view.findViewById(R.id.btnBackBawah);
        backAtas = (ImageButton) view.findViewById(R.id.btnBack);

        backAtas.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        backBawah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        title.setText(aktivitasTitle);
        name.setText(aktivitasNama);
        detail_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.detailOngoing, new MenuDetailEvent().newInstance(aktivitasId,"aktivitas"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();
            }
        });
        int numberOfColumns = 1;
        recyclerRundown.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        AdapterListAktivitasOngoing adapterListAktivitasOngoing = new AdapterListAktivitasOngoing(getActivity(), aktivitasListKegiatan);
        recyclerRundown.setAdapter(adapterListAktivitasOngoing);
        adapterListAktivitasOngoing.setmClickListener(MenuDetailActivity.this);

    }
    private void initUiApprove() {
        title = (TextView)view.findViewById(R.id.title);
        btnBack = (ImageButton) view.findViewById(R.id.btnBack);
        btn_approve = (Button) view.findViewById(R.id.btn_approve);
        btn_reject = (Button) view.findViewById(R.id.btn_reject);
        detail_event = (TextView)view.findViewById(R.id.detail_event);
        name = (TextView)view.findViewById(R.id.name);
        phone_number = (TextView)view.findViewById(R.id.phone_number);
        provinsi = (TextView)view.findViewById(R.id.provinsi);
        kota = (TextView)view.findViewById(R.id.kota);
        guarantee_letter = (TextView)view.findViewById(R.id.guarantee_letter);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        if (aktivitasStatus.equalsIgnoreCase("approved")){
            btn_approve.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.GONE);
            guarantee_letter.setText("");
            btn_approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                }
            });

        }else if (aktivitasStatus.equalsIgnoreCase("rejected")){
            btn_approve.setVisibility(View.GONE);
            btn_reject.setVisibility(View.VISIBLE);
            btn_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            guarantee_letter.setText("");
        }else {

            guarantee_letter.setText(aktivitasStatus);
            btn_approve.setVisibility(View.VISIBLE);
            btn_reject.setVisibility(View.VISIBLE);
            btn_approve.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    approvalEvent("Approved");
                }
            });
            btn_reject.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    approvalEvent("Rejected");
                }
            });
        }

        TextView valueEventTotal = (TextView) view.findViewById(R.id.valueEventTotal);
        RecyclerView recyclerListJointKegiatan = (RecyclerView) view.findViewById(R.id.recyclerListJointKegiatan);

        detail_event.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.detailApprove, new MenuDetailEvent().newInstance(aktivitasId,"aktivitas"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();
            }
        });

        int numberOfColumns = 1;
        recyclerListJointKegiatan.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        AdapterListPaketSelectedAktivitas adapterListPaketSelected = new AdapterListPaketSelectedAktivitas(getActivity(), aktivitasListCost);
        recyclerListJointKegiatan.setAdapter(adapterListPaketSelected);
        int TotalPembayaran=0;
        if (aktivitasListCost!=null){
            for (int i=0; i<aktivitasListCost.size();i++){

                TotalPembayaran= TotalPembayaran+Integer.valueOf(aktivitasListCost.get(i).getBiaya());
                System.out.println("DATAKU "+aktivitasListCost.get(i).getPaket());
            }
        }

        valueEventTotal.setText(Function.ConvertRupiah(TotalPembayaran));
        if (role.equalsIgnoreCase("7")){
            name.setText(aktivitasNama);
        }else {
            name.setText(aktivitasFarmasi);
        }
        kota.setText(aktivitasLokasi);
        phone_number.setText(aktivitasNoHp);

        title.setText(aktivitasTitle);
    }

    private void approvalEvent(final String statuss){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        SessionUser sessionUser=new SessionUser();
        String noHp=sessionUser.getNoHp(getActivity(),"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("id_activity",idAktivitas);
            paramObject.put("status",statuss);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA list Aktivitas : "+paramObject.toString());

        Call<ValueDetailAktivitas> data = apiInterface.approvalEvent(paramObject.toString(),"saxasdas");
        data.enqueue(new Callback<ValueDetailAktivitas>() {
            @Override
            public void onResponse(Call<ValueDetailAktivitas> call, Response<ValueDetailAktivitas> response) {
                if (response.body() !=null) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        if (statuss.equalsIgnoreCase("approved")){
                            btn_approve.setVisibility(View.VISIBLE);
                            btn_reject.setVisibility(View.GONE);
                            btn_approve.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });

                        }else if (aktivitasStatus.equalsIgnoreCase("rejected")){
                            btn_approve.setVisibility(View.GONE);
                            btn_reject.setVisibility(View.VISIBLE);
                            btn_reject.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View view) {

                                }
                            });
                        }

//                        status.setText("");
                    }else {
                        btn_approve.setVisibility(View.VISIBLE);
                        btn_reject.setVisibility(View.VISIBLE);
                    }
                    showAllert("Informasi",response.body().getMessage());
                }

            }

            @Override
            public void onFailure(Call<ValueDetailAktivitas> call, Throwable t) {
                Toast.makeText(getActivity(), "Tidak dapat terhubung ke server", Toast.LENGTH_SHORT).show();
//                showAllert("Informasi","Tidak dapat terhubung ke server");


            }
        });
    }

    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    @Override
    public void onQrCodeScanClickListener() {
        Intent i = new Intent(getActivity(),MenuBarcode.class);
        startActivity(i);
    }




}
