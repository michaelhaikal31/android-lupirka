package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListBooth;
import com.kriyanesia.lupirka.AdapterModel.AdapterListDetailEventDate;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuSelectTenant extends Fragment implements AdapterListBooth.ItemClickListener{
    TextView textTitleSambutan;
    RecyclerView recyclerListTenant;
    ImageView imageDenah;
    Button btnNextRegister;
    ImageButton btnBack;
    int amount=0,idTenant=0;

    String idEvent, rumahSakit,alamat,eventName,farmasiDaftar,namaDokter,userType;

    public static MenuSelectTenant newInstance(String idEvent,
                                                  String rumahSakit,String alamat,
                                                  String eventName,String farmasiDaftar,
                                                  String namaDokter,String dokterType) {
        Bundle bundle = new Bundle();
        bundle.putString("eventId", idEvent);
        bundle.putString("rumahSakit", rumahSakit);
        bundle.putString("alamat", alamat);
        bundle.putString("eventName", eventName);
        bundle.putString("farmasiDaftar", farmasiDaftar);
        bundle.putString("namaDokter", namaDokter);
        bundle.putString("userType", dokterType);

        MenuSelectTenant fragment = new MenuSelectTenant();
        fragment.setArguments(bundle);
        return fragment;
    }

    SessionUser sessionUser;
    String nohp;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_select_tenant, container, false);

        textTitleSambutan = (TextView) v.findViewById(R.id.textTitleSambutan);
        recyclerListTenant = (RecyclerView) v.findViewById(R.id.recyclerListTenant);
        imageDenah = (ImageView) v.findViewById(R.id.imageDenah);
        btnNextRegister = (Button) v.findViewById(R.id.btnNextRegister);
        btnBack = (ImageButton) v.findViewById(R.id.btnBack);

        idEvent =getArguments().getString("eventId");
        rumahSakit =getArguments().getString("rumahSakit");
        alamat =getArguments().getString("alamat");
        eventName =getArguments().getString("eventName");
        farmasiDaftar =getArguments().getString("farmasiDaftar");
        namaDokter =getArguments().getString("namaDokter");
        userType= getArguments().getString("userType");
        textTitleSambutan.setText("Pendaftaran "+eventName);


        sessionUser=new SessionUser();
        nohp= sessionUser.getNoHp(getActivity(),"nohp");
        loadTenantList();


        btnNextRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                aksiDaftarTenant();
            }
        });
        return v;
    }


    private void loadTenantList(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {

            paramObject.put("id_login",nohp);
            paramObject.put("tipe","booth");
            paramObject.put("id_event",Integer.valueOf(idEvent));
            paramObject.put("request","all");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueListBooth> dataCall = apiInterface.getListTenant(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueListBooth>() {
            @Override
            public void onResponse(Call<ValueListBooth> call, Response<ValueListBooth> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<ValueListBooth.Booth> dataBooth;
                    dataBooth = response.body().getData().getList();

                    if (!dataBooth.isEmpty()){
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerListTenant.setLayoutManager(layoutManager);
                        AdapterListBooth adapterListBooth = new AdapterListBooth(getActivity(), dataBooth);
                        adapterListBooth.setClickListener(MenuSelectTenant.this);
                        recyclerListTenant.setAdapter(adapterListBooth);


                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueListBooth> call, Throwable t) {


            }
        });
    }




    private void aksiDaftarTenant(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {

            paramObject.put("id_login",nohp);
            paramObject.put("id_tenant",idTenant);
            paramObject.put("id_event",Integer.valueOf(idEvent));
            paramObject.put("amount",amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueListBooth> dataCall = apiInterface.daftarTableTenant(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueListBooth>() {
            @Override
            public void onResponse(Call<ValueListBooth> call, Response<ValueListBooth> response) {
                showAllert("Respon Server",response.body().getMessage(),"");
            }

            @Override
            public void onFailure(Call<ValueListBooth> call, Throwable t) {


            }
        });
    }

    @Override
    public void onItemClick(View view, int position, String idTenant, int amount) {
        this.amount=amount;
        this.idTenant=Integer.valueOf(idTenant);
//        Toast.makeText(getActivity(), idTenant+" "+amount, Toast.LENGTH_SHORT).show();
    }
    private void showAllert(String title, final String message, final String action){

        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                        if (message.equalsIgnoreCase("anda sudah pernah mendaftar event ini") ||
//                                message.equalsIgnoreCase("Saldo tidak cukup mohon topup terlebih dahulu")){
//
//                        }else {
////                            startActivity(intent);
//                        }
                    }
                })
                .show();
    }
}
