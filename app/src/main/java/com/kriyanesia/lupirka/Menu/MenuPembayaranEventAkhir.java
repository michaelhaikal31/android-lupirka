package com.kriyanesia.lupirka.Menu;


import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranEvent;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Constant;

import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPembayaranEventAkhir extends Fragment {
    DatabaseHelper db;
    private String eventId;
    public MenuPembayaranEventAkhir() {
        // Required empty public constructor
    }
    public MenuPembayaranEventAkhir newInstance(String eventId) {
        Bundle bundle = new Bundle();
        bundle.putString("eventId", eventId);

        MenuPembayaranEventAkhir fragment = new MenuPembayaranEventAkhir();
        fragment.setArguments(bundle);
        return fragment;
    }

    TextView tv_hour, tv_minute, tv_second,valueEventTitle,jamNow,valueVA,valueTotalPembayaran,titikDua1,titikDua2;
    SwipeRefreshLayout swipeRefresh;
    RelativeLayout layoutContent;
    String batasWaktu="";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_menu_pembayaran_event_akhir, container, false);
        eventId =getArguments().getString("eventId");
        swipeRefresh = (SwipeRefreshLayout) v.findViewById(R.id.swipeRefresh);
        tv_hour = (TextView)v.findViewById(R.id.textJam);
        tv_minute = (TextView)v.findViewById(R.id.textMenit);
        tv_second = (TextView)v.findViewById(R.id.textDetik);
        titikDua1 = (TextView)v.findViewById(R.id.titikDua1);
        titikDua2 = (TextView)v.findViewById(R.id.titikDua2);
        valueEventTitle = (TextView)v.findViewById(R.id.valueTitleEvent);
        jamNow = (TextView)v.findViewById(R.id.jamNow);
        valueVA = (TextView)v.findViewById(R.id.valueVA);
        layoutContent = (RelativeLayout) v.findViewById(R.id.layoutContent);

        valueTotalPembayaran = (TextView)v.findViewById(R.id.valueTotalPembayaran);
        clearView();
        loadData();
        loadTimeServer();
        swipeRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                clearView();
                loadData();
                swipeRefresh.setRefreshing(false);
            }
        });
        return v;
    }
    private void clearView(){
        layoutContent.setVisibility(View.GONE);
    }
    private void loadData(){
        db= new DatabaseHelper(getActivity());
        SessionPembayaranEvent s=db.getPembayaranEvent(eventId);
        if (s!=null){
            valueEventTitle.setText(s.getTitle());
            valueVA.setText(s.getNorek());
            valueTotalPembayaran.setText(s.getNominal());
            batasWaktu=s.getBataswaktu();
            System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
            layoutContent.setVisibility(View.VISIBLE);
        }else {

        }

    }

    private void loadTimeServer(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_TIME)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        Call<String> data = apiInterface.getTimeServer("saxasdas");

        data.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                try {
                    String str_date=response.body();
                    Date date ;
                    SimpleDateFormat formatBatas = new SimpleDateFormat("dd MMM yyyy | HH:mm");
                    SimpleDateFormat formatCountDown = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
                    SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
                    date = (Date)formatDate.parse(str_date);
                    Calendar cal=Calendar.getInstance();
                    cal.setTime(date);

                    Date dateBatas = (Date)formatDate.parse(str_date);
                    Calendar calBatas=Calendar.getInstance();
                    calBatas.setTime(dateBatas);
                    jamNow.setText(formatBatas.format(dateBatas));
                    System.out.println("Today is " +cal.toString() );

                    startCountDown(formatCountDown.format(cal.getTime()));
                } catch (ParseException e)
                {
                    System.out.println("Exception :"+e);
                }

            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }


    long startTime;
    private void startCountDown(String endTime)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss");
        formatter.setLenient(false);


//        String endTime = "06.06.2018, 12:05:36";
        long milliseconds=0;

        final CountDownTimer mCountDownTimer;

        Date endDate;
        try {
            endDate = formatter.parse(endTime);
            milliseconds = endDate.getTime();

        } catch (ParseException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        startTime = System.currentTimeMillis();


        mCountDownTimer = new CountDownTimer(milliseconds, 1000) {
            @Override
            public void onTick(long millisUntilFinished) {

                startTime=startTime-1;
                Long serverUptimeSeconds =
                        (millisUntilFinished - startTime) / 1000;

                String daysLeft = String.format("%d", serverUptimeSeconds / 86400);
                //txtViewDays.setText(daysLeft);
                Log.d("daysLeft",daysLeft);

                String hoursLeft = String.format("%d", (serverUptimeSeconds % 86400) / 3600);

                if (Integer.valueOf(hoursLeft)<1){
                    tv_hour.setText("Waktu Habis");
                    tv_hour.setTextSize(30f);
                    titikDua1.setVisibility(View.GONE);
                    titikDua2.setVisibility(View.GONE);
                    tv_minute.setVisibility(View.GONE);
                    tv_second.setVisibility(View.GONE);
                }else {
                    titikDua1.setVisibility(View.VISIBLE);
                    titikDua2.setVisibility(View.VISIBLE);
                    tv_minute.setVisibility(View.VISIBLE);
                    tv_second.setVisibility(View.VISIBLE);
                    if (Integer.valueOf(hoursLeft)<10 && Integer.valueOf(hoursLeft)>0) {
                        if (hoursLeft.startsWith("0")) {
                            tv_hour.setText(hoursLeft);
                        } else {
                            tv_hour.setText("0" + hoursLeft);
                        }
                    }else {
                        tv_hour.setText(hoursLeft);
                    }

                    String minutesLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) / 60);
                    if (Integer.valueOf(minutesLeft)<10) {
                        if (minutesLeft.startsWith("0")) {
                            tv_minute.setText(hoursLeft);
                        } else {
                            tv_minute.setText("0" + minutesLeft);
                        }
                    }else {
                        tv_minute.setText(minutesLeft);
                    }

                    String secondsLeft = String.format("%d", ((serverUptimeSeconds % 86400) % 3600) % 60);
                    if (Integer.valueOf(secondsLeft)<10) {
                        if (secondsLeft.startsWith("0")) {
                            tv_second.setText(secondsLeft);
                        } else {
                            tv_second.setText("0" + secondsLeft);
                        }
                    }else {
                        tv_second.setText(secondsLeft);
                    }
                }




            }

            @Override
            public void onFinish() {

            }
        }.start();


    }



}
