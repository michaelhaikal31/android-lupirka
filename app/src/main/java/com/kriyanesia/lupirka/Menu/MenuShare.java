package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Util.Constant;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuShare extends Activity {
    @BindView(R.id.textDesc)
    TextView textDesc;
    @BindView(R.id.btnShare)
    Button btnShare;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_share);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        textDesc.setText(Constant.TEXT_SHARE);
        btnShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuShare.this, ShareSosmed.class);
                startActivity(i);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
