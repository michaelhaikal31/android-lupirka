package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.R;

import java.io.File;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuRegisterEventPreview extends Activity {
    @BindView(R.id.txtEventTitle)
    TextView txtEventTitle;
    @BindView(R.id.txtEventSubTitle)
    TextView txtEventSubTitle;
    @BindView(R.id.txtDateStartEvent)
    TextView txtDateStartEvent;
    @BindView(R.id.txtDateEndEvent)
    TextView txtDateEndEvent;
    @BindView(R.id.txtVenueEvent)
    TextView txtVenueEvent;
    @BindView(R.id.txtEventCity)
    TextView txtEventCity;
    @BindView(R.id.txtEventCategori)
    TextView txtEventCategori;
    @BindView(R.id.txtEventNote)
    TextView txtEventNote;
    @BindView(R.id.txtCP)
    TextView txtCP;
    @BindView(R.id.txtBanner)
    TextView txtBanner;
    @BindView(R.id.txtFile)
    TextView txtFile;
    @BindView(R.id.txtSertifikat)
    TextView txtSertifikat;

    @BindView(R.id.btnBack)
    ImageButton btnBack;

    @BindView(R.id.btnSendRegister)
    Button btnSendRegister;

    private String title="",subTitle="",startEvent="",endEvent="",
            venueEvent="",cityEvent="",kategoriEvent="",noteEvent="",CP="";

    private String[] pathFile;
    private String[] bannerFile;
    private String[] SertifikatFile;
    private String displayName="";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_register_event_preview);
        ButterKnife.setDebug(true);
        ButterKnife.bind(MenuRegisterEventPreview.this);
        Intent intent = getIntent();
        Bundle extras = intent.getExtras();
        title = extras.getString("title");
        subTitle = extras.getString("subtitle");
        startEvent = extras.getString("startdate");
        endEvent = extras.getString("enddate");
        venueEvent = extras.getString("venue");
        cityEvent = extras.getString("city");
        kategoriEvent = extras.getString("category");
        noteEvent = extras.getString("note");
        CP = extras.getString("cp");
        pathFile = extras.getStringArray("file");
        bannerFile = extras.getStringArray("banner");
        SertifikatFile = extras.getStringArray("sertifikat");

        txtEventTitle.setText(title);
        txtEventSubTitle.setText(subTitle);
        txtDateStartEvent.setText(startEvent);
        txtDateEndEvent.setText(endEvent);
        txtVenueEvent.setText(venueEvent);
        txtEventCity.setText(cityEvent);
        txtEventCategori.setText(kategoriEvent);
        txtEventNote.setText(noteEvent);
        txtCP.setText(CP);
        if (pathFile!=null){
            getName(pathFile,"file");
        }

        if (bannerFile!=null){
            getName(bannerFile,"banner");

        }
        if (SertifikatFile!=null){
            getName(SertifikatFile,"sertifikat");

        }

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btnSendRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finishAffinity();
                Intent intent = new Intent(getApplicationContext(), MenuMain.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        });


    }

    private void getName(String[] file,String from){
        for(int i = 0; i < file.length; i++) {
            File sourceFile = new File(file[i]);
            Uri uri = Uri.parse(file[i]);

            if (file[i].startsWith("content://")) {
                Cursor cursor = null;

                try {
                    cursor = getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        if (i+1==file.length) {
                            displayName += cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }else {
                            displayName += cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME))+" , ";
                        }
                    }
                } finally {
                    cursor.close();
                }
            } else if (file[i].startsWith("file://")) {

                Toast.makeText(this, "file://", Toast.LENGTH_SHORT).show();
                displayName = sourceFile.getName();
            }
//            Toast.makeText(this, "From "+from+" name "+displayName, Toast.LENGTH_SHORT).show();
            if (from.equalsIgnoreCase("banner")){
                txtBanner.setText(displayName);
            }else if (from.equalsIgnoreCase("sertifikat")){
                txtSertifikat.setText(displayName);
            }else if (from.equalsIgnoreCase("file")){
                txtFile.setText(displayName);
            }

        }
        displayName="";
    }
}
