package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageButton;
import android.widget.TextView;

import com.kriyanesia.lupirka.AdapterModel.AdapterBankContactUs;
import com.kriyanesia.lupirka.AdapterModel.AdapterListContactUs;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Profesi.Profesi;
import com.kriyanesia.lupirka.Data.Profesi.ValueProfesi;
import com.kriyanesia.lupirka.Data.ValueContactUs;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MenuContactUs extends Activity {
    @BindView(R.id.namaUser)
    TextView namaUser;
    @BindView(R.id.hallo)
    TextView textHalo;
    @BindView(R.id.textIntroContact)
    TextView textIntroContact;
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @BindView(R.id.recyclerContactUs)
    RecyclerView recyclerContactUs;
    @BindView(R.id.recyclerBankCP)
    RecyclerView recyclerBankCP;
    String namaPengguna;
    SessionUser sessionUser;
    AdapterListContactUs adapterListContactUs;
    AdapterBankContactUs adapterBankContactUs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_contact_us);

        ButterKnife.setDebug(true);
        ButterKnife.bind(this);

        sessionUser=new SessionUser();
        namaPengguna=sessionUser.getName(this,"name");
        loadContactList();
//        Typeface custom_font = Typeface.createFromAsset(getAssets(),  "LEELAUIB.ttf");
//
//        namaUser.setTypeface(custom_font);
//        textHalo.setTypeface(custom_font);
        namaUser.setText(namaPengguna);
        textIntroContact.setText(Constant.TEXT_INTRO_CONTAC_US);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void loadContactList(){
//        profesi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        String noHp=sessionUser.getNoHp(MenuContactUs.this,"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueContactUs> data = apiInterface.getContactUs(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueContactUs>() {
            @Override
            public void onResponse(Call<ValueContactUs> call, Response<ValueContactUs> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueContactUs.ContactUs> dataCP;
                    List<ValueContactUs.ContactBank> dataBank;
                    dataCP = response.body().getData().getList_contact();
                    dataBank = response.body().getData().getList_bank();
                    final LinearLayoutManager layoutManager = new LinearLayoutManager(MenuContactUs.this);
                    layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerContactUs.setLayoutManager(layoutManager);
                    adapterListContactUs = new AdapterListContactUs(MenuContactUs.this, dataCP);
                    recyclerContactUs.setAdapter(adapterListContactUs);


                    final LinearLayoutManager layoutManagerBank = new LinearLayoutManager(MenuContactUs.this);
                    layoutManagerBank.setOrientation(LinearLayoutManager.VERTICAL);
                    recyclerBankCP.setLayoutManager(layoutManagerBank);
                    adapterBankContactUs = new AdapterBankContactUs(MenuContactUs.this, dataBank);
                    recyclerBankCP.setAdapter(adapterBankContactUs);




//                    adapterSpinerProfesi = new AdapterSpinerProfesi(MenuRegister.this, profesi);
//                    spinerUserType.setAdapter(adapterSpinerProfesi);

//                    System.out.println("VALUENYA : "+profesi.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueContactUs> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
//                showAllert("Respon Server","Tidak dapat terhubung ke server","loadProfesi");

            }
        });
    }
}
