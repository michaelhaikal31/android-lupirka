package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.kriyanesia.lupirka.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuHelp extends Activity {
    @BindView(R.id.btnBack)
    ImageButton btnBack;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_help);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

    }
}
