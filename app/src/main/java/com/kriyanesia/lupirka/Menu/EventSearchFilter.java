package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListAlertDialog;
import com.kriyanesia.lupirka.AdapterModel.AutocompleteAdapter;
import com.kriyanesia.lupirka.AdapterModel.SearchDokterAdapter;
import com.kriyanesia.lupirka.AdapterModel.SearchEventAdapter;
import com.kriyanesia.lupirka.AdapterModel.SearchKotaAdapter;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Dokter.Dokter;
import com.kriyanesia.lupirka.Data.Event.ValueEventCategory;
import com.kriyanesia.lupirka.Data.Profesi.Profesi;
import com.kriyanesia.lupirka.Data.Profesi.ValueProfesi;
import com.kriyanesia.lupirka.Data.ValueKota;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class EventSearchFilter extends Fragment implements AdapterListAlertDialog.ItemClickListener {
    Button spinnerEventCategori, btnSpinnerEventCategori,btnFilter;
    AutoCompleteTextView autoComplateKota;
    TextView etPublisStartDate,etPublisEndDate;
    private String eventCategory;
    private int idCategori;
    private int idCity;
    private String city;
    List<ValueEventCategory.Categori> listCategori=new ArrayList<>();
    SessionUser sessionUser;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog formStartDate;
    private DatePickerDialog formEndDate;
    public EventSearchFilter() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
         View v=inflater.inflate(R.layout.fragment_event_search_filter, container, false);
        spinnerEventCategori = (Button) v.findViewById(R.id.spinnerEventCategori);
        btnSpinnerEventCategori = (Button) v.findViewById(R.id.btnSpinnerEventCategori);
        btnFilter = (Button) v.findViewById(R.id.btnFilter);

//        checkBoxSave = (CheckBox) v.findViewById(R.id.checkBoxSave);
        autoComplateKota = (AutoCompleteTextView) v.findViewById(R.id.kota);
//        etPublisStartDate = (TextView) v.findViewById(R.id.etPublisStartDate);
//        etPublisEndDate = (TextView) v.findViewById(R.id.etPublisEndDate);
        sessionUser=new SessionUser();
        city=sessionUser.getEventCityName(getActivity(),"cityName");
        try {
            idCity=Integer.valueOf(sessionUser.getEventCity(getActivity(),"city"));
            idCategori=Integer.valueOf(sessionUser.getEventCategory(getActivity(),"category"));
        } catch(NumberFormatException nfe) {
            idCity=0;
            idCategori=0;
        }
        eventCategory=sessionUser.getEventCategoryName(getActivity(),"categoryName");
        spinnerEventCategori.setText(eventCategory);
        autoComplateKota.setText(city);

        autoComplateKota.clearFocus();
        autoComplateKota.setDropDownBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.bgWhite)));
        autoComplateKota.setDropDownBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.bgWhite)));
//        final SearchKotaAdapter adapter = new SearchKotaAdapter(getActivity(),android.R.layout.simple_spinner_dropdown_item);
//        autoComplateKota.setAdapter(adapter);

//        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
//        Calendar newCalendarStart = Calendar.getInstance();
//        formStartDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                etPublisStartDate.setText(dateFormatter.format(newDate.getTime()));
//            }
//
//        },newCalendarStart.get(Calendar.YEAR), newCalendarStart.get(Calendar.MONTH), newCalendarStart.get(Calendar.DAY_OF_MONTH));
//

//        etPublisStartDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//
//            }
//        });
//        etPublisStartDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
//            @Override
//            public void onFocusChange(View v, boolean hasFocus) {
//                if(hasFocus) {
//                    formStartDate.show();
//                }
//
//            }
//        });

//        Calendar newCalendarEnd = Calendar.getInstance();
//        formEndDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                int lastBulan=monthOfYear+1;
//                if (!etPublisStartDate.getText().toString().isEmpty()){
//                    String bulan = etPublisStartDate.getText().toString().substring(5,7);
//                    String hari = etPublisStartDate.getText().toString().substring(8);
//                    String fixBulan=bulan;
//                    String fixHari=hari;
//                    if (bulan.startsWith("0")){
//                        fixBulan=bulan.substring(1);
//                    }
//                    if (hari.startsWith("0")){
//                        fixHari=hari.substring(1);
//                    }
//
//                    if (Integer.valueOf(fixHari)>dayOfMonth){
//                        if (Integer.valueOf(fixBulan)<lastBulan){
//                            etPublisEndDate.setText(dateFormatter.format(newDate.getTime()));
//                        }else {
//                            showAllert("Informasi","Tanggal Ahkir harus lebih besar atau sama dengan tanggal mulai");
//                        }
//                    }else if (Integer.valueOf(fixHari)<=dayOfMonth){
//                        if(Integer.valueOf(fixBulan)<=lastBulan){
//                            etPublisEndDate.setText(dateFormatter.format(newDate.getTime()));
//                        }else {
//                            showAllert("Informasi","Tanggal Ahkir harus lebih besar atau sama dengan tanggal mulai");
//
//                        }
//                    }
//                }else {
//                    showAllert("Informasi","Tanggal Mulai Tidak boleh kosong");
//                }
//
//            }
//
//        },newCalendarEnd.get(Calendar.YEAR), newCalendarEnd.get(Calendar.MONTH), newCalendarEnd.get(Calendar.DAY_OF_MONTH));
//
//        etPublisEndDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hideSoftKeyboard();
//                formEndDate.show();
//            }
//        });
//        etPublisStartDate.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                hideSoftKeyboard();
//                formStartDate.show();
//            }
//        });

        autoComplateKota.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                ValueKota.Kota selected = (ValueKota.Kota) arg0.getAdapter().getItem(arg2);

                idCity=Integer.valueOf(selected.getId());
                city=selected.getKota();
                autoComplateKota.setText(selected.getKota());
            }
        });


        loadCategoriEvent();

//        idCategori=spinnerEventCategori.getText().toString();
        spinnerEventCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCategoriEvent();
            }
        });
        btnSpinnerEventCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCategoriEvent();
            }
        });

        btnFilter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sessionUser= new SessionUser();
                sessionUser.setEventCategoryName(getActivity(),"categoryName",eventCategory);
                sessionUser.setEventCityName(getActivity(),"cityName",city);
                sessionUser.setEventCategory(getActivity(),"category",String.valueOf(idCategori));
                sessionUser.setEventCity(getActivity(),"city",String.valueOf(idCity));
                getActivity().finish();
            }
        });

        return v;
    }
    public void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        final View view = getActivity().findViewById(android.R.id.content);
        if (view != null){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    private void showAllert(String title, String message){
        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
    private void loadCategoriEvent(){
        listCategori.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueEventCategory> data = apiInterface.getCategoriEvent(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueEventCategory>() {
            @Override
            public void onResponse(Call<ValueEventCategory> call, Response<ValueEventCategory> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueEventCategory.Categori> data;
                    data = response.body().getData().getCategoryevent();

                    if (!data.isEmpty()){
                        listCategori.addAll(data);

                    }



                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueEventCategory> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }
    AlertDialog dialog;
    private void checkCategoriEvent() {
        if (!listCategori.isEmpty()) {


            AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getLayoutInflater();
            View convertView = (View) inflater.inflate(R.layout.row_list_alertdialog, null);
            alertDialog.setView(convertView);
            alertDialog.setTitle("Pilih Kategori");
            WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
            dialog = alertDialog.create();


            RecyclerView rv = (RecyclerView) convertView.findViewById(R.id.list_Alert);
            AdapterListAlertDialog adapterListAlertDialog = new AdapterListAlertDialog(getActivity(),listCategori);
            rv.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapterListAlertDialog.setmClickListener(EventSearchFilter.this);
            rv.setAdapter(adapterListAlertDialog);

            // Finally, display the alert dialog
            dialog.show();

            // Get screen width and height in pixels
            DisplayMetrics displayMetrics = new DisplayMetrics();
            getActivity().getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            // The absolute width of the available display size in pixels.
            int displayWidth = displayMetrics.widthPixels;
            // The absolute height of the available display size in pixels.
            int displayHeight = displayMetrics.heightPixels;

            // Initialize a new window manager layout parameters
            WindowManager.LayoutParams layoutParams = new WindowManager.LayoutParams();

            // Copy the alert dialog window attributes to new layout parameter instance
            layoutParams.copyFrom(dialog.getWindow().getAttributes());

            // Set the alert dialog window width and height
            // Set alert dialog width equal to screen width 90%
            // int dialogWindowWidth = (int) (displayWidth * 0.9f);
            // Set alert dialog height equal to screen height 90%
            // int dialogWindowHeight = (int) (displayHeight * 0.9f);

            // Set alert dialog width equal to screen width 70%
            int dialogWindowWidth = (int) (displayWidth * 0.8f);
            // Set alert dialog height equal to screen height 70%
            int dialogWindowHeight = (int) (displayHeight * 0.7f);

            // Set the width and height for the layout parameters
            // This will bet the width and height of alert dialog
            layoutParams.width = dialogWindowWidth;
            layoutParams.height = dialogWindowHeight;

            // Apply the newly created layout parameters to the alert dialog window
            dialog.getWindow().setAttributes(layoutParams);


//            alertDialog.show();

//            final String[] arrData =new String[listCategori.size()+1];
//            arrData[0] = "Pilih Kategori";
//
//            LayoutInflater inflater = (LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
//            View myScrollView = inflater.inflate(R.layout.row_list_alertdialog, null, false);
//
//            // textViewWithScroll is the name of our TextView on scroll_text.xml
//            TextView tv = (TextView) myScrollView
//                    .findViewById(R.id.textList);
//
//            // Initializing a blank textview so that we can just append a text later
//            tv.setText("");
//            int j=0;
//            for (int i = 1; i <= listCategori.size(); i++) {
//                arrData[i] = listCategori.get(j).getName();
//                tv.append(listCategori.get(j).getName());
//                j=j+1;
//            }
//            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//            builder.setTitle("Pilih Tipe User").setView(myScrollView);
//            tv.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Toast.makeText(getActivity(), "ada", Toast.LENGTH_SHORT).show();
//                }
//            });
//
//            builder.setItems(arrData, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int position) {
//
//                    if (position==0){
//                        spinnerEventCategori.setText(arrData[0]);
//                        idCategori=Integer.valueOf(0);
//                        eventCategory=arrData[0];
//                    }else if (position>0){
//                        spinnerEventCategori.setText(listCategori.get(position+1).getName());
//                        idCategori=Integer.valueOf(listCategori.get(position+1).getId());
//                        eventCategory=listCategori.get(position).getName();
//
//                    }
//
//
//                }
//            }).show();
        }
    }

    @Override
    public void onListAlertClick(int id,String nama) {
        idCategori=id;
        eventCategory=nama;
        spinnerEventCategori.setText(nama);
        dialog.dismiss();
    }
}
