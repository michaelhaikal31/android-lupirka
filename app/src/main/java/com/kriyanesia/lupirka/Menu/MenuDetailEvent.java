package com.kriyanesia.lupirka.Menu;


import android.Manifest;
import android.app.DownloadManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.FileProvider;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterAttachEvent;
import com.kriyanesia.lupirka.AdapterModel.AdapterBannerEvent;
import com.kriyanesia.lupirka.AdapterModel.AdapterListDetailEventDate;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPost;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.ValueDetailEvent;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.Manifest.permission.CAMERA;
import static android.content.Context.DOWNLOAD_SERVICE;
import static android.content.Context.NOTIFICATION_SERVICE;
import static android.provider.Settings.AUTHORITY;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuDetailEvent extends Fragment{

    private String idEvent;
    ProgressDialog mProgressDialog;
    FrameLayout detailMenuEvent;
    SwipeRefreshLayout layoutRefresh;
    RelativeLayout layoutDetailAds;
    RecyclerView recyclerDetailWaktu;
    TextView eventTitle,tempat,waktu,kota,kategori,penyelenggara,keterangan;
    Button btnRegisterEventUser;
    ImageView imgEvent;
    String urlBanner;
    RelativeLayout toolbar;
    String from="";
    private long enq;

    ImageButton btnBack;
    public static MenuDetailEvent newInstance(String id,String from) {

        Bundle bundle = new Bundle();
        bundle.putString("id", id);
        bundle.putString("from", from);

        MenuDetailEvent fragment = new MenuDetailEvent();
        fragment.setArguments(bundle);
        return fragment;
    }

    SessionUser sessionUser;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_detail_event, container, false);
        idEvent =getArguments().getString("id");
        from =getArguments().getString("from");
        loadDetailEvent(idEvent);
        eventTitle = (TextView) v.findViewById(R.id.txtValueEvent);
        layoutRefresh = (SwipeRefreshLayout) v.findViewById(R.id.layoutRefresh);
        detailMenuEvent = (FrameLayout) v.findViewById(R.id.detailMenuEvent);
        layoutDetailAds = (RelativeLayout) v.findViewById(R.id.layoutDetailAds);
        tempat = (TextView) v.findViewById(R.id.txtValueTempat);
        toolbar = (RelativeLayout) v.findViewById(R.id.toolbar);
        waktu = (TextView) v.findViewById(R.id.txtValueWaktu);
        kota = (TextView) v.findViewById(R.id.txtValueKota);
        kategori = (TextView) v.findViewById(R.id.txtValueKategori);
        penyelenggara = (TextView) v.findViewById(R.id.txtValuePenyelenggara);
        keterangan = (TextView) v.findViewById(R.id.txtValueKeterangan);
        imgEvent = (ImageView) v.findViewById(R.id.imgEvent);
        btnBack = (ImageButton) v.findViewById(R.id.btnBack);
        sessionUser= new SessionUser();
        String role=sessionUser.getRole(getActivity(),"role");
        layoutDetailAds.setVisibility(View.GONE);
        btnRegisterEventUser = (Button) v.findViewById(R.id.btnRegisterEventUser);
        imgEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                intent.putExtra("menu","PreviewImage");
                intent.putExtra("urlBanner",urlBanner);
                startActivity(intent);
            }
        });
        recyclerDetailWaktu = (RecyclerView) v.findViewById(R.id.recyclerWaktuDetail);
        layoutRefresh.setRefreshing(true);
        layoutRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                loadDetailEvent(idEvent);
            }
        });
        if (role.equalsIgnoreCase("8")){
            btnRegisterEventUser.setText("Registrasi Tenant");
        }else {
            btnRegisterEventUser.setText("Registrasi Event");
        }
        if (from.equalsIgnoreCase("aktivitas")){
            toolbar.setVisibility(View.VISIBLE);
        }else {
            toolbar.setVisibility(View.GONE);
        }
        btnRegisterEventUser.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                intent.putExtra("menu","Pendaftaran Event");
                intent.putExtra("eventName",eventTitle.getText().toString());
                intent.putExtra("eventId",idEvent);
                startActivity(intent);
            }
        });
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        return v;
    }
    public void startDownload(String url) {
        String[] attachArr=new String[2];
        String[] exten= new String[2];
        final ArrayList<Long> list = new ArrayList<>();

        if (url.startsWith("http://")) {
            attachArr = url.split("event/");
            exten=attachArr[1].split("\\.");

        }
        try {
            File direct = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                    "LUPIRKA");

            if (!direct.exists()) {
                direct.mkdir();
//            Log.d(LOG_TAG, "dir created for first time");
            }

            DownloadManager dm = (DownloadManager) getActivity().getSystemService(DOWNLOAD_SERVICE);
            Uri downloadUri = Uri.parse(url);
            if (exten[1].equalsIgnoreCase("jpg")){
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false)
                        .setTitle(attachArr[1])
                        .setMimeType("image/jpeg")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                                "/LUPIRKA/" + attachArr[1]);
                dm.enqueue(request);
            }else if (exten[1].equalsIgnoreCase("csv")){
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false)
                        .setTitle(attachArr[1])
                        .setMimeType("application/x-excel")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                                "/LUPIRKA/" + attachArr[1]);
                dm.enqueue(request);
            }else {
                getActivity().registerReceiver(attachmentDownloadCompleteReceive, new IntentFilter(
                        DownloadManager.ACTION_DOWNLOAD_COMPLETE));
                DownloadManager.Request request = new DownloadManager.Request(downloadUri);
                request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI | DownloadManager.Request.NETWORK_MOBILE)
                        .setAllowedOverRoaming(false)
                        .setTitle(attachArr[1])
                        .setMimeType("application/pdf")
                        .setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
                        .setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS,
                                "/LUPIRKA/" + attachArr[1]);
                dm.enqueue(request);
            }
            //lama
            if (DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED==1) {
                Toast.makeText(getActivity(), "Downloading", Toast.LENGTH_SHORT).show();
            }else {
                Toast.makeText(getActivity(), "Download file...", Toast.LENGTH_SHORT).show();
            }
        }catch (Exception e){
            Log.e("ERRORNYA ",e.getMessage());
        }

    }

    BroadcastReceiver attachmentDownloadCompleteReceive = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (DownloadManager.ACTION_DOWNLOAD_COMPLETE.equals(action)) {
                long downloadId = intent.getLongExtra(
                        DownloadManager.EXTRA_DOWNLOAD_ID, 0);
                //Toast.makeText(getActivity(),"beres",Toast.LENGTH_SHORT).show();
                getActivity().unregisterReceiver(attachmentDownloadCompleteReceive);
                openDownloadedAttachment(getContext(),downloadId);
            }
        }
    };
    private void openDownloadedAttachment(final Context context, final long downloadId) {
        DownloadManager downloadManager = (DownloadManager) context.getSystemService(Context.DOWNLOAD_SERVICE);
        DownloadManager.Query query = new DownloadManager.Query();
        query.setFilterById(downloadId);
        Cursor cursor = downloadManager.query(query);
        if (cursor.moveToFirst()) {
            int downloadStatus = cursor.getInt(cursor.getColumnIndex(DownloadManager.COLUMN_STATUS));
            String downloadLocalUri = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_LOCAL_URI));
            String downloadMimeType = cursor.getString(cursor.getColumnIndex(DownloadManager.COLUMN_MEDIA_TYPE));
            if ((downloadStatus == DownloadManager.STATUS_SUCCESSFUL) && downloadLocalUri != null) {
                openDownloadedAttachment(context, Uri.parse(downloadLocalUri), downloadMimeType);
            }
        }
        cursor.close();
    }
    private void openDownloadedAttachment(final Context context, Uri attachmentUri, final String attachmentMimeType) {
        if(attachmentUri!=null) {
            // Get Content Uri.
            if (ContentResolver.SCHEME_FILE.equals(attachmentUri.getScheme())) {
                // FileUri - Convert it to contentUri.
                File file = new File(attachmentUri.getPath());
                attachmentUri = FileProvider.getUriForFile(getActivity(), "com.kriyanesia.lupirka.provider", file);
            }

            Intent openAttachmentIntent = new Intent(Intent.ACTION_VIEW);
            openAttachmentIntent.setDataAndType(attachmentUri, attachmentMimeType);
            openAttachmentIntent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
            try {
                context.startActivity(openAttachmentIntent);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(context,"Cannot open file", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void showProgressNotification(int notificationId,Intent intent, String title, String message)
    {

        NotificationManager notificationManager = (NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);

        String channelId = "channel-01";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
            NotificationChannel mChannel = new NotificationChannel(
                    channelId, channelName, importance);
            notificationManager.createNotificationChannel(mChannel);
        }

        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getActivity(), channelId)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(getActivity());
        stackBuilder.addNextIntent(intent);
        PendingIntent resultPendingIntent = stackBuilder.getPendingIntent(
                0,
                PendingIntent.FLAG_UPDATE_CURRENT
        );
        mBuilder.setContentIntent(resultPendingIntent);
        notificationManager.notify(notificationId, mBuilder.build());
    }

    @Override
    public void onResume() {
        super.onResume();
        if (from.equalsIgnoreCase("aktivitas")){
            toolbar.setVisibility(View.VISIBLE);
        }else {
            toolbar.setVisibility(View.GONE);


        }
    }


    private void loadDetailEvent(final String idEvent){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_event",idEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueDetailEvent> data = apiInterface.getEventDetail(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueDetailEvent>() {
            @Override
            public void onResponse(Call<ValueDetailEvent> call, Response<ValueDetailEvent> response) {
                layoutRefresh.setRefreshing(false);
                layoutDetailAds.setVisibility(View.VISIBLE);
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    Event event =new Event();
                    event = response.body().getData();
                    eventTitle.setText(event.getTitle());
                    tempat.setText(event.getVenue());
                    String startDate= (event.getStart()).substring(0,10);
                    String endDate= (event.getEnd().substring(0,10));
                    Date dateAkhir= null;
                    Date dateAwal= null;
                    try {
                        dateAwal = new SimpleDateFormat("yyyy-MM-dd").parse(startDate);
                        dateAkhir = new SimpleDateFormat("yyyy-MM-dd").parse(endDate);
                        SimpleDateFormat awal = new SimpleDateFormat("dd MMMM yyyy");
                        SimpleDateFormat akhir = new SimpleDateFormat("dd MMMM yyyy");
                        waktu.setText(awal.format(dateAwal)+" s/d "+akhir.format(dateAkhir));
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    kota.setText(event.getCity());
                    kategori.setText(event.getKategori());
                    keterangan.setText(event.getDesc());
                    penyelenggara.setText(event.getContact_name());
                    if (event.getDetail_event().size()>0) {
                        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
                        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
                        recyclerDetailWaktu.setLayoutManager(layoutManager);
                        AdapterListDetailEventDate adapterListDetailEventDate = new AdapterListDetailEventDate(getActivity(), event.getDetail_event());
                        recyclerDetailWaktu.setAdapter(adapterListDetailEventDate);
                    }
                    String banner="";
                    String attach="";
                    if (event.getImage()!=null) {
                        banner = event.getImage();
                    }
                    if (event.getAttachment()!=null) {
                        attach = event.getAttachment();
                    }
                    String[] bannerArr;
                    if (banner!="") {

                        bannerArr = banner.split(";");
                        if (bannerArr[0].contains(".png")|| bannerArr[0].contains(".jpg")) {
                            Picasso.with(getActivity()).load(bannerArr[0]).into(imgEvent);
                            urlBanner=bannerArr[0];
                        }
//                        setFileImage(bannerArr);

                    }
                    String[] attachArr=new String[1];
                    if (attach!="") {

                        attachArr[0] = attach;
                        setFileAttach(attachArr);

                    }
//                    Toast.makeText(getActivity(), ""+attachArr[0], Toast.LENGTH_SHORT).show();
                    System.out.println("DATA ATTACH "+attachArr[0]);




                }
                System.out.println("VALUENYA : "+response.body().getStatus());
            }
            @Override
            public void onFailure(Call<ValueDetailEvent> call, Throwable t) {
                layoutDetailAds.setVisibility(View.GONE);
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                layoutRefresh.setRefreshing(false);


            }
        });
    }


    private void setFileImage(String[] image){
        AdapterBannerEvent adapterBannerEvent;
//        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycleBannerEvent);

//        int numberOfColumns = 2;
//        recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
//        adapterBannerEvent = new AdapterBannerEvent(getActivity(), image);
//        recyclerView.setAdapter(adapterBannerEvent);
    }

    private void setFileAttach(String[] files){
        AdapterAttachEvent adapterAttachEvent;
        RecyclerView recyclerView = (RecyclerView) getActivity().findViewById(R.id.recycleFileAttach);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        adapterAttachEvent = new AdapterAttachEvent(getActivity(), files);
        recyclerView.setAdapter(adapterAttachEvent);

        adapterAttachEvent.setItemClickListener(new AdapterAttachEvent.ItemAttachClickListener() {
            @Override
            public void onItemClick(String data) {
                if (checkPermission()) {
                    startDownload(data);
                }else {
                    requestPermission(data);

                }

//                ProgressDialog mProgressDialog = new ProgressDialog(getActivity());
//                mProgressDialog.setMessage("A message");
//                mProgressDialog.setIndeterminate(true);
//                mProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
//                mProgressDialog.setCancelable(true);
//                final DownloadTask downloadTask = new DownloadTask(getActivity());
//                NotificationCompat.Builder builder = new NotificationCompat.Builder(getActivity());
//                builder.setSmallIcon(android.R.drawable.ic_dialog_alert);
//                if (checkPermission()) {
//                    downloadTask.execute(data);
//                }else {
//                    requestPermission(data);
//
//                }
            }
        });
    }
    String datas;
    private void showAllert(String title, String message){
        android.app.AlertDialog.Builder alertDialog;
        alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                })
                .show();
    }

    private boolean checkPermission()
    {
        return (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED);
    }



    private static final int REQUEST_WRITE_PERMISSION = 786;
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_WRITE_PERMISSION && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            final DownloadTask downloadTask = new DownloadTask(getActivity());
            startDownload(datas);
        }
    }

    private void requestPermission(String data) {
            requestPermissions(new String[]{ Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_WRITE_PERMISSION);
        datas=data;
    }

    private boolean canReadWriteExternal() {
        return Build.VERSION.SDK_INT < Build.VERSION_CODES.M ||
                ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED;
    }
    private class DownloadTask extends AsyncTask<String, Integer, String> {

        private Context context;
        private PowerManager.WakeLock mWakeLock;
        private ProgressDialog progressDialog;
        String[] attachArr=new String[2];

        NotificationManager mNotifyManager;
        Notification.Builder mBuilder;
        String channelId = "1212";
        String channelName = "Channel Name";
        int importance = NotificationManager.IMPORTANCE_HIGH;
        PendingIntent pendingIntent;
        String title="Download File";
        String message="Download complete";
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "/");



        public DownloadTask(Context context) {
            this.context = context;
        }

        @Override
        protected String doInBackground(String... sUrl) {
            InputStream input = null;
            OutputStream output = null;
            HttpURLConnection connection = null;
            try {
                URL url = new URL(sUrl[0]);
//                System.out.println("URLNYAAA "+sUrl[0]);
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();

                // expect HTTP 200 OK, so we don't mistakenly save error report
                // instead of the file
                if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                    return "Server returned HTTP " + connection.getResponseCode()
                            + " " + connection.getResponseMessage();
                }

                // this will be useful to display download percentage
                // might be -1: server did not report the length
                int fileLength = connection.getContentLength();

                // download the file
                input = connection.getInputStream();

                if (!mediaStorageDir.exists()) {
                    if (!mediaStorageDir.mkdirs()) {
                        Log.e("DOWNLOADATTACH", "Oops! Failed create "
                                +  "LUPIRKA directory");
                        return null;
                    }
                }
                if (sUrl[0].startsWith("http://")) {
                    attachArr = sUrl[0].split("event/");
                    if (Environment.getExternalStorageState() != null) {

                        output = new FileOutputStream(mediaStorageDir.getPath()+"/"+attachArr[1]);
                    } else {
                        output = new FileOutputStream(mediaStorageDir.getPath()+"/"+attachArr[1]);
                    }


                }

                byte data[] = new byte[4096];
                long total = 0;
                int count;
                while ((count = input.read(data)) != -1) {
                    // allow canceling with back button
                    if (isCancelled()) {
                        input.close();
                        return null;
                    }
                    total += count;
                    // publishing the progress....
                    if (fileLength > 0) // only if total length is known
                        publishProgress((int) (total * 100 / fileLength));
                    output.write(data, 0, count);
                }
            } catch (Exception e) {
                return e.toString();
            } finally {
                try {
                    if (output != null)
                        output.close();
                    if (input != null)
                        input.close();
                } catch (IOException ignored) {
                }

                if (connection != null)
                    connection.disconnect();
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // take CPU lock to prevent CPU from going off if the user
            // presses the power button during download
            PowerManager pm = (PowerManager) context.getSystemService(Context.POWER_SERVICE);
            mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK,
                    getClass().getName());
            mWakeLock.acquire();
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();




        }

        @Override
        protected void onProgressUpdate(Integer... progress) {
            super.onProgressUpdate(progress);
            // if we get here, length is known, now set indeterminate to false
            this.progressDialog.setIndeterminate(false);
            this.progressDialog.setMax(100);
            this.progressDialog.setProgress(progress[0]);
        }

        @Override
        protected void onPostExecute(String result) {
            mWakeLock.release();
            this.progressDialog.dismiss();
            if (result != null) {
//                Toast.makeText(context, "Download error: " + result, Toast.LENGTH_SHORT).show();

                showAllert("Informasi","Gagal Melakukan Download file");
            }else {
                showAllert("Informasi","File berhasil di download, cek file di folder download");
//                Toast.makeText(context, "Cek file Folder LUPIRKA yang ada di folder Download", Toast.LENGTH_LONG).show();
            }
            File file = new File(mediaStorageDir.getPath()+"/"+attachArr[1]);
//

            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.fromFile(file));
            mNotifyManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.O) {
//                if (canReadWriteExternal()) {
//
//                }else {
//                    requestPermission();
//                }

                NotificationChannel mChannel = new NotificationChannel(
                        channelId, channelName, importance);
                mNotifyManager.createNotificationChannel(mChannel);
                mBuilder = new Notification.Builder(context, "0");
                String AUTHORITY = "com.kriyanesia.lupirka";
//                        File f=new File(context.getFilesDir(), "test.pdf");
                File fd = new File(Environment.getExternalStoragePublicDirectory(
                        Environment.DIRECTORY_DOWNLOADS), "LUPIRKA");
                File f = new File(fd, attachArr[1]);

                intent = new Intent(Intent.ACTION_VIEW);
                Uri uri = FileProvider.getUriForFile(context, AUTHORITY, f);
                intent.setData(uri);
                System.out.println("ADA APA " + FileProvider.getUriForFile(context, AUTHORITY, f));

                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP &
                        Intent.FLAG_ACTIVITY_SINGLE_TOP &
                        Intent.FLAG_GRANT_READ_URI_PERMISSION);
                pendingIntent = PendingIntent.getActivity(context, 1212, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);
                mBuilder = new Notification.Builder(context, channelId)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(title)
                        .setContentIntent(pendingIntent)
                        .setContentText(message);

                TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
                stackBuilder.addNextIntent(intent);
                pendingIntent = stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );
                mBuilder.setContentIntent(pendingIntent);
                mNotifyManager.notify(1212, mBuilder.build());

            }else if(android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M &&
                    android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.O) {
//                intent= new Intent(Intent.ACTION_VIEW);
//                intent.setDataAndType(Uri.fromFile(file),"file/*");
//                if (intent != null) {
//                    pendingIntent = PendingIntent.getActivity(context, 1212, intent,
//                            PendingIntent.FLAG_CANCEL_CURRENT);
//                    mBuilder = new Notification.Builder(context)
//                            .setContentTitle(title)
//                            .setContentText(message)
//                            .setSmallIcon(R.drawable.logo)
//                            .setContentIntent(pendingIntent);
//                    mNotifyManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//                    mNotifyManager.notify(1212, mBuilder.build());
//
//
//                }
//                Toast.makeText(context, "android 6,7 ", Toast.LENGTH_SHORT).show();
            }else{
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

                pendingIntent = PendingIntent.getActivity(context, 1212, intent,
                        PendingIntent.FLAG_UPDATE_CURRENT);

                Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                mBuilder = new Notification.Builder(context)
                        .setContentTitle(title)
                        .setContentText(message)
                        .setAutoCancel(true)
                        .setColor(ContextCompat.getColor(context, android.R.color.white))
                        .setSound(defaultSoundUri)
                        .setSmallIcon(R.drawable.logo)
                        .setContentIntent(pendingIntent);

                mNotifyManager = (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
                mNotifyManager.notify(1212, mBuilder.build());
            }


//            mBuilder.setContentText("Download complete");
//            mBuilder.setContentIntent(pendingIntent);
//            // Removes the progress bar
//            mNotifyManager.cancel(1212);
//            mNotifyManager.notify(0, mBuilder.build());


        }
    }

    private static void hideProgressNotification(final NotificationManager manager, final Context context, final int id)
    {

        manager.cancel(id);
    }
//    Notification.Builder mBuilder =
//            new Notification.Builder(context)
//                    .setSmallIcon(R.drawable.ic_launcher)
//                    .setContentTitle("My notification")
//                    .setContentText("Hello World!");
//
//    // Sets an ID for the notification
//    int mNotificationId = 001;
//    // Gets an instance of the NotificationManager service
//    NotificationManager mNotifyMgr =
//            (NotificationManager) context.getSystemService(NOTIFICATION_SERVICE);
//    // Builds the notification and issues it.
//            mNotifyMgr.notify(mNotificationId, mBuilder.build());
}
