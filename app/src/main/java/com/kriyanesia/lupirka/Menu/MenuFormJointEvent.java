package com.kriyanesia.lupirka.Menu;


import android.app.ActionBar;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.SearchDokterAdapter;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Dokter.Dokter;
import com.kriyanesia.lupirka.Data.Event.ValueJointEventDibayarin;
import com.kriyanesia.lupirka.Data.Event.ValueListBooth;
import com.kriyanesia.lupirka.Data.Farmasi.Farmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueFarmasi;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFormJointEvent extends Fragment {

    SessionUser sessionUser;
    String namaUser,alamatRs,namaRs,nohp,role;
    String eventName,eventId;
    String spesialisDokter;
    int idTenant,amount;
    ImageButton btnBack;



    public static MenuFormJointEvent newInstance(String eventId, String eventName) {
        Bundle bundle = new Bundle();
        bundle.putString("eventName", eventName);
        bundle.putString("eventId", eventId);

        MenuFormJointEvent fragment = new MenuFormJointEvent();
        fragment.setArguments(bundle);
        return fragment;
    }

    LinearLayout pendaftaranDokterMhs,layoutFarmasi;
    EditText etName,etRumahSakit,etAlamat;
    Button btnNextRegister;
    FrameLayout layoutRegisterEvent;
    TextView textTitleSambutan,hargaTable;
    String validasiSalah="";
    RadioGroup rgFarmasiDaftar;
    RadioButton rbDokter,rbFarmasi,rbTable;
    AutoCompleteTextView searchView;
    int idDokter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_form_joint_event, container, false);
        sessionUser=new SessionUser();
        role= sessionUser.getRole(getActivity(),"role");
        namaUser= sessionUser.getNamaSertifikat(getActivity(),"namaSertifikat");
        if (namaUser.equalsIgnoreCase("")){
            namaUser= sessionUser.getName(getActivity(),"name");
        }

        nohp= sessionUser.getNoHp(getActivity(),"nohp");
        alamatRs= sessionUser.getAlamatRs(getActivity(),"alamatRs");
        namaRs= sessionUser.getNamaRs(getActivity(),"namaRs");
        btnBack = (ImageButton) v.findViewById(R.id.btnBack);
        pendaftaranDokterMhs= (LinearLayout) v.findViewById(R.id.pendaftaranDokterMhs);
        layoutFarmasi= (LinearLayout) v.findViewById(R.id.layoutFarmasi);
        etAlamat = (EditText) v.findViewById(R.id.textAlamat);
        etRumahSakit = (EditText) v.findViewById(R.id.textRumahSakit);
        etName = (EditText) v.findViewById(R.id.textNamaLengkap);
        btnNextRegister = (Button) v.findViewById(R.id.btnNextRegister);
        layoutRegisterEvent = (FrameLayout) v.findViewById(R.id.layoutRegisterEvent);
        rgFarmasiDaftar = (RadioGroup) v.findViewById(R.id.rgFarmasiDaftar);
        rbDokter = (RadioButton) v.findViewById(R.id.rbDokter);
        rbFarmasi = (RadioButton) v.findViewById(R.id.rbFarmasi);
        rbTable = (RadioButton) v.findViewById(R.id.rbFarmasiTable);
        hargaTable = (TextView) v.findViewById(R.id.hargaTable);
        searchView = (AutoCompleteTextView) v.findViewById(R.id.form_search);
        searchView.clearFocus();
        searchView.setDropDownBackgroundDrawable(new ColorDrawable(this.getResources().getColor(R.color.bgWhite)));
        hargaTable.setVisibility(View.GONE);
        final SearchDokterAdapter adapter = new SearchDokterAdapter(getActivity(),android.R.layout.simple_spinner_dropdown_item);
        searchView.setAdapter(adapter);
        searchView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2,
                                    long arg3) {
                Dokter selected = (Dokter) arg0.getAdapter().getItem(arg2);
//                Toast.makeText(getActivity(),
//                        "Clicked " + arg2 + " name: " + selected.getSpesialis(),
//                        Toast.LENGTH_SHORT).show();
                searchView.setText(selected.getNama());
                idDokter=selected.getId_dokter();
            }
        });

        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });

        textTitleSambutan = (TextView) v.findViewById(R.id.textTitleSambutan);

        etAlamat.setText(alamatRs);
        etName.setText(namaUser);
        etRumahSakit.setText(namaRs);

        eventName =getArguments().getString("eventName");
        eventId =getArguments().getString("eventId");
        textTitleSambutan.setText("Pendaftaran "+eventName);

        if (role.equalsIgnoreCase("2")|| role.equalsIgnoreCase("3")){
            nonAktifView();
            pendaftaranDokterMhs.setVisibility(View.VISIBLE);

        }else if(role.equalsIgnoreCase("5")){
            nonAktifView();
            pendaftaranDokterMhs.setVisibility(View.VISIBLE);
            etRumahSakit.setHint("Universitas");

        }else if (role.equalsIgnoreCase("7")){
            nonAktifView();
            layoutFarmasi.setVisibility(View.VISIBLE);
        }else {
            nonAktifView();
        }

        rgFarmasiDaftar.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (rbFarmasi.isChecked()){
                    searchView.setVisibility(View.GONE);
                    hargaTable.setVisibility(View.GONE);
                }else if (rbDokter.isChecked()){
                    searchView.setVisibility(View.VISIBLE);
                    hargaTable.setVisibility(View.GONE);
                }else {
                    searchView.setVisibility(View.GONE);
                    hargaTable.setVisibility(View.VISIBLE);

                }
            }
        });

        btnNextRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (checkForm()){

                }else {
                    System.out.println("VALIDASI "+validasiSalah);
                    AlertDialog.Builder alertDialog;
                    alertDialog = new AlertDialog.Builder(getActivity());
                    alertDialog.setTitle("Informasi")
                            .setMessage("Lengkapi Field "+validasiSalah)
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            })
                            .show();
                }
            }
        });
        loadHargaTable();
        return v;
    }
    private boolean checkForm(){
        boolean isValid=true;
        validasiSalah="";
        if (role.equalsIgnoreCase("7")){

            Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
            if (rbDokter.isChecked()){
                if (searchView.getText().toString().isEmpty()){
                    validasiSalah="dokter yang ingin didaftarkan";
                    isValid=false;
                }else {

                    intent.putExtra("menu","Event Registration Fee");
                    intent.putExtra("farmasiDaftar","Dokter");
                    intent.putExtra("eventId",eventId);
                    intent.putExtra("eventName",eventName);
                    intent.putExtra("namaDokter",searchView.getText().toString());
                    intent.putExtra("idDokter",idDokter);
                    intent.putExtra("userType",spesialisDokter);
                    startActivity(intent);
                }
            }else if (rbFarmasi.isChecked()){
                if (hargaTable.getText().toString().equalsIgnoreCase("Tidak ada Tabel yang dibuka dalam event ini")){
                    showAllert("Respon Server","tidak ada booth yang tersedia","");
                }else {
                    intent.putExtra("menu", "Event Registration Booth");
                    intent.putExtra("farmasiDaftar", "Farmasi");
                    intent.putExtra("eventId", eventId);
                    intent.putExtra("eventName", eventName);
                    intent.putExtra("userType", "XX");
                    startActivity(intent);
                }
            }else if (rbTable.isChecked()){
                //pembayaran jika dipilih table
                aksiDaftarTable();

            }else {
                validasiSalah="user yang ingin anda daftarkan";
                isValid=false;
            }


        }else{
            String nama = etName.getText().toString();
            String alamat = etAlamat.getText().toString();
            String rumahSakit = etRumahSakit.getText().toString();
            if (nama.isEmpty()) {
                isValid = false;
                validasiSalah = validasiSalah + "Nama";
            }
            if (alamat.isEmpty()) {
                isValid = false;
                if (nama.isEmpty()) {
                    validasiSalah = validasiSalah + ", Email";
                } else {
                    validasiSalah = validasiSalah + "Email";
                }
            }
            if (rumahSakit.isEmpty()) {
                isValid = false;
                if (role.equalsIgnoreCase("5")) {
                    if (!alamat.isEmpty() && !nama.isEmpty()) {
                        validasiSalah = validasiSalah + "Universitas";
                    } else {
                        validasiSalah = validasiSalah + ", Universitas";
                    }
                }else {
                    if (!alamat.isEmpty() && !nama.isEmpty()) {
                        validasiSalah = validasiSalah + "Rumah Sakit";
                    } else {
                        validasiSalah = validasiSalah + ", Rumah Sakit";
                    }
                }
            }
            if (isValid){
                Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                intent.putExtra("menu","Event Registration Fee");
                intent.putExtra("eventId",eventId);
                intent.putExtra("eventName",eventName);
                intent.putExtra("eventRs",etRumahSakit.getText().toString());
                intent.putExtra("eventAlamat",etAlamat.getText().toString());
                intent.putExtra("namaDokter",etName.getText().toString());
                intent.putExtra("userType","XX");
                startActivity(intent);
            }
        }
        System.out.println("VALIDASI "+validasiSalah);

        return isValid;
    }

    private void nonAktifView(){
        pendaftaranDokterMhs.setVisibility(View.GONE);
        layoutFarmasi.setVisibility(View.GONE);
    }

    private void loadHargaTable(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("tipe","table");
            paramObject.put("id_event",eventId);
            paramObject.put("request","all");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueListBooth> dataCall = apiInterface.getListTenant(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueListBooth>() {
            @Override
            public void onResponse(Call<ValueListBooth> call, Response<ValueListBooth> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueListBooth.Booth> dataBooth;
                    dataBooth = response.body().getData().getList();

                    if (!dataBooth.isEmpty()){
                        Locale localeID = new Locale("in", "ID");
                        NumberFormat formatRupiah = NumberFormat.getCurrencyInstance(localeID);
                        try {
                            int harga = Integer.valueOf(dataBooth.get(0).getHarga());
                            idTenant=Integer.valueOf(dataBooth.get(0).getId());
                            amount=Integer.valueOf(dataBooth.get(0).getHarga());
                            String h=formatRupiah.format(harga);
                            String finalHarga= "Rp. "+h.substring(2);
                            String sourceString=finalHarga +"<b> sisa kuota ("+dataBooth.get(0).getKuota()+")</b>";
                            hargaTable.setText(Html.fromHtml(sourceString));
                        }catch (NumberFormatException e){
                            Toast.makeText(getActivity(), "tidak bisa menampilkan harga", Toast.LENGTH_SHORT).show();
                        }


                    }else {
                        hargaTable.setText("Tidak ada Tabel yang dibuka dalam event ini");
                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueListBooth> call, Throwable t) {
                loadHargaTable();

            }
        });
    }


    private void aksiDaftarTable(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {

            paramObject.put("id_login",nohp);
            paramObject.put("id_tenant",idTenant);
            paramObject.put("id_event",Integer.valueOf(eventId));
            paramObject.put("amount",amount);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueListBooth> dataCall = apiInterface.daftarTableTenant(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueListBooth>() {
            @Override
            public void onResponse(Call<ValueListBooth> call, Response<ValueListBooth> response) {
                showAllert("Respon Server",response.body().getMessage(),"");
            }

            @Override
            public void onFailure(Call<ValueListBooth> call, Throwable t) {
            }
        });
    }
    private void showAllert(String title, final String message, final String action){
        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

//                        if (message.equalsIgnoreCase("anda sudah pernah mendaftar event ini") ||
//                                message.equalsIgnoreCase("Saldo tidak cukup mohon topup terlebih dahulu")){
//
//                        }else {
////                            startActivity(intent);
//                        }
                    }
                })
                .show();
    }
}
