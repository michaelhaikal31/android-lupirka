package com.kriyanesia.lupirka.Menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.messaging.FirebaseMessaging;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;
import com.kriyanesia.lupirka.Data.User;
import com.kriyanesia.lupirka.Data.ValueUser;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.chaos.view.PinView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class OtpValidation extends AppCompatActivity implements Callback<OtpVerifikasi>{
    TextView tvIdHp,tvResend;
    EditText etUbahId;
    String noHp,from;
    ImageView ivChange;
    PinView etKodeVer;
    TextView tIdLogin;
    Button btnVerifikasi;
    AlertDialog.Builder alertDialog;
    private User valueUsers = new User();
    ProgressDialog progressDialog;
    SessionUser sessionUser = new SessionUser();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_step_two);
        tvIdHp = (TextView) findViewById(R.id.idHpLogin);
        tvResend = (TextView) findViewById(R.id.textResendKey);
        etKodeVer = (PinView) findViewById(R.id.etVerCode);
        etUbahId = (EditText) findViewById(R.id.etChangeId);
        ivChange = (ImageView) findViewById(R.id.btnEditNumber) ;
        btnVerifikasi = (Button) findViewById(R.id.btnVerifikasiOtp) ;

        Intent i = getIntent();
        Bundle extras = i.getExtras();
        noHp = extras.getString("nohp");
        from = extras.getString("from");
        etUbahId.setText(noHp);
        btnVerifikasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etKodeVer.getText().toString().isEmpty()){

                }else {
                    progressDialog = new ProgressDialog(OtpValidation.this);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Please wait...");
                    progressDialog.show();
                    if (from.equalsIgnoreCase("login")){
                        Login();
                    }else if (from.equalsIgnoreCase("register")){
                        Registrasi();
                    }
                }
            }
        });
        etUbahId.setVisibility(View.GONE);
        ivChange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (etUbahId.getVisibility() == View.GONE){
                    etUbahId.setVisibility(View.VISIBLE);
                    tvIdHp.setVisibility(View.GONE);
                }else {
                    if (etUbahId.equals("")){
                        Toast.makeText(OtpValidation.this, "gak boleh kosong", Toast.LENGTH_SHORT).show();

                    }else {
                        noHp = etUbahId.getText().toString();
                        etUbahId.setVisibility(View.GONE);
                        tvIdHp.setVisibility(View.VISIBLE);
                        tvIdHp.setText(noHp);
                    }
                }
//                Toast.makeText(OtpValidation.this, "ID LOGIN "+noHp, Toast.LENGTH_SHORT).show();
            }
        });




        Coundown();
        tvIdHp.setText(noHp);


    }
    private void Coundown(){
        CountDownTimer timer = new CountDownTimer(60*1000, 1000) {

            @Override
            public void onTick(final long millisUntilFinished) {
                tvResend.post(new Runnable() {
                    @Override
                    public void run() {
                        tvResend.setText("Resend code in 00:"+millisUntilFinished/1000); // here is the txtV which isn't shown
                    }
                });
            }

            @Override
            public void onFinish() {
                tvResend.setText("Resend");
                tvResend.setTextColor(Color.RED);
                tvResend.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (from.equalsIgnoreCase("login")) {
                            ResendLogin();
                        }else if (from.equalsIgnoreCase("register")){
                            ResendRegistrasi();
                        }
                    }
                });

            }
        };
        timer.start();
    }
    private void ResendLogin(){
        SessionDevice sd = new SessionDevice();
        String fcm = sd.getTokenDevice(this,"token");
        String imei =sd.getImei(this,"imei");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        // prepare call in Retrofit 2.0
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("from", "login");
            paramObject.put("phone_number", noHp);

            Call<OtpVerifikasi> verCall = apiInterface.resendToken(paramObject.toString(),"xsada");
            verCall.enqueue(OtpValidation.this);
            Log.d("KIRIMAN ",paramObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Login(){


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        // prepare call in Retrofit 2.0
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("token", etKodeVer.getText().toString());
            paramObject.put("id_login", noHp);
            paramObject.put("from", "login");
            System.out.println("asd "+paramObject);
            Call<ValueUser> userCall = apiInterface.getUser(paramObject.toString(),"xsada");
            userCall.enqueue(new Callback<ValueUser>() {
                @Override
                public void onResponse(Call<ValueUser> call, Response<ValueUser> response) {
                    Toast.makeText(OtpValidation.this, response.body().getStatus(), Toast.LENGTH_SHORT).show();
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        valueUsers = response.body().getData();
                        sessionUser.setNoHp(OtpValidation.this,"nohp",valueUsers.getPhone_number());
                        sessionUser.setBirthday(OtpValidation.this,"birthday",valueUsers.getBirthday());
                        sessionUser.setName(OtpValidation.this,"name",valueUsers.getName());
                        sessionUser.setEmail(OtpValidation.this,"email",valueUsers.getEmail());
                        sessionUser.setProfile(OtpValidation.this,"image",valueUsers.getImage());
                        sessionUser.setRole(OtpValidation.this,"role",valueUsers.getRole());
                        sessionUser.setGender(OtpValidation.this,"gender",valueUsers.getGender());
                        sessionUser.setAlamat(OtpValidation.this,"alamat",valueUsers.getAddress());
                        sessionUser.setKodePos(OtpValidation.this,"kodepos",valueUsers.getKodepos());
                        sessionUser.setAlamatRs(OtpValidation.this,"alamatRs",valueUsers.getAlamat_rumah_sakit());
                        sessionUser.setNamaRs(OtpValidation.this,"namaRs",valueUsers.getRumah_sakit());
                        sessionUser.setNamaSertifikat(OtpValidation.this,"namaSertifikat",valueUsers.getNama_sertifikat());
                        System.out.println("asd nih "+valueUsers.getSpesialis());
                        FirebaseMessaging.getInstance().subscribeToTopic("lupirka-dak-"+valueUsers.getRole()+"-"+valueUsers.getSpesialis().replaceAll(" ","-"));
                        Intent intent = new Intent(OtpValidation.this, MenuMain.class);
                        startActivity(intent);
                        OtpValidation.this.finish();
                    }else {
                        showAllert("Login Gagal",response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ValueUser> call, Throwable t) {
                    showAllert("Login Gagal","Terjadi kesalahan koneksi ke server");
                }
            });
            Log.d("KIRIMAN ",paramObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        progressDialog.dismiss();
    }


    private void ResendRegistrasi(){
        SessionDevice sd = new SessionDevice();
        String fcm = sd.getTokenDevice(this,"token");
        String imei =sd.getImei(this,"imei");
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        // prepare call in Retrofit 2.0
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("phone_number", noHp);
            paramObject.put("from", "registrasi");

            Call<OtpVerifikasi> verCall = apiInterface.resendToken(paramObject.toString(),"xsada");
            verCall.enqueue(OtpValidation.this);
            Log.d("KIRIMAN ",paramObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void Registrasi(){
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        // prepare call in Retrofit 2.0
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("token", etKodeVer.getText().toString());
            paramObject.put("id_login", noHp);
            paramObject.put("from", "registrasi");
            Call<ValueUser> userCall = apiInterface.getUser(paramObject.toString(),"xsada");
            userCall.enqueue(new Callback<ValueUser>() {
                @Override
                public void onResponse(Call<ValueUser> call, Response<ValueUser> response) {
                    if (response.body().getStatus().equalsIgnoreCase("success")) {
                        valueUsers = response.body().getData();

                        sessionUser.setNoHp(OtpValidation.this,"nohp",valueUsers.getPhone_number());
                        sessionUser.setBirthday(OtpValidation.this,"birthday",valueUsers.getBirthday());
                        sessionUser.setName(OtpValidation.this,"name",valueUsers.getName());
                        sessionUser.setEmail(OtpValidation.this,"email",valueUsers.getEmail());
                        sessionUser.setProfile(OtpValidation.this,"image",valueUsers.getImage());
                        sessionUser.setRole(OtpValidation.this,"role",valueUsers.getRole());
                        sessionUser.setGender(OtpValidation.this,"gender",valueUsers.getGender());
                        sessionUser.setAlamat(OtpValidation.this,"alamat",valueUsers.getAddress());
                        sessionUser.setKodePos(OtpValidation.this,"kodepos",valueUsers.getKodepos());
                        Intent intent = new Intent(OtpValidation.this, MenuMain.class);
                        startActivity(intent);
                        OtpValidation.this.finish();

                    }else {
                        showAllert("Registrasi Gagal",response.body().getMessage());
                    }
                }

                @Override
                public void onFailure(Call<ValueUser> call, Throwable t) {
                    progressDialog.dismiss();
                    showAllert("Registrasi Gagal","Terjadi kesalahan koneksi ke server");


                }
            });
            Log.d("KIRIMAN ",paramObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void onResponse(Call<OtpVerifikasi> call, Response<OtpVerifikasi> response) {
        if (response.body().getStatus().equalsIgnoreCase("success")) {
            Toast.makeText(this, "Cek PIN pada SMS anda", Toast.LENGTH_SHORT).show();
        }
        Coundown();

        tvResend.setTextColor(Color.BLACK);
    }

    @Override
    public void onFailure(Call<OtpVerifikasi> call, Throwable t) {
        Coundown();
        tvResend.setTextColor(Color.BLACK);
    }


    private void showProgressDialog(String title)
    {
        progressDialog = new ProgressDialog(this);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setTitle(title);
        progressDialog.setMessage("Please wait...");
        progressDialog.show();
    }
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setCancelable(false);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
}

