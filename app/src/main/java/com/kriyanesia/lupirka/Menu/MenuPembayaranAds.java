package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterRecyclePembayaran;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Pembayaran.ValueGenerateVA;
import com.kriyanesia.lupirka.Data.Pembayaran.ValuePembayaran;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.omega_r.libs.OmegaCenterIconButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPembayaranAds extends Fragment implements AdapterRecyclePembayaran.ItemBankClickListener{

    String adsId,adsTitle;
    OmegaCenterIconButton btnNext;

    ProgressDialog progressDialog;
    private DatabaseHelper db;

    String accountBank,title,totalBayar;
    public MenuPembayaranAds newInstance(String adsId,String adsTitle) {
        Bundle bundle = new Bundle();
        bundle.putString("adsId", adsId);
        bundle.putString("adsTitle", adsTitle);

        MenuPembayaranAds fragment = new MenuPembayaranAds();
        fragment.setArguments(bundle);
        return fragment;
    }
    TextView valueTitleAds,valueAdsTotal,valueAdsDiscountTotal,valueAdsFeeTotal,valueAdsHargaTotal,textAdsHarga;
    RecyclerView recyclePembayaran,recycleMetodeBayar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_pembayaran_ads, container, false);

        adsId =getArguments().getString("adsId");
        adsTitle =getArguments().getString("adsTitle");

        valueTitleAds = (TextView) v.findViewById(R.id.valueTitleAds);
        textAdsHarga = (TextView) v.findViewById(R.id.textAdsHarga);
        valueAdsHargaTotal = (TextView) v.findViewById(R.id.valueAdsHargaTotal);
        valueAdsFeeTotal = (TextView) v.findViewById(R.id.valueAdsFeeTotal);
        valueAdsDiscountTotal = (TextView) v.findViewById(R.id.valueAdsDiscountTotal);
        valueAdsTotal = (TextView) v.findViewById(R.id.valueAdsTotal);
//        recyclePembayaran = (RecyclerView) v.findViewById(R.id.recyclePembayaran);
        recycleMetodeBayar = (RecyclerView) v.findViewById(R.id.recycleMetodeBayar);
        btnNext = (OmegaCenterIconButton) v.findViewById(R.id.btnNext);
        valueTitleAds.setText(adsTitle);
        loadDetailAdsPayment(adsId);


        db = new DatabaseHelper(getActivity());

        //onclick
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String vaNumber="7007000547153522";
                String batas="07-06-2018 11:17:38";
//                boolean save= saveVA(adsId,adsTitle,vaNumber
//                        ,batas,totalBayar);
//                if (save){
//                    Toast.makeText(getActivity(), "berhasil", Toast.LENGTH_SHORT).show();
//                }
                boolean[] validate=generateVA();
                if (validate[1] && accountBank.isEmpty()) {
                    Intent intent = new Intent(getActivity(), ActivityDetailContent.class);
                    intent.putExtra("menu", "Pembayaran Ads Lanjut");
                    intent.putExtra("adsId", adsId);
                    startActivity(intent);
                }
            }
        });
//        recyclePembayaran.setNestedScrollingEnabled(true);
        return v;
    }
    private boolean[] generateVA(){
        progressDialog = new ProgressDialog(getActivity());
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please Wait...");
        progressDialog.show();
        final boolean[] status = {false,false};
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        try {
            paramObject.put("tipe_bayar","Virtual Account");
            paramObject.put("account",accountBank);
            paramObject.put("title",adsTitle);
            paramObject.put("amount",totalBayar);
            paramObject.put("id_login",nohp);
            paramObject.put("tipe","Event");
            paramObject.put("id_product",adsId);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueGenerateVA> profesiCall = apiInterface.createVA(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueGenerateVA>() {
            @Override
            public void onResponse(Call<ValueGenerateVA> call, Response<ValueGenerateVA> response) {
                System.out.println("VALUE "+response.body().toString());
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    status[0] = true;
                    boolean save= saveVA(adsId,adsTitle,response.body().getData().getVa_number()
                    ,response.body().getData().getExp_time(),totalBayar);
                    if (save) {
                        status[1] = true;
                    }

                }
                System.out.println("VALUENYA : "+response.body().getStatus());


            }

            @Override
            public void onFailure(Call<ValueGenerateVA> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                showAllert("Respon Server","Tidak dapat terhubung ke server");

            }
        });
        progressDialog.dismiss();
        return status;
    }
    private boolean saveVA(String adsId,String adsTitle, String norek, String batasWaktu, String nominal){
        boolean saveVA=false;
        db.insertPembayaran(adsId,adsTitle,SessionPembayaranIklan.TABLE_NAME,norek,batasWaktu,nominal);
        SessionPembayaranIklan s=db.getPembayaranIkaln(adsId);
        if (s!=null){
            saveVA=true;
            System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
        }

        return saveVA;

    }

    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }
    private void loadDetailAdsPayment(final String adsId){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("ads_id",Integer.valueOf(adsId));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValuePembayaran> dataCall = apiInterface.getDetailPayment(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValuePembayaran>() {
            @Override
            public void onResponse(Call<ValuePembayaran> call, Response<ValuePembayaran> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    ValuePembayaran.Price price = new ValuePembayaran.Price();
                    price = response.body().getData().getPrice();
                    List<ValuePembayaran.Payment> payment = new ArrayList<>();
                    payment = response.body().getData().getPayment();

                    int pembayaran = Integer.valueOf(price.getHarga())*Integer.valueOf(price.getTotal_hari());
                    textAdsHarga.setText("Ads("+price.getTotal_hari()+" days x Rp."+price.getHarga()+")");
                    valueAdsTotal.setText("Rp."+price.getTotal());
                    valueAdsDiscountTotal.setText("Rp."+price.getDiskon());
                    valueAdsFeeTotal.setText("Rp."+price.getAdmin());
                    valueAdsHargaTotal.setText("Rp."+pembayaran);
                    totalBayar = price.getTotal();

                    int numberOfColumns = 1;
                    recycleMetodeBayar.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
                    AdapterRecyclePembayaran adapterRecyclePembayaran = new AdapterRecyclePembayaran(getActivity(),payment);
                    adapterRecyclePembayaran.setItembankClick(MenuPembayaranAds.this);
                    recycleMetodeBayar.setAdapter(adapterRecyclePembayaran);


//                    String[]arrBank = new String[3];
//                    String bank = payment.get(0).getData();
//                    if (!bank.isEmpty()){
//                        arrBank = bank.split(", ");
//                    }
//

//                    int spacingInPixels = 3;
//                    int numberOfColumns = 3;
//                    recyclePembayaran.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
//                    AdapterChildpembayaranBank adapterChildpembayaranBank = new AdapterChildpembayaranBank(getActivity(), arrBank);
////                    adapterChildpembayaranBank.setClickListener(this);
//                    recyclePembayaran.setAdapter(adapterChildpembayaranBank);
//                    recyclePembayaran.addItemDecoration(new SpacesItemDecoration(spacingInPixels));
                   // Toast.makeText(getActivity(), "here i am to help you: "+arrBank[0]+" "+arrBank[1]+" "+arrBank[2], Toast.LENGTH_SHORT).show();


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

//                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ValuePembayaran> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

//                progressDialog.dismiss();
//                showAllert("Respon Server","Periksa Koneksi anda, anda tidak dapat terhubung ke server","none");

            }
        });
    }


    @Override
    public void onItemBankClick(String accountBank) {
        Toast.makeText(getActivity(), accountBank, Toast.LENGTH_SHORT).show();
        this.accountBank=accountBank;
    }
}
