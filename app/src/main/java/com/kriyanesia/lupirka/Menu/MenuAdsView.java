package com.kriyanesia.lupirka.Menu;


import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListMyAds;
import com.kriyanesia.lupirka.AdapterModel.AdapterListOtherAds;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Ads.ValueAllAds;
import com.kriyanesia.lupirka.Data.SQLiteData.SessionPembayaranIklan;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuAdsView extends Fragment implements AdapterListMyAds.ItemClickListener,AdapterListOtherAds.ItemClickListener{
    AdapterListMyAds adapterMyAds;
    AdapterListOtherAds adapterOtherAds;
    RecyclerView recycleMyAds;
    RecyclerView recycleOtherAds;
    SessionUser sessionUser;
    Button btnAddAds;
    SwipeRefreshLayout refreshAds;
    FrameLayout layoutAds;
    ImageView btnReload;
    ScrollView scrollView;
    private DatabaseHelper db;
    public MenuAdsView() {
        // Required empty public constructor
    }

    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_menu_ads_view, container, false);

        refreshAds = (SwipeRefreshLayout) v.findViewById(R.id.refreshAds);
        btnAddAds = (Button) v.findViewById(R.id.btnAddAds);
        layoutAds = (FrameLayout) v.findViewById(R.id.layoutAds);
        btnReload = (ImageView) v.findViewById(R.id.reloadData);
        scrollView = (ScrollView) v.findViewById(R.id.scrollView);
        btnReload.setVisibility(View.GONE);
        try {
            if (getContext()!=null){
                loadAds();
            }
        }catch (Error e){

        }

        btnAddAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(),ActivityDetailContent.class);
                intent.putExtra("menu","Pendaftaran Iklan");
                startActivity(intent);
            }
        });
        refreshAds.setRefreshing(true);
        refreshAds.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (recycleMyAds!=null) {
                    recycleMyAds.removeAllViews();
                }
                if (recycleOtherAds!=null){
                recycleOtherAds.removeAllViews();
                }
                loadAds();


            }
        });
        btnReload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (recycleMyAds!=null) {
                    recycleMyAds.removeAllViews();
                }
                if (recycleOtherAds!=null){
                    recycleOtherAds.removeAllViews();
                }
                loadAds();
            }
        });



//        SessionPembayaranIklan s=db.getPembayaranIkaln("43");
//        if (s!=null){
//            Toast.makeText(getActivity(), "data 43", Toast.LENGTH_SHORT).show();
//            System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
//        }

//        System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
//        Toast.makeText(getActivity(), "DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek(), Toast.LENGTH_SHORT).show();


//        ArrayList<String[]> struktur= db.getDbTableDetails();
//        for (int i=0;i<struktur.size();i++){
//            System.out.println("DATA ke- "+struktur.get(i));
//            String[] data=struktur.get(i);
//            for (int j=0; j<struktur.get(i).length;i++){
//                System.out.println("Database ke- "+i+" "+data[j]);
//
//            }
//        }


        return v;
    }

    @Override
    public void onResume() {
        if (recycleMyAds!=null) {
            recycleMyAds.removeAllViews();
        }
        if (recycleOtherAds!=null){
            recycleOtherAds.removeAllViews();
        }
        loadAds();
        super.onResume();

    }

    @Override
    public void onStart() {
        if (recycleMyAds!=null) {
            recycleMyAds.removeAllViews();
        }
        if (recycleOtherAds!=null){
            recycleOtherAds.removeAllViews();
        }
        loadAds();
        super.onStart();
    }

    //    private boolean saveVA(String adsId,String adsTitle, String norek, String batasWaktu, String nominal){
//        boolean saveVA=false;
//        db.insertPembayaranIklan(adsId,adsTitle,norek,batasWaktu,nominal);
//        SessionPembayaranIklan s=db.getPembayaranIkaln(adsId);
//        if (s!=null){
//            saveVA=true;
//            System.out.println("DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek());
//            Toast.makeText(getActivity(), "DATA "+s.getId()+" - "+s.getBataswaktu()+" - "+s.getNominal()+" - "+s.getTitle()+" - "+s.getNorek(), Toast.LENGTH_SHORT).show();
//        }else {
//            Toast.makeText(getActivity(), "gagal", Toast.LENGTH_SHORT).show();
//        }
//
//        return saveVA;
//
//    }
    private void loadAds(){
        refreshAds.setRefreshing(true);
        scrollView.setVisibility(View.VISIBLE);
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        sessionUser= new SessionUser();
        String noHp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueAllAds> data = apiInterface.getAllAds(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueAllAds>() {
            @Override
            public void onResponse(Call<ValueAllAds> call, Response<ValueAllAds> response) {

                btnReload.setVisibility(View.GONE);
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    try {
                        if (getActivity()!=null){
                            int numberOfColumns = 1;
                            sessionUser= new SessionUser();
                            String role = sessionUser.getRole(getActivity(),"role");
                            if (!role.equalsIgnoreCase("8")) {
                                List<Ads> myAds;
                                myAds = response.body().getData().getMyAds().getContent();

                                TextView txtMyAds = (TextView) v.findViewById(R.id.textMyAds);
                                TextView txtOtherAds = (TextView) v.findViewById(R.id.textOtherAds);
                                ImageView garisPertama = (ImageView) v.findViewById(R.id.garisSatu);
                                ImageView garisKedua = (ImageView) v.findViewById(R.id.garisDua);
                                TextView txtMyAdsNoData = (TextView) v.findViewById(R.id.textNoDataMyAds);




                                        recycleMyAds = v.findViewById(R.id.recycleMyAds);
                                        btnAddAds.setVisibility(View.VISIBLE);
                                        if (recycleMyAds!=null) {
                                            recycleMyAds.setVisibility(View.VISIBLE);
                                            txtMyAds.setVisibility(View.VISIBLE);
                                            txtOtherAds.setVisibility(View.VISIBLE);
                                            garisKedua.setVisibility(View.VISIBLE);
                                            garisPertama.setVisibility(View.VISIBLE);
                                            recycleMyAds.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
                                            adapterMyAds = new AdapterListMyAds(getActivity(), myAds);
                                            adapterMyAds.setmClickListener(MenuAdsView.this);
                                            recycleMyAds.setAdapter(adapterMyAds);
                                        }



                                        if (myAds.isEmpty()){

                                            txtMyAdsNoData.setVisibility(View.VISIBLE);
                                            txtMyAdsNoData.setText("Data Tidak Tersedia");
                                        }
                                        ArrayList<String> expired = new ArrayList<>();
                                        int j=0;
                                        for (int i=0; i<myAds.size();i++) {
                                            if (myAds.get(i).getStatus().equalsIgnoreCase("expired")) {
                                                expired.add(j,myAds.get(i).getIdAds());
                                                j=j+1;
                                            }
                                        }
                                        if (expired!=null && expired.size()!=0) {
                                            String[] expiredAds= new String[expired.size()];
                                            for (int h = 0; h < expired.size();h++){
                                                expiredAds[h]=expired.get(h);
                                            }

                                            db = new DatabaseHelper(getActivity());
                                            db.deleteSessionPembayaran(expiredAds,SessionPembayaranIklan.TABLE_NAME);

                                        }else {
                                        }
                                    }
                                List<Ads> otherAds ;
                                otherAds = response.body().getData().getOtherAds().getContent();
                                recycleOtherAds = v.findViewById(R.id.recycleOtherAds);
                                recycleOtherAds.setLayoutManager(new GridLayoutManager(getActivity(), 1));
                                adapterOtherAds = new AdapterListOtherAds(getActivity(), otherAds);
                                adapterOtherAds.setmClickListener(MenuAdsView.this);
                                recycleOtherAds.setAdapter(adapterOtherAds);

                                TextView txtOtherAdsNoData = (TextView) v.findViewById(R.id.textNoDataOtherAds);
                                if (otherAds.isEmpty()){

                                    txtOtherAdsNoData.setVisibility(View.VISIBLE);
                                    txtOtherAdsNoData.setText("Data Tidak Tersedia");
                                }



                        }
                    }catch (Error e){

                    }





                    refreshAds.setRefreshing(false);
//                    otherAds.clear();
                }
            }

            @Override
            public void onFailure(Call<ValueAllAds> call, Throwable t) {
                refreshAds.setRefreshing(false);
                btnReload.setVisibility(View.VISIBLE);
                scrollView.setVisibility(View.GONE);

            }
        });
    }


    @Override
    public void OnMyAdsClickListener(String idAds, String idTitle,
                                      String adsSubTitle, String categoryName,
                                      String note, String startDate, String endDAte,
                                      String urlImage) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.layoutAds, new MenuDetailMyAds().newInstance(idAds,"menuAds"));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack("DetailAds");
        trans.commit();

    }

    @Override
    public void OnOtherClickListener(String idAds, String idTitle,
                                      String adsSubTitle, String categoryName,
                                      String note, String startDate, String endDAte,
                                      String urlImage) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.layoutAds, new MenuDetailOtherAds().newInstance(idAds));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack("DetailAds");
        trans.commit();


    }
}
