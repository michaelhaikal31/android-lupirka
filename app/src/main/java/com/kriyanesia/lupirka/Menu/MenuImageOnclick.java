package com.kriyanesia.lupirka.Menu;


import android.media.Image;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.kriyanesia.lupirka.R;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuImageOnclick extends Fragment {
    String urlBanner;
    PhotoView imageBanner;

    public MenuImageOnclick() {
        // Required empty public constructor
    }

    public static MenuImageOnclick newInstance(String urlBanner) {

        Bundle bundle = new Bundle();
        bundle.putString("urlBanner", urlBanner);

        MenuImageOnclick fragment = new MenuImageOnclick();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_image_onclick, container, false);
        imageBanner=(PhotoView) v.findViewById(R.id.imageBanner);
        urlBanner =getArguments().getString("urlBanner");
        if (urlBanner.contains(".png")|| urlBanner.contains(".jpg")) {
            Picasso.with(getActivity()).load(urlBanner).into(imageBanner);
        }
        v.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    Toast.makeText(getActivity(), "down", Toast.LENGTH_SHORT).show();
                }
                if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL)
                {
                    Toast.makeText(getActivity(), "up", Toast.LENGTH_SHORT).show();
                }
                return true;
            }
        });

        return v;

    }

}
