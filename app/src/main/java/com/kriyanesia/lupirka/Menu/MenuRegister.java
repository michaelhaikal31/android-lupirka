package com.kriyanesia.lupirka.Menu;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AppCompatActivity;
import android.telephony.PhoneNumberUtils;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerHospital;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerSpesialis;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerProfesi;
import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerUniversitas;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.EO.EO;
import com.kriyanesia.lupirka.Data.EO.ValueEO;
import com.kriyanesia.lupirka.Data.Farmasi.Farmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueFarmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueJabatan;
import com.kriyanesia.lupirka.Data.Hospital.Hospital;
import com.kriyanesia.lupirka.Data.Hospital.ValueHospital;
import com.kriyanesia.lupirka.Data.Idi.IdiCabang;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiCabang;
import com.kriyanesia.lupirka.Data.Idi.ValueIdiWilayah;
import com.kriyanesia.lupirka.Data.OtpVerifikasi;
import com.kriyanesia.lupirka.Data.Profesi.Profesi;
import com.kriyanesia.lupirka.Data.Profesi.ValueProfesi;
import com.kriyanesia.lupirka.Data.Spesialis.Spesialis;
import com.kriyanesia.lupirka.Data.Spesialis.ValueSpesialis;
import com.kriyanesia.lupirka.Data.Universitas.Universitas;
import com.kriyanesia.lupirka.Data.Universitas.ValueUniversitas;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Util.Constant;
import com.basavaraj.spinnerdatepick.IDatePicker;
import com.rilixtech.Country;
import com.rilixtech.CountryCodePicker;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

public class MenuRegister extends AppCompatActivity implements View.OnClickListener,IDatePicker {

    ProgressDialog progressDialog;
    AlertDialog.Builder alertDialog;
    @BindView(R.id.btnRegister)
    Button btnRegister;
    @BindView(R.id.linkPrivacyPolice)
    TextView linkPrivacyPolice;
    @BindView(R.id.linkToS)
    TextView linkToS;
    @BindView(R.id.linkLogin)
    TextView linkLogin;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etBirthday)
    TextView etBirthday;
    @BindView(R.id.etAddressUser)
    EditText etAddressUser;
//    @BindView(R.id.etFax)
//    EditText etFax;
//    @BindView(R.id.etPrefixFax)
//    EditText etPrefixFax;
//    @BindView(R.id.etPrefixTelp)
//    EditText etPrefixTelp;
//    @BindView(R.id.etTelpNumber)
//    EditText etTelpNumber;
//    @BindView(R.id.etPostalCode)
//    EditText etPostalCode;
    @BindView(R.id.etPrefixPhone)
CountryCodePicker etPrefixPhone;
    @BindView(R.id.etPhoneNumber)
    EditText etPhoneNumber;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etAcountName)
    EditText etAcountName;
    @BindView(R.id.etBank)
    EditText etBank;
    @BindView(R.id.etCabangBank)
    EditText etCabangBank;
    @BindView(R.id.spinerEoName)
    Button spinerEoName;
    @BindView(R.id.btnEoName)
    Button btnEoName;
    @BindView(R.id.etEOAdress)
    EditText etEoAdress;
    @BindView(R.id.iconEoOther)
    ImageView iconEoOther;
    @BindView(R.id.etEoOther)
    EditText etEoOther;


    //EO
    @BindView(R.id.farmasiPosition)
    Button farmasiPosition;
    @BindView(R.id.btnFarmasiPosition)
    Button btnFarmasiPosition;
    @BindView(R.id.etNoRek)
    EditText etNoRek;

    //others
    @BindView(R.id.etOtherName)
    EditText etOtherName;
    @BindView(R.id.etOtherAdress)
    EditText etOtherAdress;
//    @BindView(R.id.rgTitle)
//    RadioGroup rgTitle;
    @BindView(R.id.rgGender)
    RadioGroup rgGender;
    @BindView(R.id.rgFemale)
    RadioButton rgFemale;
    @BindView(R.id.rgMale)
    RadioButton rgMale;
    @BindView(R.id.rtDrg)
    CheckBox rtDrg;
    @BindView(R.id.rtDR)
    CheckBox rtDR;
    @BindView(R.id.rtDr)
    CheckBox rtDr;
    @BindView(R.id.rtProf)
    CheckBox rtProf;
    @BindView(R.id.userType)
    Button spinerUserType;
    @BindView(R.id.btnUserType)
    Button btnUserType;
    @BindView(R.id.spesificTitle)
    Button spinerSpesificTitle;
//    @BindView(R.id.daerah)
//    AutoCompleteTextView textDaerah;
    @BindView(R.id.wilayah)
    Button spinerIDIwilayah;
    @BindView(R.id.btnWilayah)
    Button btnIDIwilayah;
    @BindView(R.id.kecamatan)
    Button spinerIDICabang;
    @BindView(R.id.btnKecamatan)
    Button btnIDICabang;

    @BindView(R.id.residentHospital)
    EditText residentHospital;
    @BindView(R.id.medicalMajorMahasiswa)
    Button spinerMedicalMajorMahasiswa;
    @BindView(R.id.univMahasiswa)
    Button spinerUnivMahasiswa;
    @BindView(R.id.perusahaanFarmasi)
    Button spinerPerusahaanFarmasi;
    @BindView(R.id.btnPerusahaanFarmasi)
    Button btnPerusahaanFarmasi;
    @BindView(R.id.farmasiAddress)
    EditText etFarmasiAddress;
    @BindView(R.id.iconTitle)
    ImageView iconTitle;
    @BindView(R.id.iconSpesificTitle)
    ImageView iconSpesificTitle;
    @BindView(R.id.iconSpesificTitleMahasiswa)
    ImageView iconSpesificTitleMahasiswa;
    @BindView(R.id.iconDaerah)
    ImageView iconDaerah;
    @BindView(R.id.iconKecamatan)
    ImageView iconKecamatan;
    @BindView(R.id.iconResidentHospital)
    ImageView iconResidentHospital;
    @BindView(R.id.iconUnivMahasiswa)
    ImageView iconUnivMahasiswa;
    @BindView(R.id.iconEoName)
    ImageView iconEoName;
    @BindView(R.id.iconAcountName)
    ImageView iconAcountName;
    @BindView(R.id.iconNoRek)
    ImageView iconNoRek;
    @BindView(R.id.iconEOAddress)
    ImageView iconEOAddress;
    @BindView(R.id.iconBank)
    ImageView iconBank;
    @BindView(R.id.iconCabangBank)
    ImageView iconCabangBank;
    @BindView(R.id.iconPerusahaanFarmasi)
    ImageView iconPerusahaanFarmasi;
    @BindView(R.id.iconFarmasiPosition)
    ImageView iconFarmasiPosition;
    @BindView(R.id.iconFarmasiAddress)
    ImageView iconFarmasiAddress;
    @BindView(R.id.iconFarmasiPositionOther)
    ImageView iconFarmasiPositionOther;
    @BindView(R.id.iconOtherName)
    ImageView iconOtherName;
    @BindView(R.id.iconOtherAddress)
    ImageView iconOtherAddress;

    @BindView(R.id.iconOtherFarmasiName)
    ImageView iconOtherFarmasiName;
    @BindView(R.id.otherFarmasiName)
    EditText otherFarmasiName;


    @BindView(R.id.rumahSakitMhs)
    EditText rumahSakitMhs;

    @BindView(R.id.iconRumahSakitMhs)
    ImageView iconRumahSakitMhs;

//    @BindView(R.id.btnResidentHospital)
//    Button btnResidentHospital;
    @BindView(R.id.btnMedicalMajorMahasiswa)
    Button btnMedicalMajorMahasiswa;
    @BindView(R.id.btnUnivMahasiswa)
    Button btnUnivMahasiswa;
    @BindView(R.id.btnSpesificTitle)
    Button btnSpesificTitle;

    @BindView(R.id.pilihPosisi)
    Button spinerPilihPosisi;
    @BindView(R.id.btnPilihPosisi)
    Button btnPilihPosisi;
    @BindView(R.id.iconPilihPosisi)
    ImageView iconPilihPosisi;
    @BindView(R.id.farmasiPositionOther)
    EditText farmasiPositionOther;

    @BindView(R.id.btnUnivResident)
    Button btnUnivResident;
    @BindView(R.id.univResident)
    Button spinerUnivResident;
    @BindView(R.id.iconUnivResident)
    ImageView iconUnivResident;

    String prefixPhone="62";

    List<Profesi> profesi = new ArrayList<>();
    List<Spesialis> spesialis = new ArrayList<>();
    List<Hospital> hospital = new ArrayList<>();
    List<Universitas> universitas  = new ArrayList<>();
    private List<Farmasi> farmasi= new ArrayList<>();
    private List<EO> eventOrganizer= new ArrayList<>();
    private List<String> wilayahIdi= new ArrayList<>();
    private List<IdiCabang> cabangIdi= new ArrayList<>();
    private List<String> jabatanFarmasi= new ArrayList<>();
    private List<String> tipeJabatanFarmasi= new ArrayList<>();

    private DatePickerDialog formTglLahir;

    private SimpleDateFormat dateFormatter;

    //parameter utama
    String noHp="", gender="",birthdayDate="",userAddress="",fax="",postalCode="",telpNumber="",name="",email="",role="";

    //parameter tambahan Dokter role 2
    String spesialiss="",provinsi="",kota="",kecamatan="",kelurahan="",kodepos="",gelar="",idiWilayah="",idiCabang="";

    //parameter tambahan resident 4 spesialis,gelar termasuk
    String rumahSakit="",universitasResident="",tipe="";

    //parameter tambahan mahasiswa 5 spesialis termasuk
    String universitasMhs="";

    //parameter tambahan EO 6
    String eoName="",eoAddress="",accountName="",accountNumber="",accountBank="",accountBranch="";

    //parameter tambahan farmasi 7
    String officeName="",officeOther="",jabatan="",officeAddress="";

    //parameter tambahan Others 8
    String namaOrganisasi="",alamatOrganisasi="";
    JSONObject paramObject;
    AdapterSpinerProfesi adapterSpinerProfesi;;
    AdapterSpinerSpesialis adapterSpesialis;
    AdapterSpinerHospital adapterSpinerHospital;
    AdapterSpinerUniversitas adapterSpinerUniversitas;
    String roleSpesialis="";

//    private DatePickerDialog.OnDateSetListener mDateSetListener;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_register);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);
        allFormGone();
        loadProfesi();
        loadHospital();
        loadUniversitas();
        loadFarmasi();
        loadEoName();
        loadWilayahIDI();
        loadTipeJabatan();

        role=spinerUserType.getText().toString();
        spinerUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUserType();
            }
        });
        btnUserType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUserType();
            }
        });
        etPrefixPhone.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
            @Override
            public void onCountrySelected(Country selectedCountry) {
//                Toast.makeText(MenuRegister.this, "Updated " + selectedCountry.getPhoneCode(), Toast.LENGTH_SHORT).show();
                prefixPhone=selectedCountry.getPhoneCode();
            }
        });
//        etPhoneNumber.setFilters(android.text.method.DialerKeyListener);
        PhoneNumberUtils.formatNumber(etPhoneNumber.getText().toString(),"+628");

        etPhoneNumber.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                final String number = etPhoneNumber.getText().toString();
                etPhoneNumber.setOnKeyListener(new View.OnKeyListener() {
                    @Override
                    public boolean onKey(View v, int keyCode, KeyEvent event) {

                        if (number.length()==2){
                            if (number.startsWith("08")){
                                etPhoneNumber.setText("8");
                                etPhoneNumber.setSelection(1);
                            }
                        }

                        else if (number.length()==3 && keyCode != KeyEvent.KEYCODE_DEL){
                            etPhoneNumber.setText(number+"-");
                            etPhoneNumber.setSelection(4);
                        }else if (number.length()==8 && keyCode != KeyEvent.KEYCODE_DEL){
                            etPhoneNumber.setText(number+"-");
                            etPhoneNumber.setSelection(9);
                        }
                        return false;
                    }
                });

            }

            @Override
            public void afterTextChanged(Editable editable) {
//                Toast.makeText(MenuRegister.this, etPhoneNumber.getText().toString().replace("-",""), Toast.LENGTH_SHORT).show();
            }
        });
        rumahSakit= residentHospital.getText().toString();
//        spinerResidentHospital.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkDataHospital();
//            }
//        });
//        btnResidentHospital.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                checkDataHospital();
//            }
//        });

        eoName= spinerEoName.getText().toString();
        spinerEoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEo();
            }
        });
        btnEoName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataEo();
            }
        });

        spinerMedicalMajorMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJurusan();
            }
        });
        btnMedicalMajorMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJurusan();
            }
        });


        universitasMhs = spinerUnivMahasiswa.getText().toString();
        spinerUnivMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUniversitas("mahasiswa");
            }
        });
        btnUnivMahasiswa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataUniversitas("mahasiswa");
            }
        });

        universitasResident = spinerUnivResident.getText().toString();
        spinerUnivResident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataUniversitas("resident");
            }
        });
        btnUnivResident.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataUniversitas("resident");
            }
        });


        idiWilayah = spinerIDIwilayah.getText().toString();
        spinerIDIwilayah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataIdiWilayah();
            }
        });
        btnIDIwilayah.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataIdiWilayah();
            }
        });

        idiCabang = spinerIDICabang.getText().toString();
        spinerIDICabang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataIdiCabang();
            }
        });
        btnIDICabang.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataIdiCabang();
            }
        });



        spesialiss=spinerSpesificTitle.getText().toString();
        spinerSpesificTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataSpesialisasi();
            }
        });
        btnSpesificTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataSpesialisasi();
            }
        });

        officeName=spinerPerusahaanFarmasi.getText().toString();
        spinerPerusahaanFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataFarmasi();
            }
        });
        btnPerusahaanFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataFarmasi();
            }
        });

        jabatan=farmasiPosition.getText().toString();
        farmasiPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataJabatanFarmasi();
            }
        });
        btnFarmasiPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataJabatanFarmasi();
            }
        });
        spinerPilihPosisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataTipeJabatanFarmasi();
            }
        });
        btnPilihPosisi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                checkDataTipeJabatanFarmasi();
            }
        });


//        final AutoCompleteTextView daerahtext = (AutoCompleteTextView) findViewById(R.id.daerah);
//
//        final AutocompleteAdapter adapter = new AutocompleteAdapter(this,android.R.layout.simple_dropdown_item_1line);
//        daerahtext.setAdapter(adapter);
//
//        //when autocomplete is clicked
//        daerahtext.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            @Override
//            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                String daerah = adapter.getItem(position);
//                daerahtext.setText(daerah);
//            }
//        });

        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendar = Calendar.getInstance();
//        formTglLahir = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
//
//            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
//
//                Calendar newDate = Calendar.getInstance();
//                newDate.set(year, monthOfYear, dayOfMonth);
//                etBirthday.setText(dateFormatter.format(newDate.getTime()));
//            }
//
//        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));


        etBirthday.setOnClickListener(this);
        rtDr.setOnClickListener(this);
        rtProf.setOnClickListener(this);
        rtDR.setOnClickListener(this);
        rtDrg.setOnClickListener(this);
        linkLogin.setOnClickListener(this);
        btnRegister.setOnClickListener(this);


        this.getWindow().setSoftInputMode(WindowManager.
                LayoutParams.SOFT_INPUT_STATE_HIDDEN);
//        mDateSetListener = new DatePickerDialog.OnDateSetListener() {
//            @Override
//            public void onDateSet(DatePicker datePicker, int year, int month, int day) {
//                month = month + 1;
//                String bulan=""+month;
//                String hari=""+day;
//                if (month<10){
//                    bulan="0"+month;
//                }
//                if (day<10){
//                    hari="0"+day;
//                }
//
//                String date =    year +"-" + bulan + "-"+ hari ;
//                etBirthday.setText(date);
//            }
//        };

    }
    boolean isNumber(String string) {
        try {
            int amount = Integer.parseInt(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
    public static class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener{

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState){
            final Calendar calendar = Calendar.getInstance();
            int year = calendar.get(Calendar.YEAR);
            int month = calendar.get(Calendar.MONTH);
            int day = calendar.get(Calendar.DAY_OF_MONTH);


            DatePickerDialog dpd = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_LIGHT,this,year,month,day);

            calendar.add(Calendar.YEAR, -20);
            dpd.getDatePicker().setMaxDate(calendar.getTimeInMillis());
//            calendar.add(Calendar.YEAR, -20);
//            dpd.getDatePicker().setMinDate(calendar.getTimeInMillis());
            return  dpd;
        }

        public void onDateSet(DatePicker view, int year, int month, int day){
            // Do something with the chosen date
            TextView tv = (TextView) getActivity().findViewById(R.id.etBirthday);

            // Create a Date variable/object with user chosen date
            Calendar cal = Calendar.getInstance();
            cal.setTimeInMillis(0);
            cal.set(year, month, day, 0, 0, 0);
            Date chosenDate = cal.getTime();


            SimpleDateFormat formatDate = new SimpleDateFormat("yyyy-MM-dd");
            String formattedDate = formatDate.format(chosenDate);

            // Display the chosen date to app interface
            tv.setText(formattedDate);
        }
    }
    public void hideSoftKeyboard() {
        if(getCurrentFocus()!=null) {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    String gelarBelakang="";
    private void checkDataSpesialisasi() {
        if (!spesialis.isEmpty()) {
            final String[] arrData =new String[spesialis.size()];
            for (int i = 0; i < spesialis.size(); i++) {
                arrData[i] = spesialis.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Spesialisasi");

            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    gelarBelakang="";
                    spinerSpesificTitle.setText(spesialis.get(position).getName());
                    spesialiss=spesialis.get(position).getId();
                    roleSpesialis=spesialis.get(position).getId();
                    gelarBelakang=spesialis.get(position).getGelar();
                    if (gelarBelakang==null){
                        gelarBelakang="x";
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }else {
            Toast.makeText(this, "data tidak ditemukan", Toast.LENGTH_SHORT).show();
            loadSpesialis(roleSpesialis);
        }
    }

    private void checkDataFarmasi() {
        if (!farmasi.isEmpty()) {
            final String[] arrData =new String[farmasi.size()];
            for (int i = 0; i < farmasi.size(); i++) {
                arrData[i] = farmasi.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Farmasi");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerPerusahaanFarmasi.setText(farmasi.get(position).getName());
                    officeName=farmasi.get(position).getId();
                    officeOther=farmasi.get(position).getName();
                    if (farmasi.get(position).getName().equalsIgnoreCase("others")){
                        otherFarmasiName.setVisibility(View.VISIBLE);
                        iconOtherFarmasiName.setVisibility(View.VISIBLE);
                    }else {
                        otherFarmasiName.setVisibility(View.GONE);
                        iconOtherFarmasiName.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataTipeJabatanFarmasi() {
        if (!tipeJabatanFarmasi.isEmpty()) {
            final String[] arrData =new String[tipeJabatanFarmasi.size()];
            for (int i = 0; i < tipeJabatanFarmasi.size(); i++) {
                arrData[i] = tipeJabatanFarmasi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Tipe Jabatan");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerPilihPosisi.setText(tipeJabatanFarmasi.get(position));
//                    tipePosisi=tipeJabatanFarmasi.get(position);

                    loadJabatanFarmasi();
                    farmasiPosition.setVisibility(View.VISIBLE);
                    btnFarmasiPosition.setVisibility(View.VISIBLE);
                    iconFarmasiPosition.setVisibility(View.VISIBLE);

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataJabatanFarmasi() {
        if (!jabatanFarmasi.isEmpty()) {
            final String[] arrData =new String[jabatanFarmasi.size()];
            for (int i = 0; i < jabatanFarmasi.size(); i++) {
                arrData[i] = jabatanFarmasi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Jabatan");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    farmasiPosition.setText(jabatanFarmasi.get(position));
                    jabatan=jabatanFarmasi.get(position);
                    if (jabatan.equalsIgnoreCase("others")){
                        farmasiPositionOther.setVisibility(View.VISIBLE);
                        iconFarmasiPositionOther.setVisibility(View.VISIBLE);
                    }else {
                        farmasiPositionOther.setVisibility(View.GONE);
                        iconFarmasiPositionOther.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }



    private void checkDataUniversitas(final String status) {
        if (!universitas.isEmpty()) {
            final String[] arrData =new String[universitas.size()];
            for (int i = 0; i < universitas.size(); i++) {
                arrData[i] = universitas.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Universitas");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    if (status.equalsIgnoreCase("mahasiswa")){
                        spinerUnivMahasiswa.setText(universitas.get(position).getName());
                        universitasMhs=universitas.get(position).getId();
                    }else if (status.equalsIgnoreCase("resident")){
                        spinerUnivResident.setText(universitas.get(position).getName());
                        universitasResident=universitas.get(position).getId();

                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }
    private void checkDataIdiWilayah() {
        if (!wilayahIdi.isEmpty()) {
            final String[] arrData =new String[wilayahIdi.size()];
            for (int i = 0; i < wilayahIdi.size(); i++) {
                arrData[i] = wilayahIdi.get(i);
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Wilayah IDI");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerIDIwilayah.setText(wilayahIdi.get(position));
                    idiWilayah=wilayahIdi.get(position);
                    loadCabangIDI();
                    spinerIDICabang.setVisibility(View.VISIBLE);
                    btnIDICabang.setVisibility(View.VISIBLE);
                    iconKecamatan.setVisibility(View.VISIBLE);

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }
    private void checkDataIdiCabang() {
        if (!cabangIdi.isEmpty()) {
            final String[] arrData =new String[cabangIdi.size()];
            for (int i = 0; i < cabangIdi.size(); i++) {
                arrData[i] = cabangIdi.get(i).getCabang();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Cabang IDI");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerIDICabang.setText(cabangIdi.get(position).getCabang());
                    idiCabang=cabangIdi.get(position).getId();

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataJurusan() {
        if (!hospital.isEmpty()) {
            final String[] arrData =new String[hospital.size()];
            for (int i = 0; i < hospital.size(); i++) {
                arrData[i] = hospital.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Jurusan");
            final CharSequence[] data = { "Belum Ada data"};
            builder.setItems(data, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
//
//                            spinerResidentHospital.setText(hospital.get(position).getName());
//                            changeView(hospital.get(position).getId());
//                            rumahSakit=hospital.get(position).getId();
//
                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

//    private void checkDataHospital() {
//        if (!hospital.isEmpty()) {
//            final String[] arrData =new String[hospital.size()];
//            for (int i = 0; i < hospital.size(); i++) {
//                arrData[i] = hospital.get(i).getName();
//            }
//            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
//            builder.setTitle("Pilih Rumah Sakit");
//            builder.setItems(arrData, new DialogInterface.OnClickListener() {
//                public void onClick(DialogInterface dialog, int position) {
//
//                    spinerResidentHospital.setText(hospital.get(position).getName());
//                    rumahSakit=hospital.get(position).getId();
//
//                }
//            }).show();
//        }
//    }
    private void checkDataEo() {
        if (!eventOrganizer.isEmpty()) {
            final String[] arrData =new String[eventOrganizer.size()];
            for (int i = 0; i < eventOrganizer.size(); i++) {
                arrData[i] = eventOrganizer.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih EO Name");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerEoName.setText(eventOrganizer.get(position).getName());
                    eoName=eventOrganizer.get(position).getName();
                    if (eoName.equalsIgnoreCase("Others")) {
                        etEoOther.setVisibility(View.VISIBLE);
                        iconEoOther.setVisibility(View.VISIBLE);
                    }else {
                        etEoOther.setVisibility(View.GONE);
                        iconEoOther.setVisibility(View.GONE);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }

    private void checkDataUserType() {
        if (!profesi.isEmpty()) {
            final String[] arrData =new String[profesi.size()];
            for (int i = 0; i < profesi.size(); i++) {
                arrData[i] = profesi.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(MenuRegister.this);
            builder.setTitle("Pilih Tipe User");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinerUserType.setText(profesi.get(position).getName());
                    changeView(profesi.get(position).getId());
                    role=profesi.get(position).getId();
                    roleSpesialis=role;
                    if (role.equalsIgnoreCase("4")) {
                        loadSpesialis(roleSpesialis);
                    }

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }
    }



    private void changeView(String ID){
        switch (ID){
            case "2"  :
                AktifDokter();
                break;
            case "3"  :
                AktifDrg();
                break;
            case "4"  :
                AktifResident();
                break;
            case "5"  :
                AktifMahasiswa();
                break;
            case "6"  :
                AktifEO();
                break;
            case "7"  :
                AktifFarmasi();
                break;
            case "8"  :
                AktifOther();
                break;

        }
    }
//    private void showCalendar(){
//        Calendar cal = Calendar.getInstance();
//        int year = cal.get(Calendar.YEAR);
//        int month = cal.get(Calendar.MONTH);
//        int day = cal.get(Calendar.DAY_OF_MONTH);
//
//        DatePickerDialog dialog = new DatePickerDialog(
//                MenuRegister.this,
//                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
//                mDateSetListener,
//                year,month,day);
//        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        dialog.show();
//    }
    String gelarDepan;
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.etBirthday:
//                showCalendar();
                // Initialize a new date picker dialog fragment
                DialogFragment dFragment = new DatePickerFragment();

                // Show the date picker dialog fragment
                dFragment.show(getSupportFragmentManager(), "Date Picker");
                hideSoftKeyboard();
                break;
            case R.id.btnRegister:
                if (checkReg()){
                    progressDialog = new ProgressDialog(this);
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.show();

                    sendRegistrasi();

                }else {
                    showAllert("Message","tolong isi semua form yang dibutuhkan dengan benar","none");
                }


                break;
            case R.id.linkLogin:
//                Intent i = new Intent(MenuRegister.this,MenuLogin.class);
//                startActivity(i);
                finish();
                break;
            case R.id.rtDr:

                if (role.equalsIgnoreCase("4")){
                    spinerSpesificTitle.setText("Pilih Pendidikan Spesialis");
                    spesialiss="Pilih Pendidikan Spesialis";
                }else {
                    spinerSpesificTitle.setText("pilih spesifik gelar");
                    spesialiss="pilih spesifik gelar";
                }
                if(!rtDrg.isChecked() && !rtDr.isChecked()|| role.equalsIgnoreCase("5")){
                    spinerSpesificTitle.setVisibility(View.GONE);
                    btnSpesificTitle.setVisibility(View.GONE);
                    iconSpesificTitle.setVisibility(View.GONE);
                }else {

                    spinerSpesificTitle.setVisibility(View.VISIBLE);
                    btnSpesificTitle.setVisibility(View.VISIBLE);
                    iconSpesificTitle.setVisibility(View.VISIBLE);
                    if (role.equalsIgnoreCase("4")){
                        tipe="Dr";
                        roleSpesialis="3";
                    }else {
                        roleSpesialis="2";
                    }
                    loadSpesialis(roleSpesialis);
                }

                if (rtDrg.isChecked()){
                    rtDrg.setChecked(false);

                }
                break;
            case R.id.rtDrg:
                if (role.equalsIgnoreCase("4")){
                    spinerSpesificTitle.setText("Pilih Pendidikan Spesialis");
                    spesialiss="Pilih Pendidikan Spesialis";
                }else {
                    spinerSpesificTitle.setText("pilih spesifik gelar");
                    spesialiss="pilih spesifik gelar";
                }
                if(!rtDrg.isChecked() && !rtDr.isChecked()|| role.equalsIgnoreCase("5")){
                    spinerSpesificTitle.setVisibility(View.GONE);
                    btnSpesificTitle.setVisibility(View.GONE);
                    iconSpesificTitle.setVisibility(View.GONE);
                }else {
                    spinerSpesificTitle.setVisibility(View.VISIBLE);
                    btnSpesificTitle.setVisibility(View.VISIBLE);
                    iconSpesificTitle.setVisibility(View.VISIBLE);
                    if (role.equalsIgnoreCase("4")){
                        tipe="Drg";
                        roleSpesialis="2";
                    }else {
                        roleSpesialis="3";
                    }
                    loadSpesialis(roleSpesialis);
                }

                if (rtDr.isChecked()){
                    rtDr.setChecked(false);

                }
                break;
            case R.id.rtProf:
                loadSpesialis(roleSpesialis);
                break;
            case R.id.rtDR:
                loadSpesialis(roleSpesialis);
                break;

        }
    }

    private boolean checkReg(){
        boolean validate=true;
        if (!etName.getText().toString().isEmpty()){
            name = etName.getText().toString();
        }else {
            validate=false;

            System.out.println("VALIDASI : NAMA");
        }
        if (!etBirthday.getText().toString().isEmpty()){
            birthdayDate = etBirthday.getText().toString();
        }else {
            birthdayDate="";
            validate=false;

            System.out.println("VALIDASI : Birthday");

        }
        if (!etEmail.getText().toString().isEmpty()){
            if (isEmailValid(etEmail.getText().toString())) {
                email = etEmail.getText().toString();
            }else {
                validate=false;
                System.out.println("VALIDASI : Email");
            }
        }else {
            validate=false;
            System.out.println("VALIDASI : Email");

        }
        if (!etAddressUser.getText().toString().isEmpty()){
            userAddress = etAddressUser.getText().toString();
        }else {
            userAddress="";

        }

//        if (!etFax.getText().toString().isEmpty() && !etPrefixFax.getText().toString().isEmpty()){
//            fax = etFax.getText().toString();
//        }else {
//            fax="";
//
//        }
//        String prefixNumber =etPrefixPhone.getText().toString().replace("+","");
        String phoneNumber =etPhoneNumber.getText().toString().replace("-","");
        if (!phoneNumber.isEmpty()){
            if (phoneNumber.startsWith("8")) {
                noHp = prefixPhone + phoneNumber;
//                Toast.makeText(this, noHp, Toast.LENGTH_SHORT).show();


            }else if(phoneNumber.startsWith("08")){

//                noHp = "62" + etPhoneNumber.getText().toString().substring(1);
                validate=false;
                System.out.println("VALIDASI : phone number");
            }else {
                validate = false;
                System.out.println("VALIDASI : phone number");
            }


        }else {
            validate = false;
            System.out.println("VALIDASI : phone number");

        }
//        if (!etTelpNumber.getText().toString().isEmpty() && !etPrefixTelp.getText().toString().isEmpty()){
//            telpNumber = etTelpNumber.getText().toString();
//        }else {
//            telpNumber="";
//
//        }
//        if (!etPostalCode.getText().toString().isEmpty()){
//            postalCode = etPostalCode.getText().toString();
//        }else {
//            postalCode="";
//
//        }

        int rgGenderSelected = rgGender.getCheckedRadioButtonId();
        if (rgGenderSelected != -1)
        {
            RadioButton selectedRadioButton = (RadioButton) findViewById(rgGenderSelected);
            gender = selectedRadioButton.getText().toString();

        }
        else
        {
            validate=false;
            System.out.println("VALIDASI : Gender");
        }

//        int index = spinerUserType.getSelectedItemPosition();
//        role =adapterSpinerProfesi.getIDFromIndex(index);
        if (role.equalsIgnoreCase("2")||role.equalsIgnoreCase("3")){
            //jika pilih user type Dokter dan drg
            if (gelarBelakang.equalsIgnoreCase("")|| gelarBelakang.isEmpty()){
                validate=false;
                System.out.println("VALDIDASI : gelar belakang");
            }
            if (idiCabang.isEmpty() || idiCabang.equalsIgnoreCase("")||
                    idiCabang.equalsIgnoreCase("Pilih Cabang IDI")){
                validate=false;
                System.out.println("VALIDASI : Idi Cabang");

            }

            //check rg title dan set value gelar
                gelarDepan="";
                if (rtProf.isChecked()){
                    gelarDepan=gelarDepan+"Prof. ";
                }
                if (rtDR.isChecked()){
                    gelarDepan=gelarDepan+"DR. ";
                }
                if (rtDr.isChecked()){
                    gelarDepan=gelarDepan+"Dr. ";
                }
                if (rtDrg.isChecked()){
                    gelarDepan=gelarDepan+"Drg. ";
                }


                if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                    validate=false;
                    System.out.println("VALIDASI : gelar depan");
                }
                if (gelarBelakang.equalsIgnoreCase("x")){
                    name= gelarDepan+name;
                }else {
                    name= gelarDepan+name+", "+gelarBelakang;
                }

            //check spiner title spesialis dan set value spesialis
            if (spesialiss.equalsIgnoreCase("pilih spesifik gelar") ||
                    spesialiss.equalsIgnoreCase("Pilih Pendidikan Spesialis") ||
                    spesialiss.isEmpty() || spesialiss.equalsIgnoreCase("")){
                validate=false;
                System.out.println("VALIDASI : spesialis");
            }

//            spesialiss =adapterSpesialis.getIDFromIndex(spinerSpesificTitle.getSelectedItemPosition());


            //check spiner provinsi dan set value wilayah
//            String wilayah=textDaerah.getText().toString();
//            String [] arrWilayah = wilayah.split(",");
//            if (arrWilayah.length==5) {
//                provinsi = arrWilayah[0].toString();
//                kota = arrWilayah[1].toString();
//                kecamatan = arrWilayah[2].toString();
//                kelurahan = arrWilayah[3].toString();
//                kodepos = arrWilayah[4].toString();
//            }else {
//                validate=false;
//                System.out.println("VALIDASI : Lokasi");
//            }

//            Toast.makeText(this, "sepsialis "+spesialiss, Toast.LENGTH_SHORT).show();

        }else if (role.equalsIgnoreCase("4")){
            //Resident
//            if (gelarBelakang.equalsIgnoreCase("")|| gelarBelakang.isEmpty()){
//                validate=false;
//                System.out.println("VALDIDASI : gelar belakang");
//            }
//            if (spinerSpesificTitle.getText().toString().equalsIgnoreCase("pilih spesifik gelar"))
            //check rg title dan set value gelar
            gelarDepan="";
            if (rtProf.isChecked()){
                gelarDepan="Prof. ";
            }
            if (rtDR.isChecked()){
                gelarDepan=gelarDepan+"DR. ";
            }
            if (rtDr.isChecked()){
                gelarDepan=gelarDepan+"Dr. ";
            }
            if (rtDrg.isChecked()){
                gelarDepan=gelarDepan+"Drg. ";
            }


            if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : gelar depan");
            }
            name= gelarDepan+name;
            //check spiner title spesialis dan set value spesialis
            if (spesialiss.equalsIgnoreCase("pilih spesifik gelar") ||
                    spesialiss.isEmpty() || spesialiss.equalsIgnoreCase("")){
                validate=false;
                System.out.println("VALIDASI : spesialis");
            }
//            Toast.makeText(this, "sepsialis "+spesialiss, Toast.LENGTH_SHORT).show();

//            spesialiss =adapterSpesialis.getIDFromIndex(spinerSpesificTitle.getSelectedItemPosition());

            //check rg title dan set value gelar
//            int rgTitleSelected = rgTitle.getCheckedRadioButtonId();
//            if (rgTitleSelected != -1)
//            {
//                RadioButton selectedRadioButton = (RadioButton) findViewById(rgTitleSelected);
//                gelar = selectedRadioButton.getText().toString();
//
//            }
//            else
//            {
//
//                validate=false;
//                System.out.println("VALIDASI : title");
//            }

            //check spiner hospital dan set value rumahsakit
            rumahSakit= residentHospital.getText().toString();
            if (rumahSakit.equalsIgnoreCase("") || rumahSakit.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : rumah sakit resident");

            }
//            rumahSakit =adapterSpinerHospital.getIDFromIndex(spinerResidentHospital.getSelectedItemPosition());

//            check spiner univResident dan set value universitas
            if (universitasResident.equalsIgnoreCase("pilih universitas")){
                validate=false;

                System.out.println("VALIDASI : universitas resident");
            }


        }else if (role.equalsIgnoreCase("5")){
            //Mahasiswa

            gelarDepan="";

            if (rtDr.isChecked()){
                gelarDepan=gelarDepan+"Dr. ";
            }
            if (rtDrg.isChecked()){
                gelarDepan=gelarDepan+"Drg. ";
            }


            if (gelarDepan.equalsIgnoreCase("")|| gelarDepan.isEmpty()){
                validate=false;
                System.out.println("VALIDASI : gelar depan");
            }
            //check spiner title spesialis dan set value spesialis
//            spesialiss ="1";

            //check spiner univMahasiswa dan set value universitas
            if (universitasMhs.equalsIgnoreCase("pilih universitas")){
                validate=false;

                System.out.println("VALIDASI : universitas resident");
            }


            if (rumahSakitMhs.getText().toString().isEmpty()){
                validate=false;
                System.out.println("VALIDASI : rumah sakit mahasiswa");
            }
        }else if (role.equalsIgnoreCase("6")){
            //EO

            //check edittext eo name dan set value eoName
            if (eoName.equalsIgnoreCase("Pilih EO Name")){
                validate=false;

                System.out.println("VALIDASI : EO name");
            }else if (eoName.equalsIgnoreCase("Others")){
                if (etEoOther.getText().toString().isEmpty()){
                    validate=false;
                }else {
                    eoName=etEoOther.getText().toString();
                }
            }

            //check edittext eo address dan set value eoAddress
            if (!etEoAdress.getText().toString().isEmpty()){
                eoAddress = etEoAdress.getText().toString();
            }else {
                validate=false;
            }

            //check edittext norek dan set value accountNumber
            if (!etNoRek.getText().toString().isEmpty()){
                accountNumber = etNoRek.getText().toString();
            }else {
                validate=false;
            }

            //check edittext eo acountName dan set value accountName
            if (!etAcountName.getText().toString().isEmpty()){
                accountName = etAcountName.getText().toString();
            }else {
                validate=false;
            }

            //check edittext noRek dan set value accountBank
            if (!etBank.getText().toString().isEmpty()){
                accountBank = etBank.getText().toString();
            }else {
                validate=false;
            }

            //check edittext noRek dan set value accountBranch
            if (!etCabangBank.getText().toString().isEmpty()){
                accountBranch = etCabangBank.getText().toString();
            }else {
                validate=false;
            }


        }else if (role.equalsIgnoreCase("7")){
            //farmasi

            //check spiner farmasiAlkes dan setValue officeName
            if (officeName.equalsIgnoreCase("Pilih Farmasi")){
                validate=false;
                System.out.println("VALIDASI : FARMASI");
                Toast.makeText(this, "FARMASI", Toast.LENGTH_SHORT).show();

            }else if (officeOther.equalsIgnoreCase("Others")){
                officeOther=otherFarmasiName.getText().toString();
                if (officeOther.equalsIgnoreCase("")){
                    validate=false;
                    System.out.println("VALIDASI : FARMASI");
                }
            }


            //check spiner position dan setValue jabatan
//            if (!etFarmasiPosition.getText().toString().isEmpty()){
//
//                jabatan =etFarmasiPosition.getText().toString();
//            }else {
//                validate=false;
//            }
            if (jabatan.equalsIgnoreCase("Pilih Jabatan")){
                validate=false;
                System.out.println("VALIDASI : JABATAN FARMASI");

            }else if (jabatan.equalsIgnoreCase("Others")){
                if (farmasiPositionOther.getText().toString().isEmpty()){
                    validate=false;
                    System.out.println("VALIDASI : JABATAN FARMASI");
                }else {
                    jabatan=farmasiPositionOther.getText().toString();
                }
            }

            //check spiner officeAddress dan setValue officeAddress
            if (!etFarmasiAddress.getText().toString().isEmpty()){
                officeAddress =etFarmasiAddress.getText().toString();
            }else {
                validate=false;
            }



        }else if (role.equalsIgnoreCase("8")){
            //others

            //check edittext noRek dan set value accountBranch
            if (!etOtherName.getText().toString().isEmpty()){
                namaOrganisasi = etOtherName.getText().toString();
            }else {
                validate=false;
            }
            //check edittext noRek dan set value accountBranch
            if (!etOtherAdress.getText().toString().isEmpty()){
                alamatOrganisasi = etOtherAdress.getText().toString();
            }else {
                validate=false;
            }
        }else {
            validate=false;
        }
        System.out.println("Selesai di eksekusi dengan nilai "+validate);
        return validate;
    }


    private void loadProfesi(){
        profesi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueProfesi> data = apiInterface.getAllProfesi(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueProfesi>() {
            @Override
            public void onResponse(Call<ValueProfesi> call, Response<ValueProfesi> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Profesi> prof;
                    prof = response.body().getData().getProfesi();

                    if (!prof.isEmpty()){
                        profesi.addAll(prof);

                    }




//                    adapterSpinerProfesi = new AdapterSpinerProfesi(MenuRegister.this, profesi);
//                    spinerUserType.setAdapter(adapterSpinerProfesi);

                    System.out.println("VALUENYA : "+profesi.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueProfesi> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                showAllert("Informasi","Tidak dapat terhubung ke server","loadProfesi");

            }
        });
    }
    private void loadSpesialis(String role){
        if (role.equalsIgnoreCase("4")){
            spinerSpesificTitle.setText("Pilih Pendidikan Spesialis");
            spesialiss="Pilih Pendidikan Spesialis";
        }else {
            spinerSpesificTitle.setText("pilih spesifik gelar");
            spesialiss="pilih spesifik gelar";
        }
        spesialis.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
            paramObject.put("tipe",tipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueSpesialis> spesialisCall = apiInterface.getAllSpesialis(paramObject.toString(),"saxasdas");

        spesialisCall.enqueue(new Callback<ValueSpesialis>() {
            @Override
            public void onResponse(Call<ValueSpesialis> call, Response<ValueSpesialis> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<Spesialis> spesialisData;
                    spesialisData = response.body().getData().getSpesialis();

                    if (!spesialisData.isEmpty()){

                        for (int i=0;i<spesialisData.size();i++){
                            if (spesialisData.get(i).getName().equalsIgnoreCase("Dokter Gigi Umum")&&
                                    (rtDR.isChecked() || rtProf.isChecked())){
                                System.out.println("load diakses gigi");

                            }else if (spesialisData.get(i).getName().equalsIgnoreCase("Dokter Umum") &&
                                    (rtDR.isChecked() || rtProf.isChecked())){
                                System.out.println("load diakses umum");
                            }else {
                                spesialis.add(spesialisData.get(i));
                                System.out.println("load diakses setdata");
                            }

                        }

                    }else {

                        System.out.println("DATAKU KOSONG");
                    }
//                    adapterSpesialis = new AdapterSpinerSpesialis(MenuRegister.this, spesialis);
//
//                    spinerSpesificTitle.setAdapter(adapterSpesialis);
//
//                    System.out.println("VALUENYA : "+spesialis.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueSpesialis> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }

    private void loadWilayahIDI(){
        wilayahIdi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueIdiWilayah> dataCall = apiInterface.getAllIdiWilayah(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueIdiWilayah>() {
            @Override
            public void onResponse(Call<ValueIdiWilayah> call, Response<ValueIdiWilayah> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        wilayahIdi.addAll(data);

                    }
//                    adapterSpesialis = new AdapterSpinerSpesialis(MenuRegister.this, spesialis);
//
//                    spinerSpesificTitle.setAdapter(adapterSpesialis);
//
//                    System.out.println("VALUENYA : "+spesialis.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueIdiWilayah> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }
    private void loadCabangIDI(){
        spinerIDICabang.setText("Pilih Cabang IDI");
        cabangIdi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("search",spinerIDIwilayah.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueIdiCabang> dataCall = apiInterface.getAllIdiCabang(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueIdiCabang>() {
            @Override
            public void onResponse(Call<ValueIdiCabang> call, Response<ValueIdiCabang> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<IdiCabang> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        cabangIdi.addAll(data);

                    }
//                    adapterSpesialis = new AdapterSpinerSpesialis(MenuRegister.this, spesialis);
//
//                    spinerSpesificTitle.setAdapter(adapterSpesialis);
//
//                    System.out.println("VALUENYA : "+spesialis.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueIdiCabang> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }
    private void loadTipeJabatan(){
        tipeJabatanFarmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_user","");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueJabatan> dataCall = apiInterface.getAllJabatanFarmasi(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJabatan>() {
            @Override
            public void onResponse(Call<ValueJabatan> call, Response<ValueJabatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        tipeJabatanFarmasi.addAll(data);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueJabatan> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }
    private void loadJabatanFarmasi(){
        jabatanFarmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_user","");
            paramObject.put("search",spinerPilihPosisi.getText().toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueJabatan> dataCall = apiInterface.getAllJabatanFarmasi(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJabatan>() {
            @Override
            public void onResponse(Call<ValueJabatan> call, Response<ValueJabatan> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<String> data;
                    data = response.body().getData().getList();

                    if (!data.isEmpty()){
                        jabatanFarmasi.addAll(data);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueJabatan> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadSpesialis");

            }
        });
    }

    private void loadFarmasi(){
        farmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueFarmasi> farmasiCall = apiInterface.getAllFarmasi(paramObject.toString(),"saxasdas");

        farmasiCall.enqueue(new Callback<ValueFarmasi>() {
            @Override
            public void onResponse(Call<ValueFarmasi> call, Response<ValueFarmasi> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<Farmasi> farmasiData;
                    farmasiData = response.body().getData().getFarmasi();

                    if (!farmasiData.isEmpty()){
                        farmasi.addAll(farmasiData);

                    }
//                    adapterSpesialis = new AdapterSpinerSpesialis(MenuRegister.this, spesialis);
//
//                    spinerSpesificTitle.setAdapter(adapterSpesialis);
//
//                    System.out.println("VALUENYA : "+spesialis.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueFarmasi> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadFarmasi");

            }
        });
    }
    private void loadEoName(){
        eventOrganizer.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueEO> farmasiCall = apiInterface.getAllEO(paramObject.toString(),"saxasdas");

        farmasiCall.enqueue(new Callback<ValueEO>() {
            @Override
            public void onResponse(Call<ValueEO> call, Response<ValueEO> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

//                    spesialis= new ArrayList<>();
//                    spesialis = response.body().getData().getSpesialis();

                    List<EO> eoData;
                    eoData = response.body().getData().getEvent_organizer();

                    if (!eoData.isEmpty()){
                        eventOrganizer.addAll(eoData);

                    }
//                    adapterSpesialis = new AdapterSpinerSpesialis(MenuRegister.this, spesialis);
//
//                    spinerSpesificTitle.setAdapter(adapterSpesialis);
//
//                    System.out.println("VALUENYA : "+spesialis.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueEO> call, Throwable t) {

                showAllert("Informasi","Tidak dapat terhubung ke server","loadEO");

            }
        });
    }

    private void loadHospital(){
        hospital.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueHospital> data = apiInterface.getAllHospital(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueHospital>() {
            @Override
            public void onResponse(Call<ValueHospital> call, Response<ValueHospital> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){

                    List<Hospital> hospitalData;
                    hospitalData = response.body().getData().getHospital();

                    if (!hospitalData.isEmpty()){
                        hospital.addAll(hospitalData);

                    }

//                    hospital= new ArrayList<>();
//                    hospital = response.body().getData().getAktivitas();
//
//                    adapterSpinerHospital = new AdapterSpinerHospital(MenuRegister.this, hospital);
//                    spinerResidentHospital.setAdapter(adapterSpinerHospital);
//
//                    System.out.println("VALUENYA : "+hospital.toString());


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueHospital> call, Throwable t) {
                showAllert("Informasi","Tidak dapat terhubung ke server","loadHospital");

            }
        });
    }
    private void loadUniversitas(){
        universitas.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueUniversitas> call = apiInterface.getAllUniversitas(paramObject.toString(),"saxasdas");

        call.enqueue(new Callback<ValueUniversitas>() {
            @Override
            public void onResponse(Call<ValueUniversitas> call, Response<ValueUniversitas> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<Universitas> universitasData;
                    universitasData = response.body().getData().getUniversitas();

                    if (!universitasData.isEmpty()){
                        universitas.addAll(universitasData);

                    }
//                    universitas= new ArrayList<>();
//                    universitas = response.body().getData().getUniversitas();
//
//                    adapterSpinerUniversitas = new AdapterSpinerUniversitas(MenuRegister.this, universitas);
//                    spinerUnivMahasiswa.setAdapter(adapterSpinerUniversitas);



                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueUniversitas> call, Throwable t) {
                showAllert("Informasi","Tidak dapat terhubung ke server","loadUniversitas");

            }
        });
    }
    private void sendRegistrasi(){

        SessionDevice sd = new SessionDevice();
        String fcm = sd.getTokenDevice(MenuRegister.this,"token");
        String imei =sd.getImei(MenuRegister.this,"imei");
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);


        try {
            paramObject = new JSONObject();
            paramObject.put("phone_number", noHp);
            paramObject.put("name", name);
            paramObject.put("gender", gender);
            paramObject.put("birthday", birthdayDate);
            paramObject.put("address", userAddress);
            paramObject.put("kode_pos", postalCode);
            paramObject.put("no_telp", telpNumber);
            paramObject.put("no_fax", fax);
            paramObject.put("email", email);
            paramObject.put("imei", imei);
            paramObject.put("fcm_key", fcm);
            paramObject.put("role", role);
            paramObject.put("from", "register");
            if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("provinsi",provinsi);
                paramObject.put("kota",kota);
                paramObject.put("kecamatan",kecamatan);
                paramObject.put("kelurahan",kelurahan);
                paramObject.put("kodepos",kodepos);
                paramObject.put("gelar",gelarDepan);
                paramObject.put("id_cabang",idiCabang);

            }else if (role.equalsIgnoreCase("4")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("rumah_sakit",rumahSakit);
                paramObject.put("gelar",gelarDepan);
                paramObject.put("tipe",tipe);
                paramObject.put("universitas",universitasResident);

            }else if (role.equalsIgnoreCase("5")){
                paramObject.put("spesialis",spesialiss);
                paramObject.put("universitas",universitasMhs);
                paramObject.put("gelar",gelarDepan);

            }else if (role.equalsIgnoreCase("6")){
                paramObject.put("eo_name",eoName);
                paramObject.put("eo_address",eoAddress);
                paramObject.put("account_name",accountName);
                paramObject.put("account_number",accountNumber);
                paramObject.put("account_bank",accountBank);
                paramObject.put("account_branch",accountBranch);

            }else if (role.equalsIgnoreCase("7")){
                paramObject.put("office_name", officeName);
                paramObject.put("office_others", officeOther);
                paramObject.put("jabatan", jabatan);
                paramObject.put("office_address", officeAddress);
                paramObject.put("tipe_jabatan", spinerPilihPosisi.getText().toString());

            }else if (role.equalsIgnoreCase("8")){
                paramObject.put("nama_organisasi", namaOrganisasi);
                paramObject.put("alamat_organisasi", alamatOrganisasi);
            }

            Call<OtpVerifikasi> verCall = apiInterface.getVerifikasiRegistrasi(paramObject.toString(),"saxasdas");
            System.out.println("PARAMETER SEND "+paramObject.toString());
            verCall.enqueue(new Callback<OtpVerifikasi>() {
                @Override
                public void onResponse(Call<OtpVerifikasi> call, Response<OtpVerifikasi> response) {

                    progressDialog.dismiss();
                    if (response.body().getStatus().equalsIgnoreCase("success")){
                        Log.d("Informasi", response.body().getMessage());

                        Intent i = new Intent(MenuRegister.this, OtpValidation.class);
                        i.putExtra("nohp", noHp);
                        i.putExtra("from", "register");
                        startActivity(i);
                        finish();
                    }else if (response.body().getStatus().equalsIgnoreCase("aktivasi")){
                        Intent i = new Intent(MenuRegister.this, OtpValidation.class);
//                        showAllert("Registrasi Gagal",response.body().getMessage()+" klik ok untuk resend code");
                        i.putExtra("nohp", noHp);
                        i.putExtra("from", "register");
                        startActivity(i);
                        finish();
                    }else {
                        System.out.println("asd [registrasi] 1 "+response.body().toString());
                        System.out.println("asd [registrasi] 2 "+response.body().getStatus()+"|"+response.body().getMessage());
                        showAllert("Registrasi Gagal",response.body().getMessage(),"none");
                        System.out.println("asd ");
                    }
                }

                @Override
                public void onFailure(Call<OtpVerifikasi> call, Throwable t) {
                    progressDialog.dismiss();
                    showAllert("Login Gagal","Gagal Koneksi ke server","none");
                    System.out.println("Informasi "+t.getMessage());
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
    private void showAllert(String title, String message, final String action){
        alertDialog = new AlertDialog.Builder(this);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (action.equalsIgnoreCase("loadProfesi")){
                            loadProfesi();
                        }else if (action.equalsIgnoreCase("loadHospital")){
                            loadHospital();
                        }else if (action.equalsIgnoreCase("loadUniversitas")){
                            loadUniversitas();
                        }
                    }
                })
                .show();
    }

    public boolean isEmailValid(String email)
    {
        String regExpn =
                "^(([\\w-]+\\.)+[\\w-]+|([a-zA-Z]{1}|[\\w-]{2,}))@"
                        +"((([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\."
                        +"([0-1]?[0-9]{1,2}|25[0-5]|2[0-4][0-9])\\.([0-1]?"
                        +"[0-9]{1,2}|25[0-5]|2[0-4][0-9])){1}|"
                        +"([a-zA-Z]+[\\w-]+\\.)+[a-zA-Z]{2,4})$";

        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(regExpn,Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);

        if(matcher.matches())
            return true;
        else
            return false;
    }

    private void uncheckAllTitle(){
        rtDr.setChecked(false);
        rtDR.setChecked(false);
        rtProf.setChecked(false);
        rtDrg.setChecked(false);
    }
    private void AktifDokter() {
        allFormGone();
//        rgTitle.setVisibility(View.VISIBLE);
        rtProf.setVisibility(View.VISIBLE);
        rtDR.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
//        textDaerah.setVisibility(View.VISIBLE);
//        spinerIDICabang.setVisibility(View.VISIBLE);
//        btnIDICabang.setVisibility(View.VISIBLE);
        spinerIDIwilayah.setVisibility(View.VISIBLE);
        btnIDIwilayah.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
//        spinerSpesificTitle.setVisibility(View.VISIBLE);
//        btnSpesificTitle.setVisibility(View.VISIBLE);
//        iconSpesificTitle.setVisibility(View.VISIBLE);
        iconDaerah.setVisibility(View.VISIBLE);
//        iconKecamatan.setVisibility(View.VISIBLE);

        uncheckAllTitle();

    }
    private void AktifDrg() {
        allFormGone();
//        rgTitle.setVisibility(View.VISIBLE);
        rtProf.setVisibility(View.VISIBLE);
        rtDR.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
        spinerSpesificTitle.setVisibility(View.VISIBLE);
        btnSpesificTitle.setVisibility(View.VISIBLE);
//        textDaerah.setVisibility(View.VISIBLE);
        spinerIDICabang.setVisibility(View.VISIBLE);
        btnIDICabang.setVisibility(View.VISIBLE);
        spinerIDIwilayah.setVisibility(View.VISIBLE);
        btnIDIwilayah.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
        iconSpesificTitle.setVisibility(View.VISIBLE);
        iconDaerah.setVisibility(View.VISIBLE);
//        iconKecamatan.setVisibility(View.VISIBLE);
        uncheckAllTitle();

    }
    private void AktifResident() {
        allFormGone();
//        rgTitle.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
//        spinerSpesificTitle.setVisibility(View.VISIBLE);
//        btnSpesificTitle.setVisibility(View.VISIBLE);
        residentHospital.setVisibility(View.VISIBLE);
//        btnResidentHospital.setVisibility(View.VISIBLE);
        iconUnivResident.setVisibility(View.VISIBLE);
        spinerUnivResident.setVisibility(View.VISIBLE);
        btnUnivResident.setVisibility(View.VISIBLE);

        iconTitle.setVisibility(View.VISIBLE);
//        iconSpesificTitle.setVisibility(View.VISIBLE);
        iconResidentHospital.setVisibility(View.VISIBLE);
        uncheckAllTitle();
        gelarBelakang="";


//        rgTitle.clearCheck();
        spinerSpesificTitle.setText("pilih spesifik gelar");
//        spinerResidentHospital.setText("pilih resident hospital");
    }
    private void AktifMahasiswa() {
        allFormGone();
        spinerUnivMahasiswa.setVisibility(View.VISIBLE);
        btnUnivMahasiswa.setVisibility(View.VISIBLE);
        iconTitle.setVisibility(View.VISIBLE);
        rtDrg.setVisibility(View.VISIBLE);
        rtDr.setVisibility(View.VISIBLE);
//        spinerMedicalMajorMahasiswa.setVisibility(View.VISIBLE);
        iconUnivMahasiswa.setVisibility(View.VISIBLE);
//        iconSpesificTitleMahasiswa.setVisibility(View.VISIBLE);
//        btnMedicalMajorMahasiswa.setVisibility(View.VISIBLE);

//        spinerUnivMahasiswa.setText("pilih universitas");
//        spinerMedicalMajorMahasiswa.setText("pilih jurusan");

        rumahSakitMhs.setVisibility(View.VISIBLE);
        iconRumahSakitMhs.setVisibility(View.VISIBLE);
    }
    private void AktifEO() {
        allFormGone();
        spinerEoName.setVisibility(View.VISIBLE);
        btnEoName.setVisibility(View.VISIBLE);
        etNoRek.setVisibility(View.VISIBLE);
        etAcountName.setVisibility(View.VISIBLE);
        etBank.setVisibility(View.VISIBLE);
        etCabangBank.setVisibility(View.VISIBLE);
        etEoAdress.setVisibility(View.VISIBLE);

        iconEoName.setVisibility(View.VISIBLE);
        iconNoRek.setVisibility(View.VISIBLE);
        iconAcountName.setVisibility(View.VISIBLE);
        iconBank.setVisibility(View.VISIBLE);
        iconCabangBank.setVisibility(View.VISIBLE);
        iconEOAddress.setVisibility(View.VISIBLE);
    }
    private void AktifFarmasi() {
        allFormGone();
        spinerPerusahaanFarmasi.setVisibility(View.VISIBLE);
        etFarmasiAddress.setVisibility(View.VISIBLE);
//        etFarmasiPosition.setVisibility(View.VISIBLE);
//        farmasiPosition.setVisibility(View.VISIBLE);
//        btnFarmasiPosition.setVisibility(View.VISIBLE);
//        iconFarmasiPosition.setVisibility(View.VISIBLE);

        btnPerusahaanFarmasi.setVisibility(View.VISIBLE);
        btnPilihPosisi.setVisibility(View.VISIBLE);
        spinerPilihPosisi.setVisibility(View.VISIBLE);
        iconPilihPosisi.setVisibility(View.VISIBLE);

        iconPerusahaanFarmasi.setVisibility(View.VISIBLE);
        iconFarmasiAddress.setVisibility(View.VISIBLE);


    }
    private void AktifOther() {
        allFormGone();
        etOtherName.setVisibility(View.VISIBLE);
        etOtherAdress.setVisibility(View.VISIBLE);
        iconOtherName.setVisibility(View.VISIBLE);
        iconOtherAddress.setVisibility(View.VISIBLE);
    }
    private void allFormGone(){
//        rgTitle.setVisibility(View.GONE);
        rtProf.setVisibility(View.GONE);
        rtDR.setVisibility(View.GONE);
        rtDrg.setVisibility(View.GONE);
        rtDr.setVisibility(View.GONE);
        spinerSpesificTitle.setVisibility(View.GONE);
        btnUnivMahasiswa.setVisibility(View.GONE);
        btnSpesificTitle.setVisibility(View.GONE);
//        textDaerah.setVisibility(View.GONE);
        spinerIDICabang.setVisibility(View.GONE);
        btnIDICabang.setVisibility(View.GONE);
        spinerIDIwilayah.setVisibility(View.GONE);
        btnIDIwilayah.setVisibility(View.GONE);
        etFarmasiAddress.setVisibility(View.GONE);
        spinerMedicalMajorMahasiswa.setVisibility(View.GONE);
        btnMedicalMajorMahasiswa.setVisibility(View.GONE);
        spinerPerusahaanFarmasi.setVisibility(View.GONE);
        btnPerusahaanFarmasi.setVisibility(View.GONE);
        spinerUnivMahasiswa.setVisibility(View.GONE);
        residentHospital.setVisibility(View.GONE);
//        btnResidentHospital.setVisibility(View.GONE);
        rumahSakitMhs.setVisibility(View.GONE);
        iconRumahSakitMhs.setVisibility(View.GONE);

        iconUnivResident.setVisibility(View.GONE);
        spinerUnivResident.setVisibility(View.GONE);
        btnUnivResident.setVisibility(View.GONE);


        spinerPilihPosisi.setVisibility(View.GONE);
        btnPilihPosisi.setVisibility(View.GONE);
        iconPilihPosisi.setVisibility(View.GONE);


        etEoOther.setVisibility(View.GONE);
        iconEoOther.setVisibility(View.GONE);

        iconOtherFarmasiName.setVisibility(View.GONE);
        otherFarmasiName.setVisibility(View.GONE);



        etAcountName.setVisibility(View.GONE);
//        etFarmasiPosition.setVisibility(View.GONE);
        farmasiPosition.setVisibility(View.GONE);
        btnFarmasiPosition.setVisibility(View.GONE);
        etBank.setVisibility(View.GONE);
        etCabangBank.setVisibility(View.GONE);
        etOtherName.setVisibility(View.GONE);
        etOtherAdress.setVisibility(View.GONE);
        spinerEoName.setVisibility(View.GONE);
        btnEoName.setVisibility(View.GONE);
        etNoRek.setVisibility(View.GONE);
        etEoAdress.setVisibility(View.GONE);


        iconAcountName.setVisibility(View.GONE);
        iconBank.setVisibility(View.GONE);
        iconCabangBank.setVisibility(View.GONE);
        iconEoName.setVisibility(View.GONE);
        iconFarmasiAddress.setVisibility(View.GONE);
        iconFarmasiPosition.setVisibility(View.GONE);
        iconDaerah.setVisibility(View.GONE);
        iconKecamatan.setVisibility(View.GONE);
        iconSpesificTitleMahasiswa.setVisibility(View.GONE);
        iconNoRek.setVisibility(View.GONE);
        iconEOAddress.setVisibility(View.GONE);
        iconOtherName.setVisibility(View.GONE);
        iconOtherAddress.setVisibility(View.GONE);
        iconPerusahaanFarmasi.setVisibility(View.GONE);
        iconResidentHospital.setVisibility(View.GONE);
        iconSpesificTitle.setVisibility(View.GONE);
        iconTitle.setVisibility(View.GONE);
        iconUnivMahasiswa.setVisibility(View.GONE);
        farmasiPositionOther.setVisibility(View.GONE);
        iconFarmasiPositionOther.setVisibility(View.GONE);

    }


    @Override
    public void onOkClick(String s) {

    }

    @Override
    public void onDialogDismiss() {

    }
}
