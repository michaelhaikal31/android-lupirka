package com.kriyanesia.lupirka.Menu;


import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.ContentUris;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterSpinerKategoriAds;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.ValueCategoriAds;
import com.kriyanesia.lupirka.Data.Ads.ValueResponRegisAds;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.app.Activity.RESULT_OK;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPerpanjangIklan extends Fragment {
    Button btnRegisterAds,btnSelectBannerAds;
    EditText etPrefixPhone,etPhoneNumber,
            etAdsDescription,etAdsSubTitle,etAdsTitle;
    TextView etPublisStartDate,etPublisEndDate;
    ImageView imageBanner;
    AdapterSpinerKategoriAds adapterSpinerKategoriAds;
    List<ValueCategoriAds.Categori> categoriList=new ArrayList<>() ;

    ProgressDialog progressDialog;

    Button spinnerAdsCategori;
    Button btnSpinnerAdsCategori;
    private SimpleDateFormat dateFormatter;
    private DatePickerDialog formStartDate;
    private DatePickerDialog formEndDate;

    String adsTitle, adsSubTitle,startDate,description,bannerPath,cp,urlImage,adsCategori,adsIdCategori;

    public MenuPerpanjangIklan() {
        // Required empty public constructor
    }
    public static MenuPerpanjangIklan newInstance(String adsTitle,String adsSubTitle,String startDate,
                                                  String description,String bannerPath,String cp,String urlImage,
                                                  String adsCategori,String adsIdCategori) {
        Bundle bundle = new Bundle();
        bundle.putString("adsTitle", adsTitle);
        bundle.putString("adsSubTitle", adsSubTitle);
        bundle.putString("startDate", startDate);
        bundle.putString("description", description);
        bundle.putString("cp", cp);
        bundle.putString("bannerPath", bannerPath);
        bundle.putString("urlImage", urlImage);
        bundle.putString("adsCategori", adsCategori);
        bundle.putString("adsIdCategori", adsIdCategori);

        MenuPerpanjangIklan fragment = new MenuPerpanjangIklan();
        fragment.setArguments(bundle);
        return fragment;
    }

    private int RESULT_CODE_BANER_ADS=1212;
    private String noHp="";
    private boolean validateImage=false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_pendaftaran_iklan, container, false);
        adsTitle =getArguments().getString("adsTitle");
        adsSubTitle =getArguments().getString("adsSubTitle");
        startDate =getArguments().getString("startDate");
        cp =getArguments().getString("cp");
        urlImage =getArguments().getString("urlImage");
        description =getArguments().getString("description");
        bannerPath =getArguments().getString("bannerPath");

        adsCategori =getArguments().getString("adsCategori");
        adsIdCategori =getArguments().getString("adsIdCategori");

        btnRegisterAds = (Button) v.findViewById(R.id.btnRegisterAds);
        btnSelectBannerAds = (Button) v.findViewById(R.id.btnSelectBannerAds);
        etAdsTitle = (EditText) v.findViewById(R.id.etAdsTitle);
        etAdsSubTitle = (EditText) v.findViewById(R.id.etAdsSubTitle);
        etAdsDescription = (EditText) v.findViewById(R.id.etAdsDescription);
        etPublisEndDate = (TextView) v.findViewById(R.id.etPublisEndDate);
        etPublisStartDate = (TextView) v.findViewById(R.id.etPublisStartDate);
        etPhoneNumber = (EditText) v.findViewById(R.id.etPhoneNumber);
        etPrefixPhone = (EditText) v.findViewById(R.id.etPrefixPhone);
        imageBanner = (ImageView) v.findViewById(R.id.imageBanner);
        spinnerAdsCategori = (Button) v.findViewById(R.id.spinnerAdsCategori);
        btnSpinnerAdsCategori = (Button) v.findViewById(R.id.btnSpinnerAdsCategori);

        etAdsTitle.setText(adsTitle);
        etAdsSubTitle.setText(adsSubTitle);
        etAdsDescription.setText(description);
        etPublisStartDate.setText(startDate);
        etAdsTitle.setText(adsTitle);
        etPhoneNumber.setText(cp);
        spinnerAdsCategori.setText(adsCategori);
        btnSelectBannerAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANER_ADS);
            }
        });
        Toast.makeText(getContext(), urlImage, Toast.LENGTH_SHORT).show();
        if (urlImage!=null) {
            Picasso.with(getActivity()).load(urlImage).into(imageBanner);
        }
        loadCategori();
        btnRegisterAds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (checkForm()){
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.show();
                    sendRegistrasiAds();
                }else {
                    showAllert("Informasi","lengkapi seluruh form");
                }



            }
        });


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendarStart = Calendar.getInstance();
        formStartDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etPublisStartDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendarStart.get(Calendar.YEAR), newCalendarStart.get(Calendar.MONTH), newCalendarStart.get(Calendar.DAY_OF_MONTH));


        etPublisStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

            }
        });
        etPublisStartDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    formStartDate.show();
                }

            }
        });


        Calendar newCalendarEnd = Calendar.getInstance();
        formEndDate = new DatePickerDialog(getActivity(), new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                int lastBulan=monthOfYear+1;
                if (!etPublisStartDate.getText().toString().isEmpty()){
                    String bulan = etPublisStartDate.getText().toString().substring(5,7);
                    String hari = etPublisStartDate.getText().toString().substring(8);
                    String fixBulan=bulan;
                    String fixHari=hari;
                    if (bulan.startsWith("0")){
                        fixBulan=bulan.substring(1);
                    }
                    if (hari.startsWith("0")){
                        fixHari=hari.substring(1);
                    }
                    Toast.makeText(getContext(), "Bulan "+fixBulan+" banding "+lastBulan+" Hari "+fixHari+" banding "+dayOfMonth, Toast.LENGTH_SHORT).show();

                    if (Integer.valueOf(fixHari)>dayOfMonth){
                        if (Integer.valueOf(fixBulan)<lastBulan){
                            etPublisEndDate.setText(dateFormatter.format(newDate.getTime()));
                        }else {
                            showAllert("Informasi","Tanggal Ahkir harus lebih besar atau sama dengan tanggal mulai");
                        }
                    }else if (Integer.valueOf(fixHari)<=dayOfMonth){
                        if(Integer.valueOf(fixBulan)<=lastBulan){
                            etPublisEndDate.setText(dateFormatter.format(newDate.getTime()));
                        }else {
                            showAllert("Informasi","Tanggal Ahkir harus lebih besar atau sama dengan tanggal mulai");

                        }
                    }
                }else {
                    showAllert("Informasi","Tanggal Mulai Tidak boleh kosong");
                }

            }

        },newCalendarEnd.get(Calendar.YEAR), newCalendarEnd.get(Calendar.MONTH), newCalendarEnd.get(Calendar.DAY_OF_MONTH));

        etPublisEndDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                formEndDate.show();
            }
        });
        etPublisStartDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                hideSoftKeyboard();
                formStartDate.show();
            }
        });



        getActivity().getWindow().setSoftInputMode(WindowManager.
                LayoutParams.SOFT_INPUT_STATE_HIDDEN);


        adsIdCategori= spinnerAdsCategori.getText().toString();
        spinnerAdsCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataCategori();
            }
        });
        btnSpinnerAdsCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataCategori();
            }
        });
        return v;
    }

    public void hideSoftKeyboard() {
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        final View view = getActivity().findViewById(android.R.id.content);
        if (view != null){
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }
    private boolean checkForm(){
        boolean isValid=true;

        if (etAdsTitle.getText().toString().isEmpty()||etAdsSubTitle.getText().toString().isEmpty()
                || etAdsDescription.getText().toString().isEmpty()
                || etPublisStartDate.getText().toString().isEmpty() || etPublisEndDate.getText().toString().isEmpty()
                || etPrefixPhone.getText().toString().isEmpty() || !validateImage){
            isValid=false;
        }
        String phoneNumber =etPhoneNumber.getText().toString();
        if (!phoneNumber.isEmpty()){
            if (phoneNumber.startsWith("8")) {
                noHp = "62" + etPhoneNumber.getText().toString();
            }else if(phoneNumber.startsWith("08")){

                noHp = "62" + etPhoneNumber.getText().toString().substring(1);
            }else {
                isValid = false;
                System.out.println("VALIDASI : phone number");
            }


        }else {
            isValid = false;
            System.out.println("VALIDASI : phone number");

        }


        return isValid;
    }

    private void loadCategori(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        try {
            paramObject.put("id_login",nohp);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueCategoriAds> profesiCall = apiInterface.getCategoriAds(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueCategoriAds>() {
            @Override
            public void onResponse(Call<ValueCategoriAds> call, Response<ValueCategoriAds> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueCategoriAds.Categori> data;
                    data = response.body().getData().getCategory();
//                    for (int i=0; i<data.size();i++){
//                        System.out.println("DATA "+i+" "+data.get(i).getName());
//                    }
                    if (!data.isEmpty()){
                        categoriList.addAll(data);

                    }
//                    List<ValueCategoriAds.Categori> categoriList ;
//                    categori = response.body().getData().getCategory();
//                    adapterSpinerKategoriAds = new AdapterSpinerKategoriAds(getActivity(), categori);
//                    spinnerAdsCategori.setAdapter(adapterSpinerKategoriAds);



                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueCategoriAds> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }
    private void checkDataCategori() {
        if (!categoriList.isEmpty()) {
            final String[] array =new String[categoriList.size()];
            for (int i = 0; i < categoriList.size(); i++) {
                array[i] = categoriList.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Rumah Sakit");
            builder.setItems(array, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinnerAdsCategori.setText(categoriList.get(position).getName());
                    adsIdCategori=categoriList.get(position).getId();

                }
            }).show();
        }
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    public static String getPathFromURI(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[] {
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {

            // Return the remote address
            if (isGooglePhotosUri(uri))
                return uri.getLastPathSegment();

            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(index);
            }
        } finally {
            if (cursor != null)
                cursor.close();
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return "com.google.android.apps.photos.content".equals(uri.getAuthority());
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1212:
                    getFileImage(data);
                    break;

            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    String imageAds;
    Uri uriImage;
    private void getFileImage(Intent data){
        uriImage = data.getData();
        imageAds = uriImage.toString();
        System.out.println("PATH IMAGE "+imageAds);
        int filesize = 0;

        InputStream inputStream = null;
        try {
            inputStream = getActivity().getContentResolver().openInputStream(uriImage);
            filesize = inputStream.available();
        } catch (FileNotFoundException e) {
            Toast.makeText(getActivity(), "ERROR NYA "+e.getMessage(), Toast.LENGTH_LONG);
            return;
        }
        catch (IOException e){
            Toast.makeText(getActivity(), "ERROR NYA "+e.getMessage(), Toast.LENGTH_LONG);
            return;
        }
        finally {
            try {
                inputStream.close();
            } catch (IOException e) {/*blank*/}
        }
        if (filesize!=0){
            int fileInKB= filesize/1024;
            if (fileInKB>500){
                showAllert("Informasi","File gambar ads tidak boleh lebih dari 500KB");
                validateImage=false;
            }else {
                imageBanner.setImageURI(uriImage);
                validateImage=true;
            }
        }




//        File file = new File(getPathFromURI(uriImage));
//        Picasso.with(getActivity()).load(file).into(imageBanner);
//        Toast.makeText(getActivity(), "image "+imageAds, Toast.LENGTH_SHORT).show();
    }
    private void sendRegistrasiAds(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line


                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();


        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE_VER2)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        ApiService apiInterface = retrofit.create(ApiService.class);
        SessionUser sessionUser = new SessionUser();
        String nohp = sessionUser.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("adsCategoryId",adsIdCategori);
            paramObject.put("adsTitle",etAdsTitle.getText().toString());
            paramObject.put("adsSubTitle",etAdsSubTitle.getText().toString());
            paramObject.put("startDate",etPublisStartDate.getText().toString()+" 00:00:00");
            paramObject.put("endDate",etPublisEndDate.getText().toString()+" 23:59:59");
            paramObject.put("note",etAdsDescription.getText().toString());
            paramObject.put("contactPerson","62"+etPhoneNumber.getText().toString());
            paramObject.put("image","imagefile");



        } catch (JSONException e) {
            e.printStackTrace();
        }
        String path="";
        String value =getPathFromURI(getActivity(),uriImage);
        if (value!=null){
            path=value;
        }

        System.out.println("FILE : "+value);
        File sourceFile = new File(path);
        System.out.println("file "+sourceFile.getAbsoluteFile());
        RequestBody reqBody = RequestBody.create(MediaType.parse("multipart/form-file"), sourceFile);
        MultipartBody.Part partImage = MultipartBody.Part.createFormData("files",sourceFile.getName(),reqBody);
        RequestBody req = RequestBody.create(MediaType.parse("application/json; charset=utf-8"),paramObject.toString());
        final MultipartBody.Part body = MultipartBody.Part.createFormData("body", paramObject.toString());
        Call<ValueResponRegisAds> profesiCall = apiInterface.registerAds(partImage,body,"saxasdas");

        profesiCall.enqueue(new Callback<ValueResponRegisAds>() {
            @Override
            public void onResponse(Call<ValueResponRegisAds> call, Response<ValueResponRegisAds> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                    trans.replace(R.id.layoutRegisterAds, new MenuDetailMyAds().newInstance(response.body().getData().getAdsId(),"Pendaftaran"));
                    trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                    trans.addToBackStack("DetailAds");
                    trans.commit();
                }else {
                    showAllert("Respon Server", response.body().getMessage());
                }

                progressDialog.dismiss();

            }

            @Override
            public void onFailure(Call<ValueResponRegisAds> call, Throwable t) {
                showAllert("Respon Server",t.getMessage());

                progressDialog.dismiss();

            }
        });
    }


    AlertDialog.Builder alertDialog;
    private void showAllert(String title, String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

}
