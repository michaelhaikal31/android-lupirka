package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.TranslateAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.calendarlib.OnEventClickListener;
import com.kriyanesia.calendarlib.OnNextMonthClickListener;
import com.kriyanesia.calendarlib.OnPreviousMonthClickListener;
import com.kriyanesia.lupirka.AdapterModel.AdapterEventList;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.AdapterModel.RecyclerViewAdapter;
import com.kriyanesia.lupirka.AdapterModel.SearchKotaAdapter;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.ValueEvent;
import com.kriyanesia.lupirka.Data.Event.ValueEventCategory;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.Data.ValueKota;
import com.kriyanesia.lupirka.Data.ValueTest;
import com.kriyanesia.lupirka.OnLoadMoreListener;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.calendarlib.EventModel;
import com.kriyanesia.calendarlib.GetEventListListener;
import com.kriyanesia.calendarlib.MyDynamicCalendar;
import com.kriyanesia.calendarlib.OnDateClickListener;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.kriyanesia.lupirka.Util.EndlessOnScrollListener;
import com.tuyenmonkey.mkloader.model.Line;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.security.KeyStore;
import java.security.cert.CertificateException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TreeMap;

import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuEvent extends Fragment implements RecyclerViewAdapter.OnItemClickListener{
    private MyDynamicCalendar myEventCalendar;
    //    @BindView(R.id.btnDaftarEvent)
    SwipeRefreshLayout swipeRefreshLayoutList,swipeRefreshLayoutCalendar;

    private EndlessOnScrollListener scrollListener;
    private LinearLayoutManager linearLayoutManager;
    private RecyclerViewAdapter myRecyclerViewAdapter;

    android.app.AlertDialog.Builder alertDialog;
    AdapterEventList adapterEventList;

    RecyclerView recyclerViewEvent;

    List<Event> eventList ;
    AdapterEventList listEventAdapter;
    String dateNow="";
    ImageView btnReloadData;
    SessionUser sessionUser=new SessionUser();
    private int idCategori;
    private int idCity;
    boolean calendarFiltering=false;
    boolean listFilter=false;
    TabLayout tabLayout;
    Button spinnerEventCategori, btnSpinnerEventCategori,spinnerEventCity,btnSpinnerEventCity,actionFilter;
    private String eventCategory;
    private String city;
    private View view;
    FrameLayout eventLayout;
    List<ValueEventCategory.Categori> listCategori=new ArrayList<>();
    List<ValueKota.Kota> listKota=new ArrayList<>();
    String from;
    public static MenuEvent newInstance(String from) {
        Bundle bundle = new Bundle();
        bundle.putString("from", from);

        MenuEvent fragment = new MenuEvent();
        fragment.setArguments(bundle);
        return fragment;
    }
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v = inflater.inflate(R.layout.fragment_menu_event, container, false);

        eventList= new ArrayList<>();
        from =getArguments().getString("from");
        myEventCalendar = (MyDynamicCalendar) v.findViewById(R.id.eventCalendar);
        tabLayout = (TabLayout) v.findViewById(R.id.tabLayout);
        recyclerViewEvent = (RecyclerView) v.findViewById(R.id.recycleEvent);
//        recyclerViewEvent.setLayoutManager(new LinearLayoutManager(getActivity()));
        myEventCalendar.showMonthViewWithBelowEvents();
        btnReloadData= (ImageView) v.findViewById(R.id.reloadData);
        sessionUser=new SessionUser();
        eventLayout = (FrameLayout) v.findViewById(R.id.eventLayout);
        spinnerEventCategori = (Button) v.findViewById(R.id.spinnerEventCategori);
        btnSpinnerEventCategori = (Button) v.findViewById(R.id.btnSpinnerEventCategori);
        spinnerEventCity = (Button) v.findViewById(R.id.spinnerEventCity);
        btnSpinnerEventCity = (Button) v.findViewById(R.id.btnSpinnerEventCity);

        swipeRefreshLayoutList = (SwipeRefreshLayout) v.findViewById(R.id.swiperefreshlayoutEvent);
        swipeRefreshLayoutCalendar = (SwipeRefreshLayout) v.findViewById(R.id.swiperefreshlayoutEventCalendar);

        sessionUser=new SessionUser();

        try {
            idCity=Integer.valueOf(sessionUser.getEventCity(getActivity(),"city"));
            idCategori=Integer.valueOf(sessionUser.getEventCategory(getActivity(),"category"));
        } catch(NumberFormatException nfe) {
            idCity=0;
            idCategori=0;
        }
        loadCategoriEvent();
        loadKotaEvent();

        eventCategory=sessionUser.getEventCategoryName(getActivity(),"categoryName");
        spinnerEventCategori.setText(eventCategory);
        spinnerEventCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCategoriEvent();
            }
        });
        btnSpinnerEventCategori.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCategoriEvent();
            }
        });
        city=sessionUser.getEventCityName(getActivity(),"cityName");
//        Toast.makeText(getActivity(), ""+eventCategory+" "+city, Toast.LENGTH_SHORT).show();
        spinnerEventCity.setText(city);
        spinnerEventCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCityEvent();
            }
        });
        btnSpinnerEventCity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkCityEvent();
            }
        });

        tabLayout.addTab(tabLayout.newTab().setText("List"));
        tabLayout.addTab(tabLayout.newTab().setText("Calendar"));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                switch (tabLayout.getSelectedTabPosition()) {
                    case 0:
                        swipeRefreshLayoutCalendar.setVisibility(View.GONE);
                        btnReloadData.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                refreshEventList();
                            }
                        });
                        sessionUser.setEventTabIndex(getActivity(),"index","0");
                        swipeRefreshLayoutList.setVisibility(View.VISIBLE);
                        recyclerViewEvent.setVisibility(View.VISIBLE);

                        break;
                    case 1:
                        swipeRefreshLayoutList.setVisibility(View.GONE);
                        btnReloadData.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View view) {
                                refreshCalendarEvent();
                            }
                        });
                        sessionUser.setEventTabIndex(getActivity(),"index","1");
                        swipeRefreshLayoutCalendar.setVisibility(View.VISIBLE);
                        recyclerViewEvent.setVisibility(View.GONE);

                        break;

                    default:
                        break;
                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });

        loadFilter();
        swipeRefreshLayoutCalendar.setRefreshing(true);
        swipeRefreshLayoutList.setRefreshing(true);
        recyclerViewEvent.setVisibility(View.VISIBLE);
        swipeRefreshLayoutCalendar.setVisibility(View.GONE);
        swipeRefreshLayoutList.setVisibility(View.VISIBLE);
        recyclerViewEvent.setVisibility(View.VISIBLE);
        swipeRefreshLayoutList.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshEventList();
            }
        });
        swipeRefreshLayoutCalendar.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                refreshCalendarEvent();
            }
        });

        try {
            recyclerViewEvent.setLayoutManager(new LinearLayoutManager(getActivity()));
            listEventAdapter = new AdapterEventList(recyclerViewEvent, eventList, getActivity());
            recyclerViewEvent.setAdapter(listEventAdapter);

        }catch (Error e){

        }
        listEventAdapter.setOnItemClickListener(new AdapterEventList.OnItemClickListener() {
            @Override
            public void onItemClick(String idEvent) {
                FragmentTransaction trans = getFragmentManager()
                        .beginTransaction();
                trans.replace(R.id.eventLayout, new MenuDetailEvent().newInstance(idEvent,"event"));
                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
                trans.addToBackStack(null);
                trans.commit();
            }
        });

//                listEventAdapter.setOnLoadMoreListener(new OnLoadMoreListener() {
//                    @Override
//                    public void onLoadMore() {
//
//                            eventList.add(null);
//                            listEventAdapter.notifyItemInserted(eventList.size() - 1);
////                                new Handler().postDelayed(new Runnable() {
////                                    @Override
////                                    public void run() {
////
////
////                                        System.out.println("REFRESH LIST size "+eventList.size());
////                                    }
////                                }, 500);
//                            if (eventList.size()>0) {
//                                eventList.remove(eventList.size() - 1);
//                                listEventAdapter.notifyItemRemoved(eventList.size());
//                                //Generating more data
//                                int index = eventList.size();
//                                List<Event> event;
//                                if (listFilter) {
//                                    loadEventList(String.valueOf(index), "5", "filter", idCategori, idCity);
//                                    System.out.println("DATA EVENT loadmore filter offset "+index);
//                                } else {
//                                    loadEventList(String.valueOf(index), "5", "", idCategori, idCity);
//                                    System.out.println("DATA EVENT loadmore offset "+index);
//                                }
//
//
//                                listEventAdapter.notifyDataSetChanged();
//                                listEventAdapter.setLoaded();
//                            }
//                    }
//
//
//                });



        myEventCalendar.setOnDateClickListener(new OnDateClickListener() {
            @Override
            public void onClick(Date date) {
                Log.e("date", String.valueOf(date));
//                FragmentTransaction trans = getFragmentManager()
//                        .beginTransaction();
//                trans.replace(R.id.eventLayout, new MenuDetailEvent());
//                trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
//                trans.addToBackStack(null);
//                trans.commit();
//                Toast.makeText(getActivity(), "scroll untuk melihat event", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onLongClick(Date date) {
                Log.e("date", String.valueOf(date));
            }
        });
        myEventCalendar.setOnEventClickListener(new OnEventClickListener() {
            @Override
            public void onClick(String id) {

                openDetailEvent(id);
            }

            @Override
            public void onLongClick(String id) {

            }
        });


        myEventCalendar.setCalendarBackgroundColor("#eeeeee");
//        myEventCalendar.setCalendarBackgroundColor(R.color.gray);

        myEventCalendar.setHeaderBackgroundColor("#8f0805");
//        myEventCalendar.setHeaderBackgroundColor(R.color.black);

        myEventCalendar.setHeaderTextColor("#ffffff");
//        myEventCalendar.setHeaderTextColor(R.color.white);

        myEventCalendar.setNextPreviousIndicatorColor("#ffffff");
//        myEventCalendar.setNextPreviousIndicatorColor(R.color.black);

        myEventCalendar.setWeekDayLayoutBackgroundColor("#8f0805");
//        myEventCalendar.setWeekDayLayoutBackgroundColor(R.color.black);

        myEventCalendar.setWeekDayLayoutTextColor("#ffffff");
//        myEventCalendar.setWeekDayLayoutTextColor(R.color.black);

//        myEventCalendar.isSaturdayOff(true, "#ffffff", "#ff0000");
//        myEventCalendar.isSaturdayOff(true, R.color.white, R.color.red);

        myEventCalendar.isSundayOff(true, "#ffffff", "#ff0000");
        myEventCalendar.isSundayOff(true, R.color.red, R.color.white);

        myEventCalendar.setExtraDatesOfMonthBackgroundColor("#848586");
//        myEventCalendar.setExtraDatesOfMonthBackgroundColor(R.color.black);

        myEventCalendar.setExtraDatesOfMonthTextColor("#756325");
//        myEventCalendar.setExtraDatesOfMonthTextColor(R.color.black);

//        myEventCalendar.setDatesOfMonthBackgroundColor(R.drawable.event_view);
//        myEventCalendar.setDatesOfMonthBackgroundColor("#ffffff");
        myEventCalendar.setDatesOfMonthBackgroundColor(R.color.white);

        myEventCalendar.setDatesOfMonthTextColor("#000000");
//        myEventCalendar.setDatesOfMonthTextColor(R.color.black);

        myEventCalendar.setCurrentDateBackgroundColor("#fad2d1");
//        myEventCalendar.setCurrentDateBackgroundColor(R.color.black);

        myEventCalendar.setCurrentDateTextColor("#00e600");
//        myEventCalendar.setCurrentDateTextColor(R.color.black);

        myEventCalendar.setEventCellBackgroundColor("#ff0000");

//        myEventCalendar.setEventCellBackgroundColor(R.color.black);


//        myEventCalendar.setEventCellTextColor(R.color.black);

        Date currentTime = Calendar.getInstance().getTime();
        CharSequence now  = DateFormat.format("yyyy-MM", currentTime);
        btnReloadData.setVisibility(View.GONE);

        btnReloadData.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                refreshCalendarEvent();
                refreshEventList();
            }
        });


        dateNow=String.valueOf(now);
//            loadEventMonth(dateNow,idCategori,idCity);



        myEventCalendar.getEventList(new GetEventListListener() {
            @Override
            public void eventList(ArrayList<EventModel> eventList) {
                Log.e("tag", "eventList.size():-" + eventList.size());

                if (eventList.size()>0){

                }
                for (int i = 0; i < eventList.size(); i++) {
                    Log.e("tag", "eventList.getStrName:-" + eventList.get(i).getStrName());
                }
            }
        });
        myEventCalendar.setOnNextMonthClickListener(new OnNextMonthClickListener() {
            @Override
            public void onClick(String date) {
                loadEventMonth(date,idCategori,idCity);
                dateNow=date;

            }
        });
        myEventCalendar.setOnPreviousMonthClickListener(new OnPreviousMonthClickListener() {
            @Override
            public void onClick(String date) {
                loadEventMonth(date,idCategori,idCity);
                dateNow=date;
            }
        });

//        myEventCalendar.updateEvent(0, "5-10-2016", "8:00", "8:15", "Today Event 111111");

//        myEventCalendar.deleteEvent(2);

//        myEventCalendar.deleteAllEvent();

        myEventCalendar.setBelowMonthEventTextColor("#425684");
//        myEventCalendar.setBelowMonthEventTextColor(R.color.black);

        myEventCalendar.setBelowMonthEventDividerColor("#635478");
//        myEventCalendar.setBelowMonthEventDividerColor(R.color.black);
        myEventCalendar.setDatesOfMonthBackgroundColor("#ffffff");

        myEventCalendar.setHolidayCellBackgroundColor("#ffffff");
//        myEventCalendar.setHolidayCellBackgroundColor(R.color.black);

        myEventCalendar.setHolidayCellTextColor("#d71d19");
//        myEventCalendar.setHolidayCellTextColor(R.color.black);

        myEventCalendar.setHolidayCellClickable(true);

        myEventCalendar.addHoliday("2-05-2018");
        myEventCalendar.addHoliday("8-05-2018");
//        loadHTTPS();
        return v;
    }
    private void loadCategoriEvent(){
        listCategori.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("nama","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueEventCategory> data = apiInterface.getCategoriEvent(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueEventCategory>() {
            @Override
            public void onResponse(Call<ValueEventCategory> call, Response<ValueEventCategory> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueEventCategory.Categori> data;
                    data = response.body().getData().getCategoryevent();

                    if (!data.isEmpty()){
                        listCategori.addAll(data);

                    }



                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueEventCategory> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }
    private void loadKotaEvent(){
        listKota.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("x","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueKota> data = apiInterface.getKotaSearch(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueKota>() {
            @Override
            public void onResponse(Call<ValueKota> call, Response<ValueKota> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<ValueKota.Kota> data;
                    data = response.body().getData().getList();
                    ValueKota.Kota row1;
                    if (!data.isEmpty()){
                        listKota.addAll(data);

                    }



                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueKota> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

    AlertDialog dialog;

    int idCategoriUpdate=999;

    private void checkCategoriEvent() {
        if (!listCategori.isEmpty()) {
            final String[] arrData =new String[listCategori.size()+1];
            arrData[0]="Semua Kategori";
            for (int i = 0; i < listCategori.size(); i++) {
                int index=i+1;
                arrData[index] = listCategori.get(i).getName();
//                System.out.println("DATA KATEGORI ke-"+index+" "+listCategori.get(i).getId()+" "+listCategori.get(i).getName());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Kategori");
            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    idCategoriUpdate=999;
                    int p=0;
                    if (position!=0){
                        p=position-1;
                        idCategoriUpdate=Integer.valueOf(listCategori.get(p).getId());
                        eventCategory=listCategori.get(p).getName();
                        spinnerEventCategori.setText(eventCategory);
                    }else {
                        eventCategory="Kategori";
                        spinnerEventCategori.setText(eventCategory);
                    }
                    filter();

                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }else {
            Toast.makeText(getActivity(), "data tidak ditemukan", Toast.LENGTH_SHORT).show();
            loadCategoriEvent();
        }
    }

    AlertDialog dialogCity;
    int idCityUpdate=999;
    private void checkCityEvent() {
        if (!listKota.isEmpty()) {
            final String[] arrData =new String[listKota.size()+1];
            arrData[0]="Semua Kota";
            for (int i = 0; i < listKota.size(); i++) {
                int index=i+1;
                arrData[index] = listKota.get(i).getKota();
//                System.out.println("DATA KATEGORI ke-"+index+" "+listCategori.get(i).getId()+" "+listCategori.get(i).getName());
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Kota");

            builder.setItems(arrData, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {
                    idCityUpdate=999;
                    int p=0;
                    if (position!=0){
                        p=position-1;
                        idCityUpdate=Integer.valueOf(listKota.get(p).getId());
                        city=listKota.get(p).getKota();
                        spinnerEventCity.setText(city);
                    }else {
                        city="Kota";
                        spinnerEventCity.setText(city);
                    }
                    filter();
                }
            });
            // Create the alert dialog
            AlertDialog dialog = builder.create();
            // Get the alert dialog ListView instance
            ListView listView = dialog.getListView();
            // Set the divider color of alert dialog list view
            listView.setDivider(new ColorDrawable(Color.GRAY));
            // Set the divider height of alert dialog list view
            listView.setDividerHeight(1);
            // Finally, display the alert dialog
            dialog.show();
        }else {
            Toast.makeText(getActivity(), "data tidak ditemukan", Toast.LENGTH_SHORT).show();
            loadCategoriEvent();
        }
    }





    @Override
    public void onResume() {
        super.onResume();
        String index = sessionUser.getEventTabIndex(getActivity(),"index");
        TabLayout.Tab selectedTab = tabLayout.getTabAt(Integer.valueOf(index));
        selectedTab.select();
        eventList.clear();
        loadFilter();
        if (listFilter){
            loadEventList("0","1000","filter",idCategori,idCity);
        }else {
            loadEventList("0","1000","",idCategori,idCity);
        }
//        myEventCalendar.deleteAllEvent();
        loadEventMonth(dateNow,idCategori,idCity);

    }

    private void loadFilter(){
        sessionUser=new SessionUser();
        String id=sessionUser.getEventCategory(getActivity(),"category");
        if (id.equalsIgnoreCase("")){
            idCategori=0;
        }else {
            idCategori=Integer.valueOf(id);
        }
        String city=sessionUser.getEventCity(getActivity(),"city");
//        Toast.makeText(getActivity(), ""+city+"category "+idCategori, Toast.LENGTH_SHORT).show();
        if (city.equalsIgnoreCase("")){
            idCity=0;
        }else {
            idCity=Integer.valueOf(city);
        }
        if (idCity!=0 || idCategori !=0){
            listFilter=true;
        }else {
            listFilter=false;
        }
        if (idCity!=0 || idCategori !=0){
            calendarFiltering=true;
        }else {
            calendarFiltering=false;
        }
    }


     private void refreshEventList(){
         swipeRefreshLayoutList.setRefreshing(true);
         eventList.clear();
         if (listFilter){
             loadEventList("0","1000","filter",idCategori,idCity);
         }else {
             loadEventList("0","1000","",idCategori,idCity);
         }



     }

     private void refreshCalendarEvent(){
         loadEventMonth(dateNow,idCategori,idCity);
         swipeRefreshLayoutCalendar.setRefreshing(false);

     }
    private void openDetailEvent(String idEvent){
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.eventLayout, new MenuDetailEvent().newInstance(idEvent,"Event"));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack(null);
        trans.commit();
    }
    private void loadEventList(String offset,String limit, String tipe , int kategori,int city){
//        System.out.println("DATA EVENT NYA "+kategori+" "+city +" "+ start+" "+end);

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("offset",offset);
            paramObject.put("limit",limit);
            if (from.equalsIgnoreCase("x")){
                if (tipe.equalsIgnoreCase("filter")){
                    paramObject.put("tipe",tipe);
                    paramObject.put("kategori",kategori);
                    paramObject.put("city",city);
                }else {
                    paramObject.put("tipe","");
                }
            }else{
                paramObject.put("tipe",from);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("Value event :"+paramObject.toString());
        Call<ValueEventList> profesiCall = apiInterface.getEventList(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueEventList>() {
            @Override
            public void onResponse(Call<ValueEventList> call, Response<ValueEventList> response) {



                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Event> list;
                    list = response.body().getData().getList_event();

                    for (int i =0; i<list.size();i++){
                        eventList.add(list.get(i));
                    }
                    if (list.size()<1){
//                        Toast.makeText(getActivity(), "tidak ada data lagi", Toast.LENGTH_SHORT).show();
                    }

                }

//                btnReloadList.setVisibility(View.GONE);
                swipeRefreshLayoutList.setRefreshing(false);

            }

            @Override
            public void onFailure(Call<ValueEventList> call, Throwable t) {
                swipeRefreshLayoutList.setRefreshing(false);

//                btnReloadList.setVisibility(View.VISIBLE);
            }
        });

    }

    @Override
    public void onStart() {
        super.onStart();
    }

    private void loadEventMonth(String month,int idCategori,int idCity){
        swipeRefreshLayoutCalendar.setRefreshing(true);
        myEventCalendar.setVisibility(View.VISIBLE);
        myEventCalendar.deleteAllEvent();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();

//        Toast.makeText(getActivity(), ""+calendarFiltering+" list "+listFilter, Toast.LENGTH_SHORT).show();
        try {
            paramObject.put("date",month);
            if (calendarFiltering){
                paramObject.put("city",idCity);
                paramObject.put("kategori",idCategori);
           }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("json :"+paramObject);

        Call<ValueEvent> profesiCall = apiInterface.getEventCalendar(paramObject.toString(),"saxasdas");

        profesiCall.enqueue(new Callback<ValueEvent>() {
            @Override
            public void onResponse(Call<ValueEvent> call, Response<ValueEvent> response) {
                swipeRefreshLayoutCalendar.setRefreshing(false);

                if (response.body().getStatus().equalsIgnoreCase("success")){
                    myEventCalendar.showMonthViewWithBelowEvents();
                    List<Event> event= new ArrayList<>();
                    event = response.body().getData().getEvents();


                    for (int i=0; i<event.size();i++) {
                        String startDate = event.get(i).getStart();
                        String endDate = event.get(i).getEnd();
                        String bulan = startDate.substring(5,7);
                        String tahun = startDate.substring(0,4);

                        String startDates = startDate.substring(8,10);
                        String endDates = endDate.substring(8,10);
                        String startJam = startDate.substring(0,11);
                        String endJam = endDate.substring(0,11);
                        String fixStart = startDates;
                        if (startDates.startsWith("0")){
                            fixStart=startDates.substring(1);
                        }


                        int start = Integer.valueOf(fixStart);
                            myEventCalendar.addEvent(event.get(i).getId(),start+"-"+bulan+"-"+tahun, startJam, endJam, event.get(i).getTitle());
//                        }
                        }


                    //myEventCalendar.addEvent("1",10+"-"+"12"+"-"+"2018","12", "14","coba tanggal");
                   // myEventCalendar.addEvent("1",20+"-"+"12"+"-"+"2018","12", "14","coba tanggal");
                    //myEventCalendar.addEvent("1",14+"-"+"12"+"-"+"2018","12", "14","coba tanggal");
                    myEventCalendar.setEventCellBackgroundColor("#ff0000");
                    myEventCalendar.setEventCellTextColor("#ffffff");
                    myEventCalendar.showMonthViewWithBelowEvents();
                    myEventCalendar.setHolidayCellTextColor("#d71d19");

                    swipeRefreshLayoutCalendar.setRefreshing(false);
//                    btnReloadClendar.setVisibility(View.GONE);

                }

            }

            @Override
            public void onFailure(Call<ValueEvent> call, Throwable t) {
                swipeRefreshLayoutCalendar.setRefreshing(false);
//                btnReloadClendar.setVisibility(View.VISIBLE);
                myEventCalendar.setVisibility(View.GONE);


            }
        });
    }

    private void showAllert(String title, String message){
        alertDialog = new android.app.AlertDialog.Builder(getActivity());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });

        alertDialog.show();

    }


    @Override
    public void onItemClick(RecyclerViewAdapter.ItemHolder item, int position) {
        Toast.makeText(getActivity(), ""+position, Toast.LENGTH_SHORT).show();
    }



    public static OkHttpClient getUnsafeOkHttpClient() {

        try {
            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[] { new X509TrustManager() {
                @Override
                public void checkClientTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public void checkServerTrusted(
                        java.security.cert.X509Certificate[] chain,
                        String authType) throws CertificateException {
                }

                @Override
                public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                    return new java.security.cert.X509Certificate[0];
                }
            } };

            // Install the all-trusting trust manager
//            final SSLContext sslContext = SSLContext.getInstance("TLS");
//            sslContext.init(null, trustAllCerts,
//                    new java.security.SecureRandom());
            TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
            trustManagerFactory.init((KeyStore) null);
            TrustManager[] trustManagers = trustManagerFactory.getTrustManagers();
            if (trustManagers.length != 1 || !(trustManagers[0] instanceof X509TrustManager)) {
                throw new IllegalStateException("Unexpected default trust managers:" + Arrays.toString(trustManagers));
            }
            X509TrustManager trustManager = (X509TrustManager) trustManagers[0];
            SSLContext sslContext = SSLContext.getInstance("SSL");
            sslContext.init(null, new TrustManager[] { trustManager }, null);



            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext
                    .getSocketFactory();

            OkHttpClient okHttpClient = new OkHttpClient();
            okHttpClient = okHttpClient.newBuilder()
                    .sslSocketFactory(sslSocketFactory,trustManager)
                    .hostnameVerifier(org.apache.http.conn.ssl.SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER).build();
//            okHttpClient = okHttpClient.newBuilder().sslSocketFactory()
            return okHttpClient;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }

    }

    private void loadHTTPS(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("https://services.aksesciptasolusi.com/kisspay/ws/")
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(getUnsafeOkHttpClient())
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role","x");
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueTest> data = apiInterface.testHTTPS(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueTest>() {
            @Override
            public void onResponse(Call<ValueTest> call, Response<ValueTest> response) {
                Toast.makeText(getActivity(), ""+response.body().getRc(), Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onFailure(Call<ValueTest> call, Throwable t) {



            }
        });
    }


    public void slideDown(View view){
        TranslateAnimation animate = new TranslateAnimation(
                0,                 // fromXDelta
                0,                 // toXDelta
                0,                 // fromYDelta
                view.getHeight()); // toYDelta
        animate.setDuration(500);
        animate.setFillAfter(true);
        view.startAnimation(animate);
    }


    private void filter(){
        eventList.clear();
        eventList= new ArrayList<>();
        sessionUser= new SessionUser();
        sessionUser.setEventCategoryName(getActivity(),"categoryName",eventCategory);
        sessionUser.setEventCityName(getActivity(),"cityName",city);
        if (idCategoriUpdate==999){
            sessionUser.setEventCategory(getActivity(),"category","");
        }else {
            sessionUser.setEventCategory(getActivity(),"category",String.valueOf(idCategoriUpdate));
        }

        if (idCityUpdate==999){
            sessionUser.setEventCity(getActivity(), "city", "");
        }else {
            sessionUser.setEventCity(getActivity(), "city", String.valueOf(idCityUpdate));
        }
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.detach(MenuEvent.this).attach(MenuEvent.this).commit();
    }

}
