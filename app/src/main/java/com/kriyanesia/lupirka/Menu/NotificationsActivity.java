package com.kriyanesia.lupirka.Menu;

import android.content.DialogInterface;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListNotif;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.Data.SoalTest.AllSoal;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionNotif;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.SplashActivity;

import java.util.List;

public class NotificationsActivity extends AppCompatActivity {
    private List<SessionNotif> listNotif;
    private AdapterListNotif adapterListNotif;
    private DatabaseHelper databaseHelper;
    private ListView listView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        listView = findViewById(R.id.listviewNotif);
        databaseHelper = new DatabaseHelper(NotificationsActivity.this);
        listNotif = databaseHelper.getSessionNotif();
        adapterListNotif = new AdapterListNotif(listNotif, NotificationsActivity.this);
        adapterListNotif.notifyDataSetChanged();
        listView.setAdapter(adapterListNotif);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                detail_notif(listNotif.get(i).getTitle(),listNotif.get(i).getMessage());
            }
        });


    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_notifications, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.clear_notif) {
            databaseHelper.deleteAllNotif();
            listNotif.clear();
            Toast.makeText(this,"Notifikasi berhasil dibersihkan ", Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void detail_notif(String title,String message){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setMessage(message);
        builder.setPositiveButton("Close", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });
        builder.create().show();
    }

    @Override
    public void onBackPressed() {
        if (isTaskRoot()) {
            Intent intent = new Intent(this, SplashActivity.class);
            startActivity(intent);
            super.onBackPressed();
        }else {
            super.onBackPressed();
        }

    }
}
