package com.kriyanesia.lupirka.Menu;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.OpenableColumns;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TableRow;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterBannerFile;
import com.kriyanesia.lupirka.AdapterModel.AdapterTableEventReg;
import com.kriyanesia.lupirka.Data.TableRegistrasiEvent.Event;
import com.kriyanesia.lupirka.Data.TableRegistrasiEvent.WaktuEvent;
import com.kriyanesia.lupirka.R;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import butterknife.BindView;
import butterknife.ButterKnife;

public class MenuRegisterEvent extends AppCompatActivity implements View.OnClickListener {
    @BindView(R.id.etTitleName)
    EditText etEventTitle;
    @BindView(R.id.etSubTitle)
    EditText etSubTitle;
    @BindView(R.id.etEventEndDate)
    EditText etEventEndDate;
    @BindView(R.id.etEventStartDate)
    EditText etEventStartDate;
    @BindView(R.id.etEventVenue)
    EditText etEventVenue;
    @BindView(R.id.etEventNote)
    EditText etEventNote;
    @BindView(R.id.etPrefixTelp)
    EditText etPrefixTelp;
    @BindView(R.id.etTelpNumber)
    EditText etTelpNumber;
    @BindView(R.id.etNamaKegiatan)
    EditText etNamaKegiatan;


    @BindView(R.id.rgPaket)
    RadioGroup rgPaket;
    @BindView(R.id.rbReguler)
    RadioButton rbReguler;
    @BindView(R.id.rbNone)
    RadioButton rbNone;
    @BindView(R.id.rbPaket)
    RadioButton rbPaket;

    @BindView(R.id.rbOther)
    RadioButton rbOther;
    @BindView(R.id.etOther)
    EditText rgEtOther;


    @BindView(R.id.spinerEventCity)
    Spinner spinerEventCity;
    @BindView(R.id.spinerEvenCategori)
    Spinner spinerEvenCategori;
    @BindView(R.id.spinerPeserta)
    Spinner spinerPeserta;
//    @BindView(R.id.spinerWaktu)
//    Spinner spinerWaktu;

    @BindView(R.id.btnSelectBanner)
    Button btnSelectBanner;
    @BindView(R.id.btnSelectFile)
    Button btnSelectFile;
    @BindView(R.id.btnSelectFileSertifikat)
    Button btnSelectFileSertifikat;
    @BindView(R.id.btnAddListEvent)
    Button btnAddListEvent;
    @BindView(R.id.btnAddWaktu)
    Button btnAddWaktu;

    @BindView(R.id.btnNextRegister)
    Button btnNextRegister;



    @BindView(R.id.imageSertifikat)
    ImageView imageSertifikat;

    AdapterTableEventReg adapterTableEventReg;

    @BindView(R.id.recycleTableEvent)
    RecyclerView recycleTableEvent;
    LinearLayoutManager linearLayoutManager;


    TableRow tableRowEvent;

    private int hint=0;
    private LinearLayout layoutHarga;
    AutoCompleteTextView txtTipe;
    EditText ethargaReg;

    
    ArrayAdapter<String> adapter;

        AdapterBannerFile adapterBannerFile;
    private int RESULT_CODE_SERTIFIKAT=1214;
    private int RESULT_CODE_BANNER=1213;
    private int RESULT_CODE_FILE=1212;

    private SimpleDateFormat dateFormatter;
    private DatePickerDialog formStartDate;
    private DatePickerDialog formEndDate;

    ArrayList<Event> arrayEvent;
    ArrayList<WaktuEvent> list;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu_register_event);
        ButterKnife.setDebug(true);
        ButterKnife.bind(MenuRegisterEvent.this);


        arrayEvent = new ArrayList<>();
        list = new ArrayList<>();
        WaktuEvent waktu = new WaktuEvent();
        waktu.setName("Early Bid");
        waktu.setHarga("Harga");
        list.add(waktu);
        Event event = new Event();
        event.setKeterangan("Keterangan");
        event.setNamaEvent("Kegiatan");
        event.setWaktuEvent(list);
        arrayEvent.add(event);




        LinearLayoutManager manager = new LinearLayoutManager(MenuRegisterEvent.this);
        manager.setOrientation(LinearLayoutManager.VERTICAL);
        recycleTableEvent.setLayoutManager(manager);
        recycleTableEvent.setHasFixedSize(true);

        adapterTableEventReg = new AdapterTableEventReg(MenuRegisterEvent.this, arrayEvent);
        recycleTableEvent.setAdapter(adapterTableEventReg);


        dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar newCalendarStart = Calendar.getInstance();
        formStartDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etEventStartDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendarStart.get(Calendar.YEAR), newCalendarStart.get(Calendar.MONTH), newCalendarStart.get(Calendar.DAY_OF_MONTH));


        etEventStartDate.setOnClickListener(this);
        etEventStartDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    formStartDate.show();
                }

            }
        });


        Calendar newCalendarEnd = Calendar.getInstance();
        formEndDate = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                etEventEndDate.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendarEnd.get(Calendar.YEAR), newCalendarEnd.get(Calendar.MONTH), newCalendarEnd.get(Calendar.DAY_OF_MONTH));

        etEventEndDate.setOnClickListener(this);
        etEventEndDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if(hasFocus) {
                    formEndDate.show();
                }

            }
        });


        rgEtOther.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus){
                    rgPaket.check(R.id.rbOther);
                }

            }

        });

        rgPaket.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int idx = rgPaket.getCheckedRadioButtonId();
                View radioButton = rgPaket.findViewById(idx);
                int indexRgPaket = rgPaket.indexOfChild(radioButton);
                Toast.makeText(MenuRegisterEvent.this, ""+indexRgPaket, Toast.LENGTH_SHORT).show();
                if (indexRgPaket==3){
                    rgEtOther.requestFocus();
                }else {
                    if (rgEtOther.isFocused()){
                        rgEtOther.clearFocus();
                    }
                }

            }
        });


        layoutHarga = (LinearLayout)findViewById(R.id.layoutWaktu);

        addViews("initial");

        btnAddWaktu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addViews("adding");


            }

        });
        btnAddListEvent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int childCount = layoutHarga.getChildCount();
                boolean validate;
                WaktuEvent wktu = new WaktuEvent();
                for (int i = 0; i < childCount; i++) {
                    View thisChild = layoutHarga.getChildAt(i);

                    AutoCompleteTextView childTipe = (AutoCompleteTextView) thisChild.findViewById(R.id.textTipeWaktu);
                    EditText childHarga = (EditText) thisChild.findViewById(R.id.ethargaRegistrasi);

                    String valueTipe = childTipe.getText().toString();
                    String valueHarga = childHarga.getText().toString();

                    if (valueTipe.isEmpty()){
                        validate=false;
                    }
                    if (valueHarga.isEmpty()){
                        validate=false;

                    }
                    if (!valueHarga.isEmpty() && !valueTipe.isEmpty()){


                        wktu.setName(valueTipe);
                        wktu.setHarga(valueHarga);
                        list.add(wktu);

                    }

                }

                Event event = new Event();
                event.setKeterangan("Dokter spesialis");
                event.setNamaEvent(etNamaKegiatan.getText().toString()+"");
                event.setWaktuEvent(list);
                arrayEvent.add(event);
//                adapterTableEventReg.updateList(arrayEvent);
                adapterTableEventReg.notifyDataSetChanged();




            }
        });

        btnSelectFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.setType("application/pdf");
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(intent, RESULT_CODE_FILE);

            }
        });
        btnSelectBanner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                intent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_BANNER);
            }
        });
        btnSelectFileSertifikat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setType("image/*");
                intent.setAction(Intent.ACTION_GET_CONTENT);
                startActivityForResult(Intent.createChooser(intent, "Select Picture"), RESULT_CODE_SERTIFIKAT);
            }
        });
        btnNextRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(MenuRegisterEvent.this,MenuRegisterEventPreview.class);
                i.putExtra("title",etEventTitle.getText().toString());
                i.putExtra("subtitle",etSubTitle.getText().toString());
                i.putExtra("venue",etEventVenue.getText().toString());
                i.putExtra("city","Bandung");
                i.putExtra("startdate",etEventStartDate.getText().toString());
                i.putExtra("enddate",etEventEndDate.getText().toString());
                i.putExtra("category","Pembelajaran");
                i.putExtra("note",etEventNote.getText().toString());
                i.putExtra("cp",etPrefixTelp.getText().toString()+etTelpNumber.getText().toString());
                i.putExtra("banner",imageBanerPath);
                i.putExtra("file",pathPdf);
                i.putExtra("sertifikat",imageSertifikatPath);
                startActivity(i);
            }
        });
    }
    private ArrayList<Event> createData() {
        arrayEvent = new ArrayList<>();

        ArrayList<WaktuEvent> list1 = new ArrayList<>();
        ArrayList<WaktuEvent> list2 = new ArrayList<>();
        ArrayList<WaktuEvent> list3 = new ArrayList<>();
        ArrayList<WaktuEvent> list4 = new ArrayList<>();
        ArrayList<WaktuEvent> list5 = new ArrayList<>();


        for (int i = 0; i < 3; i++) {
            WaktuEvent c1 = new WaktuEvent();
            c1.setName("Kegiatan");
            c1.setHarga("Harga "+i);
            list1.add(c1);
        }
        for (int i = 0; i < 3; i++) {
            WaktuEvent c1 = new WaktuEvent();
            c1.setName("Child 1." + (i + 1));
            c1.setHarga("22000");
            list2.add(c1);
        }
        for (int i = 0; i < 3; i++) {
            WaktuEvent c1 = new WaktuEvent();
            c1.setName("Child 1." + (i + 1));
            c1.setHarga("10000");
            list3.add(c1);
        }
        for (int i = 0; i < 3; i++) {
            WaktuEvent c1 = new WaktuEvent();
            c1.setName("Child 1." + (i + 1));
            c1.setHarga("40000");
            list4.add(c1);
        }
        for (int i = 0; i < 3; i++) {
            WaktuEvent c1 = new WaktuEvent();
            c1.setName("Child 1." + (i + 1));
            c1.setHarga("5000");
            list5.add(c1);
        }



        Event event = new Event();
        event.setNamaEvent("Kegiatan");
        event.setKeterangan("keterangan");
        event.setWaktuEvent(list1);
        arrayEvent.add(event);

        Event event2 = new Event();
        event2.setNamaEvent("Event 2");
        event2.setKeterangan("keterangan");
        event2.setWaktuEvent(list2);
        arrayEvent.add(event2);

        Event event3 = new Event();
        event3.setNamaEvent("Event 3");
        event3.setKeterangan("keterangan");
        event3.setWaktuEvent(list3);
        arrayEvent.add(event3);

        Event event4 = new Event();
        event4.setNamaEvent("Event 4");
        event4.setKeterangan("keterangan");
        event4.setWaktuEvent(list4);
        arrayEvent.add(event4);

        Event event5 = new Event();
        event5.setNamaEvent("Event 5");
        event5.setKeterangan("keterangan");
        event5.setWaktuEvent(list5);
        arrayEvent.add(event5);




        return arrayEvent;
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            switch (requestCode) {
                case 1212:
                        getFilePDF(data);
                    break;
                case 1213:
                    getFileImage(data,"banner");
                    break;
                case 1214:
                    getFileImage(data,"sertifikat");
                    break;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }



    private void addViews(String type){
        LayoutInflater layoutInflater =
                (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        final View addView = layoutInflater.inflate(R.layout.row_waktu_daftar_event, null);
        txtTipe = (AutoCompleteTextView)addView.findViewById(R.id.textTipeWaktu);
        ethargaReg = (EditText) addView.findViewById(R.id.ethargaRegistrasi);
        txtTipe.setAdapter(adapter);

        txtTipe.setHint("tipe waktu ");
        ethargaReg.setHint("Harga Registrasi");
        Button buttonRemove = (Button)addView.findViewById(R.id.remove);
        if (type.equalsIgnoreCase("initial")){
            buttonRemove.setEnabled(false);
            buttonRemove.setBackgroundTintList(addView.getResources().getColorStateList(R.color.grey));
        }

        final View.OnClickListener thisListener = new View.OnClickListener(){
            @Override
            public void onClick(View v) {
                ((LinearLayout)addView.getParent()).removeView(addView);
                Toast.makeText(MenuRegisterEvent.this, txtTipe.getText().toString(), Toast.LENGTH_SHORT).show();

            }
        };

        buttonRemove.setOnClickListener(thisListener);
        layoutHarga.addView(addView);


    }
     @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSelectFile :
//                setFilePdf();
                Toast.makeText(this, "tes", Toast.LENGTH_SHORT).show();


                break;

        }
    }
    String [] imageBanerPath;
    String [] imageSertifikatPath;
    private void getFileImage(Intent data,String tipe){
        if (tipe.equalsIgnoreCase("banner")){

            if(null != data.getClipData()) { // checking multiple selection or not
                int length=0;
                if (data.getClipData().getItemCount()>4){
                    length=4;
                    Toast.makeText(this, "maximum 4 file", Toast.LENGTH_SHORT).show();
                }else {
                    length=data.getClipData().getItemCount();
                }
                Uri [] imageUri = new Uri[length];
                imageBanerPath = new String [length];
                Toast.makeText(this, "jumlah "+length, Toast.LENGTH_SHORT).show();
                for(int i = 0; i < length; i++) {
                    imageUri[i] = data.getClipData().getItemAt(i).getUri();
                    imageBanerPath[i] = imageUri[i].toString();
                }
                setFileImage(imageUri);
            }else {
                Uri []imageUri = new Uri[1];
                imageUri [0] = data.getData();

                imageBanerPath[0] = imageUri.toString();
                setFileImage(imageUri);
            }
        }else {
            imageSertifikatPath = new String [1];
            Uri image = data.getData();
            imageSertifikatPath[0] = image.toString();
            imageSertifikat.setImageURI(image);


        }


    }
    String[] pathPdf;
    private void getFilePDF(Intent data){

        if(null != data.getClipData()) { // checking multiple selection or not

            System.out.println("PILIHAN multi");
            int length=0;
            if (data.getClipData().getItemCount()>4){
                length=4;
                Toast.makeText(this, "maximum 4 file", Toast.LENGTH_SHORT).show();
            }else {
                length=data.getClipData().getItemCount();
            }
            String[] displayName= new String[length] ;
            pathPdf=new String[length];

            for(int i = 0; i < length; i++) {
                Uri uri = data.getClipData().getItemAt(i).getUri();
                String uriString = uri.toString();
                File myFile = new File(uriString);

                pathPdf[i] = myFile.getAbsolutePath();
                if (uriString.startsWith("content://")) {
                    pathPdf[i]=uriString;
//                    Toast.makeText(this, uriString, Toast.LENGTH_SHORT).show();
                    Cursor cursor = null;

                    try {
                        cursor = getContentResolver().query(uri, null, null, null, null);
                        if (cursor != null && cursor.moveToFirst()) {
                            displayName[i] = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                        }
                    } finally {
                        cursor.close();
                    }
                } else if (uriString.startsWith("file://")) {
                    displayName[i] = myFile.getName();
                }
                if (displayName!=null){

                }
                if (i==3){

                }
            }
            setFilePdf(displayName);
        } else {
            String[] displayName= {""} ;
            System.out.println("PILIHAN "+1);
            Uri uri = data.getData();
            String uriString = uri.toString();
            File myFile = new File(uriString);
//            pathPdf[0] = myFile.getAbsolutePath();
//                        String displayName = null;
            pathPdf[0]=uriString;

            if (uriString.startsWith("content://")) {

                Cursor cursor = null;
                try {
                    cursor = getContentResolver().query(uri, null, null, null, null);
                    if (cursor != null && cursor.moveToFirst()) {
                        displayName[0] = cursor.getString(cursor.getColumnIndex(OpenableColumns.DISPLAY_NAME));
                    }
                } finally {
                    cursor.close();
                }
            } else if (uriString.startsWith("file://")) {
                displayName[0] = myFile.getName();
            }
            setFilePdf(displayName);

        }


    }
    private void setFilePdf(String[] pdfName){
        java.util.ArrayList<String> myValues = new java.util.ArrayList<String>();

        //Populate the ArrayList with your own values
        for (int i=0; i<pdfName.length;i++) {
            myValues.add(pdfName[i]);
        }

        com.kriyanesia.lupirka.AdapterModel.AdapterSelectFile adapter = new com.kriyanesia.lupirka.AdapterModel.AdapterSelectFile(myValues);
        android.support.v7.widget.RecyclerView myView =  (android.support.v7.widget.RecyclerView)findViewById(R.id.recycleFile);
        myView.setHasFixedSize(true);
        myView.setAdapter(adapter);
        android.support.v7.widget.LinearLayoutManager llm = new android.support.v7.widget.LinearLayoutManager(this);
        llm.setOrientation(android.support.v7.widget.LinearLayoutManager.VERTICAL);
        myView.setLayoutManager(llm);
    }

    private void setFileImage(Uri[] image){
        AdapterBannerFile adapterBannerFile;
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycleBanner);
        int numberOfColumns = 2;
        recyclerView.setLayoutManager(new GridLayoutManager(this, numberOfColumns));
        adapterBannerFile = new AdapterBannerFile(this, image);
        recyclerView.setAdapter(adapterBannerFile);
    }



}
