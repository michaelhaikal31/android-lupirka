package com.kriyanesia.lupirka.Menu;


import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.zxing.Result;


import me.dm7.barcodescanner.zxing.ZXingScannerView;

import com.kriyanesia.lupirka.AdapterModel.AdapterListAktivitas;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Aktivitas.Aktivitas;
import com.kriyanesia.lupirka.Data.Aktivitas.ValueAktivitas;
import com.kriyanesia.lupirka.Data.Event.ValueScanBarcode;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.google.android.gms.vision.CameraSource;
import com.google.android.gms.vision.Detector;
import com.google.android.gms.vision.barcode.Barcode;
import com.google.android.gms.vision.barcode.BarcodeDetector;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

import static android.Manifest.permission.CAMERA;

public class MenuBarcode extends AppCompatActivity implements ZXingScannerView.ResultHandler {

    private static final int REQUEST_CAMERA = 1;
    private ZXingScannerView scannerView;
    private static int camId = Camera.CameraInfo.CAMERA_FACING_BACK;
    private TextView txt;
    private ImageView image;
    private boolean dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        scannerView = new ZXingScannerView(this);
        setContentView(scannerView);


        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(Color.BLACK);



        int currentApiVersion = Build.VERSION.SDK_INT;
        if(currentApiVersion >=  Build.VERSION_CODES.M) {
            if(checkPermission())
            {
//                Toast.makeText(getApplicationContext(), "Permission already granted!", Toast.LENGTH_LONG).show();
            }
            else
            {
                requestPermission();
            }
        }
    }

    private boolean checkPermission()
    {
        return (ContextCompat.checkSelfPermission(getApplicationContext(), CAMERA) == PackageManager.PERMISSION_GRANTED);
    }

    private void requestPermission()
    {
        ActivityCompat.requestPermissions(this, new String[]{CAMERA}, REQUEST_CAMERA);
    }

    @Override
    public void onResume() {
        super.onResume();
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
            if (checkPermission()) {
                if(scannerView == null) {
                    scannerView = new ZXingScannerView(this);
                    setContentView(scannerView);
                }
                scannerView.setResultHandler(this);
                scannerView.startCamera();
            } else {
                requestPermission();
            }
        }
    }

    private void sendData(String text){
        Toast.makeText(MenuBarcode.this,"Sending data, make sure you have internet connection !",Toast.LENGTH_SHORT).show();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        final SessionUser sessionUser=new SessionUser();
        String noHp=sessionUser.getNoHp(MenuBarcode.this,"nohp");
        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("text",text);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println("scan barcode "+paramObject.toString());

        Call<ValueScanBarcode> data = apiInterface.scanBarcode(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueScanBarcode>() {
            @Override
            public void onResponse(Call<ValueScanBarcode> call, Response<ValueScanBarcode> response) {

               //showAllert("Informasi",response.body().getMessage());
               //onBackPressed();

                if(response.body().getStatus().equalsIgnoreCase("success") ||
                    response.body().getMessage().contains("Scan OUT") ||
                    response.body().getMessage().contains("Scan IN")
                ){
                    scan_result(response.body().getMessage(),1);
                }else{

                    if(response.body().getMessage().contains("Anda belum melakukan") ||
                            response.body().getMessage().contains("Belum melakukan pembayaran") ||
                            response.body().getMessage().contains("Anda tidak terdaftar dalam event ini") ||
                            response.body().getMessage().contains("Barcode tidak va") ||
                            response.body().getMessage().contains("QR code tidak sesuai")){
                        //scan gagal
                        System.out.println("scan barcode 2");
                        scan_result(response.body().getMessage(),2);
                    }else if(response.body().getMessage().contains("Anda sudah")||
                            response.body().getMessage().contains("Kegiatan event belum")||
                            response.body().getMessage().contains("Pembayaran belum terverifikasi")){
                        //scan semi gagal
                        System.out.println("scan barcode 3");
                        scan_result(response.body().getMessage(),3);
                    }


                }


            }

            @Override
            public void onFailure(Call<ValueScanBarcode> call, Throwable t) {
                showAllert("Respon Server","Tidak dapat terhubung ke server");


            }
        });

    }

    private void scan_result(String message,int status){
        System.out.println("asd "+message+" | "+status);
        dialog=true;
        AlertDialog.Builder alert = new AlertDialog.Builder(this);
        LayoutInflater inflater = getLayoutInflater();
        View v = inflater.inflate(R.layout.approved_layout,null);
        Button selesai = (Button)v.findViewById(R.id.btnApproved);
        ImageView imageView = (ImageView)v.findViewById(R.id.iVStatusScan);
        TextView txtKeterangan = v.findViewById(R.id.textView4);
        if(status==1) {
            Glide.with(this)
                    .load(R.drawable.ceklis)
                    .into(imageView);
            TextView txtStatus = v.findViewById(R.id.textView3);

            if(message.contains("IN") || message.contains("OUT") || message.contains("BC") || message.contains("Table")){
                txtKeterangan.setVisibility(View.INVISIBLE);
            }
            txtStatus.setText(message);
            alert.setView(v);
            final AlertDialog alertDialog = alert.create();
            selesai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    scannerView.setResultHandler(MenuBarcode.this);
                    scannerView.startCamera();

                }
            });
            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        alertDialog.dismiss();
                        scannerView.setResultHandler(MenuBarcode.this);
                        scannerView.startCamera();
                    }

                    return true;
                }
            });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.parseColor("#36AD69")));
            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);

        }else if(status==2){
            Glide.with(this)
                    .load(R.drawable.failed)
                    .into(imageView);
            alert.setView(v);
            txtKeterangan.setVisibility(View.INVISIBLE);
            final AlertDialog alertDialog = alert.create();
            selesai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    scannerView.setResultHandler(MenuBarcode.this);
                    scannerView.startCamera();
                }
            });
            TextView txtStatus = v.findViewById(R.id.textView3);
            txtStatus.setText(message);
            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        alertDialog.dismiss();
                        scannerView.setResultHandler(MenuBarcode.this);
                        scannerView.startCamera();
                    }

                    return true;
                }
            });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.parseColor("#c20000")));
            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.MATCH_PARENT);
        }else{
            Glide.with(this)
                    .load(R.drawable.denied_grey)
                    .into(imageView);
            alert.setView(v);
            final AlertDialog alertDialog = alert.create();
            selesai.setTextColor(Color.BLACK);
            selesai.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    alertDialog.dismiss();
                    scannerView.setResultHandler(MenuBarcode.this);
                    scannerView.startCamera();
                }
            });
            TextView txtStatus = v.findViewById(R.id.textView3);
            txtStatus.setTextColor(Color.BLACK);
            TextView txtKet = v.findViewById(R.id.textView4);
            txtKet.setTextColor(Color.BLACK);
            txtKet.setVisibility(View.INVISIBLE);
            txtStatus.setText(message);
            alertDialog.setOnKeyListener(new DialogInterface.OnKeyListener() {
                @Override
                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                    if (i == KeyEvent.KEYCODE_BACK) {
                        alertDialog.dismiss();
                        scannerView.setResultHandler(MenuBarcode.this);
                        scannerView.startCamera();
                    }

                    return true;
                }
            });
            alertDialog.show();
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(getResources().getColor(R.color.white)));
            alertDialog.getWindow().setLayout(WindowManager.LayoutParams.MATCH_PARENT,
                    WindowManager.LayoutParams.MATCH_PARENT);
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        scannerView.stopCamera();
    }

    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CAMERA:
                if (grantResults.length > 0) {

                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    if (cameraAccepted){
                        Toast.makeText(getApplicationContext(), "Permission Granted, Now you can access camera", Toast.LENGTH_LONG).show();
                    }else {
                        Toast.makeText(getApplicationContext(), "Permission Denied, You cannot access and camera", Toast.LENGTH_LONG).show();
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CAMERA)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CAMERA},
                                                            REQUEST_CAMERA);
                                                }
                                            }
                                        });
                                return;
                            }
                        }
                    }
                }
                break;
        }
    }

    private void showAllert(String title, String message){
        android.app.AlertDialog.Builder alertDialog;
        alertDialog = new android.app.AlertDialog.Builder(this);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        scannerView.stopCamera();
                        //scannerView.resumeCameraPreview(MenuBarcode.this);
                        onBackPressed();
                    }
                })
                .show();
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new android.support.v7.app.AlertDialog.Builder(MenuBarcode.this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

    @Override
    public void handleResult(Result result) {
        final String myResult = result.getText();
        Log.d("QRCodeScanner", result.getText());
        Log.d("QRCodeScanner", result.getBarcodeFormat().toString());
        String data =result.getText().toString();
        //System.out.println("sub string "+a.substring(0,a.indexOf(";")));
        sendData(data);
//        String[] type=data.split(";");
//        if (type[0].equalsIgnoreCase("registrasi")){
//            AlertDialog.Builder builder = new AlertDialog.Builder(MenuBarcode.this);
//
//            LayoutInflater inflater = getLayoutInflater();
//            View dialogView = inflater.inflate(R.layout.layout_dialog_change_name_sertifikat,null);
//            // Set the custom layout as alert dialog view
//            builder.setView(dialogView);
//            // Get the custom alert dialog view widgets reference
//            final ImageButton btnClose=(ImageButton) dialogView.findViewById(R.id.btnClose);
//            final EditText etName=(EditText)dialogView.findViewById(R.id.etName);
//            final TextView textName=(TextView) dialogView.findViewById(R.id.textName);
//            final Button btnSave = (Button) dialogView.findViewById(R.id.buttonSaveName);
//            Button btnUbah = (Button) dialogView.findViewById(R.id.buttonUbahNama);
//            final SessionUser sessionUser = new SessionUser();
//            String namaSertifikat=sessionUser.getNamaSertifikat(MenuBarcode.this,"namaSertifikat");
//            textName.setText(namaSertifikat);
//            etName.setText(namaSertifikat);
//            // Create the alert dialog
//            final AlertDialog dialog = builder.create();
//
//            // Set positive/yes button click listener
//            btnSave.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // Dismiss the alert dialog
//                    if (btnSave.getText().toString().equalsIgnoreCase("simpan")){
//                        Toast.makeText(getApplicationContext(), etName.getText().toString(), Toast.LENGTH_SHORT).show();
//                        sessionUser.setNamaSertifikat(MenuBarcode.this,"namaSertifikat",etName.getText().toString());
//                    }else if (btnSave.getText().toString().equalsIgnoreCase("tidak")){
//                        Toast.makeText(getApplicationContext(), textName.getText().toString(), Toast.LENGTH_SHORT).show();
//                    }
//                    dialog.cancel();
//                    scannerView.resumeCameraPreview(MenuBarcode.this);
//                }
//            });
//            btnUbah.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    // Dismiss the alert dialog
//                    btnSave.setText("Simpan");
//                    textName.setVisibility(View.GONE);
//                    etName.setVisibility(View.VISIBLE);
//
//                }
//            });
//            btnClose.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    dialog.dismiss();
//                    scannerView.resumeCameraPreview(MenuBarcode.this);
//                }
//            });
//            dialog.show();
//        }else {
//
//            showAllert("Informasi","lakukan sending");
//        }





//        String kelas=data[0];
//        Toast.makeText(this, " "+kelas+" "+nim, Toast.LENGTH_SHORT).show();
//        sendData(data);
//        showAllert("data",data);
//        AlertDialog.Builder builder = new AlertDialog.Builder(this);
//        builder.setTitle("Scan Result");
//        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                scannerView.resumeCameraPreview(MainActivity.this);
//            }
//        });
//        builder.setNeutralButton("Visit", new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialog, int which) {
//                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(myResult));
//                startActivity(browserIntent);
//            }
//        });
//        builder.setMessage(result.getText());
//        AlertDialog alert1 = builder.create();
//        alert1.show();
    }
}
