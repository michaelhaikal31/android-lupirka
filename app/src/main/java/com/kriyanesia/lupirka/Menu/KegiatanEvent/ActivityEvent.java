package com.kriyanesia.lupirka.Menu.KegiatanEvent;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.pdf.PdfDocument;
import android.os.Build;
import android.os.CancellationSignal;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.print.PageRange;
import android.print.PrintAttributes;
import android.print.PrintDocumentAdapter;
import android.print.PrintManager;
import android.print.pdf.PrintedPdfDocument;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.print.PrintHelper;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.MyPrintDocumentAdapter;
import com.kriyanesia.lupirka.Data.SoalTest.AllSoal;
import com.kriyanesia.lupirka.Data.SoalTest.Soal;
import com.kriyanesia.lupirka.Data.SoalTest.ValueSoal;
import com.kriyanesia.lupirka.Menu.MenuBarcode;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;

import java.util.ArrayList;
import java.util.List;

public class ActivityEvent extends AppCompatActivity implements MenuSoal.Callbacks {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    private TabLayout tabLayout;
    private String id_event,nama_event;
    private static final String ARG_PARAM1 = "id_event";
    private static final String ARG_PARAM2 = "nama_event";
    private int selectedTab;
    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private Bundle extras;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_event);

        Intent i = getIntent();
        id_event = i.getStringExtra(ARG_PARAM1);
        nama_event = i.getStringExtra(ARG_PARAM2);
        setTitle(nama_event);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        checkPrintCapibility();
        setSupportActionBar(toolbar);


        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.colorPrimaryDark));

        tabLayout = (TabLayout) findViewById(R.id.tabsEvent);
        //tabLayout.addTab(tabLayout.newTab().setText("QR"));
        tabLayout.addTab(tabLayout.newTab().setText("Rundown"));
        tabLayout.addTab(tabLayout.newTab().setText("Test"));
        tabLayout.addTab(tabLayout.newTab().setText("Scan"));
        tabLayout.addTab(tabLayout.newTab().setText("Sertifikat"));
        tabLayout.addTab(tabLayout.newTab().setText("Tenant"));


        if(savedInstanceState != null ){
            tabLayout.setScrollPosition(savedInstanceState.getInt("tabState"),0,true);
        }else {
            tabLayout.setScrollPosition(0, 0, true);
            tabLayout.setSelected(true);
        }

        extras = new Bundle();
        extras.putString("id_event",id_event);
        Fragment fragment = new MenuRundown();
        fragment.setArguments(extras);
        addfragment(fragment,"RundDown");
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                selectedTab=tab.getPosition();
                switch (tab.getPosition()) {

                   /*
                    case 0:
                        Intent i = new Intent(ActivityEvent.this,MenuBarcode.class);
                        startActivity(i);
                        break;
                        */
                    case 0:
                        extras = new Bundle();
                        extras.putString("id_event",id_event);
                        Fragment fragment = new MenuRundown();
                        fragment.setArguments(extras);
                        addfragment(fragment,"MenuRundown");
                        break;
                    case 1:
                        extras = new Bundle();
                        extras.putString("id_event",id_event);
                        Fragment fragment2 = new MenuSoal();
                        fragment2.setArguments(extras);
                        addfragment(fragment2,"MenuTest");
                        break;
                    case 2:
                        Intent i = new Intent(ActivityEvent.this,MenuBarcode.class);
                        startActivity(i);
                        break;
                    case 3:
                        printDocument();
                        break;

                    case 4:
                        extras = new Bundle();
                        extras.putString("id_event",id_event);
                        Fragment fragment3 = new MenuTenan();
                        fragment3.setArguments(extras);
                        addfragment(fragment3,"MenuTenant");
                        break;
//                    case 4 :
//                        doPhotoPrint();
//                        break;

                }

            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });




    }



//    private void doPhotoPrint() {
////        PrintHelper photoPrinter = new PrintHelper(this);
////
////        photoPrinter.setScaleMode(PrintHelper.SCALE_MODE_FIT);
////        Bitmap bitmap = BitmapFactory.decodeResource(getResources(),
////                R.drawable.certificate);
////        photoPrinter.printBitmap("droids.jpg - test print", bitmap);
////    }




    private void drawPage(PdfDocument.Page page) {
        Canvas canvas = page.getCanvas();

        // units are in points (1/72 of an inch)
        int titleBaseLine = 72;
        int leftMargin = 54;

        Paint paint = new Paint();
        paint.setColor(Color.BLACK);
        paint.setTextSize(36);
        canvas.drawText("Test Title", leftMargin, titleBaseLine, paint);

        paint.setTextSize(11);
        canvas.drawText("Test paragraph", leftMargin, titleBaseLine + 25, paint);

        paint.setColor(Color.BLUE);
        canvas.drawRect(100, 100, 172, 172, paint);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_event, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.

        if (item.getItemId() == R.id.action_settings) {

           SessionUser sessionUser = new SessionUser();
           sessionUser.setPengerjaanTes(this,new AllSoal(),0);
            Toast.makeText(this,"refreshed...",Toast.LENGTH_SHORT).show();
            finish();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */


    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    void addfragment(Fragment fragment,String packageName) {
        FragmentManager fragmentManager = getSupportFragmentManager();
        // this will clear the back stack and displays no animation on the screen
        fragmentManager.popBackStackImmediate(null, FragmentManager.POP_BACK_STACK_INCLUSIVE);
        FragmentTransaction trans = getSupportFragmentManager()
                .beginTransaction();
        trans.replace(R.id.frameMenuEvent, fragment,"MAIN");
        trans.setTransition(FragmentTransaction.TRANSIT_ENTER_MASK);
        trans.commit();
        getFragmentManager().executePendingTransactions();

    }

    private void checkPrintCapibility(){
        if(Build.VERSION.SDK_INT>=19){
            final AlertDialog.Builder alert = new AlertDialog.Builder(this,R.style.AlertDialogTheme);
            LayoutInflater inflater = getLayoutInflater();
            View view=inflater.inflate(R.layout.custom_title_dialog, null);
            alert.setView(view);
            alert.setPositiveButton("MENGERTI", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                return;
                }
            });
            //alert.show();
        }
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onCreate(savedInstanceState);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("tabState",selectedTab);
    }


    public void printDocument() {
        PrintManager printManager = (PrintManager) this
                .getSystemService(Context.PRINT_SERVICE);

        String jobName = this.getString(R.string.app_name) +
                " Document";

        final SessionUser sessionUser = new SessionUser();

        PrintAttributes attrib = new PrintAttributes.Builder()
                .setMediaSize(PrintAttributes.MediaSize.UNKNOWN_LANDSCAPE)
                . build();


        printManager.print(jobName, new MyPrintDocumentAdapter(ActivityEvent.this,sessionUser.getName(ActivityEvent.this,"name")),
                attrib);
    }


    @Override
    public void onButtonClicked(AllSoal allSoal, int step) {
        System.out.println("asd asd activity "+allSoal.toString());
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.frameMenuEvent, new MenuSoal().newInstance(allSoal,step,id_event))
                .commit();
    }

    @Override
    public String getIdEvent() {
        return id_event;
    }
}
