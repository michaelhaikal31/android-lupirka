package com.kriyanesia.lupirka.Menu;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListAllAdsLupirka;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPost;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Ads.Ads;
import com.kriyanesia.lupirka.Data.Ads.ValueAdsLupirka;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.daimajia.slider.library.Animations.DescriptionAnimation;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.TextSliderView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuAllAdsLupirka extends Fragment implements AdapterListAllAdsLupirka.ItemClickListener {
    SessionUser sessionUser;
    String noHp;

    List<Ads> adsList;
    View v;
    AdapterListAllAdsLupirka adapterAds;

    public MenuAllAdsLupirka() {
        // Required empty public constructor
    }
    public MenuAllAdsLupirka newInstance(String id){
        Bundle bundle = new Bundle();
        bundle.putString("id", id);

        MenuAllAdsLupirka fragment = new MenuAllAdsLupirka();
        fragment.setArguments(bundle);
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_menu_all_ads_lupirka, container, false);
        adsList = new ArrayList<>();
        sessionUser = new SessionUser();
        noHp=sessionUser.getNoHp(getActivity(),"nohp");
        loadBannerLupirka("OTHERS");


        return v;
    }


    private void loadBannerLupirka(String tipe){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("tipe",tipe);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueAdsLupirka> data = apiInterface.getAdsLupirka(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueAdsLupirka>() {
            @SuppressLint("ResourceAsColor")
            @Override
            public void onResponse(Call<ValueAdsLupirka> call, Response<ValueAdsLupirka> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Ads> lupirkaAds;
                    lupirkaAds= response.body().getData().getLupirkaAds().getContent();
//                    for (int i=0;i<lupirkaAds.size();i++) {
//                        adsList.add(lupirkaAds.get(i));
//
//                    }

                    RecyclerView recyclerView = (RecyclerView) v.findViewById(R.id.recycleAllAdsLupirka);

                    int numberOfColumns = 1;
                    recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
                    Toast.makeText(getContext(), ""+adsList.size(), Toast.LENGTH_SHORT).show();
                    adapterAds = new AdapterListAllAdsLupirka(getActivity(), lupirkaAds);
                    adapterAds.setmClickListener(MenuAllAdsLupirka.this);
                    recyclerView.setAdapter(adapterAds);

//                    Toast.makeText(getContext(), "lpk"+lupirkaAds.size(), Toast.LENGTH_SHORT).show();

                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueAdsLupirka> call, Throwable t) {

//                showAllert("Respon Server","Tidak dapat terhubung ke server","loadFarmasi");

            }
        });


    }

    @Override
    public void onAdsClickListener(String idAds) {
        FragmentTransaction trans = getFragmentManager()
                .beginTransaction();
        trans.replace(R.id.layoutFragmentHome, new MenuDetailOtherAds().newInstance(idAds));
        trans.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN);
        trans.addToBackStack("DetailAds");
        trans.commit();
    }
}
