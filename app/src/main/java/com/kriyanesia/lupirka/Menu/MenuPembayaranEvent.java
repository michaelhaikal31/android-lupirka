package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.kriyanesia.lupirka.AdapterModel.AdapterListPaketSelected;
import com.kriyanesia.lupirka.AdapterModel.AdapterRecyclePembayaran;
import com.kriyanesia.lupirka.AdapterModel.DatabaseHelper;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.ValueEventBank;
import com.kriyanesia.lupirka.Data.Event.ValueJointEventDibayarin;
import com.kriyanesia.lupirka.Data.Pembayaran.ValuePembayaran;
import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;
import com.kriyanesia.lupirka.Util.Function;
import com.omega_r.libs.OmegaCenterIconButton;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuPembayaranEvent extends Fragment implements AdapterRecyclePembayaran.ItemBankClickListener {

    TextView valueTitleEvent,valueEventTotal,pesanDibayarin;
    String eventId,eventTitle,role,noHp,pembayaran,idFarmasi="",farmasiDaftar,namaDokter,eventRs,eventAlamat,farmasiName;
    RecyclerView recyclerListJointKegiatan,recycleMetodeBayar;
    private DatabaseHelper db;
    OmegaCenterIconButton btnNext;
    String accountBank,title;
    String[]diBayarFarmasi=new String[]{"",""};
    ArrayList<PaketTemp> listKegiatan=new ArrayList<>();
    List<ValuePembayaran.Payment> payment = new ArrayList<>();
    int TotalPembayaran=0;
    String idPaket="";
    RelativeLayout layoutMetodeBayar;
    ImageButton btnBack;
    int idDokter;
    ImageView iconBank;
    ProgressDialog progressDialog;

    String totalPembayaran,noRekBank,expTime,idPembayaran, cabangBank;
    String namaBank,namaAkunBank;
    TextView valueNoRek,valueAnRek;

    public MenuPembayaranEvent() {
        // Required empty public constructor
    }
    public MenuPembayaranEvent newInstance(String eventId,String eventTitle,ArrayList<PaketTemp> list,String idFarmasi,
                                           String farmasiDaftar,String namaDokter,String eventRs,String eventAlamat,
                                           String[]diBayarFarmasi,String farmasiName,int idDokter) {
        Bundle bundle = new Bundle();
        bundle.putString("eventId", eventId);
        bundle.putString("eventTitle", eventTitle);
        bundle.putSerializable("listKegiatan", (Serializable) list);
        bundle.putString("farmasi", idFarmasi);
        bundle.putString("farmasiDaftar", farmasiDaftar);
        bundle.putString("namaDokter", namaDokter);
        bundle.putString("eventRs", eventRs);
        bundle.putString("eventAlamat", eventAlamat);
        bundle.putStringArray("diBayarFarmasi", diBayarFarmasi);
        bundle.putString("farmasiName", farmasiName);
        bundle.putInt("idDokter", idDokter);
        MenuPembayaranEvent fragment = new MenuPembayaranEvent();
        fragment.setArguments(bundle);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v= inflater.inflate(R.layout.fragment_menu_pembayaran_event, container, false);
        eventId =getArguments().getString("eventId");
        eventTitle =getArguments().getString("eventTitle");
        idFarmasi =getArguments().getString("farmasi");
        farmasiDaftar=getArguments().getString("farmasiDaftar");
        namaDokter=getArguments().getString("namaDokter");
        idDokter=getArguments().getInt("idDokter");
        eventRs=getArguments().getString("eventRs");
        eventAlamat=getArguments().getString("eventAlamat");
        farmasiName=getArguments().getString("farmasiName");
        listKegiatan = (ArrayList<PaketTemp>) getArguments().getSerializable("listKegiatan");
        System.out.println("asd "+listKegiatan.toString());
        diBayarFarmasi = getArguments().getStringArray("diBayarFarmasi");
        if (farmasiName.equalsIgnoreCase("Pilih Farmasi")){
            farmasiName=null;
        }
//        System.out.println("DATA EVENT x"+eventId);
//        System.out.println("DATA EVENT x"+eventTitle);
//        System.out.println("DATA EVENT x"+idDokter);
//        System.out.println("DATA EVENT x"+idFarmasi);
//        System.out.println("DATA EVENT x"+farmasiDaftar);
//        System.out.println("DATA EVENT x"+namaDokter);
//        System.out.println("DATA EVENT x"+eventRs);
//        System.out.println("DATA EVENT x"+eventAlamat);
//        System.out.println("DATA EVENT x"+farmasiName);
//        System.out.println("DATA EVENT x"+diBayarFarmasi);


        SessionUser sessionUser=new SessionUser();
        role=sessionUser.getRole(getActivity(),"role");
        noHp = sessionUser.getNoHp(getActivity(),"nohp");

        layoutMetodeBayar = (RelativeLayout) v.findViewById(R.id.layoutMetodeBayar);
        valueTitleEvent = (TextView) v.findViewById(R.id.valueTitleEvent);
        valueAnRek = (TextView) v.findViewById(R.id.valueAnRek);
        pesanDibayarin = (TextView) v.findViewById(R.id.pesanDibayarin);
        valueEventTotal = (TextView) v.findViewById(R.id.valueEventTotal);
        valueNoRek = (TextView) v.findViewById(R.id.valueNoRek);
        recycleMetodeBayar = (RecyclerView) v.findViewById(R.id.recycleMetodeBayar);
        recyclerListJointKegiatan = (RecyclerView) v.findViewById(R.id.recyclerListJointKegiatan);
        btnNext = (OmegaCenterIconButton) v.findViewById(R.id.btnNext);
        btnBack= (ImageButton) v.findViewById(R.id.btnBack);
        iconBank= (ImageView) v.findViewById(R.id.iconBank);
        int numberOfColumns = 1;
        recyclerListJointKegiatan.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumns));
        AdapterListPaketSelected adapterListPaketSelected = new AdapterListPaketSelected(getActivity(), listKegiatan);
        recyclerListJointKegiatan.setAdapter(adapterListPaketSelected);
        boolean checked=false;
        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
        int j=0;
        int size=listKegiatan.size();
        for (int i=0; i<size;i++) {

            if (listKegiatan.get(i).isStatusChecked()) {
                TotalPembayaran = TotalPembayaran + Integer.valueOf(listKegiatan.get(i).getHarga());
                if (j == 0) {
                    idPaket = idPaket + listKegiatan.get(i).getId();
                } else {
                    idPaket = idPaket + "," + listKegiatan.get(i).getId();
                }
                j = j + 1;
            }
        }
        valueEventTotal.setText( Function.ConvertRupiah(TotalPembayaran));
        loadMetodePembayaran(eventId);

        valueTitleEvent.setText(eventTitle);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (btnNext.getText().toString().equalsIgnoreCase("selesai")){
                    Intent intent = new Intent(getActivity(), MenuMain.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(intent);
                }else {
                    progressDialog = new ProgressDialog(getActivity());
                    progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progressDialog.setCancelable(false);
                    progressDialog.setCanceledOnTouchOutside(false);
                    progressDialog.setMessage("Please Wait...");
                    progressDialog.show();
                    pembayaranEvent();
                }
            }
        });


        db = new DatabaseHelper(getActivity());
        loadNoRek(eventId);
        return v;
    }



    ValueJointEventDibayarin valueJointEventDibayarin;
    private void pembayaranEvent(){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(@NonNull Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        ApiService apiInterface = retrofit.create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",eventId);
            paramObject.put("paket",idPaket);
//            Toast.makeText(getActivity(), idPaket, Toast.LENGTH_SHORT).show();
            paramObject.put("role",role);
            System.out.println("DAFTARNYA "+farmasiDaftar);
            if (farmasiDaftar.split("-")[2].equalsIgnoreCase("x")){
                paramObject.put("farmasi","");
                if (role.equalsIgnoreCase("5")){
                    if (farmasiDaftar.split("-")[1].equalsIgnoreCase("Sendiri")){
                        paramObject.put("pembayaran", "Mahasiswa-Sendiri-Sekarang");
                    }else {
                        paramObject.put("farmasi",idFarmasi);
                        paramObject.put("pembayaran", "Mahasiswa-Farmasi-Sekarang");
                    }

                }else if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")) {
                    if (farmasiDaftar.split("-")[1].equalsIgnoreCase("Sendiri")){
                        paramObject.put("pembayaran", "Dokter-Sendiri-Sekarang");
                    }else {
                        paramObject.put("farmasi",idFarmasi);
                        paramObject.put("pembayaran", "Dokter-Farmasi-Sekarang");
                    }
                }
            }else {
                paramObject.put("pembayaran", farmasiDaftar);
                paramObject.put("id_dokter",idDokter);
                paramObject.put("farmasi",idFarmasi);

            }
            paramObject.put("nama_dokter",namaDokter);
            paramObject.put("tipe_bayar",0);
            paramObject.put("alamat",eventAlamat);
            paramObject.put("rumah_sakit",eventRs);
            paramObject.put("amount",TotalPembayaran);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA daftar event : "+paramObject.toString());

        Call<ValueJointEventDibayarin> dataCall = apiInterface.joinEventBayarOrang(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJointEventDibayarin>() {
            @Override
            public void onResponse(Call<ValueJointEventDibayarin> call, Response<ValueJointEventDibayarin> response) {
                valueJointEventDibayarin=response.body();
                SessionUser sessionUser=new SessionUser();
                sessionUser.setAlamatRs(getActivity(),"alamatRs",eventRs);
                sessionUser.setNamaRs(getActivity(),"namaRs",eventAlamat);
                sessionUser.setNamaSertifikat(getActivity(),"namaSertifikat",namaDokter);
                if (valueJointEventDibayarin.getMessage().equalsIgnoreCase("Anda sudah pernah mendaftar event ini, mohon lakukan pembayaran sebelum waktu pembayaran habis")
                        || valueJointEventDibayarin.getStatus().equalsIgnoreCase("success")) {
                    int total = Integer.valueOf(valueJointEventDibayarin.getData().getAmount()) +
                            Integer.valueOf(valueJointEventDibayarin.getData().getUnique_code());
                    totalPembayaran = String.valueOf(total);
                    expTime= valueJointEventDibayarin.getData().getExp_time().substring(0,19);
                    idPembayaran=valueJointEventDibayarin.getData().getId();
                }else {
                }
                progressDialog.dismiss();
                showAllert("Informasi",valueJointEventDibayarin.getStatus(),valueJointEventDibayarin.getMessage());
            }

            @Override
            public void onFailure(Call<ValueJointEventDibayarin> call, Throwable t) {
                progressDialog.dismiss();
                showAllert("Informasi","failed","Koneksi ke server terputus");
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        this.getArguments().clear();
    }

    AlertDialog.Builder alertDialog;
    private void showAllert(String title,final String status, final String message){
        alertDialog = new AlertDialog.Builder(getActivity());
        alertDialog.setCancelable(false);
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (message.equalsIgnoreCase("Anda sudah pernah mendaftar event ini, mohon lakukan pembayaran sebelum waktu pembayaran habis")
                                || status.equalsIgnoreCase("success")) {
                            Intent intent = new Intent(getActivity(), MenuPembayaranAkhir.class);
                            intent.putExtra("idAktivitas",eventId);
                            intent.putExtra("menu", "Pembayaran Topup Lanjut");
                            intent.putExtra("from", "MenuPembayaranEvent");
                            intent.putExtra("title", "Pembayaran Event " + eventTitle);
                            intent.putExtra("totalPembayaran", totalPembayaran);
                            intent.putExtra("noRekBank", noRekBank);
                            intent.putExtra("namaBank", namaBank);
                            intent.putExtra("namaAkunBank", namaAkunBank);
                            intent.putExtra("expTime", expTime);
                            intent.putExtra("idPembayaran", idPembayaran);

                            startActivity(intent);
//                            Toast.makeText(getActivity(), ""+expTime, Toast.LENGTH_SHORT).show();
                        }
                    }
                })
                .show();
    }


    @Override
    public void onItemBankClick(String accountBank) {
        this.accountBank=accountBank;
    }

    private void loadNoRek(final String eventId){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });
        OkHttpClient client = httpClient.build();
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",eventId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA check no rek : "+paramObject.toString());
        Call<ValueEventBank> dataCall = apiInterface.getEventBank(paramObject.toString(),"saxasdas");
        dataCall.enqueue(new Callback<ValueEventBank>() {
            @Override
            public void onResponse(Call<ValueEventBank> call, Response<ValueEventBank> response) {
                ValueEventBank.EventBank eventBank = response.body().getData();
                noRekBank= eventBank.getNo_rekening();
                namaBank= eventBank.getNama_bank();
                namaAkunBank= eventBank.getNama_akun();
                cabangBank = eventBank.getCabang();
                valueNoRek.setText(noRekBank);
                valueAnRek.setText("AN. "+namaAkunBank+"\n Cabang "+cabangBank);
                Picasso.with(getActivity()).load(eventBank.getUrl_logo_bank()).into(iconBank);
            }

            @Override
            public void onFailure(Call<ValueEventBank> call, Throwable t) {
                Toast.makeText(getActivity(), "gagal load no rek event "+eventId, Toast.LENGTH_SHORT).show();
                System.out.println("VALUENYA : Gagal "+t.getMessage());
            }
        });
    }



    private void loadMetodePembayaran(final String eventId){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();
                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",eventId);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());
        Call<ValuePembayaran> dataCall = apiInterface.getMetodeBayar(paramObject.toString(),"saxasdas");
        dataCall.enqueue(new Callback<ValuePembayaran>() {
            @Override
            public void onResponse(Call<ValuePembayaran> call, Response<ValuePembayaran> response) {

                List<ValuePembayaran.Payment> payment = new ArrayList<>();
                payment = response.body().getData().getPayment();

                }


//                progressDialog.dismiss();

//            }

            @Override
            public void onFailure(Call<ValuePembayaran> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }



}
