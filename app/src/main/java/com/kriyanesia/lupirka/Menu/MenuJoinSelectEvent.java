package com.kriyanesia.lupirka.Menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.kriyanesia.lupirka.AdapterModel.AdapterListMyAds;
import com.kriyanesia.lupirka.AdapterModel.AdapterPaketEvent;
import com.kriyanesia.lupirka.ApiHttp.ApiService;
import com.kriyanesia.lupirka.Data.Event.AllEventPaket;
import com.kriyanesia.lupirka.Data.Event.Event;
import com.kriyanesia.lupirka.Data.Event.Paket;
import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.Data.Event.ValueDetailEvent;
import com.kriyanesia.lupirka.Data.Event.ValueDetailKegiatan;
import com.kriyanesia.lupirka.Data.Event.ValueEventList;
import com.kriyanesia.lupirka.Data.Event.ValueJointEventDibayarin;
import com.kriyanesia.lupirka.Data.Farmasi.Farmasi;
import com.kriyanesia.lupirka.Data.Farmasi.ValueFarmasi;
import com.kriyanesia.lupirka.R;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.kriyanesia.lupirka.Util.Constant;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.scalars.ScalarsConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuJoinSelectEvent extends Fragment implements AdapterPaketEvent.ItemClickListener {
    String idEvent, rumahSakit,alamat,eventName,farmasiDaftar,namaDokter,userType;
    int idDokter;

    public static MenuJoinSelectEvent newInstance(String idEvent,
                                                  String rumahSakit,String alamat,
                                                  String eventName,String farmasiDaftar,
                                                  String namaDokter,String dokterType,int idDokter) {
        Bundle bundle = new Bundle();
        bundle.putString("eventId", idEvent);
        bundle.putString("rumahSakit", rumahSakit);
        bundle.putString("alamat", alamat);
        bundle.putString("eventName", eventName);
        bundle.putString("farmasiDaftar", farmasiDaftar);
        bundle.putString("namaDokter", namaDokter);
        bundle.putInt("idDokter", idDokter);
        bundle.putString("userType", dokterType);

        MenuJoinSelectEvent fragment = new MenuJoinSelectEvent();
        fragment.setArguments(bundle);
        return fragment;
    }


    private List<Farmasi> farmasi= new ArrayList<>();
    private List<PaketTemp> listKegiatan=new ArrayList<>();
    SessionUser sessionUser;
    String role;

    TextView textTitleSambutan,textNamaFarmasi;
    RecyclerView recycleListKegiatan;
    RelativeLayout layoutFarmasiSpiner;

    Button btnSpinnerFarmasi,spinnerFarmasi,btnNextRegister;
    String idFarmasi="";
    AdapterPaketEvent adapterPaketEvent;
    RadioGroup rgPembayaranFarmasi,rgPembayaran;
    RadioButton rbSendiri,rbFarmasi,rbSeKarang,rbGuarantee;
    ImageButton btnBack;
//    LinearLayout tableKegiatan;
    String noHp;

    Intent intent;
    View v;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        v= inflater.inflate(R.layout.fragment_menu_join_select_event, container, false);
        textTitleSambutan = (TextView) v.findViewById(R.id.textTitleSambutan);
        textNamaFarmasi = (TextView) v.findViewById(R.id.textNamaFarmasi);

        recycleListKegiatan = (RecyclerView) v.findViewById(R.id.recycleListKegiatan);
        layoutFarmasiSpiner = (RelativeLayout) v.findViewById(R.id.layoutFarmasiSpiner);
        listKegiatan=new ArrayList<>();
        listKegiatan.clear();
        rgPembayaranFarmasi = (RadioGroup) v.findViewById(R.id.rgPembayaranFarmasi);
        rgPembayaran = (RadioGroup) v.findViewById(R.id.rgPembayaran);
        intent = new Intent(getActivity(), ActivityDetailContent.class);
        rbSendiri = (RadioButton) v.findViewById(R.id.rbSendiri);
        rbFarmasi = (RadioButton) v.findViewById(R.id.rbFarmasi);
        rbSeKarang = (RadioButton) v.findViewById(R.id.rbSeKarang);
        rbGuarantee = (RadioButton) v.findViewById(R.id.rbGuarantee);

        btnBack = (ImageButton) v.findViewById(R.id.btnBack);
//        tableKegiatan = v.findViewById(R.id.tableKegiatan);

        idEvent =getArguments().getString("eventId");
        rumahSakit =getArguments().getString("rumahSakit");
        alamat =getArguments().getString("alamat");
        eventName =getArguments().getString("eventName");
        farmasiDaftar =getArguments().getString("farmasiDaftar");
        namaDokter =getArguments().getString("namaDokter");
        idDokter =getArguments().getInt("idDokter");
        userType= getArguments().getString("userType");
        textTitleSambutan.setText("Pendaftaran "+eventName);
        sessionUser=new SessionUser();
        role= sessionUser.getRole(getActivity(),"role");
        noHp= sessionUser.getNoHp(getActivity(),"nohp");


        btnBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getActivity().finish();
            }
        });
//        if (role.equalsIgnoreCase("2")|| role.equalsIgnoreCase("3")){
        if (role.equalsIgnoreCase("2")|| role.equalsIgnoreCase("3")||
                role.equalsIgnoreCase("5")){
            nonAktifView();
            rgPembayaran.setVisibility(View.VISIBLE);
        }else if (role.equalsIgnoreCase("7")){
            nonAktifView();
            rgPembayaranFarmasi.setVisibility(View.VISIBLE);
            textNamaFarmasi.setVisibility(View.VISIBLE);
            if (farmasiDaftar.equalsIgnoreCase("farmasi")) {

                textNamaFarmasi.setText(sessionUser.getPerusahaanFarmasi(getActivity(),"perusahaanFarmasi"));
            }else if (farmasiDaftar.equalsIgnoreCase("dokter")){
                textNamaFarmasi.setText(namaDokter);
            }
        }else {
            nonAktifView();
        }

        rgPembayaran.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                if (rbFarmasi.isChecked()){
                    spinnerFarmasi.setText("Pilih Farmasi");
                    layoutFarmasiSpiner.setVisibility(View.VISIBLE);
                    idFarmasi="Pilih Farmasi";
                }else {
                    layoutFarmasiSpiner.setVisibility(View.GONE);
                }
            }
        });
        btnSpinnerFarmasi = (Button) v.findViewById(R.id.btnSpinnerFarmasi);
        spinnerFarmasi = (Button) v.findViewById(R.id.spinnerFarmasi);

        btnNextRegister = (Button) v.findViewById(R.id.btnNextRegister);
        loadDetailPaket();
        loadFarmasi();
        idFarmasi=spinnerFarmasi.getText().toString();
        spinnerFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataFarmasi();
            }
        });
        btnSpinnerFarmasi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkDataFarmasi();
            }
        });

        btnNextRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

//                if (!checked ||  eventName.isEmpty()){
                System.out.println("asd 2 "+listKegiatan.toString());
                if (!validate()){
                    Toast.makeText(getActivity(), "Pilih salah satu kegiatan yang tersedia", Toast.LENGTH_SHORT).show();
                }else {
                    if (role.equalsIgnoreCase("7")){
                        String tipeBayar=" ";
                        if (rgPembayaranFarmasi.getVisibility()==View.VISIBLE){
                            if (rbSeKarang.isChecked()){
                                tipeBayar="Sekarang";
                            }else if (rbGuarantee.isChecked()){
                                tipeBayar="Guarantee";
                            }
                        }

                        intent.putExtra("farmasiDaftar", "Farmasi-Booth-"+tipeBayar);
                        //menu daftar jika login farmasi belum fix
                        if (rbFarmasi.isChecked()) {
                            intent.putExtra("farmasiDaftar", "Farmasi-Tenant-"+tipeBayar);
                        }else {
                            intent.putExtra("farmasiDaftar","Farmasi-Dokter-"+tipeBayar);
                            intent.putExtra("idDokter",idDokter);
                            intent.putExtra("namaDokter",namaDokter);
                        }
                        intent.putExtra("menu", "Pembayaran Event");
                        intent.putExtra("eventId", idEvent);
                        intent.putExtra("eventName", eventName);
                        intent.putExtra("listKegiatan", (Serializable) listKegiatan);
//                        startActivity(intent);
//                        jointEventDibayarin(2);

                    }else {

                        if (rbFarmasi.isChecked()) {
                            intent.putExtra("farmasi", idFarmasi);
                            intent.putExtra("idDokter",idDokter);
                            intent.putExtra("menu", "Pembayaran Event");
//                            jointEventDibayarin(1);


                            intent.putExtra("namaDokter",namaDokter);
                            intent.putExtra("farmasiDaftar", "Dokter-Farmasi-x");



                        }else {
                            intent.putExtra("farmasi", "Tidak Ada");
                            intent.putExtra("farmasiDaftar","Dokter-Sendiri-x");
                            intent.putExtra("idDokter",idDokter);
                            //rs dan alamat adalah data user
                            intent.putExtra("eventRs",rumahSakit);
                            intent.putExtra("eventAlamat",alamat);
                            intent.putExtra("namaDokter",namaDokter);
                            intent.putExtra("menu", "Pembayaran Event");
                            intent.putExtra("eventName", eventName);
                            pesan=new String[]{"",""};
                        }

                        intent.putExtra("eventId", idEvent);

                    }

                    intent.putExtra("farmasiName",spinnerFarmasi.getText().toString() );
                    intent.putExtra("diBayarFarmasi",(String[]) pesan);
                    intent.putExtra("listKegiatan", (Serializable) listKegiatan);
                    System.out.println("morten value intent 2 "+intent.getExtras().toString());
                        startActivity(intent);

                }
            }
        });
        return v;
    }
    private boolean validate(){
        boolean valid=true;
        boolean checked=false;
        for (int i=0; i<listKegiatan.size();i++){

            if(listKegiatan.get(i).isStatusChecked()) {
                checked=true;
            }
        }
        if (!checked){
            valid=false;
            System.out.println("VALIDASI : SELECT");
        }
        if (idEvent.isEmpty()){
            valid=false;
            System.out.println("VALIDASI : ID EVENT");
        }
        if (eventName.isEmpty()){
            valid=false;
            System.out.println("VALIDASI : EVENT NAME");
        }
//        if (tableKegiatan.getVisibility()==View.GONE){
//            valid=false;
//            System.out.println("VALIDASI : TABLE");
//        }
        if (rgPembayaran.getCheckedRadioButtonId() == -1){
            //jika rb pembayaran gak dichecklist
            if (rgPembayaranFarmasi.getCheckedRadioButtonId()==-1){
                //jika rb pembayaran farmasi juga gak dichecklist
                valid=false;
                System.out.println("VALIDASI : PEMBAYARAN");
            }else {
                //jika rb pembayaran farmasi dichecklist

            }
        }else {
            if (rbFarmasi.isChecked() && idFarmasi.equalsIgnoreCase("Pilih Farmasi")){
                valid=false;
                System.out.println("VALIDASI : FARMASI");
            }
        }

        return valid;
    }

    private void loadDetailPaket(){

        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);
        SessionUser su= new SessionUser();
        String nohp= su.getNoHp(getActivity(),"nohp");
        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("id_login",nohp);
            paramObject.put("id_event",idEvent);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());


        Call<ValueDetailKegiatan> data = apiInterface.getPaketEvent(paramObject.toString(),"saxasdas");

        data.enqueue(new Callback<ValueDetailKegiatan>() {
            @Override
            public void onResponse(Call<ValueDetailKegiatan> call, Response<ValueDetailKegiatan> response) {

                if (response.body().getStatus().equalsIgnoreCase("success")){
                    List<Paket> list=new ArrayList<>();

                    List<Paket> paket_event;
                    try {
                        paket_event = response.body().getData().getPaket_event();

                        list = response.body().getData().getPaket_event();
                        for (int i=0;i<list.size();i++){
                            PaketTemp paketTemp = new PaketTemp();
                            paketTemp.setId(list.get(i).getId());
                            paketTemp.setAktifitas(paket_event.get(i).getAktifitas());
                            paketTemp.setHarga(list.get(i).getQuota());
                            paketTemp.setKategori_harga(list.get(i).getKategori_harga());
                            paketTemp.setStatusChecked(false);
                            listKegiatan.add(paketTemp);
                            System.out.println("DATA PAKET TEMP TAMBAH "+i+" "+list.get(i).getAktifitas());

                        }

                        if (listKegiatan.size()>0){
                            adapterPaketEvent = new AdapterPaketEvent(getActivity(), paket_event,userType);
                            recycleListKegiatan.setLayoutManager(new LinearLayoutManager(getActivity()));
                            recycleListKegiatan.setItemAnimator(new DefaultItemAnimator());

                            //SET ADAPTER
                            recycleListKegiatan.setAdapter(adapterPaketEvent);
                            adapterPaketEvent.setmClickListener(MenuJoinSelectEvent.this);
//                            Toast.makeText(getActivity(), ""+listKegiatan.size(), Toast.LENGTH_SHORT).show();
                        }else {
                            showAllert("Informasi",response.body().getMessage(),"","close");
                        }
                    }catch (Exception e){
                        Log.e("LOAD EVENT DETAIL ",e.getMessage());
                    }
//
//
//                    list.clear();

//




                }else if (response.body().getStatus().equalsIgnoreCase("failed")){


                    showAllert("Informasi",response.body().getMessage(),"","close");
//                        tableKegiatan.setVisibility(View.GONE);

                }

            }

            @Override
            public void onFailure(Call<ValueDetailKegiatan> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());

            }
        });
    }

    private void loadFarmasi(){
        farmasi.clear();
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        ApiService apiInterface = retrofit.create(ApiService.class);

        JSONObject paramObject = new JSONObject();
        try {
            paramObject.put("role",role);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueFarmasi> farmasiCall = apiInterface.getAllFarmasi(paramObject.toString(),"saxasdas");

        farmasiCall.enqueue(new Callback<ValueFarmasi>() {
            @Override
            public void onResponse(Call<ValueFarmasi> call, Response<ValueFarmasi> response) {
                if (response.body().getStatus().equalsIgnoreCase("success")){


                    List<Farmasi> farmasiData;
                    farmasiData = response.body().getData().getFarmasi();

                    if (!farmasiData.isEmpty()){
                        farmasi.addAll(farmasiData);

                    }


                }
                System.out.println("VALUENYA : "+response.body().getStatus());

            }

            @Override
            public void onFailure(Call<ValueFarmasi> call, Throwable t) {

                showAllert("Respon Server","Tidak dapat terhubung ke server","loadFarmasi","failed");

            }
        });
    }

    private void nonAktifView(){
        rgPembayaranFarmasi.setVisibility(View.GONE);
        rgPembayaran.setVisibility(View.GONE);
        textNamaFarmasi.setVisibility(View.GONE);
        layoutFarmasiSpiner.setVisibility(View.GONE);
    }
    private void showAllert(String title, final String message, final String action, final String status){

        AlertDialog.Builder alertDialog;
        alertDialog = new AlertDialog.Builder(getContext());
        alertDialog.setTitle(title)
                .setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (action.equalsIgnoreCase("loadFarmasi")){
                            loadFarmasi();
                        }

                        if (status.equalsIgnoreCase("failed")){

                        }else if (status.equalsIgnoreCase("close")){
                            getActivity().finish();
                        }else {
                            startActivity(intent);
                        }
                    }
                })
                .show();
    }
    private void checkDataFarmasi() {
        if (!farmasi.isEmpty()) {
            final String[] arrProfesi =new String[farmasi.size()];
            for (int i = 0; i < farmasi.size(); i++) {
                arrProfesi[i] = farmasi.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Pilih Farmasi");
            builder.setItems(arrProfesi, new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int position) {

                    spinnerFarmasi.setText(farmasi.get(position).getName());
                    idFarmasi=farmasi.get(position).getId();

                }
            }).show();
        }
    }


//    @Override
//    public void OnItemClickListener(int index, String title,String id,
//                                    String harga,String kategoriHarga,
//                                    String paket, boolean checked) {
//
////        PaketTemp p = new PaketTemp();
////        p.setAktifitas(title);
////        p.setHarga(harga);
////        p.setPaket(paket);
////        p.setId(id);
////        p.setKategori_harga(kategoriHarga);
////        if (checked){
////            p.setStatusChecked(true);
////        }else {
////            p.setStatusChecked(false);
////        }
////
////        listKegiatan.set(index,p);
//        Toast.makeText(getActivity(), "xxx "+index, Toast.LENGTH_SHORT).show();
//
//    }


    int TotalPembayaran=0;
    String idPaket="";
    String[] pesan={"",""};
    ValueJointEventDibayarin valueJointEventDibayarin;
    private void jointEventDibayarin(final int tipeBayar){
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(new Interceptor() {
            @Override
            public okhttp3.Response intercept(Chain chain) throws IOException {
                Request original = chain.request();

                // Request customization: add request headers
                Request.Builder requestBuilder = original.newBuilder()
                        .header("Authorization", "auth-value"); // <-- this is the important line

                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        });

        OkHttpClient client = httpClient.build();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constant.URL_SERVICE)
                .addConverterFactory(ScalarsConverterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();


        ApiService apiInterface = retrofit.create(ApiService.class);
        final JSONObject paramObject = new JSONObject();
        boolean checked=false;
        int j=0;
        int size=listKegiatan.size();
        for (int i=0; i<size;i++){

            if(listKegiatan.get(i).isStatusChecked()) {
                TotalPembayaran= TotalPembayaran+Integer.valueOf(listKegiatan.get(i).getHarga());
                if (j==0){
                    idPaket=idPaket+listKegiatan.get(i).getId();
                }else {
                    idPaket=idPaket+","+listKegiatan.get(i).getId();
                }
                j=j+1;
            }
        }
        try {
            paramObject.put("id_login",noHp);
            paramObject.put("event_id",idEvent);
            paramObject.put("paket",idPaket);
//            Toast.makeText(getActivity(), idPaket, Toast.LENGTH_SHORT).show();
            paramObject.put("role",role);
            if (tipeBayar==0){
                paramObject.put("farmasi","");
                if (role.equalsIgnoreCase("5")){
                    paramObject.put("pembayaran", "Mahasiswa-Sendiri-Sekarang");

                }else if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")) {
                    paramObject.put("pembayaran", "Dokter-Sendiri-Sekarang");
                }
            }else if (tipeBayar==1){

                if (role.equalsIgnoreCase("5")){
                    paramObject.put("pembayaran", "Mahasiswa-Farmasi-Approve");

                }else if (role.equalsIgnoreCase("2") || role.equalsIgnoreCase("3")) {
                    paramObject.put("pembayaran", "Dokter-Farmasi-Approve");
                }
                paramObject.put("farmasi",idFarmasi);
            }else if (tipeBayar==2){
                if (rgPembayaranFarmasi.getVisibility()==View.VISIBLE){
                    if (rbSeKarang.isChecked()){
                        paramObject.put("pembayaran","Farmasi-Dokter-Sekarang");
                        paramObject.put("id_dokter",idDokter);
                    }else if (rbGuarantee.isChecked()){
                        paramObject.put("pembayaran","Farmasi-Dokter-Guarantee");
                        paramObject.put("id_dokter",idDokter);
                    }
                }
                paramObject.put("farmasi",idFarmasi);
            }

            paramObject.put("nama_dokter",namaDokter);
//            paramObject.put("tipe_bayar",tipeBayar);
            paramObject.put("alamat",alamat);
            paramObject.put("rumah_sakit",rumahSakit);
            paramObject.put("amount",TotalPembayaran);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        System.out.println("VALUENYA : "+paramObject.toString());

        Call<ValueJointEventDibayarin> dataCall = apiInterface.joinEventBayarOrang(paramObject.toString(),"saxasdas");

        dataCall.enqueue(new Callback<ValueJointEventDibayarin>() {
            @Override
            public void onResponse(Call<ValueJointEventDibayarin> call, Response<ValueJointEventDibayarin> response) {
                valueJointEventDibayarin=response.body();
                Log.i("Responsess", "pesan "+valueJointEventDibayarin.getMessage());
                pesan[0]=valueJointEventDibayarin.getStatus();
                pesan[1]=valueJointEventDibayarin.getMessage();
                setPesan();
                Toast.makeText(getActivity(), "berhasil", Toast.LENGTH_SHORT).show();

                intent.putExtra("farmasiName",spinnerFarmasi.getText().toString() );
                intent.putExtra("diBayarFarmasi",(String[]) pesan);
                intent.putExtra("listKegiatan", (Serializable) listKegiatan);
                if (tipeBayar==1){
                    intent.putExtra("farmasiDaftar", "Dokter Farmasi");

                }else if (tipeBayar==0){
                    intent.putExtra("farmasiDaftar", "Dokter Sendiri");
                }else if (tipeBayar==2) {
                    intent.putExtra("farmasiDaftar", "Farmasi Booth");

                }
                showAllert("RESPON SERVER",valueJointEventDibayarin.getMessage(),"",valueJointEventDibayarin.getStatus());


            }

            @Override
            public void onFailure(Call<ValueJointEventDibayarin> call, Throwable t) {
                System.out.println("VALUENYA : Gagal "+t.getMessage());
                pesan[0]="";
                pesan[1]=t.getMessage();
//                progressDialog.dismiss();
//                showAllert("Respon Server","Periksa Koneksi anda, anda tidak dapat terhubung ke server","none");

            }
        });
    }
    private void setPesan(){
//        Toast.makeText(getActivity(), "final "+pesan[1], Toast.LENGTH_SHORT).show();
//        showAllert("Respon Server",pesan[1],"");
//        pesan[0]=valueJointEventDibayarinTemp.getStatus();
//        pesan[1]=valueJointEventDibayarinTemp.getMessage();
    }

    @Override
    public void OnItemClickListener(int index, String title, String id, String harga,
                                    String kategoriHarga, String paket, boolean checked) {
        System.out.println("asd 123 "+title);
        PaketTemp p = new PaketTemp();
        p.setId(id);
        p.setAktifitas(title);
        p.setHarga(harga);
        p.setId(id);
        p.setKategori_harga(kategoriHarga);
        if (checked){
            p.setStatusChecked(true);
        }else {
            p.setStatusChecked(false);
        }

        listKegiatan.set(index,p);




    }

//    @Override
//    public void OnItemClickListener(int index, String title) {
//
//    }
}
