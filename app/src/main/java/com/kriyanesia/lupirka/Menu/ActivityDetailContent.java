package com.kriyanesia.lupirka.Menu;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.kriyanesia.lupirka.Data.Aktivitas.ValueDetailAktivitas;
import com.kriyanesia.lupirka.Data.Event.PaketTemp;
import com.kriyanesia.lupirka.R;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ActivityDetailContent extends AppCompatActivity {
    Intent intent;
    @BindView(R.id.contentFragmentDetail)
    FrameLayout contentFragment;
    public boolean recentlyPay;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_content);
        ButterKnife.setDebug(true);
        ButterKnife.bind(this);
        intent = getIntent();
        Bundle extras = intent.getExtras();
        String menu = extras.getString("menu");
        String eventName = extras.getString("eventName");
        String eventId = extras.getString("eventId");
        String idFarmasi = extras.getString("farmasi");
        String cp = extras.getString("cp");
        String[] diBayarFarmasi = extras.getStringArray("diBayarFarmasi");
        ArrayList listKegiatan = new ArrayList();
        System.out.println("rece "+extras.getBoolean("recentlyPay"));
        recentlyPay = extras.getBoolean("recentlyPay");

        listKegiatan=(ArrayList<PaketTemp>) extras.getSerializable("listKegiatan");


        String adsId = extras.getString("adsId");
        String adsTitle = extras.getString("adsTitle");
        String adsSubTitle = extras.getString("adsSubTitle");
        String adsStartDate = extras.getString("adsStartDate");
        String adsDesc = extras.getString("adsDesc");
        String urlImage = extras.getString("urlImage");
        String adsPath = extras.getString("adsPath");
        String eventRs = extras.getString("eventRs");
        String eventAlamat = extras.getString("eventAlamat");
        String farmasiDaftar = extras.getString("farmasiDaftar");
        String namaDokter = extras.getString("namaDokter");
        int idDokter = extras.getInt("idDokter");
        String userType = extras.getString("userType");
        String farmasiName = extras.getString("farmasiName");
        String adsCategori = extras.getString("adsCategori");
        String adsIdCategori = extras.getString("adsIdCategori");


        String title = extras.getString("title");
        String totalPembayaran = extras.getString("totalPembayaran");
        String vaNumber = extras.getString("vaNumber");
        String expTime = extras.getString("expTime");

        String urlBanner = extras.getString("urlBanner");


        int idAktivitas = extras.getInt("idAktivitas");
        String aktivitasFarmasi = extras.getString("aktivitasFarmasi");
        String aktivitasHarga = extras.getString("aktivitasHarga");
        String aktivitasLokasi = extras.getString("aktivitasLokasi");
        String aktivitasNama = extras.getString("aktivitasNama");
        String aktivitasNoHp = extras.getString("aktivitasNoHp");
        String aktivitasStatus = extras.getString("aktivitasStatus");
        String aktivitasMessage = extras.getString("aktivitasMessage");
        String aktivitasOngoing = extras.getString("aktivitasOngoing");
        String aktivitasPembayaran = extras.getString("aktivitasPembayaran");
        String aktivitasTitle = extras.getString("aktivitasTitle");
        String aktivitasId = extras.getString("aktivitasId");
        List<ValueDetailAktivitas.DetailCostTemp> aktivitasListCost=(ArrayList<ValueDetailAktivitas.DetailCostTemp>) extras.getSerializable("aktivitasCost");
        List<ValueDetailAktivitas.DetailKegiatanTemp> aktivitasListKegiatan=(ArrayList<ValueDetailAktivitas.DetailKegiatanTemp>) extras.getSerializable("aktivitasKegiatan");



        if (menu.equalsIgnoreCase("Perpanjang Iklan")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuPerpanjangIklan().newInstance(adsTitle,adsSubTitle,adsStartDate,adsDesc,adsPath,cp,urlImage,adsCategori,adsIdCategori));
            transaction.commit();
        }else if (menu.equalsIgnoreCase("searchDetailEvent")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuDetailEvent().newInstance(eventId,"aktivitas"));
            transaction.commit();

        }else if (menu.equalsIgnoreCase("PreviewImage")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuImageOnclick().newInstance(urlBanner));
            transaction.commit();
        }else if (menu.equalsIgnoreCase("Detail Aktivitas")){
            System.out.println("morten :masuk3 "+aktivitasListKegiatan.toString());
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuDetailActivity().newInstance(idAktivitas,aktivitasFarmasi,aktivitasHarga,
                            aktivitasLokasi,aktivitasNama,aktivitasNoHp,aktivitasStatus,aktivitasMessage,aktivitasOngoing,aktivitasPembayaran,
                            aktivitasTitle,aktivitasId,aktivitasListCost,aktivitasListKegiatan));
            transaction.commit();
        }else if (menu.equalsIgnoreCase("Pendaftaran Iklan")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail, new MenuPendaftaranIklan());
            transaction.commit();
        }else if (menu.equalsIgnoreCase("Pendaftaran Event")){
            System.out.println("morten :masuk2");
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuFormJointEvent().newInstance(eventId,eventName));
            transaction.commit();

        }else if (menu.equalsIgnoreCase("Event Registration Fee")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuJoinSelectEvent().newInstance(eventId,
                            eventRs,eventAlamat,eventName,farmasiDaftar,namaDokter,userType,
                            idDokter));
            transaction.commit();

        }else if (menu.equalsIgnoreCase("Event Registration Booth")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuSelectTenant().newInstance(eventId,
                            eventRs,eventAlamat,eventName,farmasiDaftar,namaDokter,userType));
            transaction.commit();

        }else if (menu.equalsIgnoreCase("Pembayaran Event")){
            System.out.println("morten :masuk1 "+listKegiatan.toString());
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuPembayaranEvent().newInstance(eventId,eventName,listKegiatan,idFarmasi,
                            farmasiDaftar,namaDokter,eventRs,eventAlamat,diBayarFarmasi,farmasiName,idDokter));

            transaction.commit();

        }else if (menu.equalsIgnoreCase("filterEvent")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new EventSearchFilter());
            transaction.commit();

        }else if (menu.equalsIgnoreCase("Pembayaran Ads")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuPembayaranAds().newInstance(adsId,adsTitle));
            transaction.commit();

        }else if(menu.equalsIgnoreCase("Pembayaran Ads Lanjut")){
//            FragmentManager manager = getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.replace(R.id.contentFragmentDetail,
//                    new MenuPembayaranAkhir().newInstance("ads",title,totalPembayaran,vaNumber,expTime));
//            transaction.commit();
        }else if(menu.equalsIgnoreCase("Pembayaran Topup Lanjut")){
//            FragmentManager manager = getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            transaction.replace(R.id.contentFragmentDetail,
//                    new MenuPembayaranAkhir().newInstance("topup",title,totalPembayaran,vaNumber,expTime));
//            transaction.commit();

        }else if(menu.equalsIgnoreCase("topup")){
            FragmentManager manager = getSupportFragmentManager();
            FragmentTransaction transaction = manager.beginTransaction();
            transaction.replace(R.id.contentFragmentDetail,
                    new MenuTopupSaldo());
            transaction.commit();
        }else {

        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();


    }

}
