package com.kriyanesia.lupirka.Session;

public class SessionNotif {

    public static final String TABLE_NAME = "notifikasi";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_MESSAGE= "message";
    private String id;
    private String title;
    private String message;


    public SessionNotif(String id, String title, String message) {
        this.id = id;
        this.title = title;
        this.message = message;
    }

    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,"
                    + COLUMN_TITLE + " TEXT,"
                    + COLUMN_MESSAGE+ " TEXT"
                    + ")";


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

}
