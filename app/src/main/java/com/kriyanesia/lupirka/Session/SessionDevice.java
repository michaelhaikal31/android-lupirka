package com.kriyanesia.lupirka.Session;

import android.content.Context;
import android.content.SharedPreferences;

public class SessionDevice {
    public void setImei(Context context, String imei, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("imei", Context.MODE_PRIVATE).edit();
        editor.putString(imei, value);
        editor.commit();

    }

    public  String getImei(Context context, String imei) {

        SharedPreferences prefs = context.getSharedPreferences("imei",	Context.MODE_PRIVATE);
        String i_mei = prefs.getString(imei, "");
        return i_mei;
    }



    public void setTokenDevice(Context context, String token, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("token", Context.MODE_PRIVATE).edit();
        editor.putString(token, value);
        editor.commit();

    }

    public  String getTokenDevice(Context ctx, String token) {
        SharedPreferences prefs = ctx.getSharedPreferences("token",	Context.MODE_PRIVATE);
        String tokenDevice = prefs.getString(token, "");
        return tokenDevice;
    }

    public void setUpdate(Context context, String versionName,String level) {
        SharedPreferences.Editor editor = context.getSharedPreferences("setting", Context.MODE_PRIVATE).edit();
        editor.putString("versionName", versionName);
        editor.putString("updateLevel",level);
        editor.commit();

    }

    public String getVersionName(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences("setting",	Context.MODE_PRIVATE);
        return  prefs.getString("versionName", "");
    }

    public String getUpdateLevel(Context ctx) {
        SharedPreferences prefs = ctx.getSharedPreferences("setting",	Context.MODE_PRIVATE);
        return  prefs.getString("updateLevel", "");
    }



}
