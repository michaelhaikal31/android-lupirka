package com.kriyanesia.lupirka.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.text.format.DateUtils;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.kriyanesia.lupirka.Data.SoalTest.AllSoal;
import com.kriyanesia.lupirka.Data.SoalTest.Soal;

import java.lang.reflect.Type;
import java.sql.SQLOutput;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

public class SessionUser {
    public void setName(Context context, String name, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("name", Context.MODE_PRIVATE).edit();
        editor.putString(name, value);
        editor.commit();

    }

    public  String getName(Context context, String name) {

        SharedPreferences prefs = context.getSharedPreferences("name",	Context.MODE_PRIVATE);
        String nma = prefs.getString(name, "");
        return nma;
    }


    public void setNamaSertifikat(Context context, String namaSertifikat, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("namaSertifikat", Context.MODE_PRIVATE).edit();
        editor.putString(namaSertifikat, value);
        editor.commit();

    }

    public  String getNamaSertifikat(Context context, String namaSertifikat) {

        SharedPreferences prefs = context.getSharedPreferences("namaSertifikat",	Context.MODE_PRIVATE);
        String nma = prefs.getString(namaSertifikat, "");
        return nma;
    }

    public void setNoHp(Context context, String nohp, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("nohp", Context.MODE_PRIVATE).edit();
        editor.putString(nohp, value);
        editor.commit();

    }

    public  String getNoHp(Context ctx, String nohp) {

        SharedPreferences prefs = ctx.getSharedPreferences("nohp",	Context.MODE_PRIVATE);
        String id = prefs.getString(nohp, "");
        return id;
    }

    public void setAlamat(Context context, String alamat, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("alamat", Context.MODE_PRIVATE).edit();
        editor.putString(alamat, value);
        editor.commit();

    }

    public  String getAlamat(Context ctx, String alamat) {

        SharedPreferences prefs = ctx.getSharedPreferences("alamat",	Context.MODE_PRIVATE);
        String data = prefs.getString(alamat, "");
        return data;
    }


    public void setBirthday(Context context, String birthday, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("birthday", Context.MODE_PRIVATE).edit();
        editor.putString(birthday, value);
        editor.commit();

    }

    public  String getKodePos(Context ctx, String kodepos) {

        SharedPreferences prefs = ctx.getSharedPreferences("kodepos",	Context.MODE_PRIVATE);
        String data = prefs.getString(kodepos, "");
        return data;
    }


    public void setKodePos(Context context, String kodepos, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("kodepos", Context.MODE_PRIVATE).edit();
        editor.putString(kodepos, value);
        editor.commit();

    }

    public  String getBirthday(Context ctx, String birthday) {

        SharedPreferences prefs = ctx.getSharedPreferences("birthday",	Context.MODE_PRIVATE);
        String message = prefs.getString(birthday, "");
        return message;
    }
    public void setRole(Context context, String Role, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("role", Context.MODE_PRIVATE).edit();
        editor.putString(Role, value);
        editor.commit();

    }

    public  String getRole(Context ctx, String role) {

        SharedPreferences prefs = ctx.getSharedPreferences("role",	Context.MODE_PRIVATE);
        String message = prefs.getString(role, "");
        return message;
    }
    public void setProfile(Context context, String image, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("image", Context.MODE_PRIVATE).edit();
        editor.putString(image, value);
        editor.commit();

    }

    public  String getProfile(Context ctx, String image) {

        SharedPreferences prefs = ctx.getSharedPreferences("image",	Context.MODE_PRIVATE);
        String message = prefs.getString(image, "");
        return message;
    }
    public void setGender(Context context, String gender, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("gender", Context.MODE_PRIVATE).edit();
        editor.putString(gender, value);
        editor.commit();

    }

    public  String getGender(Context ctx, String nohp) {

        SharedPreferences prefs = ctx.getSharedPreferences("gender",	Context.MODE_PRIVATE);
        String message = prefs.getString(nohp, "");
        return message;
    }
    public void setEmail(Context context, String email, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("email", Context.MODE_PRIVATE).edit();
        editor.putString(email, value);
        editor.commit();

    }

    public  String getEmail(Context ctx, String email) {

        SharedPreferences prefs = ctx.getSharedPreferences("email",	Context.MODE_PRIVATE);
        String data = prefs.getString(email, "");
        return data;
    }
    //data dokter dan mahasiswa
    public void setAlamatRs(Context context, String alamatRs, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("alamatRs", Context.MODE_PRIVATE).edit();
        editor.putString(alamatRs, value);
        editor.commit();

    }

    public  String getAlamatRs(Context ctx, String alamatRs) {

        SharedPreferences prefs = ctx.getSharedPreferences("alamatRs",	Context.MODE_PRIVATE);
        String data = prefs.getString(alamatRs, "");
        return data;
    }
    public void setNamaRs(Context context, String namaRs, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("namaRs", Context.MODE_PRIVATE).edit();
        editor.putString(namaRs, value);
        editor.commit();

    }

    public  String getNamaRs(Context ctx, String namaRs) {

        SharedPreferences prefs = ctx.getSharedPreferences("namaRs",	Context.MODE_PRIVATE);
        String data = prefs.getString(namaRs, "");
        return data;
    }

//    data Farmasi
public void setJabatanFarmasi(Context context, String jabatan, String value) {

    SharedPreferences.Editor editor = context.getSharedPreferences("jabatan", Context.MODE_PRIVATE).edit();
    editor.putString(jabatan, value);
    editor.commit();

}

    public  String getJabatanFarmasi(Context ctx, String jabatan) {

        SharedPreferences prefs = ctx.getSharedPreferences("jabatan",	Context.MODE_PRIVATE);
        String data = prefs.getString(jabatan, "");
        return data;
    }

    public void setPerusahaanFarmasi(Context context, String perusahaanFarmasi, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("perusahaanFarmasi", Context.MODE_PRIVATE).edit();
        editor.putString(perusahaanFarmasi, value);
        editor.commit();

    }

    public  String getPerusahaanFarmasi(Context ctx, String perusahaanFarmasi) {

        SharedPreferences prefs = ctx.getSharedPreferences("perusahaanFarmasi",	Context.MODE_PRIVATE);
        String data = prefs.getString(perusahaanFarmasi, "");
        return data;
    }

    public void setTipeJabatanFarmasi(Context context, String tipeJabatan, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("tipeJabatan", Context.MODE_PRIVATE).edit();
        editor.putString(tipeJabatan, value);
        editor.commit();

    }

    public  String getTipeJabatanFarmasi(Context ctx, String tipeJabatan) {

        SharedPreferences prefs = ctx.getSharedPreferences("tipeJabatan",	Context.MODE_PRIVATE);
        String data = prefs.getString(tipeJabatan, "");
        return data;
    }

    public void setAlamatFarmasi(Context context, String alamat, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("alamat", Context.MODE_PRIVATE).edit();
        editor.putString(alamat, value);
        editor.commit();

    }

    public  String getAlamatFarmasi(Context ctx, String alamat) {

        SharedPreferences prefs = ctx.getSharedPreferences("alamat",	Context.MODE_PRIVATE);
        String data = prefs.getString(alamat, "");
        return data;
    }


    //EVENT TAB
    public void setEventTabIndex(Context context, String index, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("index", Context.MODE_PRIVATE).edit();
        editor.putString(index, value);
        editor.commit();

    }

    public  String getEventTabIndex(Context ctx, String index) {

        SharedPreferences prefs = ctx.getSharedPreferences("index",	Context.MODE_PRIVATE);
        String data = prefs.getString(index, "0");
        return data;
    }

    //filter event
    public void setEventCategory(Context context, String category, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("category", Context.MODE_PRIVATE).edit();
        editor.putString(category, value);
        editor.commit();

    }

    public  String getEventCategory(Context ctx, String category) {

        SharedPreferences prefs = ctx.getSharedPreferences("category",	Context.MODE_PRIVATE);
        String data = prefs.getString(category, "");
        return data;
    }

    public void setEventCategoryName(Context context, String categoryName, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("categoryName", Context.MODE_PRIVATE).edit();
        editor.putString(categoryName, value);
        editor.commit();

    }

    public  String getEventCategoryName(Context ctx, String categoryName) {

        SharedPreferences prefs = ctx.getSharedPreferences("categoryName",	Context.MODE_PRIVATE);
        String data = prefs.getString(categoryName, "Kategori");
        return data;
    }



    public void setEventCity(Context context, String city, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("city", Context.MODE_PRIVATE).edit();
        editor.putString(city, value);
        editor.commit();

    }

    public  String getEventCity(Context ctx, String city) {

        SharedPreferences prefs = ctx.getSharedPreferences("city",	Context.MODE_PRIVATE);
        String data = prefs.getString(city, "");
        return data;
    }

    public void setEventCityName(Context context, String cityName, String value) {

        SharedPreferences.Editor editor = context.getSharedPreferences("cityName", Context.MODE_PRIVATE).edit();
        editor.putString(cityName, value);
        editor.commit();

    }

    public  String getEventCityName(Context ctx, String cityName) {

        SharedPreferences prefs = ctx.getSharedPreferences("cityName",	Context.MODE_PRIVATE);
        String data = prefs.getString(cityName, "Kota");
        return data;
    }

    public void setPengerjaanTes(Context context, AllSoal soal, int durasi) {
        SharedPreferences.Editor editor = context.getSharedPreferences("pengerjaan", Context.MODE_PRIVATE).edit();
        Gson gson = new Gson();
        String json = gson.toJson(soal);
        editor.putString("soal", json);
        if (durasi != 0){
            Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("EEE MMM d hh:mm:ss z yyyy");
        dateFormat.setTimeZone(TimeZone.getTimeZone("GMT"));
        editor.putString("startTime", dateFormat.format(cal.getTime()));
        Date temp = new Date(System.currentTimeMillis() + durasi * 60 * 1000);
        editor.putString("endTime", temp.toString());
        }
    editor.commit();
    }

    public AllSoal getSoalPengerjaan(Context context){
        AllSoal soal = new AllSoal();
        try {
            SharedPreferences editor = context.getSharedPreferences("pengerjaan", Context.MODE_PRIVATE);
            Gson gson = new Gson();
            String json = editor.getString("soal", "");
            if (json.isEmpty()) {
                soal = new AllSoal();
            } else {
                Type type = new TypeToken<AllSoal>() {
                }.getType();
                soal = gson.fromJson(json, type);
            }
        }catch(Exception e){
            System.out.println("asd catch "+e.toString());
        }

        return soal;
    }

    public Long getTimeLeft(Context context){
        SharedPreferences prefs = context.getSharedPreferences("pengerjaan",	Context.MODE_PRIVATE);
        long timeLeft=4444444l;
        try {
            SimpleDateFormat dateFormat= new SimpleDateFormat("EEE MMM dd HH:mm:ss zzz yyyy");
            Date endTime = dateFormat.parse(prefs.getString("endTime",""));
            Date now = new Date();
            timeLeft = endTime.getTime()-now.getTime();

            //String now = dateFormat.format(Calendar.getInstance().getTime());
            //System.out.println("asd now "+now);


        }catch (Exception e){
            System.out.println("asd "+e.toString());
        }
        return timeLeft;
    }

}
