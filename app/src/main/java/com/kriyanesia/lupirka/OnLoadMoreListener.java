package com.kriyanesia.lupirka;

public interface OnLoadMoreListener {
    void onLoadMore();
}
