package com.kriyanesia.lupirka.Data.Aktivitas;

public class ValueAktivitas {
    String message;
    String status;
    AllAktivitas data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllAktivitas getData() {
        return data;
    }
}
