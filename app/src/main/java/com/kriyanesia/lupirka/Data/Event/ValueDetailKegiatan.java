package com.kriyanesia.lupirka.Data.Event;

import java.util.List;

public class ValueDetailKegiatan {
    String message;
    String status;
    AllEventPaket data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllEventPaket getData() {
        return data;
    }
}
