package com.kriyanesia.lupirka.Data.Pembayaran;

import java.util.List;

public class ValuePembayaran {
    String message;
    String status;
    Data data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public class Data{
        List<Payment> payment;
        Price price;

        public List<Payment> getPayment() {
            return payment;
        }

        public Price getPrice() {
            return price;
        }
    }
    public class Payment{
        private String data;
        private String tipe;

        public String getData() {
            return data;
        }

        public String getTipe() {
            return tipe;
        }
    }

    public static class Price{
        private String total;
        private String total_hari;
        private String harga;
        private String admin;
        private String diskon;

        public String getTotal() {
            return total;
        }

        public String getTotal_hari() {
            return total_hari;
        }

        public String getHarga() {
            return harga;
        }

        public String getAdmin() {
            return admin;
        }

        public String getDiskon() {
            return diskon;
        }
    }



}
