package com.kriyanesia.lupirka.Data.EO;

public class ValueEO {
    String message;
    String status;
    AllEO data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllEO getData() {
        return data;
    }
}
