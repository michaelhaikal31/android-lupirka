package com.kriyanesia.lupirka.Data.Ads;

public class ValueResponRegisAds {
    String message;
    String status;
    Data data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Data getData() {
        return data;
    }

    public class Data{
        private String adsId;

        public String getAdsId() {
            return adsId;
        }
    }
}
