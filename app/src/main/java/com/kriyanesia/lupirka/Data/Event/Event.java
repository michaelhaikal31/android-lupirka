package com.kriyanesia.lupirka.Data.Event;

import java.util.List;

public class Event {
    private String title;
    private String id;
    private String start;
    private String end;
    private String venue;
    private String image;
    private String attachment;
    private String desc;
    private String kota;
    private String city;
    private String kategori;
    private String contact_name;
    private List<EventDetail> detail_event;


    public String getCity() {
        return city;
    }

    public String getKategori() {
        return kategori;
    }

    public String getContact_name() {
        return contact_name;
    }

    public String getKota() {
        return kota;
    }

    public String getTitle() {
        return title;
    }

    public String getId() {
        return id;
    }

    public String getStart() {
        return start;
    }

    public String getEnd() {
        return end;
    }

    public String getVenue() {
        return venue;
    }

    public String getImage() {
        return image;
    }

    public String getAttachment() {
        return attachment;
    }

    public String getDesc() {
        return desc;
    }

    public List<EventDetail> getDetail_event() {
        return detail_event;
    }

    public class EventDetail{
        private String name;
        private String start;
        private String end;

        public String getName() {
            return name;
        }

        public String getStart() {
            return start;
        }

        public String getEnd() {
            return end;
        }
    }
}
