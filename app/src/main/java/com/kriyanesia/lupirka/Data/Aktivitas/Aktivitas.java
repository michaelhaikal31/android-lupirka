package com.kriyanesia.lupirka.Data.Aktivitas;

public class Aktivitas {
    private String id_activity;
    private String title;
    private String tanggal;
    private String ongoing;

    public String getOngoing() {
        return ongoing;
    }

    public String getId_activity() {
        return id_activity;
    }

    public String getTitle() {
        return title;
    }

    public String getTanggal() {
        return tanggal;
    }

}
