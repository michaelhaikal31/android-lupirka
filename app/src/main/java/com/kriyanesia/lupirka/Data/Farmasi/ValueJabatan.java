package com.kriyanesia.lupirka.Data.Farmasi;

public class ValueJabatan {
    String message;
    String status;
    AllPosition data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllPosition getData() {
        return data;
    }
}
