package com.kriyanesia.lupirka.Data;

public class Tenant {

    private String idTenan;
    private String namaTenant;
    private String keteranganTenant;
    private String iconTenant;

    public Tenant() {
    }

    public Tenant(String idTenan, String namaTenant, String keteranganTenant, String iconTenant) {
        this.idTenan = idTenan;
        this.namaTenant = namaTenant;
        this.keteranganTenant = keteranganTenant;
        this.iconTenant = iconTenant;
    }

    public String getIdTenan() {
        return idTenan;
    }

    public void setIdTenan(String idTenan) {
        this.idTenan = idTenan;
    }

    public String getNamaTenant() {
        return namaTenant;
    }

    public void setNamaTenant(String namaTenant) {
        this.namaTenant = namaTenant;
    }

    public String getKeteranganTenant() {
        return keteranganTenant;
    }

    public void setKeteranganTenant(String keteranganTenant) {
        this.keteranganTenant = keteranganTenant;
    }

    public String getIconTenant() {
        return iconTenant;
    }

    public void setIconTenant(String iconTenant) {
        this.iconTenant = iconTenant;
    }
}