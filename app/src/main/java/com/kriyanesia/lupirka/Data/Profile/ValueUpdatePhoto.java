package com.kriyanesia.lupirka.Data.Profile;

public class ValueUpdatePhoto {
    String message;
    String status;
    Data data;

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }



    public class Data{
        String image;
        public String getImage() {
            return image;
        }
    }
}
