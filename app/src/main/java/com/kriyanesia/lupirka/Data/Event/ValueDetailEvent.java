package com.kriyanesia.lupirka.Data.Event;

import java.util.List;

public class ValueDetailEvent {
    String message;
    String status;
    Event data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public Event getData() {
        return data;
    }
}
