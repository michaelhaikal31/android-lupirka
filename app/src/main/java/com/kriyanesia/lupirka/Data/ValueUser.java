package com.kriyanesia.lupirka.Data;

import java.util.List;

public class ValueUser {
    String message;
    String status;
    User data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public User getData() {
        return data;
    }
}
