package com.kriyanesia.lupirka.Data.TableRegistrasiEvent;

/**
 * Created by ferna on 5/7/2018.
 */

public class WaktuEvent {
    private String name;
    private String harga;

    public WaktuEvent() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }
}
