package com.kriyanesia.lupirka.Data.Universitas;

public class Universitas {
    private String name;
    private String provinsi;
    private String kota;
    private String id;

    public Universitas(String name, String provinsi, String kota, String id) {
        this.name = name;
        this.provinsi = provinsi;
        this.kota = kota;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public String getKota() {
        return kota;
    }

    public String getId() {
        return id;
    }
}
