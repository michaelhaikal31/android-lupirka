package com.kriyanesia.lupirka.Data.Ads;

import java.util.List;

public class ValueCategoriAds {
    private String status;
    private String message;
    private Data data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data{

        private List<Categori> category;

        public List<Categori> getCategory() {
            return category;
        }

    }

    public class Categori{
        private String name;
        private String id;

        public String getName() {
            return name;
        }

        public String getId() {
            return id;
        }
    }
}
