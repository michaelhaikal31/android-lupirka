package com.kriyanesia.lupirka.Data.Ads;

public class ValueDetailAds {
    String message;
    String status;
    DetailAds data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public DetailAds getData() {
        return data;
    }
}
