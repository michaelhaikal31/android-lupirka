package com.kriyanesia.lupirka.Data.Ads;

public class Ads {
    private String note;
    private String endDAte;
    private String idAds;
    private String adsSubTitle;
    private String cp;
    private String adsTitle;
    private String categoryName;
    private String startDate;
    private String urlImage;
    private String status;


    public String getCp() {
        return cp;
    }

    public String getNote() {
        return note;
    }

    public String getEndDAte() {
        return endDAte;
    }

    public String getIdAds() {
        return idAds;
    }

    public String getAdsSubTitle() {
        return adsSubTitle;
    }

    public String getAdsTitle() {
        return adsTitle;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public String getStatus() {
        return status;
    }


}
