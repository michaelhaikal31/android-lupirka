package com.kriyanesia.lupirka.Data.Spesialis;

public class Spesialis {
    private String name;
    private String id;
    private String gelar;


    public Spesialis(String name, String id, String gelar) {
        this.name = name;
        this.id = id;
        this.gelar = gelar;
    }

    public String getName() {
        return name;
    }

    public String getId() {
        return id;
    }

    public String getGelar() {
        return gelar;
    }
}
