package com.kriyanesia.lupirka.Data.Event;

public class Paket {
    private String kategori_peserta;
    private String harga;
    private String kegiatan;
    private String quota;
    private String start;
    private String kategori_harga;
    private String end;
    private String id;
    private String aktifitas;
    private int position;

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public String getKategori_peserta() {
        return kategori_peserta;
    }

    public String getHarga() {
        return harga;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public String getQuota() {
        return quota;
    }

    public String getStart() {
        return start;
    }

    public String getKategori_harga() {
        return kategori_harga;
    }

    public String getEnd() {
        return end;
    }

    public String getId() {
        return id;
    }

    public String getAktifitas() {
        return aktifitas;
    }
}
