package com.kriyanesia.lupirka.Data.Spesialis;

public class ValueSpesialis {
    String message;
    String status;
    AllSpesialis data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllSpesialis getData() {
        return data;
    }
}
