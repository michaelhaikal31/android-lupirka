package com.kriyanesia.lupirka.Data;

public class HomePost {
    String icon;
    String desc;

    public HomePost(String icon, String desc) {
        this.icon = icon;
        this.desc = desc;
    }

    public String getIcon() {
        return icon;
    }

    public void setIcon(String icon) {
        this.icon = icon;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
