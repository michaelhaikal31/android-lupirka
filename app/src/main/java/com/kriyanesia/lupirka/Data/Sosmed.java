package com.kriyanesia.lupirka.Data;

public class Sosmed {
    private String name;
    private int image;

    public Sosmed(String name, int image) {
        this.name = name;
        this.image = image;
    }

    public String getName() {
        return name;
    }

    public int getImage() {
        return image;
    }
}
