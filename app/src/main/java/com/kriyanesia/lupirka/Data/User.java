package com.kriyanesia.lupirka.Data;

public class User {
    String name;
    String phone_number;
    String birthday;
    String image;
    String role;
    String gender;
    String email;
    String address;
    String kodepos;
    String rumah_sakit;
    String alamat_rumah_sakit;
    String nama_sertifikat;
    String spesialis;

    public String getSpesialis() {
        return spesialis;
    }

    public String getNama_sertifikat() {
        return nama_sertifikat;
    }

    public String getName() {
        return name;
    }

    public String getPhone_number() {
        return phone_number;
    }


    public String getBirthday() {
        return birthday;
    }

    public String getImage() {
        return image;
    }

    public String getRole() {
        return role;
    }

    public String getGender() {
        return gender;
    }

    public String getEmail() {
        return email;
    }

    public String getAddress() {
        return address;
    }

    public String getKodepos() {
        return kodepos;
    }

    public String getRumah_sakit() {
        return rumah_sakit;
    }

    public String getAlamat_rumah_sakit() {
        return alamat_rumah_sakit;
    }
}
