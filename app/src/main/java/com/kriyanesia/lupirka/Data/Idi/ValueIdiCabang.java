package com.kriyanesia.lupirka.Data.Idi;

public class ValueIdiCabang {
    String message;
    String status;
    AllIdiCabang data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllIdiCabang getData() {
        return data;
    }
}
