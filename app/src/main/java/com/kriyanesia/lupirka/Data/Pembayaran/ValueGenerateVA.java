package com.kriyanesia.lupirka.Data.Pembayaran;

public class ValueGenerateVA {
    String message;
    String status;
    VA data;

    public VA getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public class VA{
        private String va_number;
        private String exp_time;
        private String amount;

        public String getVa_number() {
            return va_number;
        }

        public String getExp_time() {
            return exp_time;
        }

        public String getAmount() {
            return amount;
        }
    }
}
