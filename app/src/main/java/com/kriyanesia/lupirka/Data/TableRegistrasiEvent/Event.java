package com.kriyanesia.lupirka.Data.TableRegistrasiEvent;

import java.util.ArrayList;

/**
 * Created by ferna on 5/7/2018.
 */

public class Event {
    private String namaEvent;
    private String keterangan;
    ArrayList<WaktuEvent> waktuEvent;

    public Event() {
    }

    public String getNamaEvent() {
        return namaEvent;
    }

    public void setNamaEvent(String namaEvent) {
        this.namaEvent = namaEvent;
    }

    public String getKeterangan() {
        return keterangan;
    }

    public void setKeterangan(String keterangan) {
        this.keterangan = keterangan;
    }

    public ArrayList<WaktuEvent> getWaktuEvent() {
        return waktuEvent;
    }

    public void setWaktuEvent(ArrayList<WaktuEvent> waktuEvent) {
        this.waktuEvent = waktuEvent;
    }
}
