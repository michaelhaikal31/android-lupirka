package com.kriyanesia.lupirka.Data.Event;

public class ValueEvent {
    String message;
    String status;
    AllEvents data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllEvents getData() {
        return data;
    }
}
