package com.kriyanesia.lupirka.Data.SoalTest;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AllSoal implements Serializable {
   List<Soal> list_soal;
   String duration;
   String jenis;


    public AllSoal() {
        this.list_soal = new ArrayList<>();
    }



    public String getJenis() {
        return jenis;
    }

    public String getDuration() {
        return duration;
    }

    public List<Soal> getListSoal() {
        return list_soal;
    }

    public void setList_soal(List<Soal> list_soal) {
        this.list_soal = list_soal;
    }

    @Override
    public String toString() {
        return "AllSoal{" +
                "list_soal=" + list_soal +
                '}';
    }
}
