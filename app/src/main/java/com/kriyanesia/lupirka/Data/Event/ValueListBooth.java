package com.kriyanesia.lupirka.Data.Event;

import java.util.List;

public class ValueListBooth {
    private String message;
    private String status;
    private Data data;

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }


    public class Data{

        private List<Booth> list;

        public List<Booth> getList() {
            return list;
        }
    }

    public class Booth{
        private String kode_tenant;
        private String image;
        private String harga;
        private String id;
        private String kuota;
        private String note;
        private String status;

        public String getKode_tenant() {
            return kode_tenant;
        }

        public String getImage() {
            return image;
        }

        public String getHarga() {
            return harga;
        }

        public String getId() {
            return id;
        }

        public String getKuota() {
            return kuota;
        }

        public String getNote() {
            return note;
        }

        public String getStatus() {
            return status;
        }
    }
}
