package com.kriyanesia.lupirka.Data.Profesi;

import java.util.List;

public class ValueProfesi {
    String message;
    String status;
    AllProfesi data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllProfesi getData() {
        return data;
    }
}
