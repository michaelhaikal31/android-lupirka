package com.kriyanesia.lupirka.Data.Pembayaran;

import java.util.List;

public class ValueMetodeBayar {
    String message;
    String status;
    List<Payment> data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public List<Payment> getData() {
        return data;
    }

    public class Payment{
        String data;
        String tipe;

        public String getData() {
            return data;
        }

        public String getTipe() {
            return tipe;
        }
    }
}
