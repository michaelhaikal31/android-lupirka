package com.kriyanesia.lupirka.Data.Event;

import java.io.Serializable;

public class PaketTemp implements Serializable{
    private String kategori_peserta;
    private String harga;
    private String kegiatan;
    private String quota;
    private String start;
    private String kategori_harga;
    private String end;
    private String id;
    private String aktifitas;
    private boolean statusChecked;


    public String getKategori_peserta() {
        return kategori_peserta;
    }

    public void setKategori_peserta(String kategori_peserta) {
        this.kategori_peserta = kategori_peserta;
    }

    public String getHarga() {
        return harga;
    }

    public void setHarga(String harga) {
        this.harga = harga;
    }

    public String getKegiatan() {
        return kegiatan;
    }

    public void setKegiatan(String kegiatan) {
        this.kegiatan = kegiatan;
    }

    public String getQuota() {
        return quota;
    }

    public void setQuota(String quota) {
        this.quota = quota;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getKategori_harga() {
        return kategori_harga;
    }

    public void setKategori_harga(String kategori_harga) {
        this.kategori_harga = kategori_harga;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAktifitas() {
        return aktifitas;
    }

    public void setAktifitas(String aktifitas) {
        this.aktifitas = aktifitas;
    }

    public boolean isStatusChecked() {
        return statusChecked;
    }

    public void setStatusChecked(boolean statusChecked) {
        this.statusChecked = statusChecked;
    }

    @Override
    public String toString() {
        return "PaketTemp{" +
                "kategori_peserta='" + kategori_peserta + '\'' +
                ", harga='" + harga + '\'' +
                ", kegiatan='" + kegiatan + '\'' +
                ", quota='" + quota + '\'' +
                ", start='" + start + '\'' +
                ", kategori_harga='" + kategori_harga + '\'' +
                ", end='" + end + '\'' +
                ", id='" + id + '\'' +
                ", aktifitas='" + aktifitas + '\'' +
                ", statusChecked=" + statusChecked +
                '}';
    }
}
