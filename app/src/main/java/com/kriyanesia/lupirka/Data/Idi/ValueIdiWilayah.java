package com.kriyanesia.lupirka.Data.Idi;

import com.kriyanesia.lupirka.Data.Hospital.AllHospital;

public class ValueIdiWilayah {
    String message;
    String status;
    AllIdiWilayah data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllIdiWilayah getData() {
        return data;
    }
}
