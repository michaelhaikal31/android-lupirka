package com.kriyanesia.lupirka.Data.Farmasi;

public class ValueFarmasi {
    String message;
    String status;
    AllFarmasi data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllFarmasi getData() {
        return data;
    }
}
