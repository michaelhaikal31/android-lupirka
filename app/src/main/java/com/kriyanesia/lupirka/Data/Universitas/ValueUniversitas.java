package com.kriyanesia.lupirka.Data.Universitas;

public class ValueUniversitas {
    String message;
    String status;
    AllUniversitas data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllUniversitas getData() {
        return data;
    }
}
