package com.kriyanesia.lupirka.Data.Ads;

public class ValueAllAds {
    String message;
    String status;
    AllAds data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllAds getData() {
        return data;
    }
}
