package com.kriyanesia.lupirka.Data;

import java.util.List;

public class ValueKota {
    private Data data;
    private String message;
    private String status;


    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class Data{
        private List<Kota> list;

        public List<Kota> getList() {
            return list;
        }
    }

    public class Kota{
        private String id;
        private String kota;

        public String getId() {
            return id;
        }

        public String getKota() {
            return kota;
        }
    }
}
