package com.kriyanesia.lupirka.Data.Hospital;

public class ValueHospital {
    String message;
    String status;
    AllHospital data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllHospital getData() {
        return data;
    }
}
