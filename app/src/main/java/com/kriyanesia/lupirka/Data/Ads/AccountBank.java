package com.kriyanesia.lupirka.Data.Ads;

public class AccountBank {
    private int drawable;
    private String accountName;

    public int getDrawable() {
        return drawable;
    }

    public void setDrawable(int drawable) {
        this.drawable = drawable;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }
}
