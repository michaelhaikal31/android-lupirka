package com.kriyanesia.lupirka.Data.Ads;

import java.util.List;

public class AllAds {
    ArrayAds otherAds;
    ArrayAds myAds;

    public ArrayAds getOtherAds() {
        return otherAds;
    }

    public ArrayAds getMyAds() {
        return myAds;
    }


    public class ArrayAds{
        private int jumlah;
        List<Ads> content;

        public int getJumlah() {
            return jumlah;
        }

        public List<Ads> getContent() {
            return content;
        }
    }
}
