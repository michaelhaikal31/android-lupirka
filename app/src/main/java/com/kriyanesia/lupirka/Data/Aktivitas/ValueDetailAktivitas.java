package com.kriyanesia.lupirka.Data.Aktivitas;

import java.io.Serializable;
import java.util.List;

public class ValueDetailAktivitas {
    DetailAktivitas data;
    private String status;
    private String message;

    public DetailAktivitas getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class DetailAktivitas{
        private String harga;
        private String nama;
        private String lokasi;
        private String farmasi;
        private String nomor_hp;
        private String status;
        private String status_event;
        private String pembayaran;
        private String title;
        private String id_event;
        private String status_message;


        private List<DetailKegiatan> detail_kegiatan;

        private List<DetailCost> detail_cost;

        private List<BankData> bank_data;

        public String getId_event() {
            return id_event;
        }

        public List<DetailKegiatan> getDetail_kegiatan() {
            return detail_kegiatan;
        }

        public List<DetailCost> getDetail_cost() {
            return detail_cost;
        }

        public List<BankData> getBank_data() {
            return bank_data;
        }

        public String getTitle() {
            return title;
        }

        public String getPembayaran() {
            return pembayaran;
        }

        public String getStatus_event() {
            return status_event;
        }

        public String getHarga() {
            return harga;
        }

        public String getNama() {
            return nama;
        }

        public String getLokasi() {
            return lokasi;
        }

        public String getFarmasi() {
            return farmasi;
        }

        public String getNomor_hp() {
            return nomor_hp;
        }

        public String getStatus() {
            return status;
        }

        public String getStatus_message() {
            return status_message;
        }

        @Override
        public String toString() {
            return "DetailAktivitas{" +
                    "harga='" + harga + '\'' +
                    ", nama='" + nama + '\'' +
                    ", lokasi='" + lokasi + '\'' +
                    ", farmasi='" + farmasi + '\'' +
                    ", nomor_hp='" + nomor_hp + '\'' +
                    ", status='" + status + '\'' +
                    ", status_event='" + status_event + '\'' +
                    ", pembayaran='" + pembayaran + '\'' +
                    ", title='" + title + '\'' +
                    ", id_event='" + id_event + '\'' +
                    ", status_message='" + status_message + '\'' +
                    ", detail_kegiatan=" + detail_kegiatan +
                    ", detail_cost=" + detail_cost +
                    ", bank_data=" + bank_data +
                    '}';
        }
    }

    public class DetailKegiatan{
        private String enddate;
        private String kegiatan;
        private String starddate;
        private String detail;

        public String getEnddate() {
            return enddate;
        }

        public String getKegiatan() {
            return kegiatan;
        }

        public String getStarddate() {
            return starddate;
        }

        public String getDetail() {
            return detail;
        }
    }

    public static class DetailKegiatanTemp implements Serializable{
        private String enddate;
        private String kegiatan;
        private String starddate;
        private String detail;

        public String getEnddate() {
            return enddate;
        }

        public void setEnddate(String enddate) {
            this.enddate = enddate;
        }

        public String getKegiatan() {
            return kegiatan;
        }

        public void setKegiatan(String kegiatan) {
            this.kegiatan = kegiatan;
        }

        public String getStarddate() {
            return starddate;
        }

        public void setStarddate(String starddate) {
            this.starddate = starddate;
        }

        public String getDetail() {
            return detail;
        }

        public void setDetail(String detail) {
            this.detail = detail;
        }
    }


    public class DetailCost{
        private String biaya;
        private String paket;

        public String getBiaya() {
            return biaya;
        }

        public String getPaket() {
            return paket;
        }
    }



    public static class DetailCostTemp implements Serializable {
        private String biaya;
        private String paket;
        private String tipe_bayar;

        public String getTipe_bayar() {
            return tipe_bayar;
        }

        public String getBiaya() {
            return biaya;
        }

        public void setBiaya(String biaya) {
            this.biaya = biaya;
        }

        public String getPaket() {
            return paket;
        }

        public void setPaket(String paket) {
            this.paket = paket;
        }
    }

    public class BankData{
       private String cabang;
       private String nama_akun;
        private String payment_id;
        private String no_rekening;
        private String nama_bank;
        private String exp_time;
        private String url_logo_bank;

        public String getCabang() {
            return cabang;
        }

        public void setCabang(String cabang) {
            this.cabang = cabang;
        }

        public String getNama_akun() {
            return nama_akun;
        }

        public void setNama_akun(String nama_akun) {
            this.nama_akun = nama_akun;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getNo_rekening() {
            return no_rekening;
        }

        public void setNo_rekening(String no_rekening) {
            this.no_rekening = no_rekening;
        }

        public String getNama_bank() {
            return nama_bank;
        }

        public void setNama_bank(String nama_bank) {
            this.nama_bank = nama_bank;
        }

        public String getExp_time() {
            return exp_time;
        }

        public void setExp_time(String exp_time) {
            this.exp_time = exp_time;
        }

        public String getUrl_logo_bank() {
            return url_logo_bank;
        }

        public void setUrl_logo_bank(String url_logo_bank) {
            this.url_logo_bank = url_logo_bank;
        }
    }



    public static class BankDataTemp implements Serializable{
        private String cabang;
        private String nama_akun;
        private String payment_id;
        private String no_rekening;
        private String nama_bank;
        private String exp_time;
        private String url_logo_bank;

        public String getCabang() {
            return cabang;
        }

        public void setCabang(String cabang) {
            this.cabang = cabang;
        }

        public String getNama_akun() {
            return nama_akun;
        }

        public void setNama_akun(String nama_akun) {
            this.nama_akun = nama_akun;
        }

        public String getPayment_id() {
            return payment_id;
        }

        public void setPayment_id(String payment_id) {
            this.payment_id = payment_id;
        }

        public String getNo_rekening() {
            return no_rekening;
        }

        public void setNo_rekening(String no_rekening) {
            this.no_rekening = no_rekening;
        }

        public String getNama_bank() {
            return nama_bank;
        }

        public void setNama_bank(String nama_bank) {
            this.nama_bank = nama_bank;
        }

        public String getExp_time() {
            return exp_time;
        }

        public void setExp_time(String exp_time) {
            this.exp_time = exp_time;
        }

        public String getUrl_logo_bank() {
            return url_logo_bank;
        }

        public void setUrl_logo_bank(String url_logo_bank) {
            this.url_logo_bank = url_logo_bank;
        }
    }

    public static class DetailAktivitasTemp{
        private String harga;
        private String nama;
        private String lokasi;
        private String farmasi;
        private String nomor_hp;
        private String status;
        private String status_event;
        private String pembayaran;
        private String status_message;
        private String title;
        private String id_event;
        private List<DetailCostTemp> detail_cost;
        private List<BankData> bank_data;


        public List<BankData> getDetail_pembayaran() {
            return bank_data;
        }

        public void setDetail_pembayaran(List<BankData> detail_pembayaran) {
            this.bank_data = detail_pembayaran;
        }

        public String getId_event() {
            return id_event;
        }

        public void setId_event(String id_event) {
            this.id_event = id_event;
        }

        public List<DetailCostTemp> getDetail_cost() {
            return detail_cost;
        }

        public void setDetail_cost(List<DetailCostTemp> detail_cost) {
            this.detail_cost = detail_cost;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getPembayaran() {
            return pembayaran;
        }

        public void setPembayaran(String pembayaran) {
            this.pembayaran = pembayaran;
        }

        public String getStatus_event() {
            return status_event;
        }

        public void setStatus_event(String status_event) {
            this.status_event = status_event;
        }

        public String getHarga() {
            return harga;
        }

        public void setHarga(String harga) {
            this.harga = harga;
        }

        public String getNama() {
            return nama;
        }

        public void setNama(String nama) {
            this.nama = nama;
        }

        public String getLokasi() {
            return lokasi;
        }

        public void setLokasi(String lokasi) {
            this.lokasi = lokasi;
        }

        public String getFarmasi() {
            return farmasi;
        }

        public void setFarmasi(String farmasi) {
            this.farmasi = farmasi;
        }

        public String getNomor_hp() {
            return nomor_hp;
        }

        public void setNomor_hp(String nomor_hp) {
            this.nomor_hp = nomor_hp;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getStatus_message() {
            return status_message;
        }

        public void setStatus_message(String status_message) {
            this.status_message = status_message;
        }
    }
}
