package com.kriyanesia.lupirka.Data;

import java.util.List;

public class ValueContactUs {
    private String message;
    private String status;
    Data data;

    public Data getData() {
        return data;
    }

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public class Data{

        List<ContactUs> list_contact;
        List<ContactBank> list_bank;
        public List<ContactUs> getList_contact() {
            return list_contact;
        }

        public List<ContactBank> getList_bank() {
            return list_bank;
        }
    }

    public class ContactUs{
        private String nama;
        private String phone;
        private String posisi;
        private String email;

        public String getNama() {
            return nama;
        }

        public String getPhone() {
            return phone;
        }

        public String getPosisi() {
            return posisi;
        }

        public String getEmail() {
            return email;
        }
    }

    public class ContactBank{
        private String no_rekening;
        private String nama_bank;
        private String atas_nama;

        public String getNo_rekening() {
            return no_rekening;
        }

        public String getNama_bank() {
            return nama_bank;
        }

        public String getAtas_nama() {
            return atas_nama;
        }
    }
}
