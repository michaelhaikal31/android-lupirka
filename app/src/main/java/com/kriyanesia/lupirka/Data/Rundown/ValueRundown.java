package com.kriyanesia.lupirka.Data.Rundown;
public class ValueRundown {
    String message;
    String status;
    AllRundown data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllRundown getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ValueRundown{" +
                "data=" + data +
                '}';
    }
}
