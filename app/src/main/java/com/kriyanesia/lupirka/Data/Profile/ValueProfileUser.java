package com.kriyanesia.lupirka.Data.Profile;

public class ValueProfileUser {
    private String status;
    private String message;
    Data data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public Data getData() {
        return data;
    }

    public class Data{
        //umum
        private String birthday;
        private String address;
        private String gender;
        private String phone;
        private String name;
        private String email;


        //farmasi
        private String jabatan;
        private String nama_perusahaan_others;
        private String tipe_jabatan;
        private String alamat_kantor;
//        private String nama_perusahaan;
        private String id_perusahaan;



        //dokter
        private String gelar;
        private String cabang;
        private String spesialis;
        private String wilayah;
        private String spesialis_id;
        private String cabang_id;
        private String gelar_belakang;

        //resident
        private String rumahsakit_id;
//        private String gelar_belakang;
        //mahasiswa
        private String unversitas;
//        private String gelar;
        private String unversitas_id;

        //other
        private String nama_perusahaan;
//        private String alamat_kantor;


        //getter


        public String getGelar_belakang() {
            return gelar_belakang;
        }

        public String getRumahsakit_id() {
            return rumahsakit_id;
        }

        public String getId_perusahaan() {
            return id_perusahaan;
        }

        public String getSpesialis_id() {
            return spesialis_id;
        }

        public String getCabang_id() {
            return cabang_id;
        }

        public String getUnversitas_id() {
            return unversitas_id;
        }

        public String getBirthday() {
            return birthday;
        }

        public String getAddress() {
            return address;
        }

        public String getGender() {
            return gender;
        }

        public String getPhone() {
            return phone;
        }

        public String getGelar() {
            return gelar;
        }

        public String getName() {
            return name;
        }

        public String getEmail() {
            return email;
        }

        public String getJabatan() {
            return jabatan;
        }

        public String getNama_perusahaan_others() {
            return nama_perusahaan_others;
        }

        public String getTipe_jabatan() {
            return tipe_jabatan;
        }

        public String getAlamat_kantor() {
            return alamat_kantor;
        }

        public String getCabang() {
            return cabang;
        }

        public String getSpesialis() {
            return spesialis;
        }

        public String getWilayah() {
            return wilayah;
        }

        public String getUnversitas() {
            return unversitas;
        }

        public String getNama_perusahaan() {
            return nama_perusahaan;
        }
    }
}
