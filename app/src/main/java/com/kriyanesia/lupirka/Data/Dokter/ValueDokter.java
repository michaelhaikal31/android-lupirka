package com.kriyanesia.lupirka.Data.Dokter;

public class ValueDokter {
    private String status;
    private String message;
    private AllDokter data;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public AllDokter getData() {
        return data;
    }
}
