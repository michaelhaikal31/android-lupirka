package com.kriyanesia.lupirka.Data.SoalTest;

public class Soal {
    private String id_soal;
    private String soal;
    private String pilihan;
    private String answer;
    private String user_answer;

    public Soal() {
    }

    public String getId_soal() {
        return id_soal;
    }

    public String getSoal() {
        return soal;
    }

    public String getPilihan() {
        return pilihan;
    }

    public String getQuestion_answer() {
        return answer;
    }

    public String getUser_answer() {
        return user_answer;
    }

    public void setUser_answer(String user_answer) {
        this.user_answer = user_answer;
    }

    @Override
    public String toString() {
        return "Soal{" +
                "id_soal='" + id_soal + '\'' +
                ", soal='" + soal + '\'' +
                '}';
    }
}
