package com.kriyanesia.lupirka.Data.Event;

public class ValueEventList {
    String message;
    String status;
    AllEventList data;

    public String getMessage() {
        return message;
    }

    public String getStatus() {
        return status;
    }

    public AllEventList getData() {
        return data;
    }
}
