package com.kriyanesia.lupirka.Data.Rundown;



public class Rundown {
    public String id;
    public String date;
    public String startTime;
    public String endTime;
    public String event;
    public String description;
    public String id_event_cost;


    public Rundown() {
    }

    public Rundown(String id, String date, String startTime, String endTime, String event, String description,String id_event_cost) {
        this.id = id;
        this.date = date;
        this.startTime = startTime;
        this.endTime = endTime;
        this.event = event;
        this.description = description;
        this.id_event_cost = id_event_cost;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public String getEndTime() {
        return endTime;
    }

    public void setEndTime(String endTime) {
        this.endTime = endTime;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String descripstion) {
        this.description = descripstion;
    }

    public String getId_event_cost() {
        return id_event_cost;
    }

    @Override
    public String toString() {
        return date.substring(0,date.indexOf("/"));
    }
}
