package com.kriyanesia.lupirka.Data.Event;

public class ValueScanBarcode {
    private String status;
    private String message;

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }
}
