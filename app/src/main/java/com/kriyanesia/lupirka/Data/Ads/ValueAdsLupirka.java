package com.kriyanesia.lupirka.Data.Ads;

import java.util.List;

public class ValueAdsLupirka {
    Data data;
    private String status;
    private String message;

    public Data getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public class Data{
        LupirkaAds lupirkaAds;

        public LupirkaAds getLupirkaAds() {
            return lupirkaAds;
        }
    }

    public class LupirkaAds{
        private int jumlah;
        List<Ads> content;

        public int getJumlah() {
            return jumlah;
        }

        public List<Ads> getContent() {
            return content;
        }
    }


}
