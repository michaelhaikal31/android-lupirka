package com.kriyanesia.lupirka.Data.Dokter;

public class Dokter {
    private String nama;
    private String spesialis;
    private int id_dokter;

    public String getNama() {
        return nama;
    }

    public String getSpesialis() {
        return spesialis;
    }

    public int getId_dokter() {
        return id_dokter;
    }
}
