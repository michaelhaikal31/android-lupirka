package com.kriyanesia.lupirka.Data.Event;

public class ValueEventBank {
    EventBank data;
    private String status;
    private String message;

    public EventBank getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }

    public class EventBank{

        private String nama_akun;
        private String no_rekening;
        private String nama_bank;
        private String cabang, url_logo_bank;

        public String getCabang() {
            return cabang;
        }

        public String getUrl_logo_bank() {
            return url_logo_bank;
        }

        public String getNama_akun() {
            return nama_akun;
        }

        public String getNo_rekening() {
            return no_rekening;
        }

        public String getNama_bank() {
            return nama_bank;
        }
    }
}
