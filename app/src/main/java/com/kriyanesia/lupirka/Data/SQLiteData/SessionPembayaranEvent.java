package com.kriyanesia.lupirka.Data.SQLiteData;

public class SessionPembayaranEvent {
    public static final String TABLE_NAME = "pembayaranevent";

    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NOMINAL = "nominal";
    public static final String COLUMN_BATASWAKTU = "bataswaktu";
    public static final String COLUMN_TITLE = "title";
    public static final String COLUMN_NOREK = "norek";

    private String id;
    private String nominal;
    private String bataswaktu;
    private String title;
    private String norek;

    public SessionPembayaranEvent(String id, String nominal, String bataswaktu, String title, String norek) {
        this.id = id;
        this.nominal = nominal;
        this.bataswaktu = bataswaktu;
        this.title = title;
        this.norek = norek;
    }

    // Create table SQL query
    public static final String CREATE_TABLE =
            "CREATE TABLE " + TABLE_NAME + "("
                    + COLUMN_ID + " TEXT PRIMARY KEY,"
                    + COLUMN_NOMINAL + " TEXT,"
                    + COLUMN_BATASWAKTU + " TEXT,"
                    + COLUMN_TITLE + " TEXT,"
                    + COLUMN_NOREK + " TEXT"
                    + ")";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNominal() {
        return nominal;
    }

    public void setNominal(String nominal) {
        this.nominal = nominal;
    }

    public String getBataswaktu() {
        return bataswaktu;
    }

    public void setBataswaktu(String bataswaktu) {
        this.bataswaktu = bataswaktu;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNorek() {
        return norek;
    }

    public void setNorek(String norek) {
        this.norek = norek;
    }
}
