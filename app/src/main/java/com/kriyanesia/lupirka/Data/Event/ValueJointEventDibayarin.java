package com.kriyanesia.lupirka.Data.Event;

public class ValueJointEventDibayarin {
    private DataAmmount data;
    private String status;
    private String message;

    public DataAmmount getData() {
        return data;
    }

    public String getStatus() {
        return status;
    }

    public String getMessage() {
        return message;
    }


    public class DataAmmount{
        private String id;
        private String amount;
        private String exp_time;
        private String unique_code;

        public String getId() {
            return id;
        }

        public String getAmount() {
            return amount;
        }

        public String getExp_time() {
            return exp_time;
        }

        public String getUnique_code() {
            return unique_code;
        }
    }
}
