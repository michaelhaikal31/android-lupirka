package com.kriyanesia.lupirka;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.view.KeyEvent;
import android.view.Window;
import android.view.WindowManager;

import com.kriyanesia.lupirka.Menu.MenuLogin;
import com.kriyanesia.lupirka.Menu.MenuMain;
import com.kriyanesia.lupirka.Session.SessionDevice;
import com.kriyanesia.lupirka.Session.SessionUser;
import com.crashlytics.android.Crashlytics;

import org.json.JSONObject;

import io.fabric.sdk.android.Fabric;

public class SplashActivity extends Activity {
    // Splash screen timer
    private static int SPLASH_TIME_OUT = 500;
    SessionUser su;
     AlertDialog alert=null;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Window window = this.getWindow();
        window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
        window.setStatusBarColor(ContextCompat.getColor(this,R.color.bgWhite));
        setContentView(R.layout.activity_splash);
        //cek_version();

    }


    private void cek_version(){
        final SessionDevice sessionDevice = new SessionDevice();

         if(!sessionDevice.getVersionName(this).equalsIgnoreCase("")) {
             System.out.println("asd check version !=null");
             try {
                 final PackageInfo pinfo = getPackageManager().getPackageInfo(getPackageName(), 0);
                 System.out.println("asd check version "+pinfo.versionName);
                 System.out.println("asd check version "+sessionDevice.getVersionName(this));


                 if (!pinfo.versionName.equalsIgnoreCase(sessionDevice.getVersionName(this))) {
                     System.out.println("asd check version update");
                     final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                     builder.setTitle("Update Available");
                     if (sessionDevice.getUpdateLevel(this).equalsIgnoreCase("1")) {
                         builder.setMessage(getResources().getString(R.string.update_1));
                         builder.setCancelable(false);
                         builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
                             @Override
                             public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                 if (i == KeyEvent.KEYCODE_BACK) {
                                     alert.dismiss();
                                     finish();
                                 }
                                    return true;
                             }
                         });

                     } else {
                         builder.setMessage(getResources().getString(R.string.update_2));
                         builder.setNegativeButton("Lain kali", new DialogInterface.OnClickListener() {
                             @Override
                             public void onClick(DialogInterface dialogInterface, int i) {
                                 alert.dismiss();
                                 sessionDevice.setUpdate(SplashActivity.this,"","");
                                 initialize();
                             }
                         });
                         builder.setOnKeyListener(new DialogInterface.OnKeyListener() {
                             @Override
                             public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                 if (i == KeyEvent.KEYCODE_BACK) {
                                     alert.dismiss();
                                     initialize();
                                     return false;
                                 }
                                 return true;
                             }
                         });

                     }
                     builder.setPositiveButton("Update", new DialogInterface.OnClickListener() {
                         @Override
                         public void onClick(DialogInterface dialogInterface, int i) {
                             try {
                                 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + pinfo.packageName)));
                             } catch (android.content.ActivityNotFoundException anfe) {
                                 startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + pinfo.packageName)));
                             }
                         }
                     });

                     alert = builder.create();
                     alert.show();
                 } else {
                     initialize();
                 }
             } catch (Exception e) {
             }
         }else{
             initialize();
         }
    }

    private void initialize(){
        final SessionUser sessionUser = new SessionUser();
        com.kriyanesia.lupirka.Session.SessionDevice sd = new com.kriyanesia.lupirka.Session.SessionDevice();
        String fcm = sd.getTokenDevice(this,"token");
        String imei =sd.getImei(this,"imei");
        System.out.println("imei: "+imei+"FCM: "+fcm);
        su= new SessionUser();
        final String sessionNomer =sessionUser.getNoHp(SplashActivity.this,"nohp");
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                // This method will be executed once the timer is over
                // Start your app main activity
                Intent i = null;
                if (sessionNomer.isEmpty() || sessionNomer.equalsIgnoreCase("")){
                    i = new Intent(SplashActivity.this, MenuLogin.class);
                }else {
                    i = new Intent(SplashActivity.this, MenuMain.class);
                    su= new SessionUser();
                    su.setEventCategoryName(SplashActivity.this,"categoryName","Kategori");
                    su.setEventCityName(SplashActivity.this,"cityName","Kota");
                    su.setEventCategory(SplashActivity.this,"category","");
                    su.setEventCity(SplashActivity.this,"city","");
                }
                startActivity(i);
                overridePendingTransition(R.anim.splash_fade_in,R.anim.splash_fade_out);

                // close this activity
                finish();
            }
        }, SPLASH_TIME_OUT);
    }

    @Override
    protected void onResume() {
        super.onResume();
        cek_version();
    }



}
