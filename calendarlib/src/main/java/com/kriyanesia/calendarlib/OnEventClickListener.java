package com.kriyanesia.calendarlib;

/**
 * Created by HCL on 04-10-2016.
 */
public interface OnEventClickListener {

    void onClick(String id);

    void onLongClick(String id);

}
