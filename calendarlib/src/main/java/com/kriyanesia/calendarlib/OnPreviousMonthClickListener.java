package com.kriyanesia.calendarlib;

public interface OnPreviousMonthClickListener {
    void onClick(String date);

}
