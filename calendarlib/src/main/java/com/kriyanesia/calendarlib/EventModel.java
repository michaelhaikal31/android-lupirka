package com.kriyanesia.calendarlib;

/**
 * Created by HCL on 02-10-2016.
 */
public class EventModel {
    private String id;
    private String strDate;
    private String strStartTime;
    private String strEndTime;
    private String strName;
    private int image = -1;


    public EventModel(String id,String strDate, String strStartTime, String strEndTime, String strName) {
        this.id = id;
        this.strDate = strDate;
        this.strStartTime = strStartTime;
        this.strEndTime = strEndTime;
        this.strName = strName;
    }

    public EventModel(String id,String strDate, String strStartTime, String strEndTime, String strName, int image) {
        this.id = id;
        this.strDate = strDate;
        this.strStartTime = strStartTime;
        this.strEndTime = strEndTime;
        this.strName = strName;
        this.image = image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getStrDate() {
        return strDate;
    }

    public void setStrDate(String strDate) {
        this.strDate = strDate;
    }

    public String getStrStartTime() {
        return strStartTime;
    }

    public void setStrStartTime(String strStartTime) {
        this.strStartTime = strStartTime;
    }

    public String getStrEndTime() {
        return strEndTime;
    }

    public void setStrEndTime(String strEndTime) {
        this.strEndTime = strEndTime;
    }

    public String getStrName() {
        return strName;
    }

    public void setStrName(String strName) {
        this.strName = strName;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
